################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../source/GAsymmetricHollowBox.cpp \
../source/GAsymmetricRoundedHollowBox.cpp \
../source/GBox.cpp \
../source/GCone.cpp \
../source/GCylinder.cpp \
../source/GHollowBox.cpp \
../source/GHollowCylinder.cpp \
../source/GHollowTruncatedCone.cpp \
../source/GInputs.cpp \
../source/GPartialHollowCylinder.cpp \
../source/GPlainInputs.cpp \
../source/GPrimitives.cpp \
../source/GRoundedHollowBox.cpp \
../source/GSphere.cpp \
../source/GTruncatedCone.cpp \
../source/GXMLInputs.cpp 

OBJS += \
./source/GAsymmetricHollowBox.o \
./source/GAsymmetricRoundedHollowBox.o \
./source/GBox.o \
./source/GCone.o \
./source/GCylinder.o \
./source/GHollowBox.o \
./source/GHollowCylinder.o \
./source/GHollowTruncatedCone.o \
./source/GInputs.o \
./source/GPartialHollowCylinder.o \
./source/GPlainInputs.o \
./source/GPrimitives.o \
./source/GRoundedHollowBox.o \
./source/GSphere.o \
./source/GTruncatedCone.o \
./source/GXMLInputs.o 

CPP_DEPS += \
./source/GAsymmetricHollowBox.d \
./source/GAsymmetricRoundedHollowBox.d \
./source/GBox.d \
./source/GCone.d \
./source/GCylinder.d \
./source/GHollowBox.d \
./source/GHollowCylinder.d \
./source/GHollowTruncatedCone.d \
./source/GInputs.d \
./source/GPartialHollowCylinder.d \
./source/GPlainInputs.d \
./source/GPrimitives.d \
./source/GRoundedHollowBox.d \
./source/GSphere.d \
./source/GTruncatedCone.d \
./source/GXMLInputs.d 


# Each subdirectory must supply rules for building sources it contributes
source/%.o: ../source/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/home/khayyam/Desktop/prolec/Geometria/gapi/include" -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


