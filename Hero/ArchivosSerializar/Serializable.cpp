#include "stdafx.h"
#include "Serializable.h"
using namespace std;

Serializable::Serializable(char *nombre){
	serialized=false;
	classTag=new SXP::Tag(nombre);
}

Serializable::~Serializable(){
	delete classTag;
}

void Serializable::agregarAtributo(const AtributoXML &atributo){
	atributos.push_back(atributo);
}

void Serializable::agregarNodo(Serializable *nodo){
	nodos.push_back(nodo);
}

void Serializable::SaveFile(SXP::IOutStream *pOut){
	SXP::dict attribs;
	WriteElement(pOut, attribs);	
}

void Serializable::WriteElement(SXP::IOutStream *pOut, SXP::dict& attribs){
	BeginObject(pOut);
	for(unsigned i=0;i<atributos.size();++i){
		SXP::Tag tag(atributos[i].obtenerNombre().c_str());
		atributos[i].escribirTag(pOut,tag);
	}
	for(unsigned i=0;i<nodos.size();++i){
		SXP::dict attribs;
		pOut->WriteSubElement(nodos[i], attribs);
	}
	EndObject(pOut);
}

void Serializable::BeginElement(SXP::IParser *pIn, SXP::IElement *pElement){
	for(unsigned i=0;i<nodos.size();++i){
		if(pElement->IsA(nodos[i]->GetClassTag())){
			pIn->ReadTo(nodos[i]);
			nodos[i]->BeginElement(pIn, pElement);
		}
	}

}

void Serializable::BeginObject(SXP::IOutStream *pOut){
	SXP::dict attribs;
	pOut->BeginObject(*classTag, attribs);
}

void Serializable::EndObject(SXP::IOutStream *pOut){
	pOut->EndObject(*classTag);
}
		
void Serializable::EndElement(SXP::IParser *pIn, SXP::IElement *pElement){
	for(unsigned i=0;i<atributos.size();++i){
		SXP::Tag tag(atributos[i].obtenerNombre().c_str());
		if(pElement->IsA(tag)){
			atributos[i].leerTag(pElement);
		}
	}
}

SXP::Tag& Serializable::GetClassTag(void)const{
	return *classTag;
}

int Serializable::leerArchivo(char* nombreArchivo){
	if(!serialized){
		recursiveSerialize();
		serialized=true;
	}
	SXP::CParser parserBaseConfig(this);

	SXP::ErrCode errorCode = parserBaseConfig.FeedFile(nombreArchivo);
	if( errorCode != SXP::err_no_error ) {
		string errorStr=parserBaseConfig.GetErrorStr();
		int errorLine=parserBaseConfig.GetErrorLine();
		int errorCol=parserBaseConfig.GetErrorCol();
		printf("Oops, error \"%s\" at line %d, char %d",
			errorStr.c_str(),
			errorLine,
			errorCol);
		return 1;
	}
	return 0;
}
		
int Serializable::escribirArchivo(char *nombreArchivo){
	if(!serialized){
		recursiveSerialize();
		serialized=true;
	}
	FILE *F=fopen(nombreArchivo, "wt");
	if(F==NULL){
		return -1;
	}
	SXP::CFileOutStream *fileOutStream = new SXP::CFileOutStream(nombreArchivo);
	fileOutStream->BeginXML();
	SXP::dict attributes;
	WriteElement(fileOutStream, attributes);
	//fprintf(F,"%s",fileOutStream->getData().c_str());
	fclose(F);
	delete fileOutStream;
	return 0;
}

void Serializable::recursiveSerialize(void){
	serializar();
	for(unsigned i=0;i<nodos.size();++i){
		nodos[i]->recursiveSerialize();
	}		
}

std::string Serializable::escribirCadena(string tempFileName){
	if(!serialized){
		recursiveSerialize();
		serialized=true;
	}
	SXP::CFileOutStream *fileOutStream = new SXP::CFileOutStream(tempFileName.c_str());
	fileOutStream->BeginXML();
	SXP::dict attributes;
	WriteElement(fileOutStream, attributes);
	//std::string data=fileOutStream->getData();
	delete fileOutStream;
	return "<test></test>";
}