#ifndef SERIALIZABLE_H
#define SERIALIZABLE_H

#include <string>
#include <vector>
#include <sstream>
#include "sxp.h"
#include "AtributoXML.h"


///////////////////////////////////////////////////////////////////////////////////////////////
//! \author Omar Ocegueda
//!
//! \brief Interfaz para serializacion de objetos
//!
//! Para hacer uso de esta clase basta extender la clase a serializar e implementar la funcion
//! privada "void serializar(void)" en la que se registren todos los atributos y sub-nodos,
//! esta funcion se llama internamente, la clase derivada no debe llamarla explicitamente
//!
//! \date August-28-2009
///////////////////////////////////////////////////////////////////////////////////////////////
class __declspec(dllexport)Serializable:public SXP::IPersistObj{
	private:
		bool serialized;
		void SaveFile(SXP::IOutStream *pOut);
		void WriteElement(SXP::IOutStream *pOut, SXP::dict& attribs);
		void BeginElement(SXP::IParser *pIn, SXP::IElement *pElement);
		void BeginObject(SXP::IOutStream *pOut);
		void EndObject(SXP::IOutStream *pOut);
		void EndElement(SXP::IParser *pIn, SXP::IElement *pElement);
		SXP::Tag& GetClassTag(void)const;
		virtual void serializar(void)=0;//registrar los atributos

		void recursiveSerialize(void);
	protected:
		std::vector<Serializable*> nodos;
		std::vector<AtributoXML> atributos;
		SXP::Tag *classTag;
		void agregarAtributo(const AtributoXML &atributo);
		void agregarNodo(Serializable *nodo);
	public:
		Serializable(char *nombre);
		virtual ~Serializable();
		int leerArchivo(char *nombreArchivo);
		int escribirArchivo(char *nombreArchivo);
		std::string escribirCadena(std::string tempFileName);
};
#endif
