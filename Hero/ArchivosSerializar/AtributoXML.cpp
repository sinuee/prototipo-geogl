#include "stdafx.h"
#include "AtributoXML.h"
using namespace std;
AtributoXML::AtributoXML(string _nombre, int &_valor){
	nombre=_nombre;
	tipo=TA_INT;
	valor=&_valor;
}
		
AtributoXML::AtributoXML(string _nombre, double &_valor){
	nombre=_nombre;
	tipo=TA_DOUBLE;
	valor=&_valor;
}

AtributoXML::AtributoXML(string _nombre, string &_valor){
	nombre=_nombre;
	tipo=TA_STRING;
	valor=&_valor;
}

AtributoXML::AtributoXML(std::string _nombre, bool &_valor){
	nombre=_nombre;
	tipo=TA_BOOL;
	valor=&_valor;
}

AtributoXML::AtributoXML(std::string _nombre, float &_valor){
	nombre=_nombre;
	tipo=TA_FLOAT;
	valor=&_valor;
}

AtributoXML::~AtributoXML(){
}

void AtributoXML::escribirTag(SXP::IOutStream *pOut, SXP::Tag &tag){
	if(valor==NULL){
		/*string msg=string("JOOG: el atributo '")+nombre+"' no fue asignado correctamente.";
		imprimirError(msg.c_str());*/
		return;
	}
	switch(tipo){
		case TA_INT:
			pOut->WriteElement(tag, *((int*)valor));
		break;
		case TA_DOUBLE:
			pOut->WriteElement(tag, *((double*)valor));
		break;
		case TA_STRING:
			pOut->WriteElement(tag, *((string*)valor));
		break;				
		case TA_BOOL:
			pOut->WriteElement(tag, *((bool*)valor));
		break;
		case TA_FLOAT:
			pOut->WriteElement(tag, *((float*)valor));
		break;
	}
}

void AtributoXML::leerTag(SXP::IElement *pElement){
	switch(tipo){
		case TA_INT:
			pElement->Retrieve(*((int*)valor));
		break;
		case TA_DOUBLE:
			pElement->Retrieve(*((double*)valor));
		break;
		case TA_STRING:
			pElement->Retrieve(*((string*)valor));
		break;
		case TA_BOOL:
			pElement->Retrieve(*((bool*)valor));
		break;
		case TA_FLOAT:
			pElement->Retrieve(*((float*)valor));
		break;
	}
}

const string &AtributoXML::obtenerNombre(void)const{
	return nombre;
}
