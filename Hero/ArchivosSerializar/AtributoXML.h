#ifndef ATRIBUTOXML_H
#define ATRIBUTOXML_H
#include <string>
#include "sxp.h"
enum TipoAtributo{TA_BOOL, TA_INT, TA_FLOAT, TA_DOUBLE, TA_STRING};
#define XML_ENUM(x) (*((int*)&x))

///////////////////////////////////////////////////////////////////////////////////////////////
//! \author Omar Ocegueda
//!
//! \brief Atributo de una clase serializable
//!
//! La interfaz "Serializable" hace uso de esta clase para mantener el registro de todos los
//! atributos y sus nombres de tag para su serializacion
//!
//! \date August-28-2009
///////////////////////////////////////////////////////////////////////////////////////////////
class __declspec(dllexport)AtributoXML{
	protected:
		std::string nombre;
		TipoAtributo tipo;
		void *valor;
	public:
		AtributoXML(std::string _nombre, int &_valor);
		AtributoXML(std::string _nombre, double &_valor);
		AtributoXML(std::string _nombre, std::string &_valor);
		AtributoXML(std::string _nombre, bool &_valor);
		AtributoXML(std::string _nombre, float &_valor);


		virtual ~AtributoXML();
		void escribirTag(SXP::IOutStream *pOut, SXP::Tag &tag);
		void leerTag(SXP::IElement *pElement);
		const std::string &obtenerNombre(void)const;
};
#endif
