/*
 * GRoundedHollowBox.cpp
 *
 *  Created on: Dec 15, 2008
 *      Author: khayyam
 */
#include "GRoundedHollowBox.h"
#include "GMacros.h"
#include "GPrimitives.h"
#include <iostream>
Hero::GRoundedHollowBox::GRoundedHollowBox():GHollowBox(){
	slices=20;
	displayType=SOLID;
	//strcpy(rotar.c_str(),"");
	//strcpy(posi.c_str(),"");

}

Hero::GRoundedHollowBox::GRoundedHollowBox(GBox &parent):GHollowBox(parent){
	slices=20;
	displayType=SOLID;
	this->rotar = "";
	this->posi = "";
}

Hero::GRoundedHollowBox::~GRoundedHollowBox(){

}

void Hero::GRoundedHollowBox::doExport(void)
{
	char *str=new char[15];	
	char centro[20];
	char valor[350],rect[100],suba[100],subb[100],subc[100],sub2b[100],sub2a[100],sub2c[100],exta[100],extb[100];	
	char sub3a [100], sub3b [100], rott[200];
	double flt;		
	int x=0;
	//centro se utiliza para generar los cambios de variables 
	std::string sAux;
	//Centro ? **********
	double _points[1][4], radioint;//,paso[3];	
	/*points[0].copyTo(_points[0]);
	sAux = "";
	for (int k=0;k<3;k++)
			sAux = sAux +  convertToString<int >((int)(points[k][0]/ points[k][3])) + " ";
		
	strcpy(centro,sAux.c_str());*/		
	strcpy(centro, "");			
	sprintf(str, "%.6g",this->width/2);
	strcat(centro,str);		
	sprintf(str, " %.6g",(-this->height/2)+this->thickness);
	strcat(centro,str);	
	sprintf(str, " %.6g)",0.000);
	strcat(centro,str);	
	strcpy(valor,"");
	strcat(valor,"(COMMAND \"._PLINE\" '("); 
	strcat(valor,centro);		
	strcat(valor," \"_LINE\" "); //c
	strcpy(centro, "");			
	sprintf(str, "'(%.6g",this->width/2);
	strcat(centro,str);		
	sprintf(str, " %.6g)",(this->height/2)-this->thickness);
	strcat(centro,str);	
	strcat(valor,centro);	
	strcat(valor," \"_ARC\" "); //d
	strcpy(centro, "");			
	sprintf(str, "'(%.6g",(this->width/2)-this->thickness);
	strcat(centro,str);		
	sprintf(str, " %.6g)",this->height/2);
	strcat(centro,str);	
	strcat(valor,centro);
	
	strcat(valor," \"_LINE\" ");//e
	strcpy(centro, "");			
	sprintf(str, "'(%.6g",(-this->width/2)+this->thickness);
	strcat(centro,str);		
	sprintf(str, " %.6g)",this->height/2);
	strcat(centro,str);	
	strcat(valor,centro);
	
	strcat(valor," \"_ARC\" ");//f
	strcpy(centro, "");			
	sprintf(str, "'(-%.6g",this->width/2);
	strcat(centro,str);		
	sprintf(str, " %.6g)",(this->height/2)-this->thickness);
	strcat(centro,str);	
	strcat(valor,centro);
	
	strcat(valor," \"_LINE\" ");//g
	strcpy(centro, "");			
	sprintf(str, "'(-%.6g",this->width/2);
	strcat(centro,str);		
	sprintf(str, " %.6g)",(-this->height/2)+this->thickness);
	strcat(centro,str);	
	strcat(valor,centro);
	
	strcat(valor," \"_ARC\" ");//h
	strcpy(centro, "");			
	sprintf(str, "'(%.6g",(-this->width/2)+this->thickness);
	strcat(centro,str);		
	sprintf(str, " -%.6g)",this->height/2);
	strcat(centro,str);	
	strcat(valor,centro);
	
	strcat(valor," \"_LINE\" ");//a
	strcpy(centro, "");		
	sprintf(str, "'(%.6g",(this->width/2)-this->thickness);
	strcat(centro,str);		
	sprintf(str, " -%.6g)",this->height/2);
	strcat(centro,str);	
	strcat(valor,centro);
	
	strcat(valor," \"_ARC\" ");//b
	strcpy(centro, "");			
	sprintf(str, "'(%.6g",this->width/2);
	strcat(centro,str);		
	sprintf(str, " %.6g)",(-this->height/2)+this->thickness);
	strcat(centro,str);	
	strcat(valor,centro);
	
	strcat(valor," \"_CLOSE\" )");

	strcpy(rect,"");
	strcat(rect,"(COMMAND \"._RECTANG\" '(");	
	sprintf(str, " %.6g",(this->width/2)-this->thickness);
	strcat(rect,str);		
	sprintf(str, " %.6g)",(-this->height/2)+this->thickness);
	strcat(rect,str);		
	sprintf(str, " '(%.6g",(-this->width/2)+this->thickness);
	strcat(rect,str);		
	sprintf(str, " %.6g)",(this->height/2)-this->thickness);
	strcat(rect,str);	
	strcat(rect,")");

	strcpy(suba,"");
	strcat(suba,"(SETQ ");
	strcat(suba,name);
	strcat(suba," (ENTLAST))");
	strcpy(subb,"");
	strcat(subb,"(SETQ c2 (ENTLAST))");
	strcpy(subc,"");
	strcat(subc,"(SETQ c3 (ENTLAST))");
	strcpy(sub2a,"");
	strcat(sub2a,"(SETQ D1 (ENTLAST))");
	strcpy(sub2b,"");
	strcat(sub2b,"(SETQ D2 (ENTLAST))");
	strcpy(sub2c,"");
	strcat(sub2c,"(COMMAND \"._SUBTRACT\" D1 \"\" D2 \"\")");
	strcpy(sub3a,"");
	strcat(sub3a,"(SETQ M (ENTLAST))");
	strcpy(sub3b,"");
	strcat(sub3b,"(COMMAND \"._MOVE\" M \"\" '(0 0 0) '(0 0");
	/*strcat(sub3b,"0 0 0)");		
	strcat(sub3b," '(0");*/
	flt=this->innerWidth/2;
	sprintf(str," -%.6g))",flt);
	strcat(sub3b,str);
	//strcat(sub3b," 0))");
	strcpy(exta,"");
	strcat(exta,"(COMMAND \"._EXTRUDE\" c2 \"\" ");
	sprintf(str,"%.6g)",this->innerWidth);
	strcat(exta,str);
	strcpy(extb,"");
	strcat(extb,"(COMMAND \"._EXTRUDE\" c3 \"\" ");
	sprintf(str,"%.6g)",this->innerWidth);
	strcat(extb,str);
	strcpy(rott,"");
	strcat(rott,"(COMMAND \"_.ROTATE3D\" ");
	strcat(rott,name);
	strcat(rott," \"\" '(1 0 0) '(0 0) 90)");

	/*if(this->radius!=0&&this->innerRadius!=0)
		{	*/	
	std::string pcNombreArch1 = "";//"d:\\cubo.lsp";
	pcNombreArch1=sFile.c_str();
	FILE *pArchivo1;			
	pArchivo1 = fopen(pcNombreArch1.c_str(),"a");
	
	fprintf(pArchivo1,"%s\n%s\n%s\n",valor,subb,exta);
	fprintf(pArchivo1,"%s\n%s\n%s\n",sub2a,rect,subc);
	fprintf(pArchivo1,"%s\n%s\n%s\n",extb,sub2b,sub2c);
	fprintf(pArchivo1,"%s\n%s\n",sub3a,sub3b);
	fprintf(pArchivo1,"%s\n",suba);
	fprintf(pArchivo1,"%s\n",rott);
	this->parent->NumHijos++;
	if(strcmp(this->rotar.c_str(),"") != 0)
		fprintf(pArchivo1,"%s\n",this->rotar.c_str());	
	if(strcmp(this->posi.c_str(),"") != 0)
		fprintf(pArchivo1,"%s\n",this->posi.c_str());
	if(strcmp(this->parent->unio.c_str(),"") !=0)//parte final +
	{
		fprintf(pArchivo1,"%s\n",this->parent->unio.c_str());
		x=1;
	}
	if(strcmp(this->parent->inters.c_str(),"") !=0)
	{
		fprintf(pArchivo1,"%s\n",this->parent->inters.c_str());
		x=1;
	}
	if(strcmp(this->parent->subs.c_str(),"") !=0)
	{
		fprintf(pArchivo1,"%s\n",this->parent->subs.c_str());
		x=1;
	}
	if(strcmp(this->parent->rotar.c_str(),"") != 0 && x==1)
		fprintf(pArchivo1,"%s\n",this->parent->rotar.c_str());		
	if(strcmp(this->parent->posi.c_str(),"") != 0&&x==1)
		fprintf(pArchivo1,"%s\n",this->parent->posi.c_str());
	fclose(pArchivo1);
	/*}*/
}

void Hero::GRoundedHollowBox::Cvolumen()
{
	volumen = M_PI*pow(thickness,2)*length+ 2*((width-(2*thickness))*thickness*length)+2*((height-(2*thickness)) *thickness*length);//AEC
}               
#ifdef USE_GAPI_GRAPHICS
void Hero::GRoundedHollowBox::doDraw(void){
	//GLenum tdisplayType=this->displayType==WIRED?GL_LINE_LOOP:GL_TRIANGLE_FAN;
	GLenum tdisplayType;
	if (displayType==WIRED)
		tdisplayType=GL_LINE_LOOP;
	else
		tdisplayType=GL_TRIANGLE_FAN;
	glPushMatrix();
	glMultMatrixd(T.getData());
	drawBox(width-2*thickness, length, thickness,0,0,0.5*(height-thickness),tdisplayType);
	drawBox(width-2*thickness, length, thickness,0,0,-0.5*(height-thickness),tdisplayType);
	drawBox(thickness,length,height-2*thickness,-0.5*(width-thickness), 0, 0, tdisplayType);
	drawBox(thickness,length,height-2*thickness,0.5*(width-thickness), 0, 0, tdisplayType);
	glPushMatrix();
	glTranslated(-0.5*width+thickness, length*0.5, 0.5*height-thickness);
	glRotated(90,1, 0,0);
	drawPartialHollowCylinder(0, thickness, length,
							   DEGREES(90), DEGREES(90), slices, tdisplayType);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0.5*width-thickness, length*0.5, 0.5*height-thickness);
	glRotated(90,1, 0,0);
	drawPartialHollowCylinder(0, thickness, length,
							   DEGREES(0), DEGREES(90), slices, tdisplayType);
	glPopMatrix();

	glPushMatrix();
	glTranslated(-0.5*width+thickness, length*0.5, -0.5*height+thickness);
	glRotated(90,1, 0,0);
	drawPartialHollowCylinder(0, thickness, length,
							   DEGREES(180), DEGREES(90), slices, tdisplayType);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0.5*width-thickness, length*0.5, -0.5*height+thickness);
	glRotated(90,1, 0,0);
	drawPartialHollowCylinder(0, thickness, length,
							   DEGREES(270), DEGREES(90), slices, tdisplayType);
	glPopMatrix();

	glPopMatrix();

}
#else
void Hero::GRoundedHollowBox::doDraw(void){
	std::cerr<<"Object: GRoundedHollowBox"<<std::endl;
}
#endif

