/*
 * GAsymmetricRoundedHollowBox.cpp
 *
 *  Created on: Dec 17, 2008
 *      Author: khayyam
 */
#include "GAsymmetricRoundedHollowBox.h"
#include "GMacros.h"
#include "GPrimitives.h"
#include <iostream>

Hero::GAsymmetricRoundedHollowBox::GAsymmetricRoundedHollowBox():GAsymmetricHollowBox(){
	slices=20;
}

Hero::GAsymmetricRoundedHollowBox::GAsymmetricRoundedHollowBox(GBox &parent):GAsymmetricHollowBox(parent){
	slices=20;
}

Hero::GAsymmetricRoundedHollowBox::~GAsymmetricRoundedHollowBox(){

}
#ifdef USE_GAPI_GRAPHICS
void Hero::GAsymmetricRoundedHollowBox::doDraw(void){
	if(!visible)
		return;
	GLenum displayType=this->displayType==WIRED?GL_LINE_LOOP:GL_TRIANGLE_FAN;
	glPushMatrix();
	glMultMatrixd(T.getData());
	double topRadius=MIN(thickness, topThickness);
	double bottomRadius=MIN(thickness, bottomThickness);
	drawBox(width-2*topRadius, length, topThickness,0,0,0.5*(height-topThickness),displayType);//top box
	drawBox(width-2*bottomRadius, length, bottomThickness,0,0,-0.5*(height-bottomThickness),displayType);//bottom box
	drawBox(thickness,length,height-(topRadius+bottomRadius),-0.5*(width-thickness), 0, 0.5*(bottomRadius-topRadius), displayType);//left box
	drawBox(thickness,length,height-(topRadius+bottomRadius),0.5*(width-thickness), 0, 0.5*(bottomRadius-topRadius), displayType);//right box
	glPushMatrix();

	glTranslated(-0.5*width+topRadius, length*0.5, 0.5*height-topRadius);//top left rounded corner
	glRotated(90,1, 0,0);
	drawPartialHollowCylinder(0, topRadius, length,
							   DEGREES(90), DEGREES(90), slices, displayType);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0.5*width-topRadius, length*0.5, 0.5*height-topRadius);//top right rounded corner
	glRotated(90,1, 0,0);
	drawPartialHollowCylinder(0, topRadius, length,
							   DEGREES(0), DEGREES(90), slices, displayType);
	glPopMatrix();

	glPushMatrix();

	glTranslated(-0.5*width+bottomRadius, length*0.5, -0.5*height+bottomRadius);//bottom left rounded corner
	glRotated(90,1, 0,0);
	drawPartialHollowCylinder(0, bottomRadius, length,
							   DEGREES(180), DEGREES(90), slices, displayType);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0.5*width-bottomRadius, length*0.5, -0.5*height+bottomRadius);//bottom right rounded corner
	glRotated(90,1, 0,0);
	drawPartialHollowCylinder(0, bottomRadius, length,
							   DEGREES(270), DEGREES(90), slices, displayType);
	glPopMatrix();

	glPopMatrix();

}
#else
void Hero::GAsymmetricRoundedHollowBox::doDraw(void){
	std::cerr<<"Object: GAsymmetricRoundedHollowBox"<<std::endl;
}
#endif
