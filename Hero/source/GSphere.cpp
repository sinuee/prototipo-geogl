/*
 * GSphere.cpp
 *
 *  Created on: Dec 11, 2008
 *      Author: khayyam
 */
#include "GSphere.h"
#include "GMacros.h"
#include <iostream>
#include <fstream>
#include <string>
//#include <stdio>

float x[4];
Hero::GSphere::GSphere():GBox(){
	slices=20;
	stacks=20;
	radius=MIN(MIN(width, length), height)*0.5;
	width=length=height=radius;
	/*strcpy(rotar.c_str(),"");
	strcpy(posi.c_str(),"");*/
}

Hero::GSphere::GSphere(GBox &parent):GBox(parent){
	slices=20;
	stacks=20;
	radius=MIN(MIN(width, length), height)*0.5;
	width=length=height=radius;
}

Hero::GSphere::~GSphere(){

}

void Hero::GSphere::setRadius(double radius){
	this->radius=radius;
	width=length=height=2*radius;
}

void Hero::GSphere::doExport(void){	
	
	char *str=new char[8];	
	char centro[25],valor[100],suba[100];	
	double flt;
	int x=0;
	Vector desp=normals[BOTTOM]*(height*0.5);
	std::string sAux;
	//Centro ? **********
	double _points[1][4], radioint;	
	points[0].copyTo(_points[0]);
	sAux = "";
	for (int k=0;k<3;k++)
		sAux = sAux +  convertToString<int >((int)(points[k][0]/ points[k][3])) + " ";
	strcpy(centro,sAux.c_str());	
	strcpy(suba,"");
	strcat(suba,"(SETQ ");
	strcat(suba,this->getName());
	strcat(suba," (ENTLAST))");

	/*strcpy(centro, "");
	flt=desp[0];
	sprintf(str, "%.6g",flt);
	strcat(centro,str);
	strcat(centro," ");
	flt=desp[1];
	sprintf(str, "%.6g",flt);
	strcat(centro,str);
	strcat(centro," ");
	flt=desp[2];
	sprintf(str, "%.6g",flt);
	strcat(centro,str);*/

	strcpy(valor,"");
	strcat(valor,"(COMMAND \"_.SPHERE\" '(");	
	strcat(valor,centro);
			
	sprintf(str, ") %.6g",this->radius);
	strcat(valor,str);			
	strcat(valor,")");

	//std::string pcNombreArch1 = "d:\\esfera.lsp";
	if(this->radius!=0)
		{
	std::string pcNombreArch1 = "";//"d:\\cubo.lsp";
	pcNombreArch1=sFile.c_str();

	FILE *pArchivo1;			
    pArchivo1 = fopen(pcNombreArch1.c_str(),"a");
	fprintf(pArchivo1,"%s\n%s\n",valor,suba);	
	this->parent->NumHijos++;
	if(strcmp(this->rotar.c_str(),"")!=0)
		fprintf(pArchivo1,"%s\n",this->rotar.c_str());	
	if(strcmp(this->posi.c_str(),"")!=0)
		fprintf(pArchivo1,"%s\n",this->posi.c_str());
	if(strcmp(this->parent->unio.c_str(),"") !=0)//parte final +
	{
		fprintf(pArchivo1,"%s\n",this->parent->unio.c_str());
		x=1;
	}
	if(strcmp(this->parent->inters.c_str(),"") !=0)
	{
		fprintf(pArchivo1,"%s\n",this->parent->inters.c_str());
		x=1;
	}
	if(strcmp(this->parent->subs.c_str(),"") !=0)
	{
		fprintf(pArchivo1,"%s\n",this->parent->subs.c_str());
		x=1;
	}
	if(strcmp(this->parent->rotar.c_str(),"")!=0&&x==1)
		fprintf(pArchivo1,"%s\n",this->parent->rotar.c_str());	
	if(strcmp(this->parent->posi.c_str(),"")!=0&&x==1)
		fprintf(pArchivo1,"%s\n",this->parent->posi.c_str());
	fclose(pArchivo1);
	}
}

/*void Hero::GSphere::Export()
{
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->Export();		
	if(!hidden)
		this->doExport();
}*/

char* Hero::GSphere::getName()
{
	return name;
}

void Hero::GSphere::setName(char *data)
{
	name = data;
//	delete data;
}

void Hero::GSphere::Cvolumen()
{
	volumen=4/3*M_PI*pow(radius,3);//AEC
}

#ifdef USE_GAPI_GRAPHICS
void Hero::GSphere::doDraw(void){
	if(!visible)
		return;
	GLUquadricObj *quadObj=gluNewQuadric();
	gluQuadricDrawStyle(quadObj, ((displayType==WIRED)?GLU_LINE:GLU_FILL));
	glPushMatrix();
	Vector desp=normals[BOTTOM]*(height*0.5);

	glTranslated(desp[0], desp[1], desp[2]);
	glMultMatrixd(T.getData());
	gluQuadricOrientation(quadObj, GLU_OUTSIDE);
	gluSphere(quadObj, radius, slices, stacks);
	glPopMatrix();
	gluDeleteQuadric(quadObj);
}
#else
void Hero::GSphere::doDraw(void){
	std::cerr<<"Object: GSphere"<<std::endl;
}
#endif