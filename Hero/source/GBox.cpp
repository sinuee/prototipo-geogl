/*
 * GBox.cpp
 *
 *  Created on: Dec 4, 2008
 *      Author: khayyam
 */
///con GBOX funcional
#include "GBox.h"
#include "Matrix.h"
#include "GMacros.h"
#include "GPrimitives.h"
#include "GFunctions.h"
#include <fstream>
#include <string>
#include <iostream>
#include <ctime>
Hero::GBox::GBox(){
	
	objectType=OT_GBox;
	parent=NULL;
	rotar="";
	posi="";
	restoreDefaults();
	bindParts=false;
	hidden=false;
	isReference = false;
	name=" ";
	Materialest x;
	GLfloat GAPIGray[4] = {0.5, 0.5, 0.5, 1};
	x.GLcolor= GAPIGray[4];
	x.id_material= 01;
	x.material= "aluminio";
	x.peso_especifico=2.7;
	x.UM="gr/cm3";
	this->TMat.push_back(x);
	NumHijos=0;
	NHijos=0;
	nameHijo = "";
	//sFile = "d:\cubo.lsp";
	/*unio = "";*/
}

Hero::GBox::GBox(GBox &parent){
	objectType=OT_GBox;
	this->parent=&parent;
	inheritFromParent();
	this->parent->addChild(*this);
	this->isReference = false;
	name=parent.getName();
	/******************/
		Materialest x;
	GLfloat GAPIGray[4] = {0.5, 0.5, 0.5, 1};
	x.GLcolor= GAPIGray[4];
	x.id_material= 01;
	x.material= "aluminio";
	x.peso_especifico=2.7;
	x.UM="gr/cm3";
	this->TMat.push_back(x);
	NumHijos=0;
	NHijos=0;
	/************************/
}

void Hero::GBox::inheritFromParent(void){
	if(parent==NULL)
	{
		restoreDefaults();
		return;
	
	}
	/*for(int i=0;i<3;i++)
		dimmensions[i]=parent->dimmensions[i];*/
	this->width=parent->width;
	this->height=parent->height;
	this->length=parent->length;

	for(int i=0;i<15;i++)
		this->points[i]=parent->points[i];
	for(int i=0;i<6;i++){
		this->normals[i]=parent->normals[i];
		this->normalsExport[i]=parent->normals[i];
	}
	T=parent->T;
	visible=true;
	//this->displayType=WIRED;
	this->displayType=parent->getDisplayType();
	bindParts=parent->bindParts;
	this->hidden=false;
}

void Hero::GBox::restoreDefaults(void){
	this->width=1;
	this->height=1;
	this->length=1;
	/*for(int i=0;i<3;i++)
		dimmensions[i]=1;*/
	T.identity();
	build();
	visible=true;
	this->displayType=WIRED;
	bindParts=false;
	this->hidden=false;
}

void Hero::GBox::build(void){

	 double _points[15][4]={
			{ 0,  0,  0, 1},
			{-1, -1, -1, 1},
			{-1, -1,  1, 1},
			{-1,  1,  1, 1},
			{-1,  1, -1, 1},
			{ 1, -1, -1, 1},
			{ 1, -1,  1, 1},
			{ 1,  1,  1, 1},
			{ 1,  1, -1, 1},
			{-1,  0,  0, 1},
			{ 0,  1,  0, 1},
			{ 1,  0,  0, 1},
			{ 0, -1,  0, 1},
			{ 0,  0,  1, 1},
			{ 0,  0, -1, 1}
	};
	for(int i=0;i<15;i++)
	{
		_points[i][0]*=(width*0.5);
		_points[i][1]*=(length*0.5);
		_points[i][2]*=(height*0.5);
	}

	double _vectors[6][4]={
			{-1,  0,  0, 0},
			{ 0,  1,  0, 0},
			{ 1,  0,  0, 0},
			{ 0, -1,  0, 0},
			{ 0,  0,  1, 0},
			{ 0,  0, -1, 0}
	};

	for(int i=0;i<15;i++)
	{
		points[i]=T*_points[i];
	}
	for(int i=0;i<6;i++)
	{
		normals[i]=T*_vectors[i];
	}

}

Hero::GBox::~GBox(){
	if(this->parent!=NULL)
	{
		std::cout<<"Precaucion: un objeto fue borrado fuera de su padre.\n";
	}
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
	{
		(*it)->parent=NULL;
		delete *it;
	}
}

Hero::Point& Hero::GBox::getPoint(EBoxPoint point){
	return points[point];
}

Hero::Vector& Hero::GBox::getNormal(EFace face){
	return normals[face];
}
Hero::Vector& Hero::GBox::getNormalExport(EFace face){
	return normalsExport[face];
}

void Hero::GBox::draw(void){
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->draw();
	if(!hidden)
		this->doDraw();
}

void Hero::GBox::doExport(void){
	
	//char nombreArchivo[25];	strcpy(nombreArchivo,sFile.c_str());	
	char buffer[255],suba[300];
	char *str=new char[8];
	double flt;
	int x=0;
			
	//Codigo de Alfredo
	std::string sAux;
	char centro[8];
	//Centro ? **********
	double _points[1][4];
	points[0].copyTo(_points[0]);
	sAux = "";
	for (int k=0;k<3;k++)
	sAux = sAux + convertToString<int >((int)(points[k][0]/ points[k][3])) + " ";
	strcpy(centro,sAux.c_str());
	strcpy(centro, "");	
	flt=0;	
	sprintf(str, "%.6g",flt);
	strcat(centro,str);	
	flt=0;
	sprintf(str, " %.6g",flt);
	strcat(centro,str);	
	flt=0;
	sprintf(str, " %.6g",flt);
	strcat(centro,str);	


	FILE *fp;
	//sFile = "d:\\cubo.lsp";	
	fp = fopen(this->sFile.c_str(), "a");   /* Abrir archivo para escritura */

	strcpy(buffer,"");
	//strcat(buffer,"(COMMAND \"_.BOX\" \"_C\" '(0 0 0) \"_LENGTH\" ");	
	strcat(buffer,"(COMMAND \"_.BOX\" \"_C\" ");
	strcat(buffer,"'(");
	strcat(buffer,centro);
	strcat(buffer,")");
	strcat(buffer," \"_LENGTH\" ");	
	sprintf(str, "%.6f ", this->getWidth());//largo 	
	strcat(buffer,str);
	//strcat(buffer," ");
	sprintf(str, "%.6f ", this->getLength()); // ancho 
	strcat(buffer,str);
	//strcat(buffer," ");
	sprintf(str, "%.6f", this->getHeight());  //alto
	strcat(buffer,str);
	strcat(buffer,")\n");
	fprintf(fp, buffer);
	//(COMMAND "_.BOX" "_C" '(0 0 0) "_LENGTH" 1000.000000 1000.000000 1000.000000)
	strcpy(suba,"");
	strcat(suba,"(SETQ ");
	strcat(suba,this->getName());
	strcat(suba," (ENTLAST))\n");
	fprintf(fp,suba);
	if(strcmp(this->rotar.c_str(),"")!=0)
	{
		fprintf(fp,this->rotar.c_str());	
		fprintf(fp,"\n");	
	}
	if(strcmp(this->posi.c_str(),"")!=0)
	{
		fprintf(fp,this->posi.c_str());	
		fprintf(fp,"\n");	
	}
	//this->parent->NumHijos++;
	/*std::string lastName;
		for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it){
			lastName=(*it)->getName();
		}*/
	/*for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		this->Arbol[i].isParent;*/
		this->parent->NumHijos++;
	if(strcmp(this->parent->unio.c_str(),"")!=0 && this->parent->NumHijos==this->parent->NHijos)	//parte final +	
	{	
		fprintf(fp,this->parent->unio.c_str());
		fprintf(fp,"\n");
		x=1;
		
	/*	unio="";*/
	}
	if(strcmp(this->parent->inters.c_str(),"") !=0)
	{
		fprintf(fp,this->parent->inters.c_str());
		fprintf(fp,"\n");
		x=1;
	/*	inters="";*/
	}
	if(strcmp(this->parent->subs.c_str(),"") !=0)
	{
		fprintf(fp,this->parent->subs.c_str());
		fprintf(fp,"\n");
		x=1;
		/*subs="";*/
	}
	if(strcmp(this->parent->rotar.c_str(),"")!=0&& x==1)
		{
			fprintf(fp,this->parent->rotar.c_str());
			fprintf(fp,"\n");
		}
	if(strcmp(this->parent->posi.c_str(),"")!=0&&x==1)
		{
			fprintf(fp,this->parent->posi.c_str());
			fprintf(fp,"\n");

		}
	
	fclose(fp);    /* Cerrar el archivo antes de terminar el programa */
	printf("Se ha creado el archivo: \n");	
}

void Hero::GBox::Export()
{
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->Export();
	if(!hidden)
		this->doExport();
}
void Hero::GBox::SetFile(std::string Files)
{	
	sFile=Files;
	
	//strcpy(sFile.c_str,Valor);
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)		
		(*it)->SetFile(Files);
}
char *Hero::GBox::getName()
{
	return name;
}

void Hero::GBox::setName(char *data)
{
	name=data;	
}

void Hero::GBox::setPosition()
{ 
	Vector uso;
if(Poss.size()>0)
	{
		int x=Poss.size(),i,j;
		
 char *str=new char[15]; 
 char paso [100],posicion[500]; 
  strcpy(posicion,"");
  strcat(posicion,"(COMMAND \"_.MOVE\" ");
  strcat(posicion,name);
  strcat(posicion," \"\" '(0 0 0) ");
    
 uso[0]=0;
 uso[1]=0;
 uso[2]=0;
 
	
	for(i=0;i<x;i++)
  {
	uso[0]=uso[0]+Poss[i].Destino[0];	  
	uso[1]=uso[1]+Poss[i].Destino[1];
	uso[2]=uso[2]+Poss[i].Destino[2];
	}

	strcpy(paso,"");
	sprintf(str, "'(%.6g",uso[0]);
	strcat(paso,str);	
	sprintf(str, " %.6g",uso[1]);
	strcat(paso,str);	
	sprintf(str, " %.6g)",uso[2]);
	strcat(paso,str);		
	strcat(posicion,paso);	
	strcat(posicion,") ");
	
	/*strcat(paso,") ");
	strcat(poss,paso);
	strcpy(paso,"");
	if(([0]/reference[3])==0)
		flt=0;
	else
		flt=dis*reference[0];
	
	sprintf(str, "'(%.6g",flt);
	strcat(paso,str);	
	if((reference[1]/reference[3])==0)
		flt=0;
	else
		flt=dis*reference[1];
	sprintf(str, " %.6g",flt);
	strcat(paso,str);	
	if((reference[2]/reference[3])==0)
		flt=0;
	else
		flt=dis*reference[2];*/	
 posi=posicion;
	}
}


void Hero::GBox::setOrientation(Vector reference ,float angle){
 char *str=new char[15];
 char paso[25],rotar1[500];
 float flt;
  
  strcpy(rotar1,"");
  strcat(rotar1,"(COMMAND \"_.ROTATE3D\" ");
  strcat(rotar1,this->getName());
  strcat(rotar1," \"\" '(");//0 0 0)");
  strcpy(paso, "");	
  flt=reference[0];	
  sprintf(str, "%.6g",flt);
  strcat(paso,str);	
  flt=reference[1];
  sprintf(str, " %.6g",flt);
  strcat(paso,str);	
  flt=reference[2];
  sprintf(str, " %.6g)",flt);
  strcat(paso,str);	
  strcat(rotar1,paso);

  strcpy(paso, "");	
  flt=0;	
  sprintf(str," '(%.6g",flt);
  strcat(paso,str);	
 /* flt=0;
  sprintf(str, " %.6g",flt);
  strcat(paso,str);	*/
  flt=0;
  sprintf(str, " %.6g)",flt);
  strcat(paso,str);	
  strcat(rotar1,paso);

  strcpy(paso,"");
  flt=angle*180/3.1416;
  sprintf(str, " %.3g",flt);
  strcat(paso,str);

  strcat(rotar1,paso);
  strcat(rotar1,") ");
  rotar=rotar+rotar1;
}

void Hero::GBox::modificar(char *operacion,std::vector<std::string> &Nombres,char *namen){
	char paso[500];
	int i;
strcpy(paso,"");
strcat(paso,"(COMMAND \"_.");
strcat(paso,operacion);
strcat(paso,"\" ");
for(i=0;i<Nombres.size();i++)
{
strcat(paso,Nombres[i].c_str());
strcat(paso," ");
}
strcat(paso," \"\" )");
strcat(paso,"\n(SETQ ");
strcat(paso,namen);
strcat(paso," (entlast))");
if(strcmp(namen,"UNION")==1)
	unio=paso;
else
	if(strcmp(namen,"INTERSECT")==1)
		inters=paso;
	else
		subs=paso;
//std::string pcNombreArch1 = "d:\\cubo.lsp";
//FILE *pArchivo1;
//pArchivo1 = fopen(pcNombreArch1.c_str(),"a");
//fprintf(pArchivo1,"%s\n%s\n",inte,nombre);			
//fclose(pArchivo1);
}
void Hero::GBox::rotate(const double angleInRadians, const double x, const double y, const double z)
{
	double cosT=cos(angleInRadians);
	double sinT=sin(angleInRadians);

	

	double _T0[4][4]={
    	 {1,  0,  0, T[3][0]},
    	 {0,  1,  0, T[3][1]},
    	 {0,  0,  1, T[3][2]},
    	 {0,  0,  0,  1}};
	Matrix<double, 4, 4> A(_T0);

    double _R[4][4]={
    	{cosT+(1-cosT)*x*x  , (1-cosT)*x*y-sinT*z, (1-cosT)*x*z+sinT*y, 	0},
    	{(1-cosT)*y*x+sinT*z, cosT+(1-cosT)*y*y  , (1-cosT)*y*z-sinT*x, 	0},
    	{(1-cosT)*z*x-sinT*y, (1-cosT)*z*y+sinT*x, cosT+(1-cosT)*z*z  , 	0},
    	{0				   , 0					, 0				  	 ,  1}
    };
    A*=_R;
    double _T1[4][4]={
    	 {1,  0,  0, -T[3][0]},
    	 {0,  1,  0, -T[3][1]},
    	 {0,  0,  1, -T[3][2]},
    	 {0,  0,  0,  1}};
    A*=_T1;
    T=A*T;
    build();

}

void Hero::GBox::rotate90(const double x, const double y, const double z)
{	
    double _T[4][4]={
    	{x*x  , x*y-z, x*z+y, 	0},
    	{y*x+z, y*y  , y*z-x, 	0},
    	{z*x-y, z*y+x, z*z  , 	0},
    	{0	  , 0	 , 0	,	1}
    };
    Matrix<double, 4, 4> A(_T);
    T=A*T;
    build();

}

void Hero::GBox::rotate180(const double x, const double y, const double z)
{	
    double _T[4][4]={
    	{2*x*x-1, 2*x*y,	2*x*z, 		0},
    	{2*y*x,   2*y*y-1,	2*y*z, 		0},
    	{2*z*x,   2*z*y, 	2*z*z-1, 	0},
    	{0	  ,   0,		0,			1}
    };
    Matrix<double, 4, 4> A(_T);
    T=A*T;
    build();

}


void Hero::GBox::rotateX(double angleInRadians)
{	
    double _T[4][4]={
    	{1, 0, 					  0, 				   0},
    	{0, cos(angleInRadians),  sin(angleInRadians), 0},
    	{0, -sin(angleInRadians), cos(angleInRadians), 0},
    	{0, 0, 					  0, 				   1}
    };
    Matrix<double, 4, 4> A(_T);
    T=A*T;
    build();

}

void Hero::GBox::rotateY(double angleInRadians)
{	
	double _T[4][4]={
    	{cos(angleInRadians), 0, -sin(angleInRadians), 0},
    	{0, 				  1,  0, 				   0},
    	{sin(angleInRadians), 0,  cos(angleInRadians), 0},
    	{0, 				  0,  0, 				   1}};
    Matrix<double, 4, 4> A(_T);
    T=A*T;
    build();
}

void Hero::GBox::rotateZ(double angleInRadians)
{	
	double _T[4][4]={
    	{cos(angleInRadians), sin(angleInRadians), 0, 0},
    	{-sin(angleInRadians), cos(angleInRadians), 0, 0},
    	{0, 				   0,  					1, 0},
    	{0, 				   0,  					0, 1}};
	Matrix<double, 4, 4> A(_T);
    T=A*T;
    build();
}

void Hero::GBox::rotate90(EFace faceNormal){
	
	if(faceNormal<0)
		return;
	Vector reference=normals[faceNormal];
	Vector referenceExport=this->parent->getNormalExport(faceNormal);
	rotate90(reference[0],reference[1],reference[2]);
	setOrientation(referenceExport,M_PI/2);
	build();
}

void Hero::GBox::rotate180(EAxis axis){
	
	if(axis<0)
		return;
	Vector reference;
	Vector referenceExport;
	switch(axis)
	{
		case LATERAL:
			reference=normals[2];
			//referenceExport=this->parent->getNormalExport(2);
			break;
		case LONGITUDINAL:
			reference=normals[1];
			//referenceExport=this->parent->getNormalExport(1);
			break;
		case VERTICAL:
			reference=normals[4];
			//referenceExport=this->parent->getNormalExport(4);
			break;
		default:
			return;
	}
	rotate180(reference[0],reference[1],reference[2]);
	setOrientation(reference,M_PI);
	build();
	return;
	build();
}

void Hero::GBox::rotate(EFace faceNormal, double angleRadians){
	
	if(faceNormal<0)
		return;
	Vector reference=normals[faceNormal];
	Vector referenceExport=this->parent->getNormalExport(faceNormal);
	rotate(angleRadians, reference[0],reference[1],reference[2]);
	setOrientation(referenceExport ,angleRadians);
	build();
}

void Hero::GBox::translate(double dx, double dy, double dz)
{
    double _T[4][4]={
    	 {1,  0,  0, dx},
    	 {0,  1,  0, dy},
    	 {0,  0,  1, dz},
    	 {0,  0,  0,  1}};
	Matrix<double, 4, 4> A(_T);
    T=A*T;
    build();
}

void Hero::GBox::translate(const Vector &v)
{
	double _T[4][4]={
    	 {1,  0,  0, (*(const_cast<Vector*>(&v)))[0]},
    	 {0,  1,  0, (*(const_cast<Vector*>(&v)))[1]},
    	 {0,  0,  1, (*(const_cast<Vector*>(&v)))[2]},
    	 {0,  0,  0,  1}};
	Matrix<double, 4, 4> A(_T);
    T=A*T;
    build();
}

void Hero::GBox::translate(const Vector &v, double factor)
{
	double n=v.norm();
	if(ABS(n)<EPSILON)
		return;
	Vector w=v/n;
	double _T[4][4]={
    	 {1,  0,  0, w[0]*factor},
    	 {0,  1,  0, w[1]*factor},
    	 {0,  0,  1, w[2]*factor},
    	 {0,  0,  0, 1}};
	Matrix<double, 4, 4> A(_T);
    T=A*T;
    build();
}

void Hero::GBox::beginDefinition(void){
	//if(strcmp(this->name,"Root")!=0)		this->parent->NumHijos++;
	if(parent==NULL)
	{
		
		T.identity();
	}
	else
	{
		this->parent->NHijos++;
		T=parent->T;
	}
	defSelf();
	build();
}

void Hero::GBox::endDefinition(void){
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->inheritFromParent();
	defParts();
}


void Hero::GBox::addChild(GBox &child){
	parts.insert(&child);
}

double Hero::GBox::getWidth(void){
	return width;
}

double Hero::GBox::getLength(void){
	return length;
}

double Hero::GBox::getHeight(void){
	return height;
}


//int Hero::GBox::main(int argc, char* argv[]){
//	return 0;
//}

Hero::GBox& Hero::GBox::getPart(unsigned index){
	if(index<0 || parts.size()<=index)
		return *this;
	std::set<GBox*>::iterator it;
	unsigned count;
	for(count=0, it=parts.begin(); count<index;++count, ++it);
	return *(*(it));
}

unsigned Hero::GBox::partsCount(void){
	return (unsigned)parts.size();
}

std::set<Hero::GBox*>::iterator Hero::GBox::getFirstPart(void){
	return parts.begin();
}

std::set<Hero::GBox*>::iterator Hero::GBox::getLastPart(void){
	return parts.end();
}

void Hero::GBox::setVisible(bool visible){
	this->visible=visible;
	if(bindParts)
		for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
			(*it)->setVisible(visible);

}

bool Hero::GBox::getVisible(void){
	return this->visible;
}

void Hero::GBox::setBindParts(bool bindParts){
	this->bindParts=bindParts;
}

bool Hero::GBox::getBindParts(void){
	return bindParts;
}

void Hero::GBox::setHidden(bool hidden){
	this->hidden=hidden;
}

bool Hero::GBox::getHidden(void){
	return hidden;
}

void Hero::GBox::setDisplayType(Hero::EDisplayType displayType){
	this->displayType=displayType;
	if(bindParts)
		for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
			(*it)->setDisplayType(displayType);
}

Hero::EDisplayType Hero::GBox::getDisplayType(void){
	return this->displayType;
}

void Hero::GBox::setDimmensions(double length, double width, double height)
{
	this->width=width;
	this->length=length;
	this->height=height;
	build();
}


void Hero::GBox::setWidth(double width){
	this->width=width;
	build();
}

void Hero::GBox::setLength(double length){
	this->length=length;
	build();
}

void Hero::GBox::setHeight(double height){
	this->height=height;
	build();
}

void Hero::GBox::defSelf(void){
}

void Hero::GBox::defParts(void){
}

double Hero::GBox::distanceFromCenterToFaceCenter(EFace face){
	switch(face){
		case LEFT:case RIGHT:
			return width*0.5;
		case FRONT:case REAR:
			return length*0.5;
		case TOP:case BOTTOM:
			return height*0.5;
		default:
			return -1;
	}
}

double Hero::GBox::distanceFromCenterToFaceCenter(const Vector &v){
	double best=-1;
	int index=-1;
	for(int i=0;i<6;i++)
	{
		double op=this->normals[i]*v;
		if(op>best)
		{
			best=op;
			index=i;
		}
	}
	if(index<0)
		return -1;
	return distanceFromCenterToFaceCenter(EFace(index));
}

void Hero::GBox::position(EFace pos, double dist){
	Movim a;
	Vector paso;
	int i;
	if(parent==NULL)
	{
		return;
	}
	Vector parentDisplacementVector=this->parent->getNormal(pos);
	Vector parentDisplacementVectorExport=this->parent->getNormalExport(pos);
	double parentDisplacementDistance=this->parent->distanceFromCenterToFaceCenter(pos);

	double childDisplacementDistance=this->distanceFromCenterToFaceCenter(parentDisplacementVector);
	this->translate(parentDisplacementVector, parentDisplacementDistance-(childDisplacementDistance+dist));	
	
	paso=(parentDisplacementVectorExport*(parentDisplacementDistance-(childDisplacementDistance+dist)));

		a.Destino = paso;
		a.Origen = parentDisplacementVectorExport;
		a.pieza=name;
		this->Poss.push_back(a);
}

void Hero::GBox::position(EFaceReference pos, double dist){
	Movim a;
	Vector paso;
	int i;
	if(parent==NULL)
	{
		return;
	}
	Vector parentDisplacementVector=this->parent->getNormal((EFace)pos);
	Vector parentDisplacementVectorExport=this->parent->getNormalExport((EFace)pos);
	double parentDisplacementDistance=this->parent->distanceFromCenterToFaceCenter(EFace(pos));

	double childDisplacementDistance=this->distanceFromCenterToFaceCenter(-parentDisplacementVector);
	this->translate(parentDisplacementVector, parentDisplacementDistance+(childDisplacementDistance+dist));			
///terminar y agregar a los demas
	paso=(parentDisplacementVectorExport*(parentDisplacementDistance+(childDisplacementDistance+dist)));

		a.Destino = paso;
		a.Origen = parentDisplacementVectorExport;
		a.pieza=name;		
		this->Poss.push_back(a);	
}

void Hero::GBox::position(EFace pos, double dist, GBox &reference){
	Vector paso;
	int i;
	Movim a;	
	if(reference.parent==NULL)
		return;
	//this->translate(reference.points[CENTER]-this->points[CENTER]);
	Vector parentDisplacementVector=reference.parent->getNormal(pos);
	Vector parentDisplacementVectorExport=reference.parent->getNormalExport(pos);
	double referenceDisplacementDistance=reference.distanceFromCenterToFaceCenter(parentDisplacementVector);
	referenceDisplacementDistance+=distanceAlongVector(this->points[CENTER], reference.points[CENTER], parentDisplacementVector);

	double childDisplacementDistance=this->distanceFromCenterToFaceCenter(parentDisplacementVector);
	this->translate(parentDisplacementVector, referenceDisplacementDistance-(childDisplacementDistance+dist));	
	paso=(parentDisplacementVectorExport*(referenceDisplacementDistance-(childDisplacementDistance+dist)));
		a.Destino = paso;
		a.Origen = parentDisplacementVectorExport;
		a.pieza=name;	
		a.referencia=reference.getName();
		this->Poss.push_back(a);
}

void Hero::GBox::position(EFaceReference pos, double dist, GBox &reference){
	Movim a;
	int i;
	Vector paso;
	if(reference.parent==NULL)
		return;
	//this->translate(reference.points[CENTER]-this->points[CENTER]);
	Vector parentDisplacementVector=reference.parent->getNormal((EFace)pos);
	Vector parentDisplacementVectorExport=reference.parent->getNormalExport((EFace)pos);
	double referenceDisplacementDistance=reference.distanceFromCenterToFaceCenter(parentDisplacementVector);
	referenceDisplacementDistance+=distanceAlongVector(this->points[CENTER], reference.points[CENTER], parentDisplacementVector);
	double childDisplacementDistance=this->distanceFromCenterToFaceCenter(parentDisplacementVector);
	this->translate(parentDisplacementVector, referenceDisplacementDistance+(childDisplacementDistance+dist));
paso=parentDisplacementVectorExport*(referenceDisplacementDistance+(childDisplacementDistance+dist));     
	
		a.Destino = paso;
		a.Origen = parentDisplacementVectorExport;
		a.pieza=name;		
		a.referencia=reference.getName();
		this->Poss.push_back(a);
}
/*void Hero::GBox::printPoints()
{
	double _points[15][4];
	std::string pcNombreArch = "d:\\Matriz.txt";
	FILE *pArchivo;
	
	if( (pArchivo = fopen(pcNombreArch.c_str(),"wt")) == NULL ) {
		printf("No se pudo crear el archivo de salida para el comparador %s\n", pcNombreArch.c_str());
		 return;	
	}
	for(int i=0;i<15;i++)
	{
		//_points[i] = points[i];
		if(_points[i][3]==0)
		{
		fprintf(pArchivo,"\n");
		}
		else
		{
		points[i].copyTo(_points[i]);
		_points[i][0]= _points[i][0]/_points[i][3];
		_points[i][1]= _points[i][1]/_points[i][3];
		_points[i][2]= _points[i][2]/_points[i][3];
		fprintf(pArchivo,"%lf\t %lf \t %lf \n",_points[i][0],_points[i][1],_points[i][2]);
		}
		
	}	
	fclose(pArchivo);
}*/

void Hero::GBox::initposition()
{	Movim init;
init.Origen[0] = 0;
init.Origen[1] = 0;
init.Origen[2] = 0;
init.Origen[3] = 1;
init.Destino[0] = 0;
init.Destino[1] = 0;
init.Destino[2] = 0;
init.Destino[3] = 1;
init.pieza=this->getName();

Poss.push_back(init);
}
void Hero::GBox::ReDefParts()
{	
	rotar="";
	posi="";
	Nombres.clear();
	Poss.clear();
	NHijos=0;
	//cambio=1;
	
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)		
		(*it)->ReDefParts();
}

void Hero::GBox::setpeso(double pe, char *mat, int id_mat, char *UM, GLfloat color)
{ 
Materialest x;
x.GLcolor= color;
x.id_material= id_mat;
x.material= mat;
x.peso_especifico=pe;
x.UM=UM;
this->TMat.push_back(x);
}

void Hero::GBox::calcpeso()
{
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it) //Para Barrer el Arbol		
		(*it)->calcpeso();
	if(!hidden){
		this->calVolumen();
		this->peso=this->volumen*this->TMat[0].peso_especifico;
	}
}


void Hero::GBox::inTree()///JIDC 3/18/2010 void? double?
{
	Tree paso; //funcion para llenar le vector
	char * nombre;
	int Nv=0;
	paso.nivel=0;
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)	
	{				
			paso.parent=(*it)->parent->getName();	
			paso.name=(*it)->getName();				
		if(strcmp(parent->getName(),"Root")!=0)
			paso.IsChildren=false;									
		else
			paso.IsChildren=true;
		paso.IsParent=(*it)->parts.size()>0;
			
	Arbol.push_back(paso);
	(*it)->inTree(Arbol,Nv);
	}
	//	else volumen+=(*it)->calVolumen();	
}
void Hero::GBox::inTree(std::vector<Tree> &A,int N)///JIDC 3/18/2010 void? double?
{
	Tree paso; //funcion para llenar le vector
	char * nombre;
	int Nv=N+1;	
	paso.indicehijo=0;
	paso.nivel=N+1;
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)	
	{			paso.parent=(*it)->parent->getName();	
				paso.name=(*it)->getName();
				paso.indicehijo++;				
		if(strcmp(parent->getName(),"Root")!=0)
			paso.IsChildren=false;									
		else
			paso.IsChildren=true;
		paso.IsParent=(*it)->parts.size()>0;
	A.push_back(paso);
	(*it)->inTree(A,N+1);
	}
	//	else volumen+=(*it)->calVolumen();	
}

void Hero::GBox::calVolumen()///JIDC 3/19/2010 calcula volumen
{//AEC recursiva de volumen
	double dAux;
	int iContParts=0;
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it){	
		(*it)->calVolumen();
		dAux+=(*it)->volumen;
		iContParts++;
	}

	if(!hidden)
		this->Cvolumen();
	//si no es primitiva
	if (iContParts>0)
	{
		this->volumen=dAux;
	}
	
}

void Hero::GBox::Cvolumen()
{
volumen = height*length*width;//AEC
}

void Hero::GBox::generaReportePesos(std::string NombreArch)
{
	char encabezado[100]="\n\t\t\t\t\t\tREPORTE ESTRUCTURA\t\t\tFecha:",total[100]="\t\t\tTOTAL";
	char x[25],firmas[100]="ELABORO: __________________    REVISO: ____________________    VERIFICO: ___________________";
	double flt,tvolumen=0, tpeso=0;
		
   time_t tSac = time(NULL);       // instante actual
   struct tm* tmP = localtime(&tSac);
	//strcpy(fechax,"");
    strcpy(x,"");
	flt=tmP->tm_mday;
	sprintf(x, " %.6g",flt);
	strcat(encabezado,x);
	flt=tmP->tm_mon+1;
	sprintf(x, "/%.6g",flt);
	strcat(encabezado,x);
	flt=tmP->tm_year+1900;
	sprintf(x, "/%.6g",flt);
	strcat(encabezado,x);
	strcat(encabezado,"\n\n\t\t\tPARTE\t\t\t\tVOLUMEN\t\t\tPESO\n");

	std::string pcNombreArchx;
	//strcat(
	pcNombreArchx=NombreArch;//);//= "d:\\reportepesos.txt";	
	FILE *pArchivox;	
	pArchivox = fopen(pcNombreArchx.c_str(),"a");
	fprintf(pArchivox,"%s\n",encabezado);
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
	{
		/*if(paso==1)
			fprintf(pArchivox,"\t\t\t%s\t\t\t%5.2f\t\t%5.2f\n",(*it)->name,(*it)->volumen,(*it)->peso);
		else*/
		    fprintf(pArchivox,"\t\t\t%s\t\t\t\t%5.2f\t\t\t%5.2f\n",(*it)->name,(*it)->volumen,(*it)->peso);
			tvolumen=tvolumen+(*it)->volumen;
			tpeso=tpeso+(*it)->peso;
	}
	flt=tvolumen;
	sprintf(x, "\t\t\t\t%.5g",flt);
	strcat(total,x);
	flt=tpeso;
	sprintf(x, "\t\t\t%.5g",flt);
	strcat(total,x);
	fprintf(pArchivox,"\n%s\n\n\t\t%s",total,firmas);
	fclose(pArchivox);	
}
void Hero::GBox::ejecutaAccionMenu(AccionMenuTree Accion, std::string Parte){
	std::string aaaa;
	if (Parte == (std::string) this->getName()){
		switch (Accion){
				case SHOW:
					setHiddenRecusive(false);
					draw();
					break;
				case HIDE:
					setHiddenRecusive(true);
					draw();
					break;
				case SOLIDO:
					//(*it)->setBindPartsRecursive(true);
					setDisplayType(SOLID);
					draw();
					break;
				case WIRE:
					//(*it)->setBindPartsRecursive(true);
					setDisplayType(WIRED);
					draw();
					break;
			}//switch
		return;
	}
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it){	
		aaaa = (std::string) (*it)->getName();
		if (Parte == (std::string) (*it)->getName()){
			switch (Accion){
				case SHOW:
					(*it)->setHiddenRecusive(false);
					(*it)->draw();
					break;
				case HIDE:
					(*it)->setHiddenRecusive(true);
					(*it)->draw();
					break;
				case SOLIDO:
					//(*it)->setBindPartsRecursive(true);
					(*it)->setDisplayType(SOLID);
					(*it)->draw();
					break;
				case WIRE:
					//(*it)->setBindPartsRecursive(true);
					(*it)->setDisplayType(WIRED);
					(*it)->draw();
					break;
			}//switch
		}//if
		(*it)->ejecutaAccionMenu(Accion,Parte); //busca en las subpartes
	}//for
}//ejecutaAccionMenuTree
void Hero::GBox::setHiddenRecusive(bool hidden){
	std::string a;
	std::string b;
	bool its;
	a = (std::string) this->name;
	if (!this->isReference)
		this->setHidden(hidden);
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it){
		b = (std::string)(*it)->getName();
		(*it)->setHiddenRecusive(hidden);
		///if (!this->isReference)
			//(*it)->setHidden(hidden);
			its =(*it)->getHidden();
	}//For
	
}
void Hero::GBox::setBindPartsRecursive(bool bindParts){
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it){
		(*it)->setBindPartsRecursive(bindParts);
	}//for
	//this->setBindParts(bindParts);
}
#ifdef USE_GAPI_GRAPHICS
void Hero::GBox::doDraw(void){
	if(!visible)
		return;
	//GLenum displayType=this->displayType==WIRED?GL_LINE_LOOP:GL_TRIANGLE_FAN;
	GLenum displayType=(int)this->displayType==1?GL_LINE_LOOP:GL_TRIANGLE_FAN;
	glPushMatrix();
	glMultMatrixd(T.getData());
	drawBox(width, length, height,displayType);
	glPopMatrix();
}
#else
void Hero::GBox::doDraw(void){
	if(!visible)
		return;
	std::cerr<<"Object: GBox"<<std::endl;
}
#endif
