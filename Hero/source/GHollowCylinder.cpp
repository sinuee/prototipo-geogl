/*
 * HollowCylinder.cpp
 *
 *  Created on: Dec 11, 2008
 *      Author: khayyam
 */
#include "GHollowCylinder.h"
#include "GMacros.h"
#include <iostream>
Hero::GHollowCylinder::GHollowCylinder(GBox &parent):GCylinder(parent){
	slices=20;
	stacks=5;
	radius=MIN(width, length)*0.5;
	innerRadius=radius*0.5;
	/*strcpy(rotar.c_str(),"");
	strcpy(posi.c_str(),"");*/
}


Hero::GHollowCylinder::GHollowCylinder():GCylinder(){
	slices=20;
	stacks=5;
	radius=MIN(width, length)*0.5;
	innerRadius=radius*0.5;
}

Hero::GHollowCylinder::~GHollowCylinder(){

}



void Hero::GHollowCylinder::setInnerRadius(double innerRadius){
	this->innerRadius=innerRadius;
}

void Hero::GHollowCylinder::setRadius(double radius){
	this->radius=radius;
	width=length=2*this->radius;
	build();
}

double Hero::GHollowCylinder::getIneerRadius(void){
	return innerRadius;
}


void Hero::GHollowCylinder::doExport(void){
	
	char *str=new char[8];	
	char centro[15];
	char valor[100],cil2[100],suba[100],subb[100],Revol[100],rot[100];
	int x=0;
	/*std::string sAux;
	//Centro ? **********
	double _points[1][4], radioint;	
	points[0].copyTo(_points[0]);
	sAux = "";
	for (int k=0;k<3;k++)
		sAux = sAux +  convertToString<int >((int)(points[k][0]/ points[k][3])) + " ";
	strcpy(centro,sAux.c_str());*/
	//*_points[0] = points[0];
	//***************/
	double flt;		
	//Vector desp=normals[BOTTOM]*(height*0.5);
	strcpy(centro, "");	
	flt=0;	
	sprintf(str, "%.6g",flt);
	strcat(centro,str);
	//strcat(centro," ");
	flt=this->innerRadius;
	sprintf(str, " %.6g",flt);
	strcat(centro,str);
	//strcat(centro," ");
	flt=0;
	sprintf(str, " %.6g)",flt);
	strcat(centro,str);
	//se definen los dos cilindros puesto q solo se modifica el radio en el segundo cilindro
	//solo se necesita el radio
	strcpy(valor,"");
	strcat(valor,"(COMMAND \"_.RECTANG\" '(");	
	strcat(valor,centro);
	//strcat(valor, ")");	
	sprintf(str, " '(%.6g",this->height);
	strcat(valor,str);	
	//strcat(valor, " \"_TOP\"");
	//strcat(valor,str);	
	sprintf(str, " %.6g)",this->radius);
	strcat(valor,str);	
	strcat(valor,") ");

	/*sprintf(str, " %.6g",this->getIneerRadius());
	strcat(cil2,str);	
	//strcat(cil2, " \"_TOP\"");
	//strcat(cil2,str);	
	sprintf(str, " %.6g",this->height);
	strcat(cil2,str);	
	strcat(cil2,") ");*/

	//se usa para hacer el subtract de los dos cinlindros 
	strcpy(suba,"");
	strcat(suba,"(SETQ ");
	strcat(suba,name);
	strcat(suba," (ENTLAST))");
	strcpy(subb,"");
	strcat(subb,"(SETQ c2 (ENTLAST)) ");
	strcpy(Revol,"");
	strcat(Revol,"(COMMAND \"_.REVOLVE\" c2 \"\" \"x\" 360)");	
	strcpy(rot,"");
	strcat(rot,"(COMMAND \"_.ROTATE3D\" ");
	strcat(rot,name);
	strcat(rot," \"\" '(0 0 0) '(0 1) 90)");
	if(this->radius!=0&&this->innerRadius!=0)
		{
		std::string pcNombreArch1 = "";//"d:\\cubo.lsp";
		pcNombreArch1=sFile.c_str();
		FILE *pArchivo1;		
		pArchivo1 = fopen(pcNombreArch1.c_str(),"a");
		fprintf(pArchivo1,"%s\n%s\n%s\n%s\n%s\n",valor, subb, Revol, suba,rot);			
		if(strcmp(this->parent->rotar.c_str(),"") != 0)
			fprintf(pArchivo1,"%s\n",this->rotar.c_str());	
		if(strcmp(this->parent->posi.c_str(),"") != 0)
			fprintf(pArchivo1,"%s\n",this->posi.c_str());	
		if(strcmp(this->parent->unio.c_str(),"") !=0 )//parte final +
		{
			fprintf(pArchivo1,"%s\n",this->parent->unio.c_str());
			x=1;
		}
		if(strcmp(this->parent->inters.c_str(),"") !=0)
		{
			fprintf(pArchivo1,"%s\n",this->parent->inters.c_str());
			x=1;
		}
		if(strcmp(this->parent->subs.c_str(),"") !=0)
		{
			fprintf(pArchivo1,"%s\n",this->parent->subs.c_str());
			x=1;
		}
		if(strcmp(this->parent->rotar.c_str(),"") != 0&&x==1)
			fprintf(pArchivo1,"%s\n",this->parent->rotar.c_str());	
		if(strcmp(this->parent->posi.c_str(),"") != 0&&x==1)
			fprintf(pArchivo1,"%s\n",this->parent->posi.c_str());
		fclose(pArchivo1);	
       }
}

/*void Hero::GHollowCylinder::Export()
{
for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->Export();
	if(!hidden)
		this->doExport();
}*/

char* Hero::GHollowCylinder::getName()
{
	return name;
}

void Hero::GHollowCylinder::setName(char *data)
{
	name=data;
}

void Hero::GHollowCylinder::Cvolumen()
{
	volumen = height*M_PI*(pow(radius,2) - pow(innerRadius,2));//AEC
}

#ifdef USE_GAPI_GRAPHICS
void Hero::GHollowCylinder::doDraw(void){
	if(!visible)
		return;
	GLUquadricObj *quadObj=gluNewQuadric();
	gluQuadricDrawStyle(quadObj, (((displayType)==WIRED)?GLU_LINE:GLU_FILL));
	glPushMatrix();
	Vector desp=normals[BOTTOM]*(height*0.5);

	//glTranslated(desp[0], desp[1], desp[2]);
	glMultMatrixd(T.getData());
	gluQuadricOrientation(quadObj, GLU_INSIDE);
	gluCylinder(quadObj, innerRadius, innerRadius, height, slices, stacks);
	gluQuadricOrientation(quadObj, GLU_OUTSIDE);
	gluCylinder(quadObj, radius, radius, height, slices, stacks);


	gluQuadricOrientation(quadObj, GLU_INSIDE);
	gluDisk(quadObj, innerRadius, radius, slices, stacks);

	glPopMatrix();
	glPushMatrix();
	glTranslated(-desp[0], -desp[1], -desp[2]);
	glMultMatrixd(T.getData());
	gluQuadricOrientation(quadObj, GLU_OUTSIDE);
	gluDisk(quadObj, innerRadius, radius, slices, stacks);
	glPopMatrix();
	gluDeleteQuadric(quadObj);

}
#else
void Hero::GHollowCylinder::doDraw(void){
	if(!visible)
		return;
	std::cerr<<"Object: GHollowCylinder"<<std::endl;
}
#endif
