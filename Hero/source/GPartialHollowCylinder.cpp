/*
 * GPartialHollowCylinder.cpp
 *
 *  Created on: Dec 15, 2008
 *      Author: khayyam
 */
#include "GPartialHollowCylinder.h"
#include "GMacros.h"
#include "GPrimitives.h"

Hero::GPartialHollowCylinder::GPartialHollowCylinder():GHollowCylinder(){
	this->startAngle=0;
	this->sweepAngle=M_PI;	
	parent=NULL;
	rotar="";
	posi="";
	unio="";
}
Hero::GPartialHollowCylinder::GPartialHollowCylinder(GBox &parent):GHollowCylinder(parent){
	this->startAngle=0;
	this->sweepAngle=M_PI;
	//this->isReference=true;
	//this->setHidden(true);
}

Hero::GPartialHollowCylinder::~GPartialHollowCylinder(){

}

void Hero::GPartialHollowCylinder::setStartAngle(double angle){
	this->startAngle=angle;
}

void Hero::GPartialHollowCylinder::setSweepAngle(double angle){
	this->sweepAngle=angle;	
}

double Hero::GPartialHollowCylinder::getStartAngle(void){
	return startAngle;
}

double Hero::GPartialHollowCylinder::getSweepAngle(void){
	return sweepAngle;
}

//void Hero::GPartialHollowCylinder::Export()
//{
//for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
//		(*it)->Export();
//	if(!hidden)
//		this->doExport();
//}

char *Hero::GPartialHollowCylinder::getName()
{
	return name;
}

void Hero::GPartialHollowCylinder::setName(char *data)
{
	name=data;
}

void Hero::GPartialHollowCylinder::Cvolumen()
{
	double x= sweepAngle/(2*M_PI);
	volumen = x*height*M_PI*(pow(radius,2) - pow(innerRadius,2));//AEC
}
#ifdef USE_GAPI_GRAPHICS
void Hero::GPartialHollowCylinder::doDraw(void){
	if(!visible)
		return;
	GLenum displayType=this->displayType==WIRED?GL_LINE_LOOP:GL_TRIANGLE_FAN;

	Vector desp=normals[BOTTOM]*(height*0.5);

	glPushMatrix();
	glTranslated(desp[0], desp[1], desp[2]);
	glMultMatrixd(T.getData());
	drawPartialHollowCylinder(this->innerRadius, this->radius, this->height, startAngle, sweepAngle, slices, displayType);
	glPopMatrix();
}

void Hero::GPartialHollowCylinder::doExport(void)
{
	char *str=new char[15];	
	char centro[30];
	char valor[100],Revol[100],Revol1[100],Revol2[100],suba[300],subb[100],subc[100],sub2b[100],sub2a[100],sub2c[100],rot[100],rot1[100];	
	float flt;		
	int x=0;
	//centro se utiliza para generar los cambios de variables 
	std::string sAux;
	//Centro ? **********
	double _points[1][4], radioint;//,paso[3];	
	/*points[0].copyTo(_points[0]);
	sAux = "";
	for (int k=0;k<3;k++)
			sAux = sAux +  convertToString<int >((int)(points[k][0]/ points[k][3])) + " ";
		
	strcpy(centro,sAux.c_str());*/	
	strcpy(rot1,"");
	strcat(rot1,"(COMMAND \"_.ROTATE\" ");
	strcat(rot1,name);
	strcat(rot1," \"\" '(0 0) 270)");
	strcpy(centro, "");		
	sprintf(str, "-%.6g",this->height/2);
	strcat(centro,str);	
	flt=this->innerRadius;
	sprintf(str, " %.6g",flt);
	strcat(centro,str);	
	flt=0;
	sprintf(str, " %.6g)",flt);
	strcat(centro,str);	

	strcpy(valor,"");
	strcat(valor,"(COMMAND \"_.RECTANG\" '(");	
	strcat(valor,centro);	
	if(this->height>0)
		sprintf(str, " '(%.6g",this->height/2);
	else
		sprintf(str, " '(-%.6g",this->height/2);
	strcat(valor,str);		
	sprintf(str, " %.6g)",this->radius);
	strcat(valor,str);	
	strcat(valor,") ");	

	//se usa para hacer el subtract de los dos cinlindros 
	strcpy(suba,"");
	strcat(suba,"(SETQ ");
	strcat(suba,name);
	strcat(suba," (ENTLAST))");
	strcpy(subb,"");
	strcat(subb,"(SETQ c2 (ENTLAST))");	
	if(this->getStartAngle()==0)
	{
	  strcpy(Revol,"");	  
	  //strcat(Revol,"(COMMAND \"_.REVOLVE\" c2 \"\" \"x\" \"START\" -90");	
	  strcat(Revol,"(COMMAND \"_.REVOLVE\" c2 \"\" \"x\" ");	
	  strcpy(str,"");
	  strcpy(centro,"");
	  
	  if((this->getSweepAngle()-M_PI)==0)			
	  { 
		sprintf(str," 180)");
	    strcat(centro,str);
	  }
	  else
	  {		
		flt=(this->getSweepAngle())*180/M_PI;
		sprintf(str," %6.g)",flt);
	  }

	  //strcat(Revol1,
	  strcat(Revol,str);
	}
	else
	{
	strcpy(subc,"");
	strcat(subc,"(SETQ c3 (ENTLAST))");
	strcpy(Revol1,"");
	//strcat(Revol1,"(COMMAND \"_.REVOLVE\" c2 \"\" \"x\" \"START\" -90");	
	strcat(Revol1,"(COMMAND \"_.REVOLVE\" c2 \"\" \"x\" \"START\" ");	
	flt=(this->getStartAngle())*180/M_PI;
	sprintf(str, " %.6g",flt);
	strcat(Revol1,str);	
	/*strcpy(Revol2,"");
	strcat(Revol2,"(COMMAND \"_.REVOLVE\" c3 \"\" \"x\" \"START\" -90");*/
	flt=(this->getSweepAngle())*180/M_PI;
	sprintf(str, " %.6g)",flt);
	//strcat(Revol2,str);
	strcat(Revol1,str);
	/*strcpy(sub2a,"");
	strcat(sub2a,"(SETQ D1 (ENTLAST))");
	strcpy(sub2b,"");
	strcat(sub2b,"(SETQ D2 (ENTLAST))");
	strcpy(sub2c,"");
	strcat(sub2c,"(COMMAND \"_.SUBTRACT\" D1 \"\" D2 \"\")");	*/
	}
	strcpy(rot,"");
	strcat(rot,"(COMMAND \"_.ROTATE3D\" ");
	strcat(rot,name);
	strcat(rot," \"\" '(0 -1 0) '(0 0) 90)");		

	if(this->radius!=0)//&&this->innerRadius!=0)
		{		
			std::string pcNombreArch1 = this->sFile;
	FILE *pArchivo1;			
	pArchivo1 = fopen(pcNombreArch1.c_str(),"a");

	if(this->getStartAngle()==0)
	fprintf(pArchivo1,"%s\n%s\n%s\n%s\n%s\n%s\n",valor,subb,Revol,suba,rot,rot1);					

	else
	//fprintf(pArchivo1,"%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n",valor,subb,Revol1,sub2a,valor,subc,Revol2,sub2b,sub2c,suba,rot,rot1);					
	fprintf(pArchivo1,"%s\n%s\n%s\n%s\n%s\n%s\n",valor,subb,Revol1,suba,rot,rot1);					

	if(strcmp(this->rotar.c_str(),"") != 0)
		fprintf(pArchivo1,"%s\n",this->rotar.c_str());	
	if(strcmp(this->posi.c_str(),"") != 0)
		fprintf(pArchivo1,"%s\n",this->posi.c_str());
	this->parent->NumHijos++;
	if(strcmp(this->parent->unio.c_str(),"") !=0 &&	this->parent->NumHijos==this->parent->NHijos)
	{
		fprintf(pArchivo1,"%s\n",this->parent->unio.c_str());
		x=2;
	}
	if(strcmp(this->parent->inters.c_str(),"") !=0)
	{
		fprintf(pArchivo1,"%s\n",this->parent->inters.c_str());
		x=2;
	}
	if(strcmp(this->parent->subs.c_str(),"") !=0)
	{
		fprintf(pArchivo1,"%s\n",this->parent->subs.c_str());
		x=2;
	}
	if(strcmp(this->parent->rotar.c_str(),"") != 0&&x==2)		
		fprintf(pArchivo1,"%s\n",this->parent->rotar.c_str());	
	if(strcmp(this->parent->posi.c_str(),"") != 0&&x==2)
		fprintf(pArchivo1,"%s\n",this->parent->posi.c_str());
	fclose(pArchivo1);
	}
}
#else
void Hero::GPartialHollowCylinder::doDraw(void){
	if(!visible)
		return;
	std::cerr<<"Object: GPartialHollowCylinder"<<std::endl;
}
void Hero::GPartialHollowCylinder::doExport(){
}
#endif