/*
 * GTruncatedCone.cpp
 *
 *  Created on: Dec 11, 2008
 *      Author: khayyam
 */
#include "GTruncatedCone.h"
#include "GMacros.h"
#include <iostream>
Hero::GTruncatedCone::GTruncatedCone():GCylinder(){
	topRadius=0;
	//strcpy(rotar.c_str(),"");
	//strcpy(posi.c_str(),"");
}

Hero::GTruncatedCone::GTruncatedCone(GBox &parent):GCylinder(parent){
	topRadius=0;
}

Hero::GTruncatedCone::~GTruncatedCone(){

}

void Hero::GTruncatedCone::setTopRadius(double topRadius){
	this->topRadius=topRadius;
	width=length=2*MAX(this->topRadius, this->radius);
}

double Hero::GTruncatedCone::getTopRadius(void){
	return topRadius;

}

void Hero::GTruncatedCone::doExport(void){
	
	char nombreArchivo[100]= "";
	//strcat(nombreArchivo,"\";
	strcat(nombreArchivo,sFile.c_str());

	char buffer[355];
	char *str=new char[8];
	int x=0;
	if(this->radius!=0&&this->topRadius!=0)
		{				
	std::string sAux;
	char centro[15];
	//Centro ? **********
	double _points[1][4];
	points[0].copyTo(_points[0]);
	sAux = "";
	for (int k=0;k<3;k++)
		sAux = sAux + convertToString<int >((int)(points[k][0]/ points[k][3])) + " ";
	strcpy(centro,sAux.c_str());
	
		FILE *fp;

		fp = fopen(nombreArchivo, "w");   /* Abrir archivo para escritura */

	strcpy(buffer,"");
	strcat(buffer,"(COMMAND \"_.CONE\" ");	
	strcat(buffer,"'(");
	strcat(buffer,centro);
	strcat(buffer,") ");
	sprintf(str, "%.6f", this->getRadius());   
	strcat(buffer,str);
	strcat(buffer," \"T\" ");
	sprintf(str, "%.6f", this->getTopRadius()); 
	strcat(buffer,str);
	strcat(buffer," ");
	sprintf(str, "%.6f", this->getHeight());  //alto
	strcat(buffer,str);
	strcat(buffer,")\n");
	strcat(buffer,"(SETQ ");
	strcat(buffer,this->getName());
	strcat(buffer," (ENTLAST))");
	fprintf(fp, buffer);
	this->parent->NumHijos++;
	if(strcmp(this->rotar.c_str(),"") != 0)
		fprintf(fp,this->rotar.c_str());	
	if(strcmp(this->posi.c_str(),"") != 0)
		fprintf(fp,this->posi.c_str());
	if(strcmp(this->parent->unio.c_str(),"") !=0)//parte final +
	{
		fprintf(fp,this->parent->unio.c_str());
		x=1;
	}
	if(strcmp(this->parent->inters.c_str(),"") !=0)
	{
		fprintf(fp,this->parent->inters.c_str());
		x=1;
	}
	if(strcmp(this->parent->subs.c_str(),"") !=0)
	{
		fprintf(fp,this->parent->subs.c_str());
		x=1;
	}
	if(strcmp(this->parent->rotar.c_str(),"") != 0&& x==1)
		fprintf(fp,this->parent->rotar.c_str());	
	if(strcmp(this->parent->posi.c_str(),"") != 0 && x==1)
		fprintf(fp,this->parent->posi.c_str());
	//(COMMAND "_.CONE" centroc radioc "_Top" radiosup  alturac)
	
	fclose(fp);    /* Cerrar el archivo antes de terminar el programa */
	}
	//return 1;
	
}

/*void Hero::GTruncatedCone::Export()
{
for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->Export();
	if(!hidden)
		this->doExport();
}*/

char *Hero::GTruncatedCone::getName()
{
	return name;
}

void Hero::GTruncatedCone::setName(char *data)
{
	name=data;
}
void Hero::GTruncatedCone::Cvolumen()
{
	volumen=(M_PI*height*(pow(radius,2) + pow(topRadius,2) + (radius * topRadius)))/3;//AEC
}

#ifdef USE_GAPI_GRAPHICS
void Hero::GTruncatedCone::doDraw(void){
	if(!visible)
		return;
	GLUquadricObj *quadObj=gluNewQuadric();
	gluQuadricDrawStyle(quadObj, ((displayType==WIRED)?GLU_LINE:GLU_FILL));
	glPushMatrix();
	Vector desp=normals[BOTTOM]*(height*0.5);

	glTranslated(desp[0], desp[1], desp[2]);
	glMultMatrixd(T.getData());
	gluQuadricOrientation(quadObj, GLU_OUTSIDE);
	gluCylinder(quadObj, radius, topRadius, height, slices, stacks);

	gluQuadricOrientation(quadObj, GLU_INSIDE);
	gluDisk(quadObj, 0, radius, slices, stacks);

	gluQuadricOrientation(quadObj, GLU_OUTSIDE);

	glPopMatrix();
	glPushMatrix();
	glTranslated(-desp[0], -desp[1], -desp[2]);
	glMultMatrixd(T.getData());
	gluDisk(quadObj, 0, topRadius, slices, stacks);
	glPopMatrix();
	gluDeleteQuadric(quadObj);
}
#else
void Hero::GTruncatedCone::doDraw(void){
	std::cerr<<"Object: GTruncatedCone"<<std::endl;
}
#endif

