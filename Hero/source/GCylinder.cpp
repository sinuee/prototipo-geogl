/*
 * GCylinder.cpp
 *
 *  Created on: Dec 10, 2008
 *      Author: khayyam
 */
#include "GCylinder.h"
#include "GMacros.h"
#include <iostream>
Hero::GCylinder::GCylinder():GBox(){
	slices=20;
	stacks=5;
	radius=MIN(width, length)*0.5;
	//strcpy(rotar.c_str(),"");
	//strcpy(posi.c_str(),"");
}

Hero::GCylinder::GCylinder(GBox &parent):GBox(parent){
	slices=20;
	stacks=5;
	radius=MIN(width, length)*0.5;
}

Hero::GCylinder::~GCylinder(){

}

double Hero::GCylinder::getRadius(void){
	return radius;
}

double Hero::GCylinder::getHeight(void){
	return height;
}

void Hero::GCylinder::setRadius(double r){
	radius=r;
	width=length=2*r;
	build();

}

void Hero::GCylinder::setWidth(double width){
	this->width=this->length=width;
	radius=width*0.5;
	build();
}

void Hero::GCylinder::setLength(double length){
	this->width=this->length=length;
	radius=length*0.5;
	build();
}


void Hero::GCylinder::doExport(void)
{
	char *str=new char[15];	
	char centro[100];
	char valor[100],suba[100],Revol[100],subb[100],rot[100];
	int x=0;
	std::string sAux;
	//Centro ? **********
	double _points[1][4];	
	points[0].copyTo(_points[0]);
	sAux = "";
	for (int k=0;k<3;k++)
	sAux = sAux +  convertToString<int >((int)(points[k][0]/ points[k][3])) + " ";
	strcpy(centro,sAux.c_str());

	
	//*_points[0] = points[0];
	//***************
	//int j,i;
	/*strcpy(arch,"");
	strcat(arch,"d:"\\\\\");
	strcat(arch,name*/
	double flt;			
	//Vector desp=normals[BOTTOM]*(height*0.5);
	//se asigna el nombre del objeto
	strcpy(suba,"");
	strcat(suba,"(SETQ ");
	strcat(suba,name);
	strcat(suba," (ENTLAST))");
	strcpy(subb,"");
	strcat(subb,"(SETQ c1 (ENTLAST))");
	/*strcpy(centro, "");	
	flt=desp[0];	
	sprintf(str, "%.6g",flt);
	strcat(centro,str);
	//strcat(centro," ");
	flt=desp[1];
	sprintf(str, " %.6g",flt);
	strcat(centro,str);
	//strcat(centro," ");
	flt=desp[2];
	sprintf(str, " %.6g",flt);
	strcat(centro,str);*/
			
	//se genera el cilindro tomando su radio y su altura
	strcpy(valor,"");
	strcat(valor,"(COMMAND \"_.RECTANG\" '(");	
	strcat(valor,centro);
	strcat(valor, ")");
	sprintf(str, " '(%.6g",this->radius);
	strcat(valor,str);	
	//strcat(valor, " \"_TOP\"");
	//strcat(valor,str);	
	sprintf(str, " %.6g)",this->height);
	strcat(valor,str);	
	strcat(valor,")");
	strcpy(Revol,"");
	strcat(Revol,"(COMMAND \"_.REVOLVE\" c1 \"\" \"x\" 360)");	
	strcpy(rot,"");
	strcat(rot,"(COMMAND \"_.ROTATE3D\" ");
	strcat(rot,name);
	strcat(rot," \"\" '(0 0 0)  '(0 1) 90)");
	if(this->radius!=0)
		{
	std::string pcNombreArch1 = "";// "d:\\cubo.lsp";
	pcNombreArch1=sFile.c_str();
	FILE *pArchivo1;	
	pArchivo1 = fopen(pcNombreArch1.c_str(),"a");
	fprintf(pArchivo1,"%s\n%s\n%s\n%s\n%s\n",valor,subb,Revol,suba,rot);
	if(strcmp(this->rotar.c_str(),"")!=0)
		fprintf(pArchivo1,"%s\n",this->rotar.c_str());
	if(strcmp(this->posi.c_str(),"")!=0)
		fprintf(pArchivo1,"%s\n",this->posi.c_str());	
	this->parent->NumHijos++;
	if(strcmp(this->parent->unio.c_str(),"") !=0)//parte final +
	{
		fprintf(pArchivo1,"%s\n",this->parent->unio.c_str());
		x=1;
	}
	if(strcmp(this->parent->inters.c_str(),"") !=0)
	{
		fprintf(pArchivo1,"%s\n",this->parent->inters.c_str());
		x=1;
	}
	if(strcmp(this->parent->subs.c_str(),"") !=0)
	{
		fprintf(pArchivo1,"%s\n",this->parent->subs.c_str());
		x=1;
	}
	if(strcmp(this->parent->rotar.c_str(),"")!=0&&x==1)
		fprintf(pArchivo1,"%s\n",this->parent->rotar.c_str());
	if(strcmp(this->parent->posi.c_str(),"")!=0&&x==1)
		fprintf(pArchivo1,"%s\n",this->parent->posi.c_str());	

	fclose(pArchivo1);	
	}
}


/*void Hero::GCylinder::Export(void){
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->Export();
	if(!hidden)
		this->doExport();
}*/

char* Hero::GCylinder::getName()
{
	return name;
}

void Hero::GCylinder::setName(char *data)
{
	name = data;
	//delete data;
}

void Hero::GCylinder::Cvolumen()
{
	volumen = M_PI*pow(radius,2)*height;//AEC

}

#ifdef USE_GAPI_GRAPHICS
void Hero::GCylinder::doDraw(){
	if(!visible)
		return;
	GLUquadricObj *quadObj=gluNewQuadric();
	gluQuadricDrawStyle(quadObj, ((displayType==WIRED)?GLU_LINE:GLU_FILL));
	glPushMatrix();
	Vector desp=normals[BOTTOM]*(height*0.5);

	//glTranslated(desp[0], desp[1], desp[2]);
	glMultMatrixd(T.getData());
	gluQuadricOrientation(quadObj, GLU_OUTSIDE);
	gluCylinder(quadObj, radius, radius, height, slices, stacks);

	gluQuadricOrientation(quadObj, GLU_INSIDE);
	gluDisk(quadObj, 0, radius, slices, stacks);

	glPopMatrix();
	glPushMatrix();
	glTranslated(-desp[0], -desp[1], -desp[2]);
	glMultMatrixd(T.getData());
	gluQuadricOrientation(quadObj, GLU_OUTSIDE);
	gluDisk(quadObj, 0, radius, slices, stacks);
	glPopMatrix();
	gluDeleteQuadric(quadObj);
}
#else
void Hero::GCylinder::doDraw(){
	std::cerr<<"Object: GCylinder"<<std::endl;
}
#endif

