///////////////////////////////////////////////////////////////////////////////////
//! \file		FuncionesComunes.cpp
//! \author		Judith Janett Valdez Medrano
//! \date		10/Abr/2009
//! \brief		Implementacion de las funciones de uso general del proyecto
//
//				Informacion privilegiada de Prolec-GE.
//				La informacion contenida en este documento es informacion
//				privilegiada de  Prolec-GE y es revelada de manera 
//				confidencial. No debera ser utilizada, reproducida o 
//				revelada a otros sin el consentimiento escrito de Prolec-GE.
//
//				Prolec-GE Privileged information.
//				The information reveled in this document is confidential and
//				Prolec-GE property. Must not be used, reproduced or revealed 
//				to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////

//Revisiones

//Fecha			Descripcion										Persona		Folio
//dd/mmm/aa		Descipcion de revision o modificacion			XXX			XXXYYY

//#include "stdafx.h"

#include <string>
#include <math.h>
#include "FuncionesComunes.h"
#include <time.h>
#include <direct.h>
#include <io.h>

using namespace std;
#pragma warning(disable:4996) //evitar warnings por templates de stl


///////////////////////////////////////////////////////////////////////////////////
//! \fn			ROUND
//! \author			Diana Carolina Osuna Arceo
//! \brief		Funcion que redondea al entero mas proximo y multiplo del para-
//					metro div es el que corresponde a la funcion redondea-entero-sup 
//					de ICAD
//! \date			20/Mar/07
///////////////////////////////////////////////////////////////////////////////////
int Hero::ROUND (double valor, int div)
{
	return (int)(( !REAL_EQUALS(fmod(valor, div),0)) ? ( (int)valor / div + 1) * div : valor);	//div;
}

///////////////////////////////////////////////////////////////////////////////////
//! \fn			ROUND
//! \author			Diana Carolina Osuna Arceo
//! \brief		Sobrecarga de la funcion que redondea al entero mas proximo 
//! \date			02/May/07
///////////////////////////////////////////////////////////////////////////////////
int Hero::ROUND (double valor)
{
	int ceilVal=(int)ceil(valor);
	double dValorDoble = valor;
	fixNum(&dValorDoble,2);
	double diff=ceilVal - dValorDoble;
	if (REAL_LESS(diff,0.5))
		return ceilVal;
	if (REAL_EQUALS(diff,0.5))
		return (int)(((ceilVal&1) == 0) ? ceilVal : ceilVal-1);
	return  ceilVal-1;
}

///////////////////////////////////////////////////////////////////////////////////
//! \fn			ROUND
//! \author			Alejandra Guzman Osteguin
//! \brief		Redondea al entero inferior 
//! \date			10-Diciembre-08
///////////////////////////////////////////////////////////////////////////////////
double Hero::REDONDEAINF(double val, double redondeo)  //tavo feb-07-06
{
	double res;
	double a;
	if(fmod(val, redondeo) > 0.000001)
	{
		a = (int)(val / redondeo);
		res = a * redondeo;
	}
	else
	{
		res=val;
	}
	return res;
}

///////////////////////////////////////////////////////////////////////////////////
//! \fn			REDONDEASUP1
//! \author			Diana Carolina Osuna Arceo
//! \brief		Funcion que redondea al superior inmediato
//! \date			17/05/07
///////////////////////////////////////////////////////////////////////////////////
double Hero::REDONDEASUP1(double val, double redondeo)  
{
	double a=floor(val/redondeo)*redondeo;
	if(val>a)
		return a+redondeo;
	return a;
}

///////////////////////////////////////////////////////////////////////////////////
//! \fn			reemplazar
//! \author     Judith Janett Valdez Medrano
//! \brief      Funcion que reemplaza un caracter por otro en un arreglo de caracteres
//! \date       17/Abr/09
//!    @param 	sDescripcion		Arreglo de caracteres
//!    @param 	iTamanio			Tamanio del arreglo
//!    @param 	cCambiar			Caracter a reemplazar
//!    @param 	cCambiarPor			Caracter de reemplazo
//!	@return     	Ningun valor
///////////////////////////////////////////////////////////////////////////////////
void Hero::reemplazar (char *sDescripcion, size_t iTamanio, char *cCambiar, char *cCambiarPor)
{
	for (size_t i=0; i<iTamanio; i++)
	{
		if (sDescripcion[i] == cCambiar[0])
			sDescripcion[i] = cCambiarPor[0];
	}
}

///////////////////////////////////////////////////////////////////////////////////
//! \fn			MergeList
//! \author     Esteban Sinuee Hernandez Sanchez
//! \brief      Mezcla 2 arreglos devolviendo el resultado en forma de arreglo
//! \date       23/Abr/09
//!    @param 	*lista1		Arreglo a mezclar
//!    @param 	*lista2		Arreglo a mezclar
//!    @param 	i longitud de lista1
//!    @param 	j Longitud de lista2
//!	@return     Arreglo final
///////////////////////////////////////////////////////////////////////////////////
void Hero::MergeList (double *Lista1, double *Lista2, int I, int J, char forma[1], double dMergeList[])
{
	//double *dMergeList = new double [I+J];
	//
	int iNumRegistros = I + J + 1;
	int ciclo=0;
	double c;

	for (int a = 0; a < I; a++)
		dMergeList[a] = Lista1[a];

	for (int b = I; b < I + J; b++)
		dMergeList[b] = Lista2[b - I];

	if (strcmp(forma,"<") == 0)
	{
		while(ciclo <= iNumRegistros)
		{
		for(int i=0; i<iNumRegistros - 2; i++)
			{
			if(dMergeList[i] > dMergeList[i+1])
				{
				c=dMergeList[i];
				dMergeList[i]=dMergeList[i+1];
				dMergeList[i+1]= c;
				}
			}
		ciclo++;
		}
	}
	else
	{
		while(ciclo <= iNumRegistros)
		{
		for(int i=0; i<iNumRegistros - 2; i++)
			{
			if(dMergeList[i] < dMergeList[i+1])
				{
				c=dMergeList[i+1];
				dMergeList[i+1]=dMergeList[i];
				dMergeList[i]= c;
				}
			}
		ciclo++;
		}
	}
	//return dMergeList;
}
///////////////////////////////////////////////////////////////////////////////////
//! \fn			MaxMin
//! \author     Esteban Sinuee Hernandez Sanchez
//! \brief      Crea un arreglo llenandolo con valores incrementales desde el minimo hasta el maximo dado
//! \date       23/Abr/09
//!    @param 	iMin		Inico 
//!    @param 	iMax		Limite
//!    @param 	iIncremento incremento
//!	@return     Arreglo final
///////////////////////////////////////////////////////////////////////////////////
//int* Hero::MaxMin (int iMin, int iMax, int iIncremento)
//{
//	int iCont = 1 + ((iMax - iMin) / iIncremento);
//	int *Arregloi = new int [iCont];		
//	for (iCont = 0; iCont <= ((iMax - iMin) / iIncremento); iCont++)
//	{
//		Arregloi[iCont] = iMin + (iCont * iIncremento);
//	}
//	return Arregloi;
//}
void Hero::MaxMin (int iMin, int iMax, int iIncremento,std::vector <int> Arregloi)
{
	int iCont = 1 + ((iMax - iMin) / iIncremento);
//	std::vector <int> Arregloi;
	//Arregloi.resize(1);
	if (iMin > iMax)
	{
		Arregloi.push_back(-1);
//		return Arregloi;
	}
	for (iCont = 0; iCont <= ((iMax - iMin) / iIncremento); iCont++)
	{
		Arregloi.push_back(iMin + (iCont * iIncremento));
	}
//	return Arregloi;
}
//void Hero::MaxMin (int iMin, int iMax, int iIncremento, int *Arregloi)
//{
//	int iCont = 1 + ((iMax - iMin) / iIncremento);
//	//int Arregloi[100];// = new int [iCont];		
//	for (iCont = 0; iCont <= ((iMax - iMin) / iIncremento); iCont++)
//	{
//		Arregloi[iCont] = iMin + (iCont * iIncremento);
//	}
//	//return Arregloi;
//}

///////////////////////////////////////////////////////////////////////////////////
//! \fn			fixNum
//! \author     Esteban Sinuee Hernandez Sanchez
//! \brief      Redondea con una cantidad dada de decimales
//! \date       23/Abr/09
//!    @param 	pdValor el valor a redondear
//!	   @param   Precision Cantidad de decimales deseadas
////////////////////////////////////////////////////////////////////////////////////
void Hero::fixNum (double *pdValor, int Precision)
{
	double Fac = pow(10.0,Precision);
	*pdValor = (int) (*pdValor * Fac);
	*pdValor = *pdValor / Fac;
}

///////////////////////////////////////////////////////////////////////////////////
//! \fn			member
//! \author			Diana Carolina Osuna Arceo
//! \brief		Funcion que busca un elemento en un arreglo y devuelve 1 si lo 
//					encuentra, 0 caso contrario
//! \date			28/03/07
///////////////////////////////////////////////////////////////////////////////////
bool Hero::member (int iElemento, int iNumElementos, int* piValores)
{
	int i=0;
	bool bEncontrado = false;
	while (i<iNumElementos && !bEncontrado){
		if (iElemento == piValores[i])
			bEncontrado = true;
		else
			bEncontrado = false;
		i++;
	}
	return bEncontrado;
}

///////////////////////////////////////////////////////////////////////////////////
//! \fn			RedondeaDecimRgcg
//! \author			Roc�o Yazm�n Hern�ndez Ibarra
//! \brief		Funcion que Redondea a Decimas 
//! \date			08/Jun/09
///////////////////////////////////////////////////////////////////////////////////
double Hero::RedondeaDecimRgcg (double dValor, double dDecim)
{
	double dResult = 0.0;
	dResult = (ROUND(dValor * pow(10,dDecim)) )/ (pow (10,dDecim));
	return dResult;
}

 ///////////////////////////////////////////////////////////////////////////////////
//! \fn		nearTo
//! \author	Esteban Sinuee Hern�ndez S�nchez
//! \brief	funcion que dice si un valor esta cerca de otro en base a una tolerancia
//! \date	22/07/2009
///////////////////////////////////////////////////////////////////////////////////
bool Hero::nearTo (double Val1, double Val2, double Tolerance)
{
	if ((Val1 - Tolerance < Val2) && (Val2 <= Val1 + Tolerance))
		return true;
	else 
		return false;
}
///////////////////////////////////////////////////////////////////////////////////
//! \fn		Instr
//! \author	Emilio Santos
//! \brief	Regresa 1 si SearchTerm es una subcadena de SearchString y 0 de otro modo
//! \date	15/Jul/2009
///////////////////////////////////////////////////////////////////////////////////
int Hero::Instr(const char *SearchString, const char *SearchTerm)
{
/*int ReturnValue = 0;
for (int i = 0 ; i <= strlen(SearchString)-strlen(SearchTerm); i++)
{
if (SearchTerm == mid(SearchString, i, strlen(SearchTerm)))
{
ReturnValue ++;
}
}
return ReturnValue;*/
	std::string A=SearchString;
	std::string B=SearchTerm;
	return A.find(B)!=std::string::npos?1:0;
} 
////////////////////////////////////////////////////////////////////////////////////////////////////////
//! \author Omar Ocegueda
//! \brief  "Corta" la cadena de entrada \ref cadena en fragmentos definidos por los \ref separadores
//! dados. Si \ref descartarVacias es true, entonces  el vector de salida no contendra cadenas vacias. Por default
//! se permiten las cadenas vacias.
//! 
//! \date Aug-31-2009
//!
////////////////////////////////////////////////////////////////////////////////////////////////////////
std::vector<std::string> Hero::cortarCadena(std::string cadena, const std::string separadores, bool descartarVacias){
   std::vector<std::string> sol;
	size_t p=cadena.find_first_of(separadores);
	while(p!=std::string::npos){
		std::string sub=cadena.substr(0,p);
		if(!(sub.empty() && descartarVacias)){
			sol.push_back(sub);
		}
		cadena=cadena.substr(p+1,cadena.size()-p);
		p=cadena.find_first_of(separadores);
	}
	if(!(cadena.empty() && descartarVacias)){
		sol.push_back(cadena);
	}
	return sol;
}
///////////////////////////////////////////////////////////////////////////////////
//! \fn		itos
//! \author		Jos� Fernando Reyes Salda�a
//! \brief	Convierte un entero en string
//! \date		01/Jun/06
///////////////////////////////////////////////////////////////////////////////////
std::string Hero::itos(int iEntrada){
	char cSalida[20];
	std::string sSalida("*********.******");
	if(abs(iEntrada)<1e+9)  {
		sprintf(cSalida, "%d", iEntrada);
		sSalida = cSalida;
	}
	return sSalida;
}


///////////////////////////////////////////////////////////////////////////////////
//! \fn		ltos
//! \author		Jos� Fernando Reyes Salda�a
//! \brief	Convierte un entero long en string
//! \date		01/Jun/06
///////////////////////////////////////////////////////////////////////////////////
std::string Hero::ltos(long lEntrada){
	char cSalida[20];
	std::string sSalida;
	sprintf(cSalida, "%ld", lEntrada);
	sSalida = cSalida;
	return sSalida;
}


///////////////////////////////////////////////////////////////////////////////////
//! \fn		dtos
//! \author		Jos� Fernando Reyes Salda�a
//! \brief	Convierte un double en string
//! \date		01/Jun/06
///////////////////////////////////////////////////////////////////////////////////
std::string Hero::dtos(double dEntrada, int iDecimales){
	char cSalida[18];
	char cFormato[18];
	std::string sSalida("*********.******");
	sprintf(cFormato, " 0.%dlf", iDecimales);
	cFormato[0] = '%';

	if(fabs(dEntrada)<1e+9)  {
		sprintf(cSalida, cFormato, dEntrada);
		sSalida = cSalida;
	}
	return sSalida;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
//! \author Esteban Sinuee Hernandez Sanchez
//! \brief  Convierte un string a minusculas
//! 
//! \date Aug-31-2009
//!
////////////////////////////////////////////////////////////////////////////////////////////////////////
std::string Hero::LowerCase (char *str)
 {
	 int i = 0;
	 while (str[i])
	 {
		 str[i] = tolower(str[i]);
		 i++;
	 }
	 std::string Regreso = std::string(str);
	 return Regreso;
 }
std::string Hero::funcRTrim(std::string sVar)
{
	if(sVar.empty())
		return sVar;
	if ((sVar[sVar.length() - 1] == ' ') || (sVar[sVar.length() - 1] == 13) || (sVar[sVar.length() - 1] == 10) || (sVar[sVar.length() - 1] == 9)){
		sVar = sVar.substr(0, sVar.length() - 1);
		sVar = funcRTrim(sVar);
	}

	return sVar;
}

std::string Hero::extraerNombreArchivo(std::string fullPath){
	std::string nombre;
	size_t p=fullPath.find_last_of("\\");
	if(p!=std::string::npos){
		nombre=fullPath.substr(p+1, fullPath.size()-p-1);
	}else{
		nombre=fullPath;
	}
	return nombre;
}

inline string Hero::sinCaracteresDerecha(const string &s, const string &indeseables){ 
	string duplicado(s); 
	string::size_type i(duplicado.find_last_not_of(indeseables));
	if (i == string::npos)
		return "";
	else
		return duplicado.erase(i+1);
}

inline string Hero::sinCaracteresIzquierda(const string &s, const string &indeseables){ 
	string duplicado(s); 
	return duplicado.erase(0, s.find_first_not_of(indeseables)) ; 
}

inline string Hero::sinCaracteresExtremos(const string &s, const string &indeseables){ 
	string duplicado(s); 
	return sinCaracteresIzquierda(sinCaracteresDerecha(duplicado, indeseables), indeseables); 
}


//ddmmyyyyhhmiss
long Hero::FechaYHoraToLong (string sFecha){
	int d=0,m=0,y=0,h=0,mi=0,s=0;
	long lValor;

	if(sFecha!=""){
		time_t tiHora,tiHoraFinal;
		tm * tFecha;
		sscanf_s(sFecha.c_str(), "%d/%d/%d-%d:%d:%d",&d,&m,&y,&h,&mi,&s);
		time( &tiHora );
		tFecha = localtime(&tiHora);
		tFecha->tm_mday = d;
		tFecha->tm_mon = m-1;
		tFecha->tm_year = y-1900;
		tFecha->tm_hour = h;
		tFecha->tm_min = mi;
		tFecha->tm_sec = s;
		tiHoraFinal = mktime( tFecha );
		lValor = (long) tiHoraFinal;
	}
	else{
		lValor = 0;
	}
	
	return(lValor);
}

//ddmmyyyyhhmiss para query
string Hero::FechaYHoraToString(long lFecha){
	if(lFecha<=0) return "";
	string sFecha;
	time_t tiFecha = lFecha;
	tm * tFecha;
	tFecha = localtime(&tiFecha);
	char cFecha[100];
	//sprintf(cFecha, "to_date('%d-%d-%d %d:%d:%d','dd-mm-yyyy hh24:mi:ss')", 
	sprintf_s(cFecha, "to_date('%d/%d/%d-%d:%d:%d','dd-mm-yyyy-hh24:mi:ss')", 
		tFecha->tm_mday, (tFecha->tm_mon+1), (tFecha->tm_year+1900), 
		tFecha->tm_hour, tFecha->tm_min, tFecha->tm_sec);
	sFecha = cFecha;
	return sFecha;
}

//ddmmyyyyhhmiss para desplegar
string Hero::formatearFechaYHora(long lFecha){
	if(lFecha<=0) return "";
	string sFecha;
	time_t tiFecha = lFecha;
	tm * tFecha;
	tFecha = localtime(&tiFecha);
	char cFecha[100];
	//sprintf(cFecha, "('%d-%d-%d %d:%d:%d')", 
	//sprintf(cFecha, "'%d/%d/%d-%d:%d:%d'", 
	sprintf_s(cFecha, "%d/%d/%d-%d:%d:%d", 
		tFecha->tm_mday, (tFecha->tm_mon+1), (tFecha->tm_year+1900), 
		tFecha->tm_hour, tFecha->tm_min, tFecha->tm_sec);
	sFecha = cFecha;
	return sFecha;
}

string Hero::formatearFecha(long lFecha){
	if(lFecha<=0) return "";
	string sFecha;
	time_t tiFecha = lFecha;
	tm * tFecha;
	tFecha = localtime(&tiFecha);
	char cFecha[40];
	sprintf_s(cFecha, "%d/%d/%d", tFecha->tm_mday, (tFecha->tm_mon+1), (tFecha->tm_year+1900) );
	sFecha = cFecha;
	return(sFecha);
}

string Hero::formatearHora(long lFecha){
	if(lFecha<=0) return "";
	string sFecha;
	time_t tiFecha = lFecha;
	tm * tFecha;
	tFecha = localtime(&tiFecha);
	char cFecha[40];
	sprintf_s(cFecha, "%d:%d:%d", tFecha->tm_hour, tFecha->tm_min, tFecha->tm_sec);
	sFecha = cFecha;
	return(sFecha);
}

void Hero::inicializarHoraInicio(time_t &tFecha){
	tm * tmFecha;
	tmFecha = localtime(&tFecha);
	tmFecha->tm_hour=tmFecha->tm_min=0;
	tmFecha->tm_sec=1;
	tFecha = mktime(tmFecha);
}

void Hero::inicializarHoraFin(time_t &tFecha){
	tm * tmFecha;
	tmFecha = localtime(&tFecha);
	tmFecha->tm_hour=23; 
	tmFecha->tm_min=tmFecha->tm_sec=59;
	tFecha = mktime(tmFecha);
}

void Hero::construirDirectorioAplicacion(string& CFileName){

	string CPath;
	int iCont;
	char c_temp[512];


	CPath = CFileName;
	iCont = GetModuleFileName(NULL,c_temp,255);
	
	while(c_temp[--iCont]!='\\' && iCont);
	c_temp[iCont] = '\0';

	CFileName = c_temp;
	CFileName += CPath;
}


vector<string> Hero::obtenerListaArchivos(string patron,__time64_t from, __time64_t to){	
	vector<string> lista;
	struct _finddata_t datosArchivo;
	long hFile;
	string path;
	size_t p=patron.find_last_of("\\");
	if(p!=string::npos){
		path=patron.substr(0,p+1);
	}
	if ( (hFile = _findfirst(patron.c_str(), &datosArchivo)) == -1L ){
		return lista;
	}else{
		do{
			if(	((from<0)||(from<=datosArchivo.time_create))&&
				((to<0)||(datosArchivo.time_create<=to))){
					lista.push_back(path+datosArchivo.name);
			}
			
		} while ( _findnext(hFile, &datosArchivo) == 0 );
		_findclose(hFile);
	}
	return lista;
}

string Hero::cambiarExtension(string s, string nuevaExtension){
	size_t p=s.find_last_of(".");
	string sol=(p==string::npos)?(s+".")+nuevaExtension:s.substr(0,p)+nuevaExtension;
	return sol;
}
bool IsNumber(const char* str)
{
	if (!str || !*str) return false;

	const char* p = &str[0];
	while (p && *p && isspace(*p)) p++;

	const char* start = p;
	bool dot = false;

	while (p && *p) {
		if (isdigit(*p)) ;
		else if (*p == '-') {
			if (p != start) return false;
		}
		else if (*p == '.') {
			if (dot) return false;
			dot = true;
		}
		else if (isspace(*p)) {
			p++;
			while (p && *p) {
				if (!isspace(*p++)) return false;
			}
			return true;
		}
		else
			return false;
		p++;
	}
	return true;
}