/*
 * GHollowTruncatedCone.cpp
 *
 *  Created on: Dec 11, 2008
 *      Author: khayyam
 */
#include "GHollowTruncatedCone.h"
#include "GMacros.h"
#include <iostream>
Hero::GHollowTruncatedCone::GHollowTruncatedCone():GTruncatedCone(){
	innerRadius=radius*0.5;
	topInnerRadius=innerRadius*0.5;
	/*strcpy(rotar,"");
	strcpy(posi,"");*/
}

Hero::GHollowTruncatedCone::GHollowTruncatedCone(GBox &parent):GTruncatedCone(parent){
	innerRadius=radius*0.5;
	topInnerRadius=innerRadius*0.5;
}

Hero::GHollowTruncatedCone::~GHollowTruncatedCone(){

}


void Hero::GHollowTruncatedCone::setInnerRadius(double innerRadius){
	this->innerRadius=innerRadius;
}

void Hero::GHollowTruncatedCone::setTopInnerRadius(double topInnerRadius){
	this->topInnerRadius=topInnerRadius;
}

void Hero::GHollowTruncatedCone::setRadius(double r){
	this->radius=r;
	width=length=2*MAX(this->radius, this->topRadius);
}


void Hero::GHollowTruncatedCone::setTopRadius(double r){
	this->topRadius=r;
	width=length=2*MAX(this->radius, this->topRadius);
}

void Hero::GHollowTruncatedCone::doExport(void){
	
	char nombreArchivo[100]= "";//"D:\\cubo.lsp";
	char buffer[355];
	char *str=new char[8];
	int x=0;
	strcat(nombreArchivo,sFile.c_str());
	std::string sAux;
	char centro[25];
	//Centro ? **********
	double _points[1][4];
	points[0].copyTo(_points[0]);
	sAux = "";
	for (int k=0;k<3;k++)
	sAux = sAux + convertToString<int >((int)(points[k][0]/ points[k][3])) + " ";
	strcpy(centro,sAux.c_str());
	if(this->radius!=0&&this->innerRadius!=0&&this->topInnerRadius!=0&&this->topRadius!=0)
		{				

	FILE *fp;

	fp = fopen(nombreArchivo, "a");   /* Abrir archivo para escritura */

	strcpy(buffer,"");
	strcat(buffer,"(COMMAND \"_.CONE\" ");	
	strcat(buffer,"'(");
	strcat(buffer,centro);
	strcat(buffer,") ");
	sprintf(str, "%.6f", this->getRadius());   
	strcat(buffer,str);
	strcat(buffer," \"T\" ");
	sprintf(str, "%.6f", this->getTopRadius()); 
	strcat(buffer,str);
	strcat(buffer," ");
	sprintf(str, "%.6f", this->getHeight());  //alto
	strcat(buffer,str);
	strcat(buffer,")\n");
	strcat(buffer,"(SETQ ");
	strcat(buffer,this->getName());
	strcat(buffer," (ENTLAST))\n");
	fprintf(fp, buffer);	
					
	//(COMMAND "_.CONE" centroc radioc "_Top" radiosup  alturac)
					
	strcpy(buffer,"");
	strcat(buffer,"(COMMAND \"_.CONE\" ");	
	strcat(buffer,"'(");
	strcat(buffer,centro);
	strcat(buffer,") ");
	sprintf(str, "%.6f", this->innerRadius);   
	strcat(buffer,str);
	strcat(buffer," \"T\" ");
	sprintf(str, "%.6f", this->topInnerRadius); 
	strcat(buffer,str);
	strcat(buffer," ");
	sprintf(str, "%.6f", this->getHeight());  //alto
	strcat(buffer,str);
	strcat(buffer,")\n");
	fprintf(fp, buffer);

	//(COMMAND "_.CONE" centroc radioc2 "_Top" radiosup2  alturac)

	strcpy(buffer,"");
	//strcat(buffer,"(SETQ c1 (ENTNEXT))");
	strcat(buffer,"(SETQ ");
	strcat(buffer,name);
	strcat(buffer,"c2 (ENTLAST))\n");
	strcat(buffer,"(COMMAND \"_.SUBTRACT\" ");
	strcat(buffer,this->getName());
	strcat(buffer," \"\" ");
	strcat(buffer,name);
	strcat(buffer,"c2 \"\")\n");
	fprintf(fp, buffer);
	this->parent->NumHijos++;
	if(strcmp(this->rotar.c_str(),"")!=0)
		fprintf(fp,this->rotar.c_str());
	if(strcmp(this->posi.c_str(),"")!=0)
		fprintf(fp,this->posi.c_str());	
	if(strcmp(this->parent->unio.c_str(),"") !=0)//parte final +
	{
		fprintf(fp,this->parent->unio.c_str());
		x=1;
	}
	if(strcmp(this->parent->inters.c_str(),"") !=0)
	{
		fprintf(fp,this->parent->inters.c_str());
		x=1;
	}
	if(strcmp(this->parent->subs.c_str(),"") !=0)
	{
		fprintf(fp,this->parent->subs.c_str());
		x=1;
	}
	if(strcmp(this->parent->rotar.c_str(),"")!=0&&x==1)
		fprintf(fp,this->parent->rotar.c_str());
	if(strcmp(this->parent->posi.c_str(),"")!=0&&x==1)
		fprintf(fp,this->parent->posi.c_str());	
	//fprintf(fp,this->getPosition());	fprintf(fp,this->getOrientation());
	fclose(fp);    /* Cerrar el archivo antes de terminar el programa */
	}
	//return 1;
}

/*void Hero::GHollowTruncatedCone::Export()
{
for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->Export();
	if(!hidden)
		this->doExport();
}*/

char* Hero::GHollowTruncatedCone::getName()
{
	return name;
}

void Hero::GHollowTruncatedCone::setName(char *data)
{
	name=data;
}

void Hero::GHollowTruncatedCone::Cvolumen()
{
	volumen=(M_PI*height*((pow(radius,2) + pow(topRadius,2) + (radius * topRadius))-(pow(innerRadius,2) + pow(topInnerRadius,2) + (innerRadius * topInnerRadius))))/3 ;//AEC
}

#ifdef USE_GAPI_GRAPHICS
void Hero::GHollowTruncatedCone::doDraw(){
	if(!visible)
		return;
	GLUquadricObj *quadObj=gluNewQuadric();
	gluQuadricDrawStyle(quadObj, ((displayType==WIRED)?GLU_LINE:GLU_FILL));
	glPushMatrix();
	Vector desp=normals[BOTTOM]*(height*0.5);

	//glTranslated(desp[0], desp[1], desp[2]);
	glMultMatrixd(T.getData());
	gluQuadricOrientation(quadObj, GLU_INSIDE);
	gluCylinder(quadObj, innerRadius, topInnerRadius, height, slices, stacks);
	gluQuadricOrientation(quadObj, GLU_OUTSIDE);
	gluCylinder(quadObj, radius, topRadius, height, slices, stacks);


	gluQuadricOrientation(quadObj, GLU_INSIDE);
	gluDisk(quadObj, innerRadius, radius, slices, stacks);

	glPopMatrix();
	glPushMatrix();
	glTranslated(-desp[0], -desp[1], -desp[2]);
	glMultMatrixd(T.getData());
	gluQuadricOrientation(quadObj, GLU_OUTSIDE);
	gluDisk(quadObj, topInnerRadius, topRadius, slices, stacks);
	glPopMatrix();
	gluDeleteQuadric(quadObj);
}
#else
void Hero::GHollowTruncatedCone::doDraw(){
	if(!visible)
		return;
	std::cerr<<"Object: GHollowTruncatedCone"<<std::endl;
}
#endif

