/*
 * GInputs.h
 *
 *  Created on: Dec 5, 2008
 *      Author: khayyam
 */
#ifndef GINPUTS_H_
#define GINPUTS_H_
#include <map>
#include <string>
#include <sstream>
namespace Hero{
	enum GInputsResult{default_GInputsResult=-1, IR_Ok, IR_NotFound, IR_Duplicated, IR_BadFormat, IR_CreateNewInstanceNotDefined, IR_ErrorReadingFile, IR_ErrorWritingFile};
	const char INPUT_PATH_SEPARATOR=':';
	class GInputs{
		protected:
			std::map<std::string, std::string> inputs;
			std::map<std::string, GInputs*> groups;

			GInputsResult getInput(const char *name, std::string &destination)const;
			GInputs* getGroup(const char *name);
			const GInputs* getGroup(const char *name)const;
		public:
			GInputs();
			virtual~GInputs();
			std::string getLiteral(const char* name, const char* defaultValue)const;
			long int getInteger(const char* name, int defaultValue)const;
			double getReal(const char* name, double defaultValue)const;
			GInputsResult addGroup(const char *inputName, GInputs *subInputs);
			GInputsResult createGroup(const char *inputName);
			virtual GInputsResult loadFromFile(const char *inputFileName)=0;
			virtual GInputsResult saveToFile(const char *outputFileName)=0;
			virtual GInputs* createNewInstance(void);
			unsigned getInputsCount(void)const;
			unsigned getGroupsCount(void)const;
			bool hasInputs(void)const;
			bool hasGroups(void)const;
			bool hasInput(const char*inputName)const;
			bool hasGroup(const char*groupName)const;

			template<typename T> GInputsResult addInput(const char *inputName, const T &value){
				std::string s=inputName;
				size_t pos=s.find(':');
				if(pos!=std::string::npos){
					std::map<std::string, GInputs*>::iterator it=groups.find(s.substr(0,pos));
					if(it==groups.end()){
						return IR_NotFound;
					}
					else{
						return it->second->addInput(s.substr(pos+1,s.size()-(pos+1)).c_str(), value);
					}
				}
				if(inputs.find(inputName)!=inputs.end())
					return IR_Duplicated;
				std::ostringstream os;
				os<<value;
				inputs[inputName]=os.str();
				return IR_Ok;
			}
	};
}

#endif /* GINPUTS_H_ */
