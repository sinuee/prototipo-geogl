/*
 * GHollowTruncatedCone.h
 *
 *  Created on: Dec 11, 2008
 *      Author: khayyam
 */

#ifndef GHOLLOWTRUNCATEDCONE_H_
#define GHOLLOWTRUNCATEDCONE_H_
#include "GTruncatedCone.h"
namespace Hero{
	class GHollowTruncatedCone:public GTruncatedCone{
		protected:
			double innerRadius, topInnerRadius;
			//char *name;
		public:
			GHollowTruncatedCone();
			GHollowTruncatedCone(GBox &parent);
			//void Export();
			virtual void doExport(void);
			char *getName();
			~GHollowTruncatedCone();
			virtual void doDraw();
			virtual void setInnerRadius(double innerRadius);
			virtual void setTopInnerRadius(double topInnerRadius);
			virtual void setRadius(double innerRadius);
			virtual void setTopRadius(double topInnerRadius);
			virtual void setName(char *data);
			virtual void Cvolumen();//JIDC 3/19/2010
	};
}
#endif /* GHOLLOWTRUNCATEDCONE_H_ */
