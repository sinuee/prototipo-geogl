#undef DECL_ENUM_DEFAULT_ELEMENT
#undef DECL_ENUM_ELEMENT
#undef BEGIN_ENUM
#undef END_ENUM
#include <string>

#ifndef GENERATE_ENUM_STRINGS
	#define DECL_ENUM_DEFAULT_ELEMENT( element) element=-1
    #define DECL_ENUM_ELEMENT( element ) element
    #define BEGIN_ENUM( ENUM_NAME ) typedef enum tag##ENUM_NAME
    #define END_ENUM( ENUM_NAME ) ENUM_NAME; \
			__declspec(dllexport)char* GetString##ENUM_NAME(enum tag##ENUM_NAME index);
	
#else
	#define DECL_ENUM_DEFAULT_ELEMENT( element) #element
    #define DECL_ENUM_ELEMENT( element ) #element
    #define BEGIN_ENUM( ENUM_NAME ) char* gs_##ENUM_NAME [] =
    #define END_ENUM( ENUM_NAME ) ; char* GetString##ENUM_NAME(enum \
            tag##ENUM_NAME index){ return gs_##ENUM_NAME [index+1]; }\
			int GetValue##ENUM_NAME(std::string tag##ENUM_NAME)\
			{for(int i=-1;i<ENUM_NAME##_Count;++i)if(tag##ENUM_NAME==GetString##ENUM_NAME((ENUM_NAME)i)) return i;return ENUM_NAME##_Count;}
#endif
