/*
 * GSphere.h
 *
 *  Created on: Dec 11, 2008
 *      Author: khayyam
 */

#ifndef GSPHERE_H_
#define GSPHERE_H_
#include "GBox.h"
namespace Hero{
	class GSphere:public GBox{
		protected:
			double radius;
			int slices, stacks;
			//char *name;
		public:
			GSphere();
			//void Export(void);
			virtual void doExport(void);
			GSphere(GBox &parent);
			char* getName();				
			virtual ~GSphere();
			virtual void setRadius(double radius);
			virtual void doDraw(void);
			virtual void setName(char *data);
			virtual void Cvolumen();//JIDC 3/19/2010
	};
}
#endif /* GSPHERE_H_ */
