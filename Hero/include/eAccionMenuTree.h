//#if ( !defined(AccionMenuTree_H) || defined(GENERATE_ENUM_STRINGS) )
//	#if (!defined(GENERATE_ENUM_STRINGS))
//		#define AccionMenuTree_H
//#endif
//
//#include "EnumToString.h"
//BEGIN_ENUM(AccionMenuTree)
//{
//	DECL_ENUM_DEFAULT_ELEMENT(DEFAULT_AccionMenuTree),
//	DECL_ENUM_ELEMENT(SHOW),
//    DECL_ENUM_ELEMENT(HIDE),
//    DECL_ENUM_ELEMENT(SOLID),
//    DECL_ENUM_ELEMENT(WIRE),
//	DECL_ENUM_ELEMENT(ACCIONMUNETREE_COUNT)
//}
//END_ENUM(AccionMenuTree)
//#endif

#include "EnumString.h"
enum AccionMenuTree{
	SHOW = 0,
	HIDE = 1,
	SOLIDO = 2,
	WIRE = 3
};

  Begin_Enum_String( AccionMenuTree )
    {
        Enum_String( SHOW );
        Enum_String( HIDE );
		Enum_String( SOLIDO );
		Enum_String( WIRE );
  }
    End_Enum_String;