// HeroLib.h

#pragma once

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>

//*****  GAPI **********
#include <string>
#include <direct.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <sstream>
#include <map>
#include "zpr.h"
#include "GMacros.h"
#include "gapila.h"
#include "Matrix.h"
#include "Vector.h"
#include "gapiDrawingTest.h"
#include "GBox.h"
#include "GCylinder.h"
#include "GHollowCylinder.h"
#include "GCone.h"
#include "GTruncatedCone.h"
#include "GHollowTruncatedCone.h"
#include "GSphere.h"
#include "GHollowBox.h"
#include "GRoundedHollowBox.h"
#include "GPartialHollowCylinder.h"
#include "GAsymmetricHollowBox.h"
#include "GAsymmetricRoundedHollowBox.h"
#include "GPlainInputs.h"
#include "GXMLInputs.h"
#include "GPolyLine.h"
#include "HerrajeInferior.h"
#include "HerrajeSuperior.h"
#include "Canal.h"
#include "ParteViva.h"
#include "Tanque.h"
#include "SoporteDeAngulo.h"
#include "NucleoMitter.h"
#include "PeinesTX.h"
#include "Perno12.h"
#include "Boquilla.h"
#include "Boq.h"
/*************************************/
/*************** NSTRansformador ***************/
/************************************************/

using namespace System::Windows::Forms;
using namespace std;
using namespace NSTransformador;
using namespace Hero;


namespace Hero {
	

	public ref class COpenGL: public System::Windows::Forms::NativeWindow{
	public:

		#define GLERROR                                                    \
		{                                                              \
			GLenum code = glGetError();                                \
			while (code!=GL_NO_ERROR)                                  \
			{                                                          \
				printf("%s\n",(char *) gluErrorString(code));          \
					code = glGetError();                               \
			}                                                          \
		  }
		GBox *root;	

		/*std::map<GBox*, std::string> GAPI_NAMES;
		map<int, GBox*> menuKeyMap;*/
		//NSTransformador::NucleoMitter *Hijo;
		NSTransformador::ParteViva *Hijo;//cambio
		//NSTransformador::Boquilla *Hijo;
		//Tanque *Hijo;
		//GRoundedHollowBox *Hijo;//cambio
		//NSTransformador::HerrajeInferior *Hijo;
		//NSTransformador::HerrajeSuperior *Hijo2;*/
		//NSTransformador::Tanque *Hijo;
		//GRoundedHollowBox *Hijo;			
		//GPartialHollowCylinder *Hijo;
		//SoporteDeAngulo *Hijo;
		//Canal *Hijo;

		double dZoom;
		double dAngleRotaZ;
		double dAngleRotaX;
		double dAngleRotaY;
		int x;
		int y;
		int button;
		int state;
		void RotarXPlus()
		{
			dAngleRotaX += 15; 
		}
		void RotarXMinus(){
			dAngleRotaX -= 15;
		}
		void RotarYPlus()
		{
			dAngleRotaY += 15; 
		}
		void RotarYMinus(){
			dAngleRotaY -= 15;
		}
		void RotarZPlus()
		{
			dAngleRotaZ += 15; 
		}
		void RotarZMinus(){
			dAngleRotaZ -= 15;
		}
		void ZoomIn(){
			dZoom = dZoom + 0.2;
		} 
		void ZoomOut(){
			dZoom = dZoom - 0.2;
		}
		int width;
		int height;
		COpenGL(System::Windows::Forms::PictureBox ^ parentForm, GLsizei iWidth, GLsizei iHeight)
		{
			x = 0;
			y = 0;
			CreateParams^ cp = gcnew CreateParams;
			dZoom = 0.25;
			dAngleRotaX = 0;
			dAngleRotaY = 0;
			dAngleRotaZ = 0;
			button = 0;
			state=1;
			// Set the position on the form
			cp->X = 0;
			cp->Y = 0;
			cp->Height = iHeight;
			cp->Width = iWidth;
			height = iHeight;
			width = iWidth;

			// Specify the form as the parent.
			cp->Parent = parentForm->Handle;

			// Create as a child of the specified parent and make OpenGL compliant (no clipping)
			cp->Style = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_DISABLED ;

			// Create the actual window
			this->CreateHandle(cp);

			m_hDC = GetDC((HWND)this->Handle.ToPointer());

			if(m_hDC)
			{
				MySetPixelFormat(m_hDC);
				ReSizeGLScene(iWidth, iHeight);
				InitGL();
			}

			rtri = 0.0f;
			rquad = 0.0f;
			//root = new GBox();
			//createRoot(root, GBox);
			//createChild(Hijo,NSTransformador::ParteViva,*root);//cambio
		}
/********  A�ADIDO DE GDEVELOP     ************************************************/
		void printtext(int x, int y, const std::string cadena, int Width, int Heigth)
{
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0, Width, 0, Heigth, -1.0f, 1.0f);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glPushAttrib(GL_DEPTH_TEST);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	glDisable(GL_LIGHT0);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
    glColor3d(1.0,1.0,1.0);
    glRasterPos2i(x,y);
    for(int i=0;cadena.c_str()[i];i++)
    {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, cadena.c_str()[i]);
    }
    glPopAttrib();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

/**************************************************************************************/
		void Display( void )
		{
		   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		   GAPIdrawAxes(4,.01);
		   glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GAPIBlue);
		   if(root!=NULL){
			   root->draw();
		   }
		   glLoadIdentity();
		   SwapOpenGLBuffers();
		}

		void passiveMouseMotion(int x, int y)
		{
			int viewport[4];
			glGetIntegerv(GL_VIEWPORT, viewport);
			GLdouble modelview[16];
			glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
			GLdouble projection[16];
			glGetDoublev(GL_PROJECTION_MATRIX, projection);
			GLfloat winX, winY, winZ=0;
			winX=(GLfloat)x;
			winY=(GLfloat)y;
			winZ=(GLfloat)viewport[3] - winY;
			GLdouble posX, posY, posZ;
			gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);

			Display();
		}
		/************************************************************************/
		

		/*void setVisibleFunc(int value)
		{
			menuKeyMap[value]->setVisible(!menuKeyMap[value]->getVisible());
		}

		void setSolidFunc(int value)
		{
			if(menuKeyMap[value]->getDisplayType()==WIRED)
				menuKeyMap[value]->setDisplayType(SOLID);
			else
				menuKeyMap[value]->setDisplayType(WIRED);
		}




		int createMenu(GBox &box, int &valueInit, void (*func)(int)){
			int menu = glutCreateMenu(func);
			menuKeyMap[valueInit]=&box;
			string id=GAPI_NAMES[&box];
			glutAddMenuEntry(id.c_str(), valueInit);
			++valueInit;
			for(set<GBox*>::iterator it=box.getFirstPart(); it!=box.getLastPart();++it)
			{
				if((*it)->partsCount()>0)
				{
					int a=valueInit;
					int subMenu=createMenu(*(*it), valueInit, func);
					int b=valueInit-1;
					ostringstream os;
					os<<"Nodes["<<a<<", "<<b<<"]";
					glutSetMenu(menu);
					glutAddSubMenu(os.str().c_str(), subMenu);
				}
				else
				{
					glutSetMenu(menu);
					string id=GAPI_NAMES[*it];
					glutAddMenuEntry(id.c_str(), valueInit);
					menuKeyMap[valueInit]=(*it);
					++valueInit;
				}

			}
			return menu;
		}*/
		/************************************************************************/
void init(){

    int Window = 0;

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	glScaled(dZoom,dZoom,dZoom);//Zoom
	glRotated(dAngleRotaX,1,0,0);
	glRotated(dAngleRotaY,0,1,0);
	glRotated(dAngleRotaZ,0,0,1);
	zprInit(width,height,button,state,x,y);
	//============menu=============
	//glutPassiveMotionFunc(passiveMouseMotion);
	//passiveMouseMotion(x,y);
	/*int valueInit=1;
	int visibleOptionMenu=createMenu(*root, valueInit, setVisibleFunc);
	valueInit=1;
	int drawOptionMenu=createMenu(*root, valueInit, setSolidFunc);
	glutCreateMenu(setVisibleFunc);
	glutAddSubMenu("visible/invisible",visibleOptionMenu);
	glutAddSubMenu("solid/wired",drawOptionMenu);
	glutAttachMenu(GLUT_RIGHT_BUTTON);*/
	//==============================
	Display();
}
/**********************************************************/
		System::Void Render(bool Girar, int Width, int Height, std::string texto, bool bCambiaAncho,int ix, int iy)
		{
			glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
			ReSizeGLScene(Width,Height);
			x=ix;
			y=iy;
						
			if(root == NULL)
			{
				root = new GBox();

				createRoot(root, GBox);
				root->beginDefinition();
				root->setName("Root");
				root->setDisplayType(SOLID);
				root->endDefinition();
				createChild(Hijo,ParteViva,*root);//cambio
				Hijo->beginDefinition();
				Hijo->setName("ParteViva");												
				//Nombres.push_back
				/*Hijo->setHeight(3);
				Hijo->setLength(3);*/
				Hijo->calVolumen();				
				Hijo->endDefinition();
				
				root->setHidden(true);
				Hijo->setDisplayType(SOLID);
				Hijo->inTree();
				/*GXMLInputs inputs;
				Hijo->obtenerDimensiones(inputs);
				inputs.saveToFile("D:\\ParteViva.xml")*/;
			}
			
			
			/*Hijo->setDisplayType(SOLID);*/
			/*root->setHidden(true);*/
			
			init();

			printtext(1.5,15,texto, Width,Height);
		}

		System::Void SwapOpenGLBuffers(System::Void)
		{
			SwapBuffers(m_hDC) ;
		}


	private:
		HDC m_hDC;
		HGLRC m_hglrc;
		GLfloat	rtri;				// Angle for the triangle
		GLfloat	rquad;				// Angle for the quad

	protected:
		~COpenGL(System::Void)
		{
			this->DestroyHandle();
		}

		GLint MySetPixelFormat(HDC hdc)
		{
			static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
				{
					sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
					1,											// Version Number
					PFD_DRAW_TO_WINDOW |						// Format Must Support Window
					PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
					PFD_DOUBLEBUFFER,							// Must Support Double Buffering
					PFD_TYPE_RGBA,								// Request An RGBA Format
					16,										// Select Our Color Depth
					0, 0, 0, 0, 0, 0,							// Color Bits Ignored
					0,											// No Alpha Buffer
					0,											// Shift Bit Ignored
					0,											// No Accumulation Buffer
					0, 0, 0, 0,									// Accumulation Bits Ignored
					16,											// 16Bit Z-Buffer (Depth Buffer)  
					0,											// No Stencil Buffer
					0,											// No Auxiliary Buffer
					PFD_MAIN_PLANE,								// Main Drawing Layer
					0,											// Reserved
					0, 0, 0										// Layer Masks Ignored
				};
			
			GLint  iPixelFormat; 
		 
			// get the device context's best, available pixel format match 
			if((iPixelFormat = ChoosePixelFormat(hdc, &pfd)) == 0)
			{
				MessageBox::Show("ChoosePixelFormat Failed");
				return 0;
			}
			 
			// make that match the device context's current pixel format 
			if(SetPixelFormat(hdc, iPixelFormat, &pfd) == FALSE)
			{
				MessageBox::Show("SetPixelFormat Failed");
				return 0;
			}

			if((m_hglrc = wglCreateContext(m_hDC)) == NULL)
			{
				MessageBox::Show("wglCreateContext Failed");
				return 0;
			}

			if((wglMakeCurrent(m_hDC, m_hglrc)) == NULL)
			{
				MessageBox::Show("wglMakeCurrent Failed");
				return 0;
			}


			return 1;
		}

		bool InitGL(GLvoid)										// All setup for opengl goes here
		{
			glShadeModel(GL_SMOOTH);							// Enable smooth shading
			glClearColor(0.0f, 0.0f, 0.0f, 0.5f);				// Black background
			glClearDepth(1.0f);									// Depth buffer setup
			glEnable(GL_DEPTH_TEST);							// Enables depth testing
			glDepthFunc(GL_LEQUAL);								// The type of depth testing to do
			glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really nice perspective calculations
			return TRUE;										// Initialisation went ok
		}

		GLvoid ReSizeGLScene(GLsizei width, GLsizei height)		// Resize and initialise the gl window
		{
			if (height==0)										// Prevent A Divide By Zero By
			{
				height=1;										// Making Height Equal One
			}

			glViewport(0,0,width,height);						// Reset The Current Viewport

			glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
			glLoadIdentity();									// Reset The Projection Matrix

			// Calculate The Aspect Ratio Of The Window
			gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

			glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
			glLoadIdentity();									// Reset The Modelview Matrix
		}
	};

}
