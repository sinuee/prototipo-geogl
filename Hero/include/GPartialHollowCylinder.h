/*
 * GPartialHollowCylinder.h
 *
 *  Created on: Dec 15, 2008
 *      Author: khayyam
 */

#ifndef GPARTIALHOLLOWCYLINDER_H_
#define GPARTIALHOLLOWCYLINDER_H_
#include "GHollowCylinder.h"
namespace Hero{
	class GPartialHollowCylinder: public GHollowCylinder{
		protected:
			double startAngle;
			double sweepAngle;

		/*private:
			double grados;*/
			
		public:
			
			
			GPartialHollowCylinder();
			GPartialHollowCylinder(GBox &parent);
			//void Export();
			virtual void doExport(void);
			char *getName();
			~GPartialHollowCylinder();
			virtual void doDraw(void);

			void setStartAngle(double angle);
			void setSweepAngle(double angle);
			double getStartAngle(void);
			double getSweepAngle(void);
			virtual void setName(char *data);	
			virtual void Cvolumen();//JIDC 3/19/2010
	};
}
#endif /* GPARTIALHOLLOWCYLINDER_H_ */
