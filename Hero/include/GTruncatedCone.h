/*
 * GTruncatedCone.h
 *
 *  Created on: Dec 11, 2008
 *      Author: khayyam
 */

#ifndef GTRUNCATEDCONE_H_
#define GTRUNCATEDCONE_H_
#include "GCylinder.h"
namespace Hero{
	class GTruncatedCone:public GCylinder{
		protected:
			double topRadius;
			char *name;
		public:
			GTruncatedCone();
			GTruncatedCone(GBox &parent);
			~GTruncatedCone();
			//void Export();
			virtual void doExport(void);
			char *getName();
			virtual void doDraw(void);
			virtual void setTopRadius(double innerRadius);
			double getTopRadius(void);
			virtual void setName(char *data);
			virtual void Cvolumen();//JIDC 3/19/2010
	};
}
#endif /* GTRUNCATEDCONE_H_ */
