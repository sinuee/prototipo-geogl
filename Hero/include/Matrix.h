/*
 * Matrix.h
 *
 *  Created on: Dec 3, 2008
 *      Author: khayyam
 */

#ifndef MATRIX_H_
#define MATRIX_H_
#include "gapila.h"
#include "NTuple.h"
#include <fstream>
namespace Hero{
	template<typename T=double, int n=4, int m=4>class Matrix{
		protected:
			T M[n*m];
		public:
			static int size;
			Matrix(){
			}

			Matrix(const T (&data)[n][m]){
				int p=0;
				for(int j=0;j<m;++j)
					for(int i=0;i<n;++i,++p)
						{
							M[p]=data[i][j];
						}
			}

			~Matrix(){
			}

			double* operator[](int columnIndex){
				if(0<=columnIndex && columnIndex<m)
					return &M[columnIndex*n];
				return NULL;
			}

			double* operator[](int columnIndex)const {
				if(0<=columnIndex && columnIndex<m)
					return &M[columnIndex*n];
				return NULL;
			}



			double* getData(void){
				return M;
			}

			Matrix<T,n,m> operator+(const Matrix<T,n,m> &B)const{
				Matrix<T,n,m> A;
				for(int i=0;i<Matrix<T,n,m>::size;i++)
					A.M[i]=M[i]+B.M[i];
				return A;
			}

			Matrix<T,n,m> operator-(const Matrix<T,n,m> &B)const{
				Matrix<T,n,m> A;
				for(int i=0;i<Matrix<T,n,m>::size;i++)
					A.M[i]=M[i]-B.M[i];
				return A;
			}

			template<int mm> Matrix<T,n,mm> operator*(const Matrix<T,m,mm> &B)const{
				Matrix<T,n,mm> A;
				int i=0,j=0;
				for(int p=0;p<Matrix<T,n,mm>::size;++p,++i)
				{
					if(i==n)
					{
						i=0;
						j+=m;
					}
					T sum=0;
					for(int q=i,r=j;q<Matrix<T,n,m>::size;q+=n,++r)
					{
						sum+=M[q]*B.M[r];
					}
					A.M[p]=sum;
				}
				return A;
			}

			Matrix<T,n,m>& operator*=(const Matrix<T,m,m> &B){
				T tmp[Matrix<T,n,m>::size];
				int i=0,j=0;
				for(int p=0;p<Matrix<T,n,m>::size;++p,++i)
				{
					if(i==n)
					{
						i=0;
						j+=m;
					}
					T sum=0;
					for(int q=i,r=j;q<Matrix<T,n,m>::size;q+=n,++r)
					{
						sum+=M[q]*B.M[r];
					}
					tmp[p]=sum;
				}

				for(int p=0;p<Matrix<T,n,m>::size;++p)
					M[p]=tmp[p];
				return *this;
			}

			Matrix<T,n,m>& operator*=(const T (&B)[m][m]){
				T tmp[n*m];
				int i=0,j=0;
				for(int p=0;p<Matrix<T,n,m>::size;++p,++i)
				{
					if(i==n)
					{
						i=0;
						++j;
					}
					T sum=0;
					for(int q=i,r=0;r<m;q+=n,++r)
					{
						sum+=M[q]*B[r][j];
					}
					tmp[p]=sum;
				}
				for(int p=0;p<Matrix<T,n,m>::size;++p)
					M[p]=tmp[p];
				return *this;
			}

			Matrix<T,n,m>& operator=(const T (&B)[n][m]){
				int p=0;
				for(int j=0;j<m;++j)
					for(int i=0;i<n;++i,++p)
					{
						M[p]=B[i][j];
					}
				return *this;
			}

			Matrix<T,n,m>& operator=(const T (&B)[n*m]){
				for(int i=0;i<Matrix<T,n,m>::size;++i)
					M[i]=B[i];
				return *this;
			}


			NTuple<T,n> operator*(const NTuple<T,m> &v)const{
				NTuple<T,n> prod;
				for(int i=0;i<n;++i){
					T sum=0;
					for(int p=0, j=i;p<m;++p,j+=n)
						sum+=M[j]*v.val[p];
					prod.val[i]=sum;
				}
				return prod;
			}


			Matrix<T,n,m> operator+(const T x)const{
				Matrix<T,n,m> sum;
				for(int i=0;i<Matrix<T,n,m>::size;i++)
					sum.M[i]=M[i]+x;
				return sum;
			}

			Matrix<T,n,m> operator-(const T x){
				Matrix<T,n,m> diff;
				for(int i=0;i<Matrix<T,n,m>::size;i++)
					diff.M[i]=M[i]-x;
				return diff;
			}

			Matrix<T,n,m> operator*(const T x)const{
				Matrix<T,n,m> prod;
				for(int i=0;i<Matrix<T,n,m>::size;i++)
					prod.M[i]=M[i]*x;
				return prod;
			}

			Matrix<T,n,m> &operator+=(const T x){
				for(int i=0;i<Matrix<T,n,m>::size;i++)
					M[i]+=x;
				return *this;
			}

			Matrix<T,n,m> &operator-=(const T x){
				for(int i=0;i<Matrix<T,n,m>::size;i++)
					M[i]-=x;
				return *this;
			}

			Matrix<T,n,m> &operator*=(const T x){
				for(int i=0;i<n;i++)
					for(int j=0;j<m;j++)
						M[i][j]*=x;
				return *this;
			}

			Matrix<T,n,m> &operator/=(const T x){
				for(int i=0;i<n;i++)
					for(int j=0;j<m;j++)
						M[i][j]/=x;
				return *this;
			}

			Matrix<T,n,m> &operator=(const T x){
				for(int i=0;i<Matrix<T,n,m>::size;i++)
					M[i]=x;
				return *this;
			}

			void copyTo(T (&v)[n][m]){
				int p=0;
				for(int j=0;j<m;++j)
					for(int i=0;i<n;++i,++p)
						v[i][j]=M[p];
			}

			void zero(void){
				memset(M,0,sizeof(M));
			}

			void identity(void){
				memset(M,0,sizeof(M));
				for(int i=0;i<Matrix<T,n,m>::size;i+=(n+1))
					M[i]=1;
			}

			Matrix<T,m,n> transpose(void)const{
				Matrix<T,m,n> A;
				for(int i=0;i<m;i++)
					for(int j=0;j<n;j++)
						A.M[i*n+j]=M[j*m+i];
				return A;
			}
			template<typename TT, int NN, int MM>  friend class Matrix;
			template<typename TT, int nn, int mm> friend NTuple<TT,mm> operator*(const NTuple<TT,nn>&v, const Matrix<TT,nn,mm> &M);
			template<typename TT, int nn, int mm> friend std::ostream& operator<< (std::ostream& os, const Matrix<TT,nn,mm> &A);
	};

	template<typename T, int n, int m> NTuple<T,m> operator*(const NTuple<T,n>&v, const Matrix<T,n,m> &M){
		NTuple<T,m> prod;
		int p=0;
		for(int j=0;j<m;++j){
			T sum=0;
			for(int i=0;i<n;++i,++p)
				sum+=v.val[i]*M.M[p];
			prod.val[j]=sum;
		}
		return prod;
	}

	template<typename T, int n, int m> std::ostream& operator<< (std::ostream& os, const Matrix<T,n,m> &A){
			int p=0;
			os<<endl<<"[";
			for(int i=0;i<n;++i)
			{
				os<<"[";
				for(int j=0;j<m;++j,++p)
				{
					os<<A.M[p];
					if(j<m-1)
						os<<", ";
				}
				os<<"]";
				if(i<n-1)
					os<<","<<endl;
			}
			os<<"]";
			return os;
	}
	template<typename T, int n, int m>int Matrix<T,n,m>::size=n*m;
}


#endif /* MATRIX_H_ */
