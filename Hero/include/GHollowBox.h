/*
 * GHollowBox.h
 *
 *  Created on: Dec 15, 2008
 *      Author: khayyam
 */

#ifndef GHOLLOWBOX_H_
#define GHOLLOWBOX_H_
#include "GBox.h"
namespace Hero{
	class GHollowBox:public GBox{
		protected:
			double innerWidth;
			double innerHeight;
			double thickness;
			Point innerPoints[15];
			Vector innerNormals[6];
			//char *name;
		public:
			GHollowBox();
			GHollowBox(GBox &parent);
			virtual void doExport(void);
			//void Export(void);
			char *getName();
			virtual ~GHollowBox();
			virtual void doDraw(void);

			virtual void setInnerHeight(double innerHeight);
			virtual void setInnerWidth(double innerWidth);
			virtual void setThickness(double thickness);
			virtual void setName(char *data);
			double getInnerWidth(void);
			double getInnerHeight(void);
			double getThickness(void);
			virtual void build(void);
			virtual void Cvolumen();//JIDC 3/19/2010
	};
}
#endif /* GHOLLOWBOX_H_ */
