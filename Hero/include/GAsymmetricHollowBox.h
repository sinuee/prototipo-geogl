/*
 * GAsymmetricHollowBox.h
 *
 *  Created on: Dec 17, 2008
 *      Author: khayyam
 */

#ifndef GASYMMETRICHOLLOWBOX_H_
#define GASYMMETRICHOLLOWBOX_H_
#include "GHollowBox.h"
namespace Hero{
	class GAsymmetricHollowBox:public GHollowBox{
		protected:
			double topThickness;
			double bottomThickness;
			//thickness (which is used as left and right thickness) is inherited from GHollowBox
		public:
			GAsymmetricHollowBox();
			GAsymmetricHollowBox(GBox &parent);
			virtual ~GAsymmetricHollowBox();
	
			void setTopThickness(double topThickness);
			void setBottomThickness(double bottomThickness);
			double getTopThickness(void);
			double getBottomThickness(void);
			virtual void build(void);
			double calVolumen();//JIDC 3/19/2010
	};
}
#endif /* GASYMMETRICHOLLOWBOX_H_ */
