/*
 * GPolyLine.h
 *
 *  Created on: Jan 2, 2009
 *      Author: khayyam
 */

#ifndef GPOLYLINE_H_
#define GPOLYLINE_H_
#include "GMacros.h"
#include "GBox.h"
#include "gapila.h"
#include <vector>
namespace Hero{
	struct GPolyLineSegment{
		double points[8][4];
	};
	struct GPolyLineSegmentNormals{
		double normals[6][4];
	};


	class GPolyLine:public GBox{
		protected:
			std::vector<GPolyLineSegment> L;//8 3-D points per line-segment
			std::vector<GPolyLineSegmentNormals> normals;//6 normals per line-segment
			double lineWidth;
			double lineThickness;
			std::vector<Point> points;
			Vector nw;//defines the orientation of the poly-line
		public:
			GPolyLine(GBox &parent);
			virtual ~GPolyLine();
			virtual void addPoint(const Point &point);
			virtual void doDraw(void);

			void setNw(Vector &nw);
			void setLineWidth(double width);
			void setLineThickness(double thickness);
	};
}

#endif /* GPOLYLINE_H_ */
