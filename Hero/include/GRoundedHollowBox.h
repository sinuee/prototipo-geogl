/*
 * GRoundedHollowBox.h
 *
 *  Created on: Dec 15, 2008
 *      Author: khayyam
 */

#ifndef GROUNDEDHOLLOWBOX_H_
#define GROUNDEDHOLLOWBOX_H_
#include "GHollowBox.h"
namespace Hero{
	class GRoundedHollowBox:public GHollowBox{
		protected:
			int slices;
		public:
			GRoundedHollowBox();
			GRoundedHollowBox(GBox &parent);
			void doExport(void);
			virtual ~GRoundedHollowBox();
			virtual void doDraw(void);
			virtual void Cvolumen();//JIDC 3/19/2010
	};
}

#endif /* GROUNDEDHOLLOWBOX_H_ */
