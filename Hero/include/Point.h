///////////////////////////////////////////////////////////////////////////////
//! \class 					Point.h
//! \brief					Definicion de la clase Point
//! \author					J. Omar Ocegueda Glez.
//! \author					CTA, Prolec-GE
//! \version				1.0
//! \date					3-Diciembre-2008
//			Informacion privilegiada de Prolec-GE.
//                 	La informacion contenida en este documento es informacion
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential
//			and Prolec-GE property. Must not be used, reproduced or
//			revealed to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////

#ifndef POINT_H_
#define POINT_H_
#include "NTuple.h"
namespace Hero{
	typedef NTuple<double, 4> Point;
}
#endif /* POINT_H_ */
