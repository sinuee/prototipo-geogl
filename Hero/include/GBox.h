/*
 * GBox.h
 *
 *  Created on: Dec 4, 2008
 *      Author: khayyam
 */

#ifndef GBOX_H_
#define GBOX_H_
#include "EObjectType.h"
#include "gapila.h"
#include "Point.h"
#include "Vector.h"
#include "eAccionMenuTree.h"
#include <set>
#include "Serializable.h"
namespace Hero{
	typedef float		GLfloat;
	enum EEdge{default_EEdge=-1, TOPLEFT=0, TOPRIGHT,   BOTTOMLEFT,  BOTTOMRIGHT,
										TOPFRONT,  TOPREAR,    BOTTOMFRONT, BOTTOMREAR,
										FRONTLEFT, FRONTRIGHT, REARLEFT,    REARRIGHT};
	enum EBoxPoint{default_EBoxPoint=-1, CENTER=0, BOTTOMLEFTFRONT,  TOPLEFTFRONT,  TOPLEFTREAR,  BOTTOMLEFTREAR,
														BOTTOMRIGHTFRONT, TOPRIGHTFRONT, TOPRIGHTREAR, BOTTOMRIGHTREAR,
														LEFTFACE_CENTER,  REARFACE_CENTER, RIGHTFACE_CENTER,
														FRONTFACE_CENTER, TOPFACE_CENTER,  BOTTOMFACE_CENTER};
	enum EAxis{default_EAxis=-1, LATERAL=0, LONGITUDINAL, VERTICAL};
	enum EFaceReference{default_EFaceReference=-1, ONLEFT, INREAR, ONRIGHT, INFRONT, ABOVE, BELOW};
	enum EFace{default_EFace=-1, LEFT, REAR, RIGHT, FRONT, TOP, BOTTOM};
	enum EDisplayType{default_EDisplayType=-1, SOLID, WIRED};
	typedef struct Movim{
		Vector Origen;
		Vector Destino;
		char *pieza;
		char *referencia;
		};
	typedef struct Materialest{
			char *material;
			int id_material;
			char *UM;
			double peso_especifico;
			GLfloat GLcolor;
	};
	typedef struct Tree{     //JIDC 3/18/2010  estructura Tree
		std::string name;
		std::string parent;
		bool IsChildren;
		bool IsParent;
		int nivel;
		int indicehijo;
	};
	class GBox{
		private:
			void rotateX(double angleInRadians);
			void rotateY(double angleInRadians);
			void rotateZ(double angleInRadians);
						
		protected:
			EObjectType objectType;
			GBox* parent;
			std::set<GBox*> parts;
								
			
			Point points[15];
			Vector normals[6];
			Vector normalsExport[6];
			
	public:
			std::string rotar;
			std::string posi;
			std::string inters;
			std::string unio;
			std::string subs;
			double peso;
			double volumen;			
			std::vector<std::string> Nombres;
			std::vector<Movim> Poss;
			std::vector<Materialest> TMat;
			std::vector<Tree> Arbol; //JIDC
			Vector initv;//={0 0 0};
			std::string sFile;
			char *nameHijo;
					

			//double dimmensions[3];
	protected:
			char *name;	
			double width;
			double height;
			double length;
			bool visible;
			bool bindParts;//if true, its children updates their visible and displayType properties in cascade
			bool hidden;
			EDisplayType displayType;

			Hero::Matrix<double,4,4> T;

		public:			
			GBox();
			GBox(GBox &parent);
			void inheritFromParent(void);
			void restoreDefaults(void);
			/*void subtract(char *name1, char *name2,char *name3, char *name4,char *name5, char *name6);
			void uni(char *name1, char *name2,char *name3, char *name4,char *name5, char *name6);
			void intersect(char *name1,char *name2,char *name3, char *name4,char *name5, char *name6);*/
			void modificar(char *operacion,std::vector<std::string> &Nombres,char *namen);
			virtual void build(void);
			virtual ~GBox();
			Point& getPoint(EBoxPoint point);
			Vector& getNormal(EFace face);
			Vector& getNormalExport(EFace face);
			virtual void doDraw(void);			
			void draw(void);
			void rotate(double angleInRadians, double x, double y, double z);
			void rotate90(const double x, const double y, const double z);
			void rotate180(const double x, const double y, const double z);
			void rotate90(EFace faceNormal);
			void rotate180(EAxis axis);
			void rotate(EFace faceNormal, double angleRadians);
			void translate(double dx, double dy, double dz);
			void translate(const Vector &v);
			void translate(const Vector &v, double factor);
			void beginDefinition(void);
			void endDefinition(void);
			void addChild(GBox &child);
			double getLength(void);
			double getWidth(void);
			double getHeight(void);
			//virtual int main(int argc, char* argv[]);
			GBox& getPart(unsigned index);
			unsigned partsCount(void);
			std::set<GBox*>::iterator getFirstPart(void);
			std::set<GBox*>::iterator getLastPart(void);
			void setVisible(bool visible);
			bool getVisible(void);
			void setBindParts(bool bindParts);
			bool getBindParts(void);
			void setHidden(bool hidden);
			bool getHidden(void);

			void setDisplayType(EDisplayType displayType);
			EDisplayType getDisplayType(void);
			void setDimmensions(double length, double width, double height);
			void setLength(double length);
			virtual void setWidth(double width);
			virtual void setHeight(double height);
			virtual void defSelf(void);
			virtual void defParts(void);

	        double distanceFromCenterToFaceCenter(EFace face);
			double distanceFromCenterToFaceCenter(const Vector &v);//elige la cara a la que apunta el vector v centrado en this->center
			void position(EFace pos, double dist);
			void position(EFaceReference pos, double dist);
			void position(EFace pos, double dist, GBox &reference);
			void position(EFaceReference pos, double dist, GBox &reference);
			virtual void doExport(void);
			void Export();
			char* getName();
			virtual void setName(char *data);
			//char getPosition();			
			//char getOrientation();
			void setPosition(void);
			void setOrientation(Vector reference, float angle);			
			void SetFile(std::string Files);
			void initposition();
			//virtual void setPosition(Vector *data);
			void ReDefParts();	
			/*void calcpeso();*/
			void setpeso(double pe, char *mat, int id_mat, char *UM, GLfloat GLcolor);
			void calcpeso();
			void inTree();////void? double?
			void inTree(std::vector<Tree> &A,int N);
			void calVolumen();///JIDC 3/19/2010 calcula volumen cambio recursiva
			virtual void Cvolumen(void); //AEC calcula volumen
			void generaReportePesos(std::string NombreArch);//, int Niv);
			int cambio;
			int NumHijos;
			int NHijos;
			void ejecutaAccionMenu (AccionMenuTree Accion, std::string Parte);
			void setHiddenRecusive(bool hidden);
			void setBindPartsRecursive(bool bindParts);
			bool isReference;
	};
			//                Para Serializar
		class GBoxS: public GBox, virtual public Serializable{
		public:
			GBoxS(GBox &parent):GBox(parent),Serializable(parent.nameHijo){
			}//GBoxS
			GBoxS(GBox &parent,char* Nombre):GBox(parent),Serializable(Nombre){
			}
			void serializar(void){
				agregarAtributo(AtributoXML("height",height));
				agregarAtributo(AtributoXML("width",width));
				agregarAtributo(AtributoXML("length",length));
				agregarAtributo(AtributoXML("peso",peso));
				agregarAtributo(AtributoXML("volumen",volumen));
			}//serializar
		};//Class GBoxS
}
#endif /* GBOX_H_ */
