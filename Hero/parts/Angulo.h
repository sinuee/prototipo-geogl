/*
 * Angulo.h
 *
 *  Created on: Jan 5, 2009
 *      Author: khayyam
 */

#ifndef ANGULO_H_
#define ANGULO_H_
#include "GBox.h"
#include "GPartialHollowCylinder.h"
#define millimeter(x) ((x)*0.001)
namespace NSTransformador{
	using namespace Hero;
	class Angulo:public GBox{
		public:
			GBox *laminaLateral;
			GBox *laminaInferior;
			GPartialHollowCylinder *esquina;
			double espesor;
			double anchoInterior;
			double longitud;
			double alturaInterior;
			Angulo(GBox &parent):GBox(parent){
				createPart(laminaLateral, GBox);
				createPart(laminaInferior, GBox);
				createPart(esquina, GPartialHollowCylinder);
				longitud=feet(4);
				anchoInterior=feet(1);
				alturaInterior=feet(0.25);
				espesor=inch(0.5);
				actualizarDimensiones();
				setBindParts(true);
				setHidden(true);
			}

			void asignarEspesor(double espesor){
				this->espesor=espesor;
				actualizarDimensiones();
			}
			void asignarAnchoInterior(double anchoInterior){
				this->anchoInterior=anchoInterior;
				actualizarDimensiones();
			}
			void asignarLongitud(double longitud){
				this->longitud=longitud;
				actualizarDimensiones();
			}
			void asignarAlturaInterior(double alturaInterior){
				this->alturaInterior=alturaInterior;
				actualizarDimensiones();
			}

			void actualizarDimensiones(){
				this->setWidth(2*espesor+anchoInterior);
				this->setLength(longitud);
				this->setHeight(espesor+alturaInterior);
			}

			void asignarDimensiones(const GInputs &inputs){
				espesor=inputs.getReal("espesor",espesor);
				anchoInterior=inputs.getReal("anchoInterior",anchoInterior);
				longitud=inputs.getReal("longitud",longitud);
				alturaInterior=inputs.getReal("alturaInterior",alturaInterior);
			}

			void obtenerDimensiones(GInputs &inputs){
				inputs.addInput<double>("espesor", espesor);
				inputs.addInput<double>("anchoInterior", anchoInterior);
				inputs.addInput<double>("longitud", longitud);
				inputs.addInput<double>("alturaInterior", alturaInterior);
			}

			void defParts(void){
				actualizarDimensiones();
				laminaLateral->beginDefinition();
					laminaLateral->setHeight(alturaInterior);
					laminaLateral->setWidth(espesor);
					laminaLateral->setLength(longitud);
					laminaLateral->position(LEFT,0);
					laminaLateral->position(TOP,0);
				laminaLateral->endDefinition();

				laminaInferior->beginDefinition();
					laminaInferior->setHeight(espesor);
					laminaInferior->setWidth(anchoInterior);
					laminaInferior->setLength(longitud);
					laminaInferior->position(BOTTOM,0);
				laminaInferior->endDefinition();

				esquina->beginDefinition();
					esquina->setInnerRadius(0);
					esquina->setRadius(espesor);
					esquina->setHeight(longitud);
					esquina->rotate(RIGHT,DEGREES(90));
					esquina->position(LEFT,0);
					esquina->position(BOTTOM,0);
					esquina->setStartAngle(DEGREES(180));
					esquina->setSweepAngle(DEGREES(90));
				esquina->endDefinition();
			}
	};
}


#endif /* ANGULO_H_ */
