#ifndef Conector_H_
#define Conector_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "Perno12.h"
#include "ZapOClem.h"
#include "ConectorHJ.h"
#include "Definiciones.h"
namespace NSTransformador{
	using namespace Hero;
	class Conector: public GBox{
		public:
			//Perno12 *perno12;
			//ZapOClem *zapoclem;
			ConectorHJ *conector;


double DiametroPerno;
enum TipoArticulo tArt;
double Width;
double Height;
double Length;
double Largo;
enum TipoConector tConector;

			Conector(GBox &parent):GBox(parent){
				//createPart(perno12, Perno12);
				//createPart(zapoclem, ZapOClem);
				createPart(conector, ConectorHJ);

DiametroPerno = millimeter(19.5);

				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}
			void asignarArticulo(enum TipoArticulo tArt){
				this-> tArt = tArt;
				actualizarDimensiones();
			}
			void asignarConector(enum TipoConector tConector){
				this-> tConector = tConector;
				actualizarDimensiones();
			}
			void asignarWidth(double Width){
				this->Width=Width;
				actualizarDimensiones();
			}
			void asignarHeight(double Height){
				this->Height=Height;
				actualizarDimensiones();
			}
			void asignarLength(double Length){
				this->Length=Length;
				actualizarDimensiones();
			}
			void asignarLargo(double Largo){
				this->Largo=Largo;
				actualizarDimensiones();
			}


			void actualizarDimensiones(){
				/*this->setWidth(DiametroPerno);// perno
				this->setHeight(DiametroPerno);
				this->setLength(.04475);*/

				this->setWidth(millimeter(10));
				this->setHeight(millimeter(10));
				this->setLength(millimeter(56));


			}
			void defSelf(void){
			}

			void asignarDimensiones(const GInputs &inputs){
				this->DiametroPerno=inputs.getReal("DiametroPerno",DiametroPerno);
				actualizarDimensiones();
			}
			void obtenerDimensiones(GInputs &inputs){
				inputs.addInput<double>("DiametroPerno", DiametroPerno);
			}
			void defParts(void){
				actualizarDimensiones();
/*
if ((tArt == CH77)||(tArt == CH76))
{

				conector->beginDefinition();
					conector->asignarArticulo(tArt);
					conector->setHidden(true);
					conector->setBindParts(true);
				conector->endDefinition();

				perno12->beginDefinition();
					perno12->asignarDiametroPerno(0);
					perno12->asignarArticulo(tArt);
					perno12->setHidden(true);
					perno12->setBindParts(true);
				perno12->endDefinition();

				zapoclem->beginDefinition();
					zapoclem->asignarConector(tConector);
					zapoclem->setHidden(true);
					zapoclem->setBindParts(true);
				zapoclem->endDefinition();
}
else if ((tConector == PERNO1)||(tConector == PERNO2))
{
				conector->beginDefinition();
					conector->asignarArticulo(tArt);
					conector->setHidden(true);
					conector->setBindParts(true);
				conector->endDefinition();

				perno12->beginDefinition();
					perno12->asignarDiametroPerno(DiametroPerno);
					perno12->asignarArticulo(tArt);
					perno12->setHidden(true);
					perno12->setBindParts(true);
				perno12->endDefinition();

				zapoclem->beginDefinition();
					zapoclem->asignarConector(tConector);
					zapoclem->setHidden(true);
					zapoclem->setBindParts(true);
				zapoclem->endDefinition();
}
else if (tArt == EM29)//zapata2
{
				conector->beginDefinition();
					conector->asignarArticulo(tArt);
					conector->setHidden(true);
					conector->setBindParts(true);
				conector->endDefinition();

				perno12->beginDefinition();
					perno12->asignarDiametroPerno(0);
					perno12->asignarArticulo(tArt);
					perno12->setHidden(true);
					perno12->setBindParts(true);
				perno12->endDefinition();

				zapoclem->beginDefinition();
					zapoclem->asignarConector(tConector);
					zapoclem->setHidden(true);
					zapoclem->setBindParts(true);
				zapoclem->endDefinition();
}
else
{
				conector->beginDefinition();
					conector->asignarArticulo(tArt);
					conector->setHidden(true);
					conector->setBindParts(true);
				conector->endDefinition();

				perno12->beginDefinition();
					perno12->asignarDiametroPerno(0);
					perno12->asignarArticulo(tArt);
					perno12->setHidden(true);
					perno12->setBindParts(true);
				perno12->endDefinition();

				zapoclem->beginDefinition();
					zapoclem->asignarConector(tConector);
					zapoclem->setHidden(true);
					zapoclem->setBindParts(true);
				zapoclem->endDefinition();

}
*/

				conector->beginDefinition();
				conector->setName("conector2");
					conector->asignarArticulo(tArt);
					conector->setHidden(true);
					conector->setBindParts(true);
				conector->endDefinition();



			}
	};
}

#endif
