#ifndef HERRAJEINFERIOR_H_
#define HERRAJEINFERIOR_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "Canal.h"
#include "SoporteDeAngulo.h"
#include "Definiciones.h"

namespace NSTransformador{
	using namespace Hero;
	class HerrajeInferior: public GBox{
		public:
			Canal *canal;
			SoporteDeAngulo *angulo[4];
			GBox *qbox;
			double EspesorNucleo;
			double kvas;
			double dl;
			enum TipoAparato tTipoAparato;
			enum TipoConductor tTipoConductorBT;
			enum TipoArticuloHerraje tArt;
			double width;
			double length;

			double espesor;
			double longitudCanal;
			double alturaCanal;
			double anchoInterior;

			HerrajeInferior(GBox &parent):GBox(parent){
				createPart(canal, Canal);
				createPart(qbox,GBox);
				createPart(angulo[0], SoporteDeAngulo);
				createPart(angulo[1], SoporteDeAngulo);
				createPart(angulo[2], SoporteDeAngulo);
				createPart(angulo[3], SoporteDeAngulo);

			tArt = UF24;
			EspesorNucleo = millimeter(78);
			kvas = millimeter(1000);
			tTipoAparato = MEXICO;
			dl = millimeter(1417.745);
			espesor = millimeter(4.76);
			longitudCanal = millimeter(1407.75);
			alturaCanal = millimeter(88);
			anchoInterior = millimeter(280);


				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}

			void asignarEspesor(double espesor){
				this->espesor=espesor;
				actualizarDimensiones();
			}

			void asignarLongitudCanal(double longitudCanal){
				this->longitudCanal=longitudCanal;
				actualizarDimensiones();
			}

			void asignarAnchoInterior(double anchoInterior){
				this->anchoInterior=anchoInterior;
				actualizarDimensiones();
			}

			void asignarAlturaCanal(double alturaCanal){
				this->alturaCanal=alturaCanal;
				actualizarDimensiones();
			}
			void actualizarDimensiones(){

				width = ((kvas <=millimeter(500)) && (tTipoAparato != USA))?(dl - millimeter(63*2)):(dl - (2 * (EspesorNucleo - millimeter(3))));
				length = 2*espesor + anchoInterior + 2 * millimeter(63.5);

				this->setWidth(millimeter(492.48)); // Falta meter los valores
				this->setHeight(millimeter(88));
				this->setLength(millimeter(1397.74));
			}

			void defSelf(void){
			}


			void defParts(void){
				actualizarDimensiones();

				canal->beginDefinition();
				//canal->asignarDimensiones(GXMLInputs("C:\\Jan022008\\valores\\CanalS2715Test.xml"));
				canal->asignarEspesor(espesor);
				canal->asignarLongitudCanal(longitudCanal);
			    canal->asignarAnchoInterior(anchoInterior);
				canal->asignarAlturaCanal(alturaCanal);
				canal->endDefinition();


				qbox->beginDefinition();
					qbox->setWidth(length);
					qbox->setLength(width);
					qbox->setHidden(true);
				qbox->endDefinition();

			for(int i=0;i<4;i++)
			{
				angulo[i]->beginDefinition();

				angulo[i]->asignarArticulo((((kvas <=millimeter(500)) && (tTipoAparato != USA))?UE06: UF24));
					angulo[i]->position(BOTTOM,(EspesorNucleo - angulo[0]->getHeight())/2);
			}
					angulo[0]->rotate(RIGHT,DEGREES(180));
					angulo[0]->position(FRONT,0,*qbox);
					angulo[0]->position(RIGHT,0,*qbox);

					angulo[1]->rotate(FRONT,DEGREES(180));
					angulo[1]->rotate(RIGHT,DEGREES(180));
					angulo[1]->position(FRONT,0,*qbox);
					angulo[1]->position(LEFT,0,*qbox);
					
					angulo[2]->position(REAR,0,*qbox);
					angulo[2]->position(RIGHT,0,*qbox);

					angulo[3]->rotate(FRONT,DEGREES(180));
					angulo[3]->position(REAR,0,*qbox);
					angulo[3]->position(LEFT,0,*qbox);

			for(int i=0;i<4;i++)
				angulo[i]->endDefinition();
	





			}
	};
}
//Cola->rotate(RIGHT,DEGREES(90));
#endif
