#include "GBox.h"
#include "GMacros.h"
#include "GCylinder.h"
#include "GHollowCylinder.h"
#include "GCone.h"
#include "GSphere.h"
#include "GHollowTruncatedCone.h"
#include "GHollowBox.h"
#include "GAsymmetricHollowBox.h"
#include "GAsymmetricRoundedHollowBox.h"
using namespace gapi;
class GenericWall:public GBox{
public:
	GBox *wall;
	GBox *window;
	GBox *door;
	GBox *doorBell;
	double thickness;
	GenericWall(GBox &parent, double thickness=inch(6)):GBox(parent){
		this->thickness=thickness;
		createPart(window, GBox);
		createPart(door, GBox);
	}

	void defSelf(){
		setLength(thickness);
	}
	void defParts(void){
		window->beginDefinition();
			window->setLength(this->getLength());
			window->setWidth(parent->getWidth()*0.2);
			window->setHeight(parent->getHeight()*0.5);
			window->position(RIGHT,window->getWidth()*0.5);
		window->endDefinition();

		door->beginDefinition();
			door->setLength(this->getLength());
			door->setWidth(parent->getWidth()*0.2);
			door->setHeight(parent->getHeight()*0.85);
			door->position(BOTTOM, 0);
			door->position(LEFT, door->getWidth()*0.5);
		door->endDefinition();
	}
};

class Walls:public GBox{
	public:
		GenericWall *frontWall;
		GenericWall *rearWall;
		GenericWall *rightWall;
		GenericWall *leftWall;
		Walls(GBox &parent):GBox(parent){
			createPart(frontWall, GenericWall);
			createPart(rearWall, GenericWall);
			createPart(rightWall, GenericWall);
			createPart(leftWall, GenericWall);
		}

		void defSelf(void){

		}

		void defParts(void){
			frontWall->beginDefinition();
				frontWall->position(INFRONT, 0);
			frontWall->endDefinition();

			rearWall->beginDefinition();
				rearWall->rotate(TOP, DEGREES(180));
				rearWall->position(INREAR, 0);
			rearWall->endDefinition();

			rightWall->beginDefinition();
				rightWall->setWidth(this->getLength());
				rightWall->rotate90(TOP);
				rightWall->position(ONRIGHT,0);
			rightWall->endDefinition();

			leftWall->beginDefinition();
				leftWall->rotate90(BOTTOM);
				leftWall->position(ONLEFT,0);
				leftWall->setWidth(this->getLength());
			leftWall->endDefinition();
		}
};

void wallsDemo(GBox *root)
	{
		createRoot(root, GBox);
		root->beginDefinition();
		root->endDefinition();
		Walls *walls;
		createChild(walls, Walls, *root);
		walls->beginDefinition();
			walls->setWidth(feet(20));
			walls->setHeight(feet(9));
			walls->setLength(feet(40));
		walls->endDefinition();

		GCylinder *cylinder;
		createChild(cylinder, GCylinder, *walls);
		cylinder->beginDefinition();
			cylinder->setHeight(feet(3));
			cylinder->setRadius(feet(1));
			cylinder->position(BOTTOM, 0);
			cylinder->position(REAR, 0);
			cylinder->position(RIGHT, 0);
		cylinder->endDefinition();


		GHollowCylinder *hollowcylinder;
		createChild(hollowcylinder, GHollowCylinder, *walls);
		hollowcylinder->beginDefinition();
			hollowcylinder->setHeight(feet(3));
			hollowcylinder->setRadius(feet(1));
			hollowcylinder->setInnerRadius(feet(0.5));
			hollowcylinder->position(BOTTOM,0);

		hollowcylinder->endDefinition();

		GCone *cone;
		createChild(cone, GCone, *walls);
		cone->beginDefinition();
			cone->setHeight(feet(2));
			cone->setRadius(feet(0.5));
			cone->position(BOTTOM,0);
			cone->position(FRONT,0);
			cone->position(LEFT,0);
		cone->endDefinition();

		GSphere *sphere;
		createChild(sphere,GSphere,*walls);
		sphere->beginDefinition();
			sphere->setRadius(feet(1));
			sphere->rotate(RIGHT,DEGREES(90));
			sphere->position(FRONT,0);
			sphere->position(RIGHT,0);
			sphere->position(BOTTOM,0);
		sphere->endDefinition();

		GHollowTruncatedCone *hollowTruncatedCone;
		createChild(hollowTruncatedCone, GHollowTruncatedCone, *walls);
		hollowTruncatedCone->beginDefinition();
			hollowTruncatedCone->setHeight(feet(3));
			hollowTruncatedCone->setTopInnerRadius(feet(0.5)*0.5);
			hollowTruncatedCone->setTopRadius(feet(0.5));
			hollowTruncatedCone->setInnerRadius(feet(1)*0.5);
			hollowTruncatedCone->setRadius(feet(1));
			hollowTruncatedCone->position(BOTTOM,0);
			hollowTruncatedCone->position(REAR,0);
			hollowTruncatedCone->position(LEFT,0);
		hollowTruncatedCone->endDefinition();

		GHollowBox *hollowBox;
		createChild(hollowBox, GHollowBox, *walls);
		hollowBox->beginDefinition();
			hollowBox->setInnerHeight(feet(2));
			hollowBox->setInnerWidth(feet(1));
			hollowBox->setThickness(feet(0.5));
			hollowBox->setHeight(hollowBox->getInnerHeight()+2*hollowBox->getThickness());
			hollowBox->setWidth(hollowBox->getInnerWidth()+2*hollowBox->getThickness());
			hollowBox->setLength(feet(4));
			hollowBox->position(FRONT, 0);
		hollowBox->endDefinition();

		GHollowBox *hollowBox2;
		createChild(hollowBox2, GHollowBox, *walls);
		hollowBox2->beginDefinition();
			hollowBox2->setInnerHeight(feet(2));
			hollowBox2->setInnerWidth(feet(1));
			hollowBox2->setThickness(feet(0.5));
			hollowBox2->setHeight(hollowBox2->getInnerHeight()+2*hollowBox2->getThickness());
			hollowBox2->setWidth(hollowBox2->getInnerWidth()+2*hollowBox2->getThickness());
			hollowBox2->setLength(feet(4));
			hollowBox2->position(REAR, 0);
		hollowBox2->endDefinition();



		GAsymmetricHollowBox *asymmetricHollowBox;
		createChild(asymmetricHollowBox, GAsymmetricHollowBox, *walls);
		asymmetricHollowBox->beginDefinition();
			asymmetricHollowBox->setInnerHeight(feet(2));
			asymmetricHollowBox->setInnerWidth(feet(1));
			asymmetricHollowBox->setThickness(feet(0.5));
			asymmetricHollowBox->setHeight(asymmetricHollowBox->getInnerHeight()+2*asymmetricHollowBox->getThickness());
			asymmetricHollowBox->setWidth(asymmetricHollowBox->getInnerWidth()+2*asymmetricHollowBox->getThickness());
			asymmetricHollowBox->setTopThickness(asymmetricHollowBox->getThickness()*0.5);
			asymmetricHollowBox->setBottomThickness(asymmetricHollowBox->getThickness()*1.5);
			asymmetricHollowBox->setLength(feet(4));
			asymmetricHollowBox->position(LEFT, 0);
		asymmetricHollowBox->endDefinition();

		GAsymmetricRoundedHollowBox *asymmetricRoundedHollowBox;
		createChild(asymmetricRoundedHollowBox, GAsymmetricRoundedHollowBox, *walls);
		asymmetricRoundedHollowBox->beginDefinition();
			asymmetricRoundedHollowBox->setInnerHeight(feet(2));
			asymmetricRoundedHollowBox->setInnerWidth(feet(1));
			asymmetricRoundedHollowBox->setThickness(feet(0.5));
			asymmetricRoundedHollowBox->setTopThickness(asymmetricRoundedHollowBox->getThickness()*1.8);
			asymmetricRoundedHollowBox->setBottomThickness(asymmetricRoundedHollowBox->getThickness()*0.2);

			asymmetricRoundedHollowBox->setHeight(asymmetricRoundedHollowBox->getInnerHeight()+asymmetricRoundedHollowBox->getTopThickness()+asymmetricRoundedHollowBox->getBottomThickness());
			asymmetricRoundedHollowBox->setWidth(asymmetricRoundedHollowBox->getInnerWidth()+2*asymmetricRoundedHollowBox->getThickness());

			asymmetricRoundedHollowBox->setLength(feet(4));
			asymmetricRoundedHollowBox->position(RIGHT, 0);
		asymmetricRoundedHollowBox->endDefinition();
	}