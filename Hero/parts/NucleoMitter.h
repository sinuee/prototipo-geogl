#ifndef NUCLEOMITTER_H_
#define NUCLEOMITTER_H_
#include "GBox.h"
#include "GMacros.h"
#include "GHollowCylinder.h"
#include "GInputs.h"
namespace NSTransformador{
	using namespace Hero;
	class NucleoMitter: public GBox{
		public:
			GBox *yugoSuperior;
			GBox *yugoInferior;
			GBox *piernaIzquierda;
			GBox *piernaCentral;
			GBox *piernaDerecha;
			double espesor;//B
			double anchoLamina;//A
			double alturaVentana;//C
			double alturaNucleo;//H
			double entreEjes;//F
			double anchoVentana;//D

			NucleoMitter(GBox &parent):GBox(parent){
				createPart(yugoSuperior, GBox);
				createPart(yugoInferior, GBox);
				createPart(piernaIzquierda, GBox);
				createPart(piernaCentral, GBox);
				createPart(piernaDerecha, GBox);
				espesor=0.3;
				anchoLamina=0.2;
				alturaVentana=0.9;
				alturaNucleo=alturaVentana+2*anchoLamina;
				entreEjes=1.2;
				anchoVentana=entreEjes-anchoLamina;
				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}

			void actualizarDimensiones(){
				this->setWidth(3*anchoLamina+2*anchoVentana);
				this->setHeight(alturaNucleo);
				this->setLength(espesor);
			}

			void defSelf(void){
			}

			void asignarDimensiones(const GInputs &inputs){
				this->espesor=inputs.getReal("dBNucleo",espesor);
				this->anchoLamina=inputs.getReal("dANucleo",anchoLamina);
				this->alturaVentana=inputs.getReal("dC",alturaVentana);
				this->alturaNucleo=inputs.getReal("dH", alturaNucleo);
				this->entreEjes=inputs.getReal("dF2", entreEjes);
				this->anchoVentana=inputs.getReal("dD2", anchoVentana);
				actualizarDimensiones();
			}
			void obtenerDimensiones(GInputs &inputs){
				inputs.addInput<double>("dBNucleo", espesor);
				inputs.addInput<double>("dAucleo", anchoLamina);
				inputs.addInput<double>("dC", alturaVentana);
				inputs.addInput<double>("dH", alturaNucleo);
				inputs.addInput<double>("dF2", entreEjes);
				inputs.addInput<double>("dD2", anchoVentana);
				inputs.addInput<double>("dBNucleo", espesor);
			}

			void defParts(void){
				actualizarDimensiones();
				yugoSuperior->beginDefinition();
					yugoSuperior->setHeight(anchoLamina);
					yugoSuperior->position(TOP, 0);
				yugoSuperior->endDefinition();

				yugoInferior->beginDefinition();
					yugoInferior->setHeight(anchoLamina);
					yugoInferior->position(BOTTOM,0);
				yugoInferior->endDefinition();

				piernaIzquierda->beginDefinition();
					piernaIzquierda->setWidth(anchoLamina);
					piernaIzquierda->setHeight(alturaVentana);
					piernaIzquierda->position(LEFT,0);
				piernaIzquierda->endDefinition();

				piernaDerecha->beginDefinition();
					piernaDerecha->setWidth(anchoLamina);
					piernaDerecha->setHeight(alturaVentana);
					piernaDerecha->position(RIGHT,0);
				piernaDerecha->endDefinition();

				piernaCentral->beginDefinition();
					piernaCentral->setWidth(anchoLamina);
					piernaCentral->setHeight(alturaVentana);
				piernaCentral->endDefinition();

			}
	};
}

#endif
