#ifndef HERRAJESUPERIOR_H_
#define HERRAJESUPERIOR_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "Canal.h"
#include "SoporteDeAngulo.h"
#include "AnguloDePlaca.h"
#include "Definiciones.h"

namespace NSTransformador{
	using namespace Hero;
	class HerrajeSuperior: public GBox{
		public:
			Canal *canal;
			SoporteDeAngulo *angulo[4];
			GBox *qbox;
			SoporteDeAngulo *angulo12[4];
			GBox *qbox1;
			GBox *qbox2;
			SoporteDeAngulo *angulo2[2];

			double EspesorNucleo;
			double kvas;
			double dl;
			enum TipoAparato tTipoAparato;
			enum TipoConductor tTipoConductorBT;
			enum TipoArticuloHerraje tArt;
			double width;
			double length;
			double height;
			double LitHerr0;
			double LitHerr8;
			bool bLadosCortos;
			bool bPedestal;
			double BoqArreglo;
			bool bBoqEnTapa;
			enum CambiadorAncho tCambiador;
			enum TipoConexion tTipoConexion;
			enum TipoNucleo tTipoNucleo;
			double F2;

			double espesorCanal;
			double longitudCanal;
			double alturaCanal;
			double anchoInterior;
			bool bZonaSismica;

			HerrajeSuperior(GBox &parent):GBox(parent){
				createPart(canal, Canal);

				createPart(qbox,GBox);
				createPart(angulo[0], SoporteDeAngulo);
				createPart(angulo[1], SoporteDeAngulo);
				createPart(angulo[2], SoporteDeAngulo);
				createPart(angulo[3], SoporteDeAngulo);
				
				createPart(qbox1,GBox);
				createPart(angulo12[0], SoporteDeAngulo);
				createPart(angulo12[1], SoporteDeAngulo);
				createPart(angulo12[2], SoporteDeAngulo);
				createPart(angulo12[3], SoporteDeAngulo);

				createPart(qbox2,GBox);
				createPart(angulo2[0], SoporteDeAngulo);
				createPart(angulo2[1], SoporteDeAngulo);
		

			tArt = UF24;
			EspesorNucleo = millimeter(89);
			kvas = millimeter(2000);
			tTipoAparato = USA;
			dl = millimeter(1362.745);
			LitHerr0 = millimeter(1352.7914);
			LitHerr8 = millimeter(312);
			bZonaSismica = true;
		    bLadosCortos = true;
			bPedestal = false;
			BoqArreglo = 1;/////////////
			bBoqEnTapa= false;//////////////
			tCambiador = QUALITY200;
			tTipoConexion = ESTRELLA;
			tTipoNucleo = ENROLLADO;
			F2 = millimeter(388);

			//Datos Del Herraje
			espesorCanal = millimeter(6.35);
			longitudCanal = millimeter(1362.79);
			alturaCanal = millimeter(95.35);
			anchoInterior = millimeter(324.7);
			

				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}

			void asignartTipoConductorBT(enum TipoConductor tTipoConductorBT){
				this->tTipoConductorBT=tTipoConductorBT;
				actualizarDimensiones();
			}

			void asignartArt(enum TipoArticuloHerraje tArt){
				this->tArt=tArt;
				actualizarDimensiones();
			}

			void asignarbLadosCortos(bool bLadosCortos){
				this->bLadosCortos=bLadosCortos;
				actualizarDimensiones();
			}

			void asignarbPedestal(bool bPedestal){
				this->bPedestal=bPedestal;
				actualizarDimensiones();
			}

			void asignartCambiador(enum CambiadorAncho tCambiador){
				this->tCambiador=tCambiador;
				actualizarDimensiones();
			}

			void asignartTipoConexion(enum TipoConexion tTipoConexion){
				this->tTipoConexion=tTipoConexion;
				actualizarDimensiones();
			}

			void asignartTipoNucleo(enum TipoNucleo tTipoNucleo){
				this->tTipoNucleo=tTipoNucleo;
				actualizarDimensiones();
			}

			void asignarbZonaSismica(bool bZonaSismica){
				this->bZonaSismica=bZonaSismica;
				actualizarDimensiones();
			}
			void asignarbBoqEnTapa(bool bBoqEnTapa){
				this->bBoqEnTapa=bBoqEnTapa;
				actualizarDimensiones();
			}

			void asignardl(double dl){
				this->dl=dl;
				actualizarDimensiones();
			}

			void asignarLitHerr0(double LitHerr0){
				this->LitHerr0=LitHerr0;
				actualizarDimensiones();
			}

			void asignarLitHerr8(double LitHerr8){
				this->LitHerr8=LitHerr8;
				actualizarDimensiones();
			}

			void asignartTipoAparato(enum TipoAparato tTipoAparato){
				this->tTipoAparato=tTipoAparato;
				actualizarDimensiones();
			}
			
			void asignarEspesorNucleo(double EspesorNucleo){
				this->EspesorNucleo=EspesorNucleo;
				actualizarDimensiones();
			}

			void asignarkvas(double kvas){
				this->kvas=kvas;
				actualizarDimensiones();
			}

			void asignarF2(double F2){
				this->F2=F2;
				actualizarDimensiones();
			}

			void asignarBoqArreglo(double BoqArreglo){
				this->BoqArreglo=BoqArreglo;
				actualizarDimensiones();
			}

			void asignarEspesorCanal(double espesorCanal){
				this->espesorCanal=espesorCanal;
				actualizarDimensiones();
			}

			void asignarLongitudCanal(double longitudCanal){
				this->longitudCanal=longitudCanal;
				actualizarDimensiones();
			}

			void asignarAnchoInterior(double anchoInterior){
				this->anchoInterior=anchoInterior;
				actualizarDimensiones();
			}

			void asignarAlturaCanal(double alturaCanal){
				this->alturaCanal=alturaCanal;
				actualizarDimensiones();
			}
			void actualizarDimensiones(){

				this->setWidth(millimeter(1362)); 
				this->setHeight(millimeter(95.35));
				this->setLength(millimeter(324.7));

			}


			void asignarDimensiones(const GInputs &inputs){
				EspesorNucleo=inputs.getReal("EspesorNucleo",EspesorNucleo);
				kvas=inputs.getReal("kvas",kvas);
				dl=inputs.getReal("dl",dl);
				LitHerr0=inputs.getReal("LitHerr0",LitHerr0);
				LitHerr8=inputs.getReal("LitHerr8",LitHerr8);
				BoqArreglo=inputs.getReal("BoqArreglo",BoqArreglo);
				F2=inputs.getReal("F2",F2);
				espesorCanal=inputs.getReal("espesorCanal",espesorCanal);
				longitudCanal=inputs.getReal("longitudCanal",longitudCanal);
				alturaCanal=inputs.getReal("alturaCanal",alturaCanal);
				anchoInterior=inputs.getReal("anchoInterior",anchoInterior);
				tTipoAparato=(TipoAparato)inputs.getInteger("tTipoAparato",tTipoAparato);
				tTipoConductorBT=(TipoConductor)inputs.getInteger("tTipoConductorBT",tTipoConductorBT);
				tArt=(TipoArticuloHerraje)inputs.getInteger("tArt",tArt);
				tCambiador=(CambiadorAncho)inputs.getInteger("tCambiador",tCambiador);
				tTipoConexion=(TipoConexion)inputs.getInteger("tTipoConexion",tTipoConexion);
				tTipoNucleo=(TipoNucleo)inputs.getInteger("tTipoNucleo",tTipoNucleo);
				bLadosCortos=inputs.getBoolean("bLadosCortos",bLadosCortos);
				bPedestal=inputs.getBoolean("bPedestal",bPedestal);
				bBoqEnTapa=inputs.getBoolean("bBoqEnTapa",bBoqEnTapa);
				bZonaSismica=inputs.getBoolean("bZonaSismica",bZonaSismica);
			}

			void obtenerDimensiones(GInputs &inputs){
				inputs.addInput<double>("EspesorNucleo", EspesorNucleo);
				inputs.addInput<double>("kvas", kvas);
				inputs.addInput<double>("dl", dl);
				inputs.addInput<double>("LitHerr0", LitHerr0);
				inputs.addInput<double>("LitHerr8", LitHerr8);
				inputs.addInput<double>("BoqArreglo", BoqArreglo);
				inputs.addInput<double>("F2", F2);
				inputs.addInput<double>("espesorCanal", espesorCanal);
				inputs.addInput<double>("longitudCanal", longitudCanal);
				inputs.addInput<double>("alturaCanal", alturaCanal);
				inputs.addInput<double>("anchoInterior", anchoInterior);
				inputs.addInput<int>("tTipoAparato",tTipoAparato);
				inputs.addInput<int>("tTipoConductorBT",tTipoConductorBT);
				inputs.addInput<int>("tArt",tArt);
				inputs.addInput<int>("tCambiador",tCambiador);
				inputs.addInput<int>("tTipoConexion",tTipoConexion);
				inputs.addInput<int>("tTipoNucleo",tTipoNucleo);
				inputs.addInput<bool>("bLadosCortos",bLadosCortos);
				inputs.addInput<bool>("bPedestal",bPedestal);
				inputs.addInput<bool>("bBoqEnTapa",bBoqEnTapa);
				inputs.addInput<bool>("bZonaSismica",bZonaSismica);
			}
			void defSelf(void){
			}
			void defParts(void){
				actualizarDimensiones();

				canal->beginDefinition();
				canal->asignarEspesor(espesorCanal);
				canal->asignarLongitudCanal(longitudCanal);
			    canal->asignarAnchoInterior(anchoInterior);
				canal->asignarAlturaCanal(alturaCanal);
				canal->rotate(FRONT,DEGREES(180));
				canal->rotate(TOP,DEGREES(90));
				canal->endDefinition();



				width = ((kvas <=millimeter(500)) && (tTipoAparato != USA))?(dl - millimeter(63*2)):(dl - (2 * (EspesorNucleo - millimeter(3))));
				length = 2*espesorCanal + anchoInterior + 2 * millimeter(63.5);

				qbox->beginDefinition();
					qbox->setWidth(width);
					qbox->setLength(length);
					qbox->setHidden(true);
				qbox->endDefinition();

			for(int i=0;i<4;i++)
			{
				angulo[i]->beginDefinition();

				angulo[i]->asignarArticulo((((kvas <=millimeter(500)) && (tTipoAparato != USA))?UE06: UF24));
					angulo[i]->position(BOTTOM,
						((EspesorNucleo - angulo[0]->getHeight())/2) + 
						(((kvas <=millimeter(500)) && (tTipoAparato != USA))?espesorCanal:0));
						angulo[i]->rotate(BOTTOM,DEGREES(0));
						angulo[i]->rotate180(LONGITUDINAL);

			}

					angulo[0]->position(FRONT,0,*qbox);
					angulo[0]->position(RIGHT,0,*qbox);

					angulo[1]->rotate(FRONT,DEGREES(180));
					angulo[1]->position(FRONT,0,*qbox);
					angulo[1]->position(LEFT,0,*qbox);

					angulo[2]->rotate(LEFT,DEGREES(180));
					angulo[2]->position(REAR,0,*qbox);
					angulo[2]->position(RIGHT,0,*qbox);

					angulo[3]->rotate(LEFT,DEGREES(180));
					angulo[3]->rotate(FRONT,DEGREES(180));
					angulo[3]->position(REAR,0,*qbox);
					angulo[3]->position(LEFT,0,*qbox);

			for(int i=0;i<4;i++)
			{
				angulo[i]->endDefinition();

			}
			width = millimeter(LitHerr0);
			length = millimeter(LitHerr8 - 45);
			qbox1->beginDefinition();
					qbox1->setWidth(width);
					qbox1->setLength(length);
					qbox1->setHidden(true);
				qbox1->endDefinition();

			for(int i=0;i<4;i++)
			{
				angulo12[i]->beginDefinition();

				angulo12[i]->asignarArticulo((((kvas <=millimeter(500)) && (tTipoAparato != USA))?UE06: UF24));
					
					angulo12[i]->rotate(BOTTOM,DEGREES(90));
					angulo12[i]->rotate180(LONGITUDINAL);
					angulo12[i]->rotate(REAR,DEGREES(90));
					angulo12[i]->position(ABOVE,0,*qbox1);
			}

					angulo12[0]->position(FRONT,0,*qbox1);
					angulo12[0]->position(RIGHT,0,*qbox1);

					angulo12[1]->rotate(RIGHT,DEGREES(180));
					angulo12[1]->position(FRONT,0,*qbox1);
					angulo12[1]->position(LEFT,0,*qbox1);
					
					angulo12[2]->position(REAR,0,*qbox1);
					angulo12[2]->position(RIGHT,0,*qbox1);

					angulo12[3]->rotate(RIGHT,DEGREES(180));
					angulo12[3]->position(REAR,0,*qbox1);
					angulo12[3]->position(LEFT,0,*qbox1);

			for(int i=0;i<4;i++)
							{
				angulo12[i]->endDefinition();

			}

			qbox2->beginDefinition();

			for(int i=0;i<2;i++)
			{
				angulo2[i]->beginDefinition();
				angulo2[i]->asignarArticulo(UE00);
				angulo2[i]->rotate(FRONT,DEGREES(90));
			}
				qbox2->beginDefinition();
					qbox2->setWidth(angulo2[0]->getLength() + F2);
					qbox2->setLength(angulo2[0]->getHeight());
					qbox2->setHeight(angulo2[0]->getLength());
					qbox2->position(ABOVE,0);
					qbox2->position(REAR,millimeter(-20));
					qbox2->setHidden(true);

				qbox2->endDefinition();

					angulo2[0]->position(RIGHT,0,*qbox2);
					angulo2[1]->position(LEFT,0,*qbox2);
					angulo2[0]->position(ABOVE,0);
					angulo2[0]->position(REAR,millimeter(-20));
				    angulo2[1]->position(ABOVE,0);
					angulo2[1]->position(REAR,millimeter(-20));


			for(int i=0;i<2;i++)
			{
				angulo2[i]->endDefinition();

			}
	










if (tTipoConductorBT !=SOLERA)
{
	if((kvas <= millimeter(500)) && (tTipoAparato != USA) && ((tCambiador == QUALITY200) || (tCambiador == QUALITY400) || (tCambiador == MOLONEY_Cambiador)))
	{
		angulo[0]->setHidden(true);
		angulo[1]->setHidden(true);
		angulo[2]->setHidden(true);
		angulo[3]->setHidden(true);
	}
}
else{
		angulo[0]->setHidden(true);
		angulo[1]->setHidden(true);
		angulo[2]->setHidden(true);
		angulo[3]->setHidden(true);
	}

if((bZonaSismica == true) || ((kvas <= millimeter(500)) && (tTipoAparato != USA) && ((tCambiador == QUALITY200) || (tCambiador == QUALITY400) || (tCambiador == MOLONEY_Cambiador))))
{
angulo12[0]->setHidden(true);
angulo12[1]->setHidden(true);
angulo12[2]->setHidden(true);
angulo12[3]->setHidden(true);
}

if((tTipoNucleo == ENROLLADO) && (tTipoConductorBT == SOLERA))
{
		angulo2[0]->setHidden(true);
		angulo2[1]->setHidden(true);
}

	
		}
	};
}

#endif
