#ifndef ZAPATA2_H_
#define ZAPATA2_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "GPartialHollowCylinder.h"
#include "Definiciones.h"

namespace NSTransformador{
	using namespace Hero;
	class Zapata2: public GBox{
		public:
			GBox *BoxSolid;
			GBox *Caja;
			GPartialHollowCylinder *Cylinder;
			double CajaLength;
			double CajaWidth;
			double CajaHeight;
			double BoxLength;
			double BoxWidth;
			double BoxHeight;
			double Radio;
			double Largo;

			Zapata2(GBox &parent):GBox(parent){
				createPart(BoxSolid, GBox);
				createPart(Caja, GBox);
				createPart(Cylinder, GPartialHollowCylinder);
					CajaLength = millimeter(54.1375);
					CajaWidth = millimeter(76.2);
					CajaHeight = millimeter(6.35);
					BoxLength= millimeter(6.35);
					BoxWidth = millimeter(21.65);
					BoxHeight = millimeter(10.87);
					Radio = millimeter(23.418);
					Largo = millimeter(4.7625);

				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}

			void actualizarDimensiones(){
				this->setLength(millimeter(CajaLength + BoxLength + Largo));
				this->setWidth(millimeter(CajaWidth));
				this->setHeight(Radio* 2);
			}
			void defParts(void){
				actualizarDimensiones();

				Caja->beginDefinition();
					Caja->setLength(CajaLength);
					Caja->setWidth(CajaWidth);
					Caja->setHeight(CajaHeight);
				Caja->endDefinition();

				BoxSolid->beginDefinition();
					BoxSolid->setLength(BoxLength);
					BoxSolid->setWidth(BoxWidth);
					BoxSolid->setHeight(BoxHeight);
					BoxSolid->position(INREAR,0,*Caja);
				BoxSolid->endDefinition();

				Cylinder->beginDefinition();
					Cylinder->setInnerRadius(0);
					Cylinder->setRadius(Radio);
					Cylinder->setHeight(Largo);
					Cylinder->rotate(RIGHT,DEGREES(90));
					Cylinder->setStartAngle(DEGREES(0));
					Cylinder->setSweepAngle(DEGREES(360));
					Cylinder->position(INREAR,0,*BoxSolid);
				Cylinder->endDefinition();
				
			}
	};
}

#endif
