#ifndef TRAFO_H_
#define TRAFO_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "Tanque.h"
#include "boquilla.h"

namespace NSTransformador{
	using namespace Hero;
	class Trafo: public GBox{
		public:
			Tanque *tanque;
			Boquilla *boq1;
			Boquilla *boq2;
			Boquilla *boq3;
			Boquilla *boq4;
			double boq1X;
			double boq1Y;
			double boq1Z;
			double boq2X;
			double boq2Y;
			double boq2Z;
			double boq3X;
			double boq3Y;
			double boq3Z;
			double boq4X;
			double boq4Y;
			double boq4Z;
			enum TipoAparato tTipoAparato;
			enum TipoTransformador tTipoTrafo;
			double AltoInterior;
			bool Paredes4;
			double alto;

			Trafo(GBox &parent):GBox(parent){
				createPart(tanque, Tanque);
			    createPart(boq1, Boquilla);
				createPart(boq2, Boquilla);
				createPart(boq3, Boquilla);
				createPart(boq4, Boquilla);
			/* boq1X = millimeter(-719.9);
			 boq1Y = millimeter(240);
			 boq1Z = millimeter(432.0);
			 boq2X = millimeter(-719.9);
			 boq2Y = millimeter(80);
			 boq2Z = millimeter(432.0);
			 boq3X = millimeter(-719.9);
			 boq3Y = millimeter(-80);
			 boq3Z = millimeter(432.0);
			 boq4X = millimeter(-719.9);
			 boq4Y = millimeter(-240);
			 boq4Z = millimeter(432.0);*/
			 boq1X = millimeter(-225);
			 boq1Y = millimeter(-412.1);
			 boq1Z = millimeter(357.5);

			 boq2X = millimeter(-75);
			 boq2Y = millimeter(-412.1);
			 boq2Z = millimeter(357.5);

			 boq3X = millimeter(75);
			 boq3Y = millimeter(-412.1);
			 boq3Z = millimeter(357.5);

			 boq4X = millimeter(225);
			 boq4Y = millimeter(-412.1);
			 boq4Z = millimeter(357.5);

			tTipoAparato = MEXICO;
			tTipoTrafo = ESTACION;
			AltoInterior = 1235;
			alto = 1;


				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}

			void actualizarDimensiones(){
				this->setWidth(alto);
				this->setHeight(2);
				this->setLength(3);
			}

			void defSelf(void){
			}


			void defParts(void){
				actualizarDimensiones();
				Paredes4 = ((tTipoAparato== USA)||(AltoInterior >1760))? true: false;

				tanque->beginDefinition();
					tanque->asignarDimensiones(GXMLInputs("C:\\Jan022008\\valores\\tanqueSGA007Test.xml"));
				tanque->endDefinition();

				boq1->beginDefinition();
					
					boq1->asignarDimensiones(GXMLInputs("C:\\Jan022008\\valores\\BoquillaSGA007Test.xml"));
					if((tTipoTrafo == L_CORTOS) && (Paredes4 == false))
						boq1->rotate(TOP,DEGREES(0));
					else if(tTipoTrafo != L_CORTOS)
						boq1->rotate(TOP,DEGREES(180));
					else if(tTipoTrafo == L_CORTOS)
						boq1->rotate(TOP,DEGREES(90));
					boq1->rotate(RIGHT,DEGREES(180));
					boq1->translate(boq1X,boq1Y,boq1Z);
				boq1->endDefinition();

				boq2->beginDefinition();
				boq2->asignarDimensiones(GXMLInputs("C:\\Jan022008\\valores\\BoquillaSGA007Test.xml"));
					if((tTipoTrafo == L_CORTOS) && (Paredes4 == false))
						boq2->rotate(TOP,DEGREES(0));
					else if(tTipoTrafo != L_CORTOS)
						boq2->rotate(TOP,DEGREES(180));
					else if(tTipoTrafo == L_CORTOS)
						boq2->rotate(TOP,DEGREES(90));
					boq2->rotate(RIGHT,DEGREES(180));
					boq2->translate(boq2X,boq2Y,boq2Z);
				boq2->endDefinition();

				boq3->beginDefinition();
				boq3->asignarDimensiones(GXMLInputs("C:\\Jan022008\\valores\\BoquillaSGA007Test.xml"));
					if((tTipoTrafo == L_CORTOS) && (Paredes4 == false))
						boq3->rotate(TOP,DEGREES(0));
					else if(tTipoTrafo != L_CORTOS)
						boq3->rotate(TOP,DEGREES(180));
					else if(tTipoTrafo == L_CORTOS)
						boq3->rotate(TOP,DEGREES(90));
					boq2->rotate(RIGHT,DEGREES(180));

					boq3->rotate(RIGHT,DEGREES(180));
					boq3->translate(boq3X,boq3Y,boq3Z);
				boq3->endDefinition();

				if(Paredes4 = true)

					boq4->beginDefinition();
						boq4->asignarDimensiones(GXMLInputs("C:\\Jan022008\\valores\\BoquillaSGA007Test.xml"));
						if((tTipoTrafo == L_CORTOS) && (Paredes4 == false))
							boq4->rotate(TOP,DEGREES(0));
						else if(tTipoTrafo != L_CORTOS)
							boq4->rotate(TOP,DEGREES(180));
						else if(tTipoTrafo == L_CORTOS)
							boq4->rotate(TOP,DEGREES(90));
						boq2->rotate(RIGHT,DEGREES(180));
						boq4->rotate(RIGHT,DEGREES(180));
						boq4->translate(boq4X,boq4Y,boq4Z);
						if(Paredes4 = false)
						boq4->setHidden(true);

					boq4->endDefinition();

			}
	};
}
//Cola->rotate(RIGHT,DEGREES(90));
#endif
