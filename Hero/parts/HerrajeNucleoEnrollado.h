/*
 * HerrajeNucleoEnrollado.h
 *
 *  Created on: Jan 7, 2009
 *      Author: khayyam
 */

#ifndef HERRAJENUCLEOENROLLADO_H_
#define HERRAJENUCLEOENROLLADO_H_
#include "Canal.h"
#include "CanalLateral.h"

namespace NSTransformador{
	class HerrajeNucleoEnrollado:public GBox{
		public:
			Canal *canalSuperior;
			Canal *canalInferior;
			CanalLateral *canalIzquierdo;
			CanalLateral *canalDerecho;
			double alturaNucleo;
			double anchoNucleo;
			double espesorHerrajes;
			double espesorNucleo;//determina la altura de los canales laterales
			double espesorAislamientoHerrajeNucleo;


			HerrajeNucleoEnrollado(GBox){
				createPart(canalSuperior, Canal);
				createPart(canalInferior, Canal);
				createPart(canalIzquierdo, CanalLateral);
				createPart(canalDerecho, CanalLateral);
			}

			void actualizarDimensiones(void){
				this->setWidth();
				this->setLength();
				this->setHeight(alturaNucleo+2*(espesorHerrajes+espesorAislamientoHerrajeNucleo));
			}

			void defParts(void){
				actualizarDimensiones();
				canalSuperior->beginDefinition();
					canalSuperior->rotate90(TOP);
					canalSuperior->rotate180(LATERAL);
				canalSuperior->endDefinition();

				canalInferior->beginDefinition();
					canalInferior->rotate90(TOP);
				canalInferior->endDefinition();

				canalIzquierdo->beginDefinition();
					canalIzquierdo->rotate90(LEFT);
					canalIzquierdo->rotate90(BOTTOM);
				canalIzquierdo->endDefinition();

				canalDerecho->beginDefinition();
					canalDerecho->rotate90(LEFT);
					canalDerecho->rotate90(TOP);
				canalDerecho->endDefinition();
			}
	};
}

#endif /* HERRAJENUCLEOENROLLADO_H_ */
