#ifndef EJEMPLOGBOX_H_
#define EJEMPLOGBOX_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
namespace NSTransformador{
	using namespace Hero;
	class EjemploGBox: public GBox{
		public:
			//Declaración del objeto tipo Caja
			GBox *Cubo;
			//Declaración de variables que se usaran en esta clase
		double dVertical;
		double dLongitudinal;
		double dLateral;
			//Crea las formas que se desen dibujar
			EjemploGBox(GBox &parent):GBox(parent){
				createPart(Cubo, GBox);
			//Declaración de variables default de la clase
				dVertical = millimeter(100);
				dLongitudinal = millimeter(200);
				dLateral = millimeter(300);
				
				//oculta el contraste de las dimenciones
				this->setHidden(true);
				// oculta el borde de la figura
				this->setBindParts(true);
				actualizarDimensiones();
			}

			void asignarLateral(double dLateral){
				this->dLateral=dLateral;
				actualizarDimensiones();
			}
			void asignarVertical(double dVertical){
				this->dVertical=dVertical;
				actualizarDimensiones();
			}
			void asignartdLongitudinal(double dLongitudinal){
				this->dLongitudinal=dLongitudinal;
				actualizarDimensiones();
			}



			void actualizarDimensiones(){
				// Dimención de la figura (Limites)
				this->setWidth(dLateral);
				this->setHeight(dVertical);
				this->setLength(dLongitudinal);
			}
			void defSelf(void){
			}

			void asignarDimensiones(const GInputs &inputs){
				// Son variables que se envian a un txt
				this->dLateral=inputs.getReal("Lateral",dLateral);
				this->dVertical=inputs.getReal("Vertical",dVertical);
				this->dLongitudinal=inputs.getReal("Longitudinal",dLongitudinal);


				actualizarDimensiones();
			}
			void obtenerDimensiones(GInputs &inputs){
				// Son variables que se obtienen de un txt
				inputs.addInput<double>("Lateral",dLateral);
				inputs.addInput<double>("Vertical",dVertical);
				inputs.addInput<double>("Longitudinal",dLongitudinal);


			}
			void defParts(void){
				actualizarDimensiones();
				// Espesificaciones de la figura, esto pueden ser:
				// width,Length, Height, position,rotate ...
				Cubo->beginDefinition();
					Cubo->setWidth(dLateral);
					Cubo->setHeight(dVertical);
					Cubo->setLength(dLongitudinal);
				Cubo->endDefinition();


			}
	};
}

#endif
