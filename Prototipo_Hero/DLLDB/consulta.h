#if !defined Consulta_H
#define Consulta_H

//#if defined(__Consulta_PROJECT__)
//#define DLL_Consulta_API
//#else
//#ifdef Consulta_EXPORTS
//#define DLL_Consulta_API extern "C" __declspec(dllexport)
//#else
//#define DLL_Consulta_API extern "C" __declspec(dllimport)
//#endif
//#endif 
#include <string>
#include <sstream>
#include <iostream>
#pragma warning(disable:4996)
#import "C:\Program Files\Common Files\System\ADO\msado15.dll" rename_namespace("ADOCG") rename("EOF", "EndOfFile")

using namespace ADOCG;
struct tblPrototipo
{
	std::string NODISENO;
	std::string DISENADOR;
	std::string KVAS;
	std::string FASES;
	std::string CLIENTE;
	std::string CTEEVALNUC;
	std::string CTEEVALDEV;
	std::string PARIDAD;
	std::string NORMA;
	std::string FRECUENCIA;
	std::string ALTITUD;
	std::string SOBREELEV;
	std::string TIPOENFTO;
	std::string PANTALLAELECTROSTATICA;
	std::string ZONASISMICA;
	std::string TIPOTRANSFORMADOR;
	std::string TIPOGARGANTA;
	std::string INTERFASEAT;
	std::string INTERFASEBT;
	std::string BOQUILLATAPA;
	std::string NUMRADIDADORES;
	std::string TCS;
	std::string TANQUECONSERVADOR;
	std::string TIPOBASE;
	std::string APOYOPARAGATO;
};
__declspec(dllexport) bool ConsultarBD (char *nodis, char *BaseDatos, char *revision, tblPrototipo *Renglon);
__declspec(dllexport) bool GrabarBD (char *nodis, char *BaseDatos, char *revision, tblPrototipo *Renglon);
template <typename T> std::string NumberToString ( T Number )
  {
	  std::ostringstream ss;
     ss << Number;
     return ss.str();
  }
template<typename T>std::string convertToString(T x, int precision=-1){
	std::ostringstream os;
	if(0<=precision){
		os<<std::fixed<<std::setprecision(precision);
	}else{
		os<<std::scientific;
	}
	os<<x;
	return os.str();
}
#endif