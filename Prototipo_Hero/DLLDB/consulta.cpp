
// CAMBIOS
// Folio	Autor	Fecha		Descripcion												Revision
// 19800	EEDEC	01/09/2008	Seleccion de Reactor Limitador Estandar para XLPT 1F4P	NA

#include <math.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <string>
#include <search.h>


#include "Consulta.h"
//EEDEC  C19800  01/09/2008  INI
int f_comparacion (const void *a, const void *b)
{
	int resultado;

	double a1;
	double b1;

	a1 = *((double *)a);
	b1 = *((double *)b);

	resultado = (a1>b1) ? 1 : 
				(a1<b1) ? -1 : 0;

	return resultado;
}


int f_comparacion2 (const void *a, const void *b)
{
	int resultado;

	double a1;
	double b1;

	a1 = ((double *)a)[0];
	b1 = ((double *)b)[0];

	resultado = (a1>b1) ? 1 : 
				(a1<b1) ? -1 : 0;

	return resultado;
}

//EEDEC  C19800  01/09/2008  FINI



/******************************************************************************
Funcion: ConsultarBD            
Autor:  Esteban Sinuee Hernandez Sanchez ESHS
/******************************************************************************/

bool ConsultarBD (char *nodis, char *BaseDatos, char *revision, tblPrototipo *Renglon)
{
	_ConnectionPtr pConn;
	_CommandPtr pCmd;
	_RecordsetPtr pRs;	
	std::string strSQL;
	std::string strconnection;
	std::string cadena;
	char *buffer;
	int i,j,k;
	i=0;

	strconnection = "DSN=" + std::string(BaseDatos) + ";DATABASE=baan" + std::string(BaseDatos) +";UID=pot_audi ;PWD=audi_pot;";
	strSQL = "SELECT * from prototipohero where nodiseno = '" + std::string(nodis) + "'";
	::CoInitialize(NULL);
	pConn.CreateInstance(__uuidof(Connection));
	pConn->ConnectionString = (strconnection.c_str());
	pConn->Open("","","",adConnectUnspecified);
	pCmd.CreateInstance(__uuidof(Command));
	pCmd->ActiveConnection = pConn;
	pCmd->CommandText = _bstr_t (strSQL.c_str());
	pRs.CreateInstance(__uuidof(Recordset));
	pRs->PutRefSource(pCmd);
	_variant_t vNull;
	vNull.vt = VT_ERROR;
	vNull.scode = DISP_E_PARAMNOTFOUND;
	pRs->Open(vNull, vNull, adOpenDynamic, adLockOptimistic, adCmdUnknown);
	_variant_t *vVariantCero = new _variant_t(0.0);
	_variant_t vPaso;
			
		if (!pRs->EndOfFile)
		{
			vPaso = pRs->Fields->GetItem("DISENADOR")->GetValue();
			Renglon->DISENADOR = (LPCSTR)((_bstr_t)vPaso.bstrVal);
			vPaso = pRs->Fields->GetItem("KVAS")->GetValue();
			Renglon->KVAS  = NumberToString(vPaso.intVal);
			vPaso = pRs->Fields->GetItem("CLIENTE")->GetValue();
			Renglon->CLIENTE  = (LPCSTR)((_bstr_t)vPaso.bstrVal);
			vPaso = pRs->Fields->GetItem("ALTITUD")->GetValue();
			Renglon->ALTITUD  = NumberToString(vPaso.intVal);
			vPaso = pRs->Fields->GetItem("APOYOPARAGATO")->GetValue();
			Renglon->APOYOPARAGATO  = NumberToString(vPaso.intVal);
			vPaso = pRs->Fields->GetItem("BOQUILLATAPA")->GetValue();
			Renglon->BOQUILLATAPA = NumberToString(vPaso.intVal);
			vPaso = pRs->Fields->GetItem("CTEEVALDEV")->GetValue();
			Renglon->CTEEVALDEV = NumberToString(vPaso.intVal);
			vPaso = pRs->Fields->GetItem("CTEEVALNUC")->GetValue();
			Renglon->CTEEVALNUC = NumberToString(vPaso.intVal);
			vPaso = pRs->Fields->GetItem("FASES")->GetValue();
			Renglon->FASES = NumberToString(vPaso.intVal);
			vPaso = pRs->Fields->GetItem("FRECUENCIA")->GetValue();
			Renglon->FRECUENCIA = NumberToString(vPaso.intVal);
			vPaso = pRs->Fields->GetItem("INTERFASEAT")->GetValue();
			Renglon->INTERFASEAT = (LPCSTR)((_bstr_t)vPaso.bstrVal);
			vPaso = pRs->Fields->GetItem("INTERFASEBT")->GetValue();
			Renglon->INTERFASEBT = (LPCSTR)((_bstr_t)vPaso.bstrVal);
			vPaso = pRs->Fields->GetItem("NORMA")->GetValue();
			Renglon->NORMA = (LPCSTR)((_bstr_t)vPaso.bstrVal);
			vPaso = pRs->Fields->GetItem("NUMRADIDADORES")->GetValue();
			Renglon->NUMRADIDADORES = NumberToString(vPaso.intVal);
			vPaso = pRs->Fields->GetItem("PANTALLAELECTROSTATICA")->GetValue();
			Renglon->PANTALLAELECTROSTATICA = NumberToString(vPaso.intVal);
			vPaso = pRs->Fields->GetItem("PARIDAD")->GetValue();
			Renglon->PARIDAD = (LPCSTR)((_bstr_t)vPaso.bstrVal);
			vPaso = pRs->Fields->GetItem("SOBREELEV")->GetValue();
			Renglon->SOBREELEV = NumberToString(vPaso.intVal);
			vPaso = pRs->Fields->GetItem("TANQUECONSERVADOR")->GetValue();
			Renglon->TANQUECONSERVADOR = NumberToString(vPaso.intVal);
			vPaso = pRs->Fields->GetItem("TCS")->GetValue();
			Renglon->TCS = NumberToString(vPaso.intVal);
			vPaso = pRs->Fields->GetItem("TIPOBASE")->GetValue();
			Renglon->TIPOBASE = (LPCSTR)((_bstr_t)vPaso.bstrVal);
			vPaso = pRs->Fields->GetItem("TIPOENFTO")->GetValue();
			Renglon->TIPOENFTO = (LPCSTR)((_bstr_t)vPaso.bstrVal);
			vPaso = pRs->Fields->GetItem("TIPOGARGANTA")->GetValue();
			Renglon->TIPOGARGANTA = (LPCSTR)((_bstr_t)vPaso.bstrVal);
			vPaso = pRs->Fields->GetItem("TIPOTRANSFORMADOR")->GetValue();
			Renglon->TIPOTRANSFORMADOR = (LPCSTR)((_bstr_t)vPaso.bstrVal);
			vPaso = pRs->Fields->GetItem("ZONASISMICA")->GetValue();
			Renglon->ZONASISMICA = NumberToString(vPaso.intVal);
			return true;
		}
		else
		{
			return false;
		}
}
bool GrabarBD (char *nodis, char *BaseDatos, char *revision, tblPrototipo *Renglon)
{
	_ConnectionPtr pConn;
	_CommandPtr pCmd;
	_RecordsetPtr pRs;	
	std::string strSQL;
	std::string strconnection;
	std::string cadena;
	char *buffer;
	int i,j,k;
	i=0;

	strconnection = "DSN=" + std::string(BaseDatos) + ";DATABASE=baan" + std::string(BaseDatos) +";UID=pot_audi ;PWD=audi_pot;";
	strSQL = "SELECT * from prototipohero where nodiseno = '" + std::string(nodis) + "'";
	::CoInitialize(NULL);
	pConn.CreateInstance(__uuidof(Connection));
	pConn->ConnectionString = (strconnection.c_str());
	pConn->Open("","","",adConnectUnspecified);
	pCmd.CreateInstance(__uuidof(Command));
	pCmd->ActiveConnection = pConn;
	pCmd->CommandText = _bstr_t (strSQL.c_str());
	pRs.CreateInstance(__uuidof(Recordset));
	pRs->PutRefSource(pCmd);
	_variant_t vNull;
	vNull.vt = VT_ERROR;
	vNull.scode = DISP_E_PARAMNOTFOUND;
	pRs->Open(vNull, vNull, adOpenDynamic, adLockOptimistic, adCmdUnknown);
	_variant_t *vVariantCero = new _variant_t(0.0);
	_variant_t vPaso;

	if (pRs->EndOfFile)
	{
		strSQL = "insert into prototipohero values('" + Renglon->NODISENO + "','" + 
			Renglon->DISENADOR + "','" + 
			Renglon->KVAS + "','" + 
			Renglon->FASES + "','" + 
			Renglon->CLIENTE + "','" + 
			Renglon->CTEEVALNUC + "','" + 
			Renglon->CTEEVALDEV + "','" + 
			Renglon->PARIDAD + "','" + 
			Renglon->NORMA + "','" + 
			Renglon->FRECUENCIA + "','" + 
			Renglon->ALTITUD + "','" + 
			Renglon->SOBREELEV + "','" + 
			Renglon->TIPOENFTO + "','" + 
			Renglon->PANTALLAELECTROSTATICA + "','" + 
			Renglon->ZONASISMICA + "','" + 
			Renglon->TIPOTRANSFORMADOR + "','" + 
			Renglon->TIPOGARGANTA + "','" + 
			Renglon->INTERFASEAT + "','" + 
			Renglon->INTERFASEBT + "','" + 
			Renglon->BOQUILLATAPA + "','" + 
			Renglon->NUMRADIDADORES + "','" + 
			Renglon->TCS + "','" + 
			Renglon->TANQUECONSERVADOR + "','" + 
			Renglon->TIPOBASE + "','" + 
			Renglon->APOYOPARAGATO + "')";
		pCmd.CreateInstance(__uuidof(Command));
	pCmd->ActiveConnection = pConn;
	pCmd->CommandText = _bstr_t (strSQL.c_str());
	pRs.CreateInstance(__uuidof(Recordset));
	pRs->PutRefSource(pCmd);
	_variant_t vNull;
	vNull.vt = VT_ERROR;
	vNull.scode = DISP_E_PARAMNOTFOUND;
	pRs->Open(vNull, vNull, adOpenDynamic, adLockOptimistic, adCmdUnknown);
	_variant_t *vVariantCero = new _variant_t(0.0);
	}
return true;
}