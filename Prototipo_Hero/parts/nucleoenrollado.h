#ifndef NUCLEOENROLLADO_H_
#define NUCLEOENROLLADO_H_
#include "GBox.h"
#include "GInputs.h"
#include "GRoundedHollowBox.h"
namespace NSTransformador{
	using namespace std;
	using namespace Hero;
	class NucleoEnrollado:public GBox{
		public:
			double espesor;
			double anchoLamina;
			double alturaVentana;
			double alturaNucleo;
			double anchoVentanaDonaChica;
			double anchoDonaChica;
			double anchoVentanaDonaGrande;
			double anchoDonaGrande;
			double separacionDonas;
			GRoundedHollowBox *donaChicaIzquierda;
			GRoundedHollowBox *donaIzquierda;
			GRoundedHollowBox *donaDerecha;
			GRoundedHollowBox *donaChicaDerecha;

			NucleoEnrollado(GBox &parent):GBox(parent){
				createPart(donaChicaIzquierda, GRoundedHollowBox);
				createPart(donaIzquierda, GRoundedHollowBox);
				createPart(donaDerecha, GRoundedHollowBox);
				createPart(donaChicaDerecha, GRoundedHollowBox);
				espesor=.200;
				anchoLamina=1.500;
				alturaVentana=.900;
				alturaNucleo=alturaVentana+2*espesor;
				anchoDonaChica=.900;
				anchoVentanaDonaChica=anchoDonaChica-2*espesor;
				anchoDonaGrande=1.200;
				anchoVentanaDonaGrande=anchoDonaGrande-2*espesor;
				separacionDonas=.005;
				this->setHeight(alturaNucleo);
				this->setLength(anchoLamina);
				this->setWidth(2*(anchoDonaGrande+anchoDonaChica)+3*separacionDonas);
				this->setHidden(true);
				this->setBindParts(true);
			}

			void asignarDimensiones(const GInputs &inputs){
				this->espesor=inputs.getReal("dANucleo",espesor);
				this->anchoLamina=inputs.getReal("dBNucleo",anchoLamina);
				this->alturaVentana=inputs.getReal("dC",alturaVentana);
				this->alturaNucleo=inputs.getReal("dH", alturaNucleo);
				this->anchoVentanaDonaChica=inputs.getReal("dD1", anchoVentanaDonaChica);
				this->anchoDonaChica=inputs.getReal("dF1", anchoDonaChica);
				this->anchoVentanaDonaGrande=inputs.getReal("dD2", anchoVentanaDonaGrande);
				this->anchoDonaGrande=inputs.getReal("dF2", anchoDonaGrande);
				this->separacionDonas=inputs.getReal("dEspSepEntreDonas", separacionDonas);
				this->setHeight(alturaNucleo);
				this->setLength(anchoLamina);
				this->setWidth(2*(anchoDonaGrande+anchoDonaChica)+3*separacionDonas);
			}

			void obtenerDimensiones(GInputs &inputs){
				inputs.addInput<double>("dANucleo", espesor);
				inputs.addInput<double>("dBucleo", anchoLamina);
				inputs.addInput<double>("dC", alturaVentana);
				inputs.addInput<double>("dD1", anchoVentanaDonaChica);
				inputs.addInput<double>("dF1", anchoDonaChica);
				inputs.addInput<double>("dD2", anchoVentanaDonaGrande);
				inputs.addInput<double>("dF2", anchoDonaGrande);
				inputs.addInput<double>("dEspSepEntreDonas", separacionDonas);
			}

			void defParts(void){
				donaChicaIzquierda->beginDefinition();
					donaChicaIzquierda->setInnerHeight(alturaVentana);
					donaChicaIzquierda->setInnerWidth(anchoVentanaDonaChica);
					donaChicaIzquierda->setThickness(espesor);
					donaChicaIzquierda->setWidth(anchoDonaChica);
					donaChicaIzquierda->setHeight(alturaNucleo);
					donaChicaIzquierda->setLength(anchoLamina);
					donaChicaIzquierda->position(LEFT,0);
				donaChicaIzquierda->endDefinition();

				donaIzquierda->beginDefinition();
					donaIzquierda->setInnerHeight(alturaVentana);
					donaIzquierda->setInnerWidth(anchoVentanaDonaGrande);
					donaIzquierda->setThickness(espesor);
					donaIzquierda->setWidth(anchoDonaGrande);
					donaIzquierda->setHeight(alturaNucleo);
					donaIzquierda->setLength(anchoLamina);
					donaIzquierda->position(ONRIGHT,separacionDonas,*donaChicaIzquierda);
				donaIzquierda->endDefinition();

				donaDerecha->beginDefinition();
					donaDerecha->setInnerHeight(alturaVentana);
					donaDerecha->setInnerWidth(anchoVentanaDonaGrande);
					donaDerecha->setThickness(espesor);
					donaDerecha->setWidth(anchoDonaGrande);
					donaDerecha->setHeight(alturaNucleo);
					donaDerecha->setLength(anchoLamina);
					donaDerecha->position(ONRIGHT,separacionDonas,*donaIzquierda);
				donaDerecha->endDefinition();

				donaChicaDerecha->beginDefinition();
					donaChicaDerecha->setInnerHeight(alturaVentana);
					donaChicaDerecha->setInnerWidth(anchoVentanaDonaChica);
					donaChicaDerecha->setThickness(espesor);
					donaChicaDerecha->setWidth(anchoDonaChica);
					donaChicaDerecha->setHeight(alturaNucleo);
					donaChicaDerecha->setLength(anchoLamina);
					donaChicaDerecha->position(ONRIGHT,separacionDonas,*donaDerecha);
				donaChicaDerecha->endDefinition();
			}
	};
}

#endif