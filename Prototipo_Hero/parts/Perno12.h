#ifndef PERNO12_H_
#define PERNO12_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "GPartialHollowCylinder.h"
#include "Definiciones.h"

namespace NSTransformador{
	using namespace Hero;
	class Perno12: public GBox{
		public:
			GPartialHollowCylinder *Cilindro;

double DiametroPerno;
double Height;
double Length;
enum TipoArticulo tArt;
enum TipoConector tConector;
			Perno12(GBox &parent):GBox(parent){

				createPart(Cilindro, GPartialHollowCylinder);

//DiametroPerno = millimeter(19.05);

				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}

			void asignarArticulo(enum TipoArticulo tArt){
				this->tArt=tArt;
				actualizarDimensiones();
			}

			void asignarDiametroPerno(double DiametroPerno){
				this->DiametroPerno=DiametroPerno;
				actualizarDimensiones();
			}

			void actualizarDimensiones(){
				this->setWidth(DiametroPerno);
				this->setHeight(DiametroPerno);
				this->setLength(Length);
			}
			void defSelf(void){
			}

			void asignarDimensiones(const GInputs &inputs){
				this->DiametroPerno=inputs.getReal("DiametroPerno",DiametroPerno);
				actualizarDimensiones();
			}
			void obtenerDimensiones(GInputs &inputs){
				inputs.addInput<double>("DiametroPerno", DiametroPerno);
			}
			void defParts(void){
				actualizarDimensiones();

				Cilindro->beginDefinition();
					Cilindro->setInnerRadius(0);
					Cilindro->setRadius(DiametroPerno/2);
					Cilindro->setHeight((tArt== B325) ? millimeter(83.9) : millimeter(44.75));
					Cilindro->rotate(RIGHT,DEGREES(90));
					Cilindro->setStartAngle(DEGREES(0));
					Cilindro->setSweepAngle(DEGREES(360));
				Cilindro->endDefinition();
				
			}
	};
}

#endif
