#ifndef PEINESTX_H_
#define PEINESTX_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "FuncionesComunes.h"
#include "Definiciones.h"
namespace NSTransformador{
	using namespace Hero;
	class PeinesTX: public GBox{
		public:
			GBox *Placa1;
			GBox *Placa2;
			GBox *Placa3;
			GBox *Placa4;
			GBox *Placa5;
			double AnchoPeine;
			double AltoPeine;
			double LargoPeine;
			enum TipoConexion tConexionBt;
			bool bLadosLargos;
			int Ran1;
			int Ran2;
			int Ran4;
			int Cte;
			enum TipoArmado tDimArm;
			int RanuraPeineK;
			int SeparadorRP;
			int b20;
			enum TipoNucleo tTipoNucleo;
			int RanuraPeineRG;
			double NumeroLaminas;
			double EspesorPunta;
			double EspesorCarton;
			double AltoRanura;
			int LargoRanura;
			int LargoRanura1;
			double PeineNum;
			double LARGObq;
			double PositionPlaca1;
			double PositionPlaca2;
			double PositionPlaca3;
			double length1;
			double length2;
			double length3;
			double length4;
			int NumPlacas;

			PeinesTX(GBox &parent):GBox(parent){
				createPart(Placa1, GBox);
				createPart(Placa2, GBox);
				createPart(Placa3, GBox);
				createPart(Placa4, GBox);
				createPart(Placa5, GBox);

				AnchoPeine = 50;
				AltoPeine = 90;
				tConexionBt = DELTA;
				bLadosLargos = false;
				tDimArm = STB0922;
				tTipoNucleo = ENROLLADO;
				NumeroLaminas = 16;
				EspesorPunta = 0.4;
				EspesorCarton = 1.6;
				AltoRanura = 60;
				PeineNum = 1;



				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}
			void asignarAnchoPeine(double AnchoPeine){
				this->AnchoPeine=AnchoPeine;
				actualizarDimensiones();
			}
			void asignarAltoPeine(double AltoPeine){
				this->AltoPeine=AltoPeine;
				actualizarDimensiones();
			}
			void asignartConexionBt(enum TipoConexion tConexionBt){
				this->tConexionBt=tConexionBt;
				actualizarDimensiones();
			}
			void asignarbLadosLargos(bool bLadosLargos){
				this->bLadosLargos=bLadosLargos;
				actualizarDimensiones();
			}
			void asignartDimArm(enum TipoArmado tDimArm){
				this->tDimArm=tDimArm;
				actualizarDimensiones();
			}
			void asignartTipoNucleo(enum TipoNucleo tTipoNucleo){
				this->tTipoNucleo=tTipoNucleo;
				actualizarDimensiones();
			}
			void asignarNumeroLaminas(double NumeroLaminas){
				this->NumeroLaminas=NumeroLaminas;
				actualizarDimensiones();
			}
			void asignarEspesorPunta(double EspesorPunta){
				this->EspesorPunta=EspesorPunta;
				actualizarDimensiones();
			}
			void asignarEspesorCarton(double EspesorCarton){
				this->EspesorCarton=EspesorCarton;
				actualizarDimensiones();
			}
			void asignarAltoRanura(double AltoRanura){
				this->AltoRanura=AltoRanura;
				actualizarDimensiones();
			}
			void asignarPeineNum(double PeineNum){
				this->PeineNum=PeineNum;
				actualizarDimensiones();
			}
			//void asignarDimensiones(const GInputs &inputs){
			//	/*AnchoPeine=inputs.getReal("AnchoPeine",AnchoPeine);
			//	AltoPeine=inputs.getReal("AltoPeine",AltoPeine);
			//	NumeroLaminas=inputs.getReal("NumeroLaminas",NumeroLaminas);
			//	EspesorPunta=inputs.getReal("EspesorPunta",EspesorPunta);
			//	EspesorCarton=inputs.getReal("EspesorCarton",EspesorCarton);
			//	AltoRanura=inputs.getReal("AltoRanura",AltoRanura);
			//	PeineNum=inputs.getReal("PeineNum",PeineNum);

			//	tConexionBt=(TipoConexion)inputs.getInteger("tConexionBt",tConexionBt);
			//	tDimArm=(TipoArmado)inputs.getInteger("tDimArm",tDimArm);
			//	tTipoNucleo=(TipoNucleo)inputs.getInteger("tTipoNucleo",tTipoNucleo);
			//	bLadosLargos=inputs.getBoolean("bLadosLargos",bLadosLargos);*/
			//}

			//void obtenerDimensiones(GInputs &inputs){
			//	/*inputs.addInput<double>("AnchoPeine", AnchoPeine);
			//	inputs.addInput<double>("AltoPeine", AltoPeine);
			//	inputs.addInput<double>("NumeroLaminas", NumeroLaminas);
			//	inputs.addInput<double>("EspesorPunta", EspesorPunta);
			//	inputs.addInput<double>("EspesorCarton", EspesorCarton);
			//	inputs.addInput<double>("AltoRanura", AltoRanura);
			//	inputs.addInput<double>("PeineNum", PeineNum);

			//	inputs.addInput<int>("tConexionBt",tConexionBt);
			//	inputs.addInput<int>("tDimArm",tDimArm);
			//	inputs.addInput<int>("tTipoNucleo",tTipoNucleo);
			//	inputs.addInput<bool>("bLadosLargos",bLadosLargos);*/

			//}


			void actualizarDimensiones(){
				
				Cte = (NumeroLaminas > 8)? 1:2;
				Ran1 = ROUND((NumeroLaminas * EspesorPunta)  + 2 * EspesorCarton + Cte) + 5;
				Ran2 = ROUND((NumeroLaminas * EspesorPunta * 2)  + 2 * EspesorCarton + Cte) + 5;
				Ran4 = ROUND((NumeroLaminas * EspesorPunta * 4)  + 2 * EspesorCarton + Cte);
				b20 = (tTipoNucleo == ENROLLADO)? 80:140;
				RanuraPeineK  = ROUND(NumeroLaminas * EspesorPunta + 5);
				SeparadorRP = ROUND((bLadosLargos)?(NumeroLaminas * EspesorPunta * 2.5 + 28): 15);
				RanuraPeineRG = ROUND(NumeroLaminas * EspesorPunta * 2 + 5);


				if(tConexionBt == DELTA){
					if(bLadosLargos)
						LargoPeine = 2 * Ran1 + Ran2 + 150;
					else
						LargoPeine = 135 + 40 + Ran4 + Ran2;
				}
				else{
					if(bLadosLargos){
						if(tDimArm == STB0256)
							LargoPeine = 3 * RanuraPeineK + 150;
						else
							LargoPeine = SeparadorRP + ((tDimArm == STB0256)?135:110) + 3 * RanuraPeineK;
					}
					else
						LargoPeine = 2 * RanuraPeineK + SeparadorRP + 15 + 20 + b20 + RanuraPeineRG;

				}


				if(tConexionBt == DELTA){
					if(bLadosLargos){
						if(PeineNum == 1){
							LargoRanura1 = Ran2;
							LargoRanura = Ran1;
						}
						else{
							LargoRanura1 = Ran1;
							LargoRanura = Ran1;
						}
					}
					else{
						LargoRanura1 = Ran4;
						LargoRanura = Ran2;
					}
				}
				else{
					if(bLadosLargos == false){
					LargoRanura1 = RanuraPeineRG;
					LargoRanura = RanuraPeineK;
					}
					else{
						LargoRanura1 = RanuraPeineK;
						LargoRanura = RanuraPeineK;
					}

				}


			if(tConexionBt == DELTA){
				if(bLadosLargos){
					if(PeineNum == 1)
						NumPlacas =  3;
					else
						NumPlacas = 2;
				}
				else
					NumPlacas = 2;
			}
			else{
				if(bLadosLargos)
					NumPlacas =  3;
				else
					NumPlacas = 3;
					}


			if(tConexionBt == DELTA){
				if((bLadosLargos) && (PeineNum == 1)){
					PositionPlaca1 = 0;
					PositionPlaca2 = Ran2 + 15;
					PositionPlaca3 = Ran2 + Ran2 + 30;
				}
				else if((bLadosLargos) && (PeineNum == 2)){
					PositionPlaca1 = 0;
					PositionPlaca2 = Ran2 + 50;
					PositionPlaca3 = Ran2 + 50;
				}
				else if(bLadosLargos == false){
					PositionPlaca1 = 0;
					PositionPlaca2 = Ran4 + 15;
					PositionPlaca3 = Ran4 + 15;
				}
			}
			else{
				if(bLadosLargos){
					PositionPlaca1 = RanuraPeineK + 15;
						if(NumPlacas == 3)
							PositionPlaca2 = RanuraPeineK + 15;
						else
							PositionPlaca2 = RanuraPeineK * 2 + SeparadorRP + 15;

					PositionPlaca3 = RanuraPeineK * 2 + SeparadorRP + 15;
				}
				else{
					PositionPlaca1 = RanuraPeineRG + 15;
						if(NumPlacas == 3)
							PositionPlaca2 = RanuraPeineRG + 15;
						else
							PositionPlaca2 = RanuraPeineRG + RanuraPeineK  + SeparadorRP + 15;
					PositionPlaca3 = RanuraPeineRG + RanuraPeineK  + SeparadorRP + 15;
				}
			}
				
length1 =20;
length2 = PositionPlaca2 - LargoRanura1;
if(NumPlacas == 3)
length3 = PositionPlaca3 - PositionPlaca2 - LargoRanura;
length4 = LargoPeine - PositionPlaca3 - LargoRanura - 20;

				this->setWidth(millimeter(AnchoPeine));
				this->setHeight(millimeter(AltoPeine));
				this->setLength(millimeter(LargoPeine));
			}
			void defSelf(void){
				actualizarDimensiones();
			}

			
			void defParts(void){
				actualizarDimensiones();

				Placa1->beginDefinition();
					Placa1->setWidth(millimeter(AnchoPeine));
					Placa1->setHeight(millimeter(AltoPeine - AltoRanura));
					Placa1->setLength(millimeter(LargoPeine));
					Placa1->position(BOTTOM,0);
				Placa1->endDefinition();

				Placa2->beginDefinition();
					Placa2->setWidth(millimeter(AnchoPeine));
					Placa2->setHeight(millimeter(AltoRanura));
					Placa2->setLength(millimeter(length1));
					Placa2->position(ABOVE,0,*Placa1);
					Placa2->position(FRONT,0);

				Placa2->endDefinition();

				Placa3->beginDefinition();
					Placa3->setWidth(millimeter(AnchoPeine));
					Placa3->setHeight(millimeter(AltoRanura));
					Placa3->setLength(millimeter(length2));
					Placa3->position(ABOVE,0,*Placa1);
					Placa3->position(FRONT,millimeter(20 + LargoRanura1));

				Placa3->endDefinition();

				Placa4->beginDefinition();
					Placa4->setWidth(millimeter(AnchoPeine));
					Placa4->setHeight(millimeter( AltoRanura));
					Placa4->setLength(millimeter(length3));
					Placa4->position(ABOVE,0,*Placa1);
					Placa4->position(REAR,millimeter(length4 + LargoRanura));

				Placa4->endDefinition();

				Placa5->beginDefinition();
					Placa5->setWidth(millimeter(AnchoPeine));
					Placa5->setHeight(millimeter(AltoRanura));
					Placa5->setLength(millimeter(length4));
					Placa5->position(ABOVE,0,*Placa1);
					Placa5->position(REAR,0);

				Placa5->endDefinition();

				
			}
	};
}

#endif
