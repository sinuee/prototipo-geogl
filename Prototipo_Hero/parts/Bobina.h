#ifndef BOBINA_H_
#define BOBINA_H_
#include "GRoundedHollowBox.h"
#include "GBox.h"
namespace NSTransformador{
	using namespace Hero;
	class Bobina:public GRoundedHollowBox{
		public:
			double alturaRadialLV;
			double alturaAxial;
			double alturaVentana;
			double obtenerAlturaRadialLV(void){return thickness;}
//			double obtenerAlturaRadialLPBT(void){return topThickness;}
//			double obtenerAlturaRadialLPAT(void){return bottomThickness;}
			double obtenerAncho(void){return width;}
			double obtenerLargo(void){return length;}
			double obtenerAlturaAxial(void){return height;}
			double obtenerAlturaVentana(void){return innerHeight;}
			double obtenerAnchoVentana(void){return innerWidth;}

			void asignarAlturaRadialLV(double _alturaRadialLV){
				alturaRadialLV = _alturaRadialLV;
				setInnerWidth(alturaRadialLV);
			}
			/*void asignarAlturaRadialLPBT(double alturaRadial){setTopThickness(alturaRadial);}
			void asignarAlturaRadialLPAT(double alturaRadial){setBottomThickness(alturaRadial);}*/
			void asignarAncho(double ancho){setWidth(ancho);}
			void asignarLargo(double largo){setLength(largo);}
			void asignarAlturaAxial(double _alturaAxial){
				alturaAxial = _alturaAxial;
				setHeight(alturaAxial);}
			void asignarAlturaVentana(double _alturaVentana){
				alturaVentana = _alturaVentana;
				setInnerHeight(alturaVentana);}
			void asignarAnchoVentana(double anchoVentana){setInnerWidth(anchoVentana);}

			Bobina(GBox &parent):GRoundedHollowBox(parent){
				alturaVentana = 2;
				alturaRadialLV = 1;
				alturaAxial = 4;
			}

			void defSelf(void){

				//valores por default
				innerHeight=feet(alturaVentana);
				innerWidth=feet(alturaRadialLV);
				thickness=feet(0.5);
//				topThickness=getThickness()*1.8;
//				bottomThickness=getThickness()*0.2;
				height=innerHeight/*+topThickness+bottomThickness*/;
				width=innerWidth+2*thickness;
				length=feet(alturaAxial);
			}

			void asignarDimensiones(const GInputs &inputs){
				innerHeight=inputs.getReal("dLadoLargoMolde",innerHeight);
				innerWidth=inputs.getReal("dLadoCortoMolde",innerWidth);
				thickness=inputs.getReal("dDimRadialLV",thickness);
				//topThickness=inputs.getReal("dDimRadialLPBT",topThickness);
				//bottomThickness=inputs.getReal("dDimRadialLPAT",bottomThickness);
				height=inputs.getReal("dAltBobina",height);
				width=inputs.getReal("dAnchoBobina",width);
				length=inputs.getReal("dLargoBobina",length);
			}

			void obtenerDimensiones(GInputs &inputs){
				inputs.addInput<double>("dLadoLargoMolde", innerHeight);
				inputs.addInput<double>("dLadoCortoMolde", innerWidth);
				inputs.addInput<double>("dDimRadialLV", thickness);
				//inputs.addInput<double>("dDimRadialLPBT", topThickness);
				//inputs.addInput<double>("dDimRadialLPAT", bottomThickness);
				inputs.addInput<double>("dAltBobina", height);
				inputs.addInput<double>("dAnchoBobina", width);
				inputs.addInput<double>("dLargoBobina", length);
			}
			void actualizarDimensiones()
			{
				innerHeight = feet(alturaVentana);
				innerWidth = feet(alturaRadialLV/2);
				length = feet(alturaAxial);
				this->defSelf();
			}
	};
};
#endif
