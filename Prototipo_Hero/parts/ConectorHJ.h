#ifndef ConectorHJ_H_
#define ConectorHJ_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "GPartialHollowCylinder.h"
#include "Definiciones.h"

namespace NSTransformador{
	using namespace Hero;
	class ConectorHJ: public GBox{
		public:
			GBox *Placa;
			GPartialHollowCylinder *Cilindro;
double length;
double Width;
double Height;
double Radio;
double Largo;
enum TipoArticulo tArt;
			ConectorHJ(GBox &parent):GBox(parent){
				createPart(Placa, GBox);
				createPart(Cilindro, GPartialHollowCylinder);

//DiametroPerno = millimeter(19.05);

				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}
			void asignarArticulo(enum TipoArticulo tArt){
				this->tArt=tArt;
				actualizarDimensiones();
			}
				void actualizarDimensiones(){
				this->setLength(Largo);
				this->setWidth(Width);
				this->setHeight(Height);
			}


			void defParts(void){
				actualizarDimensiones();



if (tArt==CH77)
	length = millimeter(161.93);
else if (tArt==CH76)
	length =millimeter(105.28);
else
	length =0;

if (tArt==CH77)
	Width = millimeter(25.4);
else if (tArt==CH76)
	Width =millimeter(12.7);
else
	Width =0;

if (tArt==CH77)
	Height = millimeter(152.4);
else if (tArt==CH76)
	Height =millimeter(101.6);
else
	Height =0;

if (tArt==CH77)
{
	Radio = millimeter(60.81)/2;
	Largo = millimeter(464.56) - length;;
}
else if (tArt==CH76)
{
	Radio = millimeter(48.11)/2;
	Largo = millimeter(410.46) - length;
}
else
{
	Radio = millimeter(0)/2;
	Largo = millimeter(0) - length;
}



    //:position (:in-rear (:from (the :placa) 0))



				Placa->beginDefinition();
					Placa->setLength(length);
					Placa->setWidth(Width);
					Placa->setHeight(Height);
				Placa->endDefinition();

				Cilindro->beginDefinition();
					Cilindro->setInnerRadius(0);
					Cilindro->setRadius(Radio);
					Cilindro->setHeight(Largo);
					Cilindro->rotate(RIGHT,DEGREES(90));
					Cilindro->setStartAngle(DEGREES(0));
					Cilindro->setSweepAngle(DEGREES(360));
					Cilindro->position(INREAR,0,*Placa);
				Cilindro->endDefinition();
				
			}
	};
}

#endif
