#ifndef ANGULODEPLACA_H_
#define ANGULODEPLACA_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "Definiciones.h"
namespace NSTransformador{
	using namespace Hero;
	class AnguloDePlaca: public GBox{
		public:
			GBox *Parte2;
			GBox *Parte3;

			bool bZonaSismica;
			double Espesor;
			double Width;
			double Length;
			double Height;


			AnguloDePlaca(GBox &parent):GBox(parent){
				createPart(Parte2, GBox);
				createPart(Parte3, GBox);

						Width = millimeter(40);
						Length = millimeter(40);
						Height = millimeter(70);
						Espesor = millimeter(6.35);
						bZonaSismica = false;

		
					
				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}

			void asignarZonaSismica(bool bZonaSismica){
				this->bZonaSismica=bZonaSismica;
				actualizarDimensiones();
			}


			void actualizarDimensiones(){
				if (bZonaSismica==true)
					Height = millimeter(135);
				else
					Height = millimeter(95);

				this->setWidth(Width);
				this->setHeight(Height);
				this->setLength(Length);
			}
			void defSelf(void){
			}

			void asignarDimensiones(const GInputs &inputs){
				this->Width=inputs.getReal("Width",Width);
				this->Length=inputs.getReal("Length",Length);
				this->Height=inputs.getReal("Height",Height);
				this->Espesor=inputs.getReal("Espesor", Espesor);

				actualizarDimensiones();
			}
			void obtenerDimensiones(GInputs &inputs){
				inputs.addInput<double>("Width", Width);
				inputs.addInput<double>("Length", Length);
				inputs.addInput<double>("Height", Height);
				inputs.addInput<double>("Espesor", Espesor);

			}
			void defParts(void){
				actualizarDimensiones();

				Parte2->beginDefinition();
					Parte2->setWidth(Width);
					Parte2->setHeight(Height);
					Parte2->setLength(Espesor);
					Parte2->position(FRONT, 0);
				Parte2->endDefinition();

				Parte3->beginDefinition();
					Parte3->setWidth(Width);
					Parte3->setLength(Length);
					Parte3->setHeight(Espesor);
					Parte3->position(BOTTOM,0);

				Parte3->endDefinition();


				/*Parte2->beginDefinition();
					Parte2->setWidth(Espesor);
					Parte2->position(LEFT, 0);
				Parte2->endDefinition();

				Parte3->beginDefinition();
					Parte3->setLength(Espesor);
					Parte3->position(REAR,0);
				Parte3->endDefinition();*/


			}
	};
}

#endif
