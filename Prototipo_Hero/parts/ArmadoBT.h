#ifndef ARMADOBT_H_
#define ARMADOBT_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "PeinesTX.h"
#include "HerrajeSuperior.h"
#include "Definiciones.h"
namespace NSTransformador{
	using namespace Hero;
	class ArmadoBT: public GBox{

		public:
			HerrajeSuperior *HerrSup;
			PeinesTX *PeineLL[2];
			PeinesTX *PeineLC;
			GBox *qBox;
			AnguloDePlaca *angulo3[3];

			bool bZonaSismica;
			double kvas;
			enum TipoConductor tTipoConductorBT;
			bool bLadosCortos;
			bool bPedestal;
			double BoqArreglo;
			bool bBoqEnTapa;
			enum TipoConexion tTipoConexion;
			enum TipoNucleo tTipoNucleo;

			ArmadoBT(GBox &parent):GBox(parent){
				createPart(HerrSup, HerrajeSuperior);
				createPart(PeineLC, PeinesTX);
				createPart(PeineLL[0], PeinesTX);
				createPart(PeineLL[1], PeinesTX);
				createPart(qBox, GBox);
				
				createPart(angulo3[0], AnguloDePlaca);
				createPart(angulo3[1], AnguloDePlaca);
				createPart(angulo3[2], AnguloDePlaca);

			kvas = millimeter(2000);
			bZonaSismica = true;
		    bLadosCortos = true;
			bPedestal = false;
			BoqArreglo = 1;/////////////
			bBoqEnTapa= false;//////////////
	
			tTipoConexion = ESTRELLA;
			tTipoNucleo = ENROLLADO;

				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}

			void asignartTipoConductorBT(enum TipoConductor tTipoConductorBT){
				this->tTipoConductorBT=tTipoConductorBT;
				actualizarDimensiones();
			}

			void asignarbLadosCortos(bool bLadosCortos){
				this->bLadosCortos=bLadosCortos;
				actualizarDimensiones();
			}

			void asignarbPedestal(bool bPedestal){
				this->bPedestal=bPedestal;
				actualizarDimensiones();
			}


			void asignartTipoConexion(enum TipoConexion tTipoConexion){
				this->tTipoConexion=tTipoConexion;
				actualizarDimensiones();
			}

			void asignartTipoNucleo(enum TipoNucleo tTipoNucleo){
				this->tTipoNucleo=tTipoNucleo;
				actualizarDimensiones();
			}

			void asignarbZonaSismica(bool bZonaSismica){
				this->bZonaSismica=bZonaSismica;
				actualizarDimensiones();
			}


			void asignarkvas(double kvas){
				this->kvas=kvas;
				actualizarDimensiones();
			}

			void asignarBoqArreglo(double BoqArreglo){
				this->BoqArreglo=BoqArreglo;
				actualizarDimensiones();
			}

			void asignarbBoqEnTapa(bool bBoqEnTapa){
				this->bBoqEnTapa=bBoqEnTapa;
				actualizarDimensiones();
			}

			void actualizarDimensiones(){
				
				this->setWidth(millimeter(1362)); 
				this->setHeight(millimeter(95.35));
				this->setLength(millimeter(324.7));
			}

			
			//void asignarDimensiones(const GInputs &inputs){
			//	/*kvas=inputs.getReal("kvas",kvas);
			//	BoqArreglo=inputs.getReal("BoqArreglo",BoqArreglo);
			//	tTipoConductorBT=(TipoConductor)inputs.getInteger("tTipoConductorBT",tTipoConductorBT);
			//	tTipoConexion=(TipoConexion)inputs.getInteger("tTipoConexion",tTipoConexion);
			//	tTipoNucleo=(TipoNucleo)inputs.getInteger("tTipoNucleo",tTipoNucleo);
			//	bLadosCortos=inputs.getBoolean("bLadosCortos",bLadosCortos);
			//	bPedestal=inputs.getBoolean("bPedestal",bPedestal);
			//	bBoqEnTapa=inputs.getBoolean("bBoqEnTapa",bBoqEnTapa);
			//	bZonaSismica=inputs.getBoolean("bZonaSismica",bZonaSismica);*/
			//}

			//void obtenerDimensiones(GInputs &inputs){
			//	/*inputs.addInput<double>("kvas", kvas);
			//	inputs.addInput<double>("BoqArreglo", BoqArreglo);
			//	inputs.addInput<int>("tTipoConductorBT",tTipoConductorBT);
			//	inputs.addInput<int>("tTipoConexion",tTipoConexion);
			//	inputs.addInput<int>("tTipoNucleo",tTipoNucleo);
			//	inputs.addInput<bool>("bLadosCortos",bLadosCortos);
			//	inputs.addInput<bool>("bPedestal",bPedestal);
			//	inputs.addInput<bool>("bBoqEnTapa",bBoqEnTapa);
			//	inputs.addInput<bool>("bZonaSismica",bZonaSismica);*/

			//}


			void defSelf(void){
				actualizarDimensiones();
			}

			void defParts(void){
				actualizarDimensiones();

				HerrSup->beginDefinition();
					//HerrSup->asignarDimensiones(GXMLInputs("C:\\HerrajeSuperiorS2715Test.xml"));

				HerrSup->endDefinition();

				PeineLC->beginDefinition();
					//PeineLC->asignarDimensiones(GXMLInputs("C:\\PeineLCS2715Test.xml"));
					PeineLC->rotate(TOP,DEGREES(270));
					PeineLC->position(ABOVE,millimeter(45),*HerrSup);
					PeineLC->translate(getNormal(LEFT), millimeter(483.5));
					PeineLC->position(REAR,millimeter(209.0254));
				PeineLC->endDefinition();


				qBox->beginDefinition();
					qBox->setWidth(millimeter(485.9));
					qBox->setLength(millimeter(202));
					qBox->setHeight(millimeter(90));
					qBox->setHidden(true);
				qBox->endDefinition();

				PeineLL[0]->beginDefinition();
//					PeineLL[0]->asignarDimensiones(GXMLInputs("C:\\PeineLLS2715Test.xml"));
					PeineLL[0]->position(LEFT,0,*qBox);
					PeineLL[0]->position(ABOVE,millimeter(45),*HerrSup);
					PeineLL[0]->position(INFRONT,millimeter(-50),*HerrSup);
				PeineLL[0]->endDefinition();

				PeineLL[1]->beginDefinition();
//					PeineLL[1]->asignarDimensiones(GXMLInputs("C:\\PeineLLS2715Test.xml"));
					PeineLL[1]->position(RIGHT,0,*qBox);
					PeineLL[1]->position(ABOVE,millimeter(45),*HerrSup);
					PeineLL[1]->position(INFRONT,millimeter(-50),*HerrSup);
				PeineLL[1]->endDefinition();

			
			for(int i=0;i<3;i++)
			{
				angulo3[i]->asignarZonaSismica(bZonaSismica);
				angulo3[i]->beginDefinition();
				angulo3[i]->position(ABOVE,0,*HerrSup);
			}
				angulo3[1]->rotate(TOP,DEGREES(180));
				angulo3[2]->rotate(TOP,DEGREES(270));


				angulo3[0]->position(RIGHT,0,*PeineLC);
				angulo3[0]->position(INREAR,0,*PeineLC);
				angulo3[1]->position(ONRIGHT,0,*PeineLL[0]);
				angulo3[1]->position(REAR,0,*PeineLL[0]);
				angulo3[2]->position(ONRIGHT,0,*PeineLL[1]);
				angulo3[2]->position(REAR,0,*PeineLL[1]);

			for(int i=0;i<3;i++)
				{
				angulo3[i]->endDefinition();
			}

if((bPedestal == true) || (tTipoConductorBT == SOLERA) || ((BoqArreglo == 1) && (bBoqEnTapa == true) && (tTipoConductorBT == LAMINA)))
{
		angulo3[0]->setHidden(true);
		angulo3[1]->setHidden(true);
		angulo3[2]->setHidden(true);
		angulo3[3]->setHidden(true);
}
else{
	if(bLadosCortos == false)
	{
		angulo3[0]->setHidden(true);
		angulo3[1]->setHidden(true);
		angulo3[2]->setHidden(true);
		angulo3[3]->setHidden(true);
	}
	else
	{
		if ((kvas <millimeter(2000)) && (tTipoConexion!=ESTRELLA) && (tTipoNucleo != ENROLLADO))
		{
			angulo3[3]->setHidden(true);
		}
	}
}

				
			}
	};
}

#endif
