#ifndef HERRAJESUPERIOR_H_
#define HERRAJESUPERIOR_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "Canal.h"
#include "Definiciones.h"

namespace NSTransformador{
	using namespace Hero;
	class HerrajeSuperior: public GBox{
		public:
			Canal *canal3;
			Canal *canal4;
			GBox *qbox;

			double EspesorNucleo;
			double kvas;
			double dl;
			enum TipoAparato tTipoAparato;
			enum TipoConductor tTipoConductorBT;
			enum TipoArticuloHerraje tArt;
			double width;
			double length;
			double height;
			double LitHerr0;
			double LitHerr8;
			bool bLadosCortos;
			bool bPedestal;
			double BoqArreglo;
			bool bBoqEnTapa;
			enum CambiadorAncho tCambiador;
			enum TipoConexion tTipoConexion;
			enum TipoNucleo tTipoNucleo;
			double F2;

			double espesor;
			double longitudCanal;
			double alturaCanal;
			double anchoInterior;
			bool bZonaSismica;

			HerrajeSuperior(GBox &parent):GBox(parent){
				createPart(canal3, Canal);
				createPart(canal4, Canal);
				createPart(qbox,GBox);
					
				espesor = 0.05;//millimeter(6.35);
				longitudCanal = 0.9;//millimeter(1362.79);
				alturaCanal = millimeter(95.35);
				anchoInterior = millimeter(324.7);
				length=longitudCanal;
				width=2.5*longitudCanal;
				height=anchoInterior;
			
				this->isReference = true;
				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}

			void asignartTipoConductorBT(enum TipoConductor tTipoConductorBT){
				this->tTipoConductorBT=tTipoConductorBT;
				actualizarDimensiones();
			}

			void asignartArt(enum TipoArticuloHerraje tArt){
				this->tArt=tArt;
				actualizarDimensiones();
			}

			void asignarbLadosCortos(bool bLadosCortos){
				this->bLadosCortos=bLadosCortos;
				actualizarDimensiones();
			}

			void asignarbPedestal(bool bPedestal){
				this->bPedestal=bPedestal;
				actualizarDimensiones();
			}

			void asignartCambiador(enum CambiadorAncho tCambiador){
				this->tCambiador=tCambiador;
				actualizarDimensiones();
			}

			void asignartTipoConexion(enum TipoConexion tTipoConexion){
				this->tTipoConexion=tTipoConexion;
				actualizarDimensiones();
			}

			void asignartTipoNucleo(enum TipoNucleo tTipoNucleo){
				this->tTipoNucleo=tTipoNucleo;
				actualizarDimensiones();
			}

			void asignarbZonaSismica(bool bZonaSismica){
				this->bZonaSismica=bZonaSismica;
				actualizarDimensiones();
			}
			void asignarbBoqEnTapa(bool bBoqEnTapa){
				this->bBoqEnTapa=bBoqEnTapa;
				actualizarDimensiones();
			}

			void asignardl(double dl){
				this->dl=dl;
				actualizarDimensiones();
			}

			void asignarLitHerr0(double LitHerr0){
				this->LitHerr0=LitHerr0;
				actualizarDimensiones();
			}

			void asignarLitHerr8(double LitHerr8){
				this->LitHerr8=LitHerr8;
				actualizarDimensiones();
			}

			void asignartTipoAparato(enum TipoAparato tTipoAparato){
				this->tTipoAparato=tTipoAparato;
				actualizarDimensiones();
			}
			
			void asignarEspesorNucleo(double EspesorNucleo){
				this->EspesorNucleo=EspesorNucleo;
				actualizarDimensiones();
			}

			void asignarkvas(double kvas){
				this->kvas=kvas;
				actualizarDimensiones();
			}

			void asignarF2(double F2){
				this->F2=F2;
				actualizarDimensiones();
			}

			void asignarBoqArreglo(double BoqArreglo){
				this->BoqArreglo=BoqArreglo;
				actualizarDimensiones();
			}

			void asignarEspesorCanal(double espesorCanal){
				/*this->espesorCanal=espesorCanal;*/
				actualizarDimensiones();
			}

			void asignarLongitudCanal(double longitudCanal){
				/*this->longitudCanal=longitudCanal;*/
				actualizarDimensiones();
			}

			void asignarAnchoInterior(double anchoInterior){
				/*this->anchoInterior=anchoInterior;*/
				actualizarDimensiones();
			}

			void asignarAlturaCanal(double alturaCanal){
				/*this->alturaCanal=alturaCanal;*/
				actualizarDimensiones();
			}
			void actualizarDimensiones(){
				this->setWidth(millimeter(1362)); 
				this->setHeight(millimeter(95.35));
				this->setLength(millimeter(324.7));
				this->setHidden(true);
			}


			void asignarDimensiones(const GInputs &inputs){
				/*EspesorNucleo=inputs.getReal("EspesorNucleo",EspesorNucleo);
				kvas=inputs.getReal("kvas",kvas);
				dl=inputs.getReal("dl",dl);
				LitHerr0=inputs.getReal("LitHerr0",LitHerr0);
				LitHerr8=inputs.getReal("LitHerr8",LitHerr8);
				BoqArreglo=inputs.getReal("BoqArreglo",BoqArreglo);
				F2=inputs.getReal("F2",F2);
				espesorCanal=inputs.getReal("espesorCanal",espesorCanal);
				longitudCanal=inputs.getReal("longitudCanal",longitudCanal);
				alturaCanal=inputs.getReal("alturaCanal",alturaCanal);
				anchoInterior=inputs.getReal("anchoInterior",anchoInterior);
				tTipoAparato=(TipoAparato)inputs.getInteger("tTipoAparato",tTipoAparato);
				tTipoConductorBT=(TipoConductor)inputs.getInteger("tTipoConductorBT",tTipoConductorBT);
				tArt=(TipoArticuloHerraje)inputs.getInteger("tArt",tArt);
				tCambiador=(CambiadorAncho)inputs.getInteger("tCambiador",tCambiador);
				tTipoConexion=(TipoConexion)inputs.getInteger("tTipoConexion",tTipoConexion);
				tTipoNucleo=(TipoNucleo)inputs.getInteger("tTipoNucleo",tTipoNucleo);
				bLadosCortos=inputs.getBoolean("bLadosCortos",bLadosCortos);
				bPedestal=inputs.getBoolean("bPedestal",bPedestal);
				bBoqEnTapa=inputs.getBoolean("bBoqEnTapa",bBoqEnTapa);
				bZonaSismica=inputs.getBoolean("bZonaSismica",bZonaSismica);*/
			}

			void obtenerDimensiones(GInputs &inputs){
				/*inputs.addInput<double>("EspesorNucleo", EspesorNucleo);
				inputs.addInput<double>("kvas", kvas);
				inputs.addInput<double>("dl", dl);
				inputs.addInput<double>("LitHerr0", LitHerr0);
				inputs.addInput<double>("LitHerr8", LitHerr8);
				inputs.addInput<double>("BoqArreglo", BoqArreglo);
				inputs.addInput<double>("F2", F2);
				inputs.addInput<double>("espesorCanal", espesorCanal);
				inputs.addInput<double>("longitudCanal", longitudCanal);
				inputs.addInput<double>("alturaCanal", alturaCanal);
				inputs.addInput<double>("anchoInterior", anchoInterior);
				inputs.addInput<int>("tTipoAparato",tTipoAparato);
				inputs.addInput<int>("tTipoConductorBT",tTipoConductorBT);
				inputs.addInput<int>("tArt",tArt);
				inputs.addInput<int>("tCambiador",tCambiador);
				inputs.addInput<int>("tTipoConexion",tTipoConexion);
				inputs.addInput<int>("tTipoNucleo",tTipoNucleo);
				inputs.addInput<bool>("bLadosCortos",bLadosCortos);
				inputs.addInput<bool>("bPedestal",bPedestal);
				inputs.addInput<bool>("bBoqEnTapa",bBoqEnTapa);
				inputs.addInput<bool>("bZonaSismica",bZonaSismica);*/
			}
			void defSelf(void){
			}
			void defParts(void){
				actualizarDimensiones();

				canal3->beginDefinition();				
					canal3->setName("canal3");
					canal3->initposition();
					canal3->alturaCanal=alturaCanal;
					canal3->anchoInterior=anchoInterior;
					canal3->espesor=espesor;
					canal3->longitudCanal=longitudCanal;
					Nombres.push_back("laminaIzquierda");
					Nombres.push_back("laminaDerecha");
					Nombres.push_back("laminaInferior");
					Nombres.push_back("esquinaIzquierda");
					Nombres.push_back("esquinaDerecha");
					canal3->modificar("UNION",Nombres,"canal3");
					Nombres.clear();
					canal3->rotate(LEFT,DEGREES(180));
					canal3->position(LEFT,longitudCanal);
					canal3->setPosition();
					canal3->calcpeso();
				canal3->endDefinition();
				//canal3->setHiddenRecusive(true);
			
				
				canal4->beginDefinition();
					canal4->setName("canal4");	
					canal4->alturaCanal=alturaCanal;
					canal4->anchoInterior=anchoInterior;
					canal4->espesor=espesor;
					canal4->longitudCanal=longitudCanal;
					Nombres.push_back("laminaIzquierda");
					Nombres.push_back("laminaDerecha");
					Nombres.push_back("laminaInferior");
					Nombres.push_back("esquinaIzquierda");
					Nombres.push_back("esquinaDerecha");
					canal4->modificar("UNION",Nombres,"canal4");
					Nombres.clear();
					canal4->position(RIGHT,longitudCanal);	
					canal4->rotate(FRONT,DEGREES(180));
					canal4->setPosition();
					canal4->calcpeso();
				canal4->endDefinition();

				qbox->beginDefinition();
					qbox->setName("qbox2");
					qbox->initposition();
					qbox->setWidth(width);
					qbox->setLength(length);
					qbox->setHeight(espesor);
					qbox->position(ABOVE,0,*canal4);
					qbox->setPosition();
					qbox->calcpeso();
				qbox->endDefinition();

		}
	};
}

#endif
