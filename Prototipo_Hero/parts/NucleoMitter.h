#ifndef NUCLEOMITTER_H_
#define NUCLEOMITTER_H_
#include "GBox.h"
#include "GMacros.h"
#include "GHollowCylinder.h"
#include "GInputs.h"
#include "Serializable.h"
namespace NSTransformador{
	using namespace Hero;		
	class NucleoMitter: public GBox, virtual public Serializable{
		public:
			GBoxS *yugoSuperior;
			GBoxS *yugoInferior;
			GBoxS *piernaIzquierda;
			GBoxS *piernaCentral;
			GBoxS *piernaDerecha;
	
			double espesor;//B
			double anchoLamina;//A
			double alturaVentana;//C
			double alturaNucleo;//H
			double entreEjes;//F
			double anchoVentana;//D
	
			NucleoMitter(GBox &parent):GBox(parent),Serializable("nucleoMitter"){
				nameHijo="yugoSuperior";
				createPart(yugoSuperior, GBoxS);
				nameHijo="yugoInferior";
				createPart(yugoInferior, GBoxS);
				nameHijo="piernaIzquierda";
				createPart(piernaIzquierda, GBoxS);
				nameHijo="piernaCentral";
				createPart(piernaCentral, GBoxS);
				nameHijo="piernaDerecha";
				createPart(piernaDerecha, GBoxS);
				espesor=0.3;
				anchoLamina=0.2;
				alturaVentana=0.9;
				alturaNucleo=alturaVentana+2*anchoLamina;
				entreEjes=1.2;
				anchoVentana=entreEjes-anchoLamina;
				this->setHidden(true);
				this->isReference = true;
				this->setBindParts(true);
				actualizarDimensiones();
			}
			void serializar(void){
				//Inicia Elementos
				agregarAtributo(AtributoXML("espesor",espesor));
				agregarAtributo(AtributoXML("anchoLamina",anchoLamina));
				agregarAtributo(AtributoXML("alturaVentana",alturaVentana));
				agregarAtributo(AtributoXML("alturaNucleo",alturaNucleo));
				agregarAtributo(AtributoXML("entreEjes",entreEjes));
				agregarAtributo(AtributoXML("anchoVentana",anchoVentana));
				//Nodos
				agregarNodo(yugoSuperior);
				agregarNodo(yugoInferior);
				agregarNodo(piernaIzquierda);
				agregarNodo(piernaCentral);
				agregarNodo(piernaDerecha);
			}

			void actualizarDimensiones(){
				this->setWidth(3*anchoLamina+2*anchoVentana);
				this->setHeight(alturaNucleo);
				this->setLength(espesor);
			}

			void defSelf(void){
			}

			void asignarDimensiones(const GInputs &inputs){
				this->espesor=inputs.getReal("dBNucleo",espesor);
				this->anchoLamina=inputs.getReal("dANucleo",anchoLamina);
				this->alturaVentana=inputs.getReal("dC",alturaVentana);
				this->alturaNucleo=inputs.getReal("dH", alturaNucleo);
				this->entreEjes=inputs.getReal("dF2", entreEjes);
				this->anchoVentana=inputs.getReal("dD2", anchoVentana);
				actualizarDimensiones();
			}
			void obtenerDimensiones(GInputs &inputs){
				inputs.addInput<double>("dBNucleo", espesor);
				inputs.addInput<double>("dAucleo", anchoLamina);
				inputs.addInput<double>("dC", alturaVentana);
				inputs.addInput<double>("dH", alturaNucleo);
				inputs.addInput<double>("dF2", entreEjes);
				inputs.addInput<double>("dD2", anchoVentana);
				inputs.addInput<double>("dBNucleo", espesor);
			}

			void defParts(void){
				this->NHijos=0;
				actualizarDimensiones();
				yugoSuperior->beginDefinition();
				yugoSuperior->setName("yugoSuperior");
					yugoSuperior->setHeight(anchoLamina);
					yugoSuperior->position(TOP, 0);
					yugoSuperior->initposition();
					yugoSuperior->setPosition();
					yugoSuperior->calcpeso();
				yugoSuperior->endDefinition();

				yugoInferior->beginDefinition();
					yugoInferior->setName("yugoInferior");
					yugoInferior->setHeight(anchoLamina);
					yugoInferior->position(BOTTOM,0);
					yugoInferior->initposition();
					yugoInferior->setPosition();					
					yugoInferior->calcpeso(),
				yugoInferior->endDefinition();

				piernaIzquierda->beginDefinition();
					piernaIzquierda->setName("piernaIzquierda");
					piernaIzquierda->setWidth(anchoLamina);
					piernaIzquierda->setHeight(alturaVentana);
					piernaIzquierda->position(LEFT,0);
					piernaIzquierda->initposition();
					piernaIzquierda->setPosition();					
					piernaIzquierda->calcpeso();
				piernaIzquierda->endDefinition();

				piernaDerecha->beginDefinition();
					piernaDerecha->setName("piernaDerecha");
					piernaDerecha->setWidth(anchoLamina);
					piernaDerecha->setHeight(alturaVentana);
					piernaDerecha->position(RIGHT,0);
					piernaDerecha->initposition();
					piernaDerecha->setPosition();	
					piernaDerecha->calcpeso();				
				piernaDerecha->endDefinition();

				piernaCentral->beginDefinition();
					piernaCentral->setName("piernaCentral");
					piernaCentral->setWidth(anchoLamina);
					piernaCentral->setHeight(alturaVentana);					
					piernaCentral->calcpeso();
				piernaCentral->endDefinition();

			}//defparts
	};//class NucleoMitter
}//NameSpace

#endif
