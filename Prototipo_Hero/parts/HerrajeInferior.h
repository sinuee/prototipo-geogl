#ifndef HERRAJEINFERIOR_H_
#define HERRAJEINFERIOR_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "Canal.h"
#include "SoporteDeAngulo.h"
#include "Definiciones.h"
#include "NucleoMitter.h"
//#include "nucleoenrollado.h"

namespace NSTransformador{
	using namespace Hero;
	class HerrajeInferior: public GBox{
		public:
			Canal *canal;			
			Canal *canal1;	
			/*SoporteDeAngulo *angulo[4];*/
			GBox *qbox;		
			double EspesorNucleo;
			double kvas;
			double dl;
			enum TipoAparato tTipoAparato;
			enum TipoConductor tTipoConductorBT;
			enum TipoArticuloHerraje tArt;
			double width;
			double length;

			double espesor;
			double longitudCanal;
			double alturaCanal;
			double anchoInterior;

			HerrajeInferior(GBox &parent):GBox(parent){				
				createPart(canal,Canal);
				createPart(canal1,Canal);
				createPart(qbox, GBox);			
				/*createPart(angulo[0], SoporteDeAngulo);
				createPart(angulo[1], SoporteDeAngulo);
				createPart(angulo[2], SoporteDeAngulo);
				createPart(angulo[3], SoporteDeAngulo);*/
				espesor = 0.05;//millimeter(6.35);
				longitudCanal = 0.9;//millimeter(1362.79);
				alturaCanal = millimeter(95.35);
				anchoInterior = millimeter(324.7);
			//alturaCanal=5;			anchoInterior=0.5;
			//espesor=0.05;			longitudCanal=1.7;
			width=longitudCanal;
			length=2.5*longitudCanal;

				this->isReference = true;
				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}

			void asignarEspesor(double espesor){
				this->espesor=espesor;
				actualizarDimensiones();
			}

			void asignarLongitudCanal(double longitudCanal){
				this->longitudCanal=longitudCanal;
				actualizarDimensiones();
			}

			void asignarAnchoInterior(double anchoInterior){
				this->anchoInterior=anchoInterior;
				actualizarDimensiones();
			}

			void asignarAlturaCanal(double alturaCanal){
				this->alturaCanal=alturaCanal;
				actualizarDimensiones();
			}
			void actualizarDimensiones(){

				/*width = ((kvas <=millimeter(500)) && (tTipoAparato != USA))?(dl - millimeter(63*2)):(dl - (2 * (EspesorNucleo - millimeter(3))));
				length = 2*espesor + anchoInterior + 2 * millimeter(63.5);*/

				//this->setWidth(millimeter(492.48)); // Falta meter los valores
				//this->setHeight(millimeter(88));
				//this->setLength(millimeter(1397.74));
				this->setWidth(millimeter(1362)); 
				this->setHeight(millimeter(95.35));
				this->setLength(millimeter(324.7));

			}
		
			void defSelf(void){
			}


			void defParts(void){
				actualizarDimensiones();
				canal->beginDefinition();
					canal->setName("canal");
					canal->initposition();
					canal->alturaCanal=alturaCanal;//getHeight()/2;
					canal->anchoInterior=anchoInterior;
					canal->espesor=espesor;
					canal->longitudCanal=longitudCanal;	
					canal->position(LEFT,longitudCanal);
					Nombres.push_back("laminaIzquierda");
					Nombres.push_back("laminaDerecha");
					Nombres.push_back("laminaInferior");
					Nombres.push_back("esquinaIzquierda");
					Nombres.push_back("esquinaDerecha");
					canal->modificar("UNION",Nombres,"canal");
					canal->initposition();
					canal->setPosition();
					Nombres.clear();
					canal->calcpeso();
				canal->endDefinition();
				

				canal1->beginDefinition();
					canal1->setName("canal2");
					canal1->alturaCanal=alturaCanal;//this->getHeight()/2;
					canal1->anchoInterior=anchoInterior;
					canal1->espesor=espesor;
					canal1->longitudCanal=longitudCanal;	
					
					canal1->position(RIGHT,longitudCanal);
					Nombres.push_back("laminaIzquierda");
					Nombres.push_back("laminaDerecha");
					Nombres.push_back("laminaInferior");
					Nombres.push_back("esquinaIzquierda");
					Nombres.push_back("esquinaDerecha");
					canal1->modificar("UNION",Nombres,"canal2");
					canal1->initposition();
					canal1->setPosition();
					Nombres.clear();
					canal1->calcpeso();
				canal1->endDefinition();
				
				qbox->beginDefinition();
				    qbox->setName("qbox1");
					qbox->setWidth(length);
					qbox->setLength(width);
					qbox->setHeight(espesor);
					qbox->initposition();					
					qbox->setHidden(false);
					qbox->position(BELOW,0,*canal);
					qbox->setPosition();
					qbox->calcpeso();
				qbox->endDefinition();
				
				
				
			/*	
			for(int i=0;i<4;i++)
			{
				angulo[i]->beginDefinition();

				angulo[i]->asignarArticulo((((kvas <=millimeter(500)) && (tTipoAparato != USA))?UE06: UF24));
					angulo[i]->position(BOTTOM,(EspesorNucleo - angulo[0]->getHeight())/2);
			} */
					
			}	
			
	};
}
//Cola->rotate(RIGHT,DEGREES(90));
#endif