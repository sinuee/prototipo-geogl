#ifndef ParteViva_H_
#define ParteViva_H_
#include "GBox.h"
#include "GMacros.h"
//#include "GInputs.h"
#include "Definiciones.h"
#include "bobina.h"
#include "NucleoEnrollado.h"
#include "NucleoMitter.h"
#include "HerrajeSuperior.h"
#include "HerrajeInferior.h"
#include "FuncionesComunes.h"
#include "Serializable.h"

namespace NSTransformador{
	using namespace Hero;
	class ParteViva: public GBox, virtual public Serializable{
		public:
			//variables
			double anchoLamina;
			double alturaVentana;
			double anchoVentana;
			double alturaRadial;
			double LargoPV;
			//parttes
			NucleoMitter *nucleoMitter;
			Bobina *bobina1;
			Bobina *bobina2;
			HerrajeInferior *herrajeInferior;
			HerrajeSuperior *herrajeSuperior;
			ParteViva(GBox &parent):GBox(parent),Serializable("ParteViva")
			{
				createPart(nucleoMitter,NucleoMitter);
				createPart(bobina1,Bobina);
				createPart(bobina2,Bobina);
				createPart(herrajeInferior,HerrajeInferior);
				createPart(herrajeSuperior,HerrajeSuperior);
				this->isReference = true;
				this->setHidden(true);
				this->setBindParts(true);
				anchoLamina = 0.2;
				alturaVentana = 2;
				anchoVentana = 1;
				alturaRadial=0.5;
				LargoPV = 2;
				actualizarDimensiones();
			}

			void actualizarDimensiones()
			{
				this->setWidth(anchoLamina*7);
				this->setHeight(anchoLamina*7);
				this->setLength(LargoPV);
			}
			void defSelf(void)
			{
			}

			void asignarDimensiones(const GInputs &inputs)
			{
			}
			void obtenerDimensiones(GInputs &inputs)
			{
				//inputs.addInput<double>("anchoLamina",anchoLamina);
			}
			void defParts(void){
				actualizarDimensiones();				
				Nombres.clear();
				//Nucleo Mitter
				nucleoMitter->beginDefinition();
				nucleoMitter->setName("nucleoMitter");
				nucleoMitter->anchoLamina = anchoLamina;
				nucleoMitter->alturaNucleo = alturaVentana;
				nucleoMitter->alturaVentana = alturaVentana;
				nucleoMitter->anchoVentana = anchoLamina;
				nucleoMitter->setHidden(true);			
				Nombres.push_back("yugoSuperior");
				Nombres.push_back("yugoInferior");
				Nombres.push_back("piernaIzquierda");
				Nombres.push_back("piernaDerecha");
				Nombres.push_back("piernaCentral");
				nucleoMitter->modificar("UNION",Nombres,"nucleoMitter");
				this->NHijos=Nombres.size();
				Nombres.clear();
				nucleoMitter->calcpeso();
				nucleoMitter->endDefinition();

				bobina1->beginDefinition();
				bobina1->setName("bobina1");
				bobina1->initposition();
				bobina1->rotate90(LEFT);
				bobina1->position(Hero::EFace::RIGHT,-(anchoLamina));//,*nucleoMitter);
				bobina1->setPosition();
				bobina1->asignarAnchoVentana(anchoVentana);
				bobina1->asignarAlturaAxial(this->getLength()+alturaRadial);
				bobina1->asignarAlturaVentana(alturaVentana);
			//	bobina1->setDisplayType(WIRED);
				bobina1->calcpeso();
				bobina1->endDefinition();

				bobina2->beginDefinition();
				bobina2->setName("bobina2");
				bobina2->initposition();
				bobina2->asignarAlturaVentana(alturaVentana);
				bobina2->rotate90(LEFT);
				bobina2->position(Hero::EFace::LEFT,-(anchoLamina));//,*nucleoMitter);				
				bobina2->setPosition();
				bobina2->asignarAnchoVentana(anchoVentana);
				bobina2->asignarAlturaAxial(this->getLength()+alturaRadial);
				bobina2->asignarAlturaVentana(alturaVentana);
			//	bobina2->setDisplayType(WIRED);
				bobina2->setHidden(false);
				bobina2->calcpeso();
				bobina2->endDefinition();

				
				herrajeInferior->beginDefinition();
				herrajeInferior->setName("HerrajeInferior");
				herrajeInferior->initposition();
				herrajeInferior->espesor = 0.05;//millimeter(6.35);
				herrajeInferior->longitudCanal = 0.9;//millimeter(1362.79);
				herrajeInferior->alturaCanal = millimeter(95.35);
				herrajeInferior->anchoInterior = millimeter(324.7);
				herrajeInferior->setHidden(true);
				Nombres.push_back("canal");
				Nombres.push_back("canal2");
				Nombres.push_back("qbox1");
				herrajeInferior->modificar("UNION",Nombres,"HerrajeInferior");
				this->NHijos=Nombres.size();
				Nombres.clear();
				herrajeInferior->position(BOTTOM,-1,*nucleoMitter);
				herrajeInferior->setPosition();
				herrajeInferior->calcpeso();
				herrajeInferior->endDefinition();


				herrajeSuperior->beginDefinition();
				herrajeSuperior->setName("HerrajeSuperior");
				herrajeSuperior->espesor = 0.05;//millimeter(6.35);
				herrajeSuperior->longitudCanal = 0.9;//millimeter(1362.79);
				herrajeSuperior->alturaCanal = millimeter(95.35);
				herrajeSuperior->anchoInterior = millimeter(324.7);
				herrajeSuperior->height=0.02;
				herrajeSuperior->setHidden(true);
				herrajeSuperior->initposition();
				Nombres.push_back("canal3");
				Nombres.push_back("canal4");
				Nombres.push_back("qbox2");
				herrajeSuperior->modificar("UNION",Nombres,"HerrajeSuperior");
				this->NHijos=Nombres.size();
				herrajeSuperior->Nombres.clear();
				herrajeSuperior->position(TOP,-1,*nucleoMitter);
				herrajeSuperior->setPosition();
				herrajeSuperior->calcpeso();
				herrajeSuperior->endDefinition();
				
			}
			void serializar(void){
				//Inicia Elementos
				agregarAtributo(AtributoXML("anchoLamina",anchoLamina));
				agregarAtributo(AtributoXML("alturaVentana",alturaVentana));
				agregarAtributo(AtributoXML("alturaRadial",alturaRadial));
				agregarAtributo(AtributoXML("anchoVentana",anchoVentana));
				agregarAtributo(AtributoXML("alturaRadial",alturaRadial));
				agregarAtributo(AtributoXML("LargoPV",LargoPV));
				agregarAtributo(AtributoXML("volumen",volumen));
				agregarNodo(nucleoMitter);
			}
			bool guardar(char *file){
				escribirArchivo(file);
				return true;
			}
	};
}

#endif
