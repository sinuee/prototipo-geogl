#ifndef BOQ_H_
#define BOQ_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"


namespace NSTransformador{
	using namespace Hero;
	class Boq: public GBox{
		public:
			GBox *Punta;
			GPartialHollowCylinder *Cola;
			GPartialHollowCylinder *Cuello;
			GPartialHollowCylinder *Base;
			GPartialHollowCylinder *Campana;

double DiametroCola;
double Largocola;
double DimC;
double DimA;
double DimD;
double DimB;
double DiametroCampana;
double AlturaCampana;
double InnerRadius;
//enum tArt;
			Boq(GBox &parent):GBox(parent){
				createPart(Cola, GPartialHollowCylinder);
				createPart(Base, GPartialHollowCylinder);
				createPart(Cuello, GPartialHollowCylinder);
				createPart(Campana, GPartialHollowCylinder);

InnerRadius = millimeter(15);
				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}

			void asignarDiametroCola(double DiametroCola){
				this->DiametroCola=DiametroCola;
				actualizarDimensiones();
			}
			void asignarLargocola(double Largocola){
				this->Largocola=Largocola;
				actualizarDimensiones();
			}
			void asignarDimA(double DimA){
				this->DimA=DimA;
				actualizarDimensiones();
			}
			void asignarDimB(double DimB){
				this->DimB=DimB;
				actualizarDimensiones();
			}
			void asignarDimC(double DimC){
				this->DimC=DimC;
				actualizarDimensiones();
			}
			void asignarDimD(double DimD){
				this->DimD=DimD;
				actualizarDimensiones();
			}
			void asignarDiametroCampana(double DiametroCampana){
				this->DiametroCampana=DiametroCampana;
				actualizarDimensiones();
			}
			void asignarAlturaCampana(double AlturaCampana){
				this->AlturaCampana=AlturaCampana;
				actualizarDimensiones();
			}


			void actualizarDimensiones(){
				this->setWidth(DiametroCampana);
				this->setHeight(DiametroCampana);
				this->setLength(Largocola + AlturaCampana);
			}
			void defSelf(void){
			}

			void asignarDimensiones(const GInputs &inputs){
				this->DiametroCola=inputs.getReal("DiametroCola",DiametroCola);
				this->Largocola=inputs.getReal("Largocola",Largocola);
				this->DimA=inputs.getReal("DimA",DimA);
				this->DimB=inputs.getReal("DimB",DimB);
				this->DimC=inputs.getReal("DimC",DimC);
				this->DimD=inputs.getReal("DimD",DimD);
				this->DiametroCampana=inputs.getReal("DiametroCampana",DiametroCampana);
				this->AlturaCampana=inputs.getReal("AlturaCampana",AlturaCampana);
				//this->tArt=inputs.getReal("tArt",tArt);
				actualizarDimensiones();
			}
			void obtenerDimensiones(GInputs &inputs){
				inputs.addInput<double>("DiametroCola", DiametroCola);
				inputs.addInput<double>("Largocola", Largocola);
				inputs.addInput<double>("DimA", DimA);
				inputs.addInput<double>("DimB", DimB);
				inputs.addInput<double>("DimC", DimC);
				inputs.addInput<double>("DimD", DimD);
				inputs.addInput<double>("DiametroCampana", DiametroCampana);
				inputs.addInput<double>("AlturaCampana", AlturaCampana);
				//inputs.addInput<double>("tArt", tArt);

			}
			void defParts(void){
				actualizarDimensiones();


				Cola->beginDefinition();
					Cola->setInnerRadius(InnerRadius);
					Cola->setRadius(DiametroCola/2);
					Cola->setHeight(Largocola);
					Cola->rotate(RIGHT,DEGREES(90));
					Cola->setStartAngle(DEGREES(0));
					Cola->setSweepAngle(DEGREES(360));
					Cola->position(FRONT,0);
				Cola->endDefinition();


				Base->beginDefinition();
					if(DimA>0)
						Base->setInnerRadius(InnerRadius);
					else
						Base->setInnerRadius(0);
						Base->setRadius(DimC/2);
						Base->setHeight(DimA);
						Base->rotate(RIGHT,DEGREES(90));
						Base->setStartAngle(DEGREES(0));
						Base->setSweepAngle(DEGREES(360));
						Base->position(FRONT,Largocola);
				Base->endDefinition();

				Cuello->beginDefinition();
					if (DimB>0)
						Cuello->setInnerRadius(InnerRadius);
					else
						Cuello->setInnerRadius(0);
					Cuello->setRadius(DimD/2);
					Cuello->setHeight(DimB);
					Cuello->rotate(RIGHT,DEGREES(90));
					Cuello->setStartAngle(DEGREES(0));
					Cuello->setSweepAngle(DEGREES(360));
					Cuello->position(FRONT,Largocola + DimA);
				Cuello->endDefinition();

				Campana->beginDefinition();
					Campana->setInnerRadius(InnerRadius);
					Campana->setRadius(DiametroCampana/2);
					Campana->setHeight(AlturaCampana - DimA - DimB);
					Campana->rotate(RIGHT,DEGREES(90));
					Campana->setStartAngle(DEGREES(0));
					Campana->setSweepAngle(DEGREES(360));
					Campana->position(FRONT,Largocola + DimA + DimB);
				Campana->endDefinition();

			}
	};
}

#endif
