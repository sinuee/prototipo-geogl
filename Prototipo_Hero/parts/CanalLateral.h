/*
 * CanalLateral.h
 *
 *  Created on: Jan 5, 2009
 *      Author: khayyam
 */

#ifndef CANALLATERAL_H_
#define CANALLATERAL_H_
#include "GBox.h"
#include "Angulo.h"
#include "GPartialHollowCylinder.h"
namespace NSTransformador{
	using namespace Hero;
	class CanalLateral:public GBox{
		public:
			GBox *laminaInferior;
			Angulo *anguloIzquierdo;
			Angulo *anguloDerecho;
			GPartialHollowCylinder *esquinaIzquierda;
			GPartialHollowCylinder *esquinaDerecha;
			double espesor;
			double anchoInterior;
			double patin;
			double altura;
			double longitud;

			CanalLateral(GBox &parent):GBox(parent){
				createPart(laminaInferior, GBox);
				createPart(anguloIzquierdo, Angulo);
				createPart(anguloDerecho, Angulo);
				createPart(esquinaIzquierda, GPartialHollowCylinder);
				createPart(esquinaDerecha, GPartialHollowCylinder);
				longitud=feet(4);
				anchoInterior=feet(1);
				altura=feet(0.25);
				espesor=inch(0.5);
				patin=feet(0.75);
				actualizarDimensiones();
				setBindParts(true);
				setHidden(true);
				isReference = true;
			}

			void actualizarDimensiones(void){
				setWidth(anchoInterior+2*patin);
				setHeight(altura);
				setLength(longitud);
			}

			void defParts(void){
				actualizarDimensiones();
				laminaInferior->beginDefinition();
					laminaInferior->setWidth(anchoInterior);
					laminaInferior->setLength(longitud);
					laminaInferior->setHeight(espesor);
					laminaInferior->position(BOTTOM,0);
				laminaInferior->endDefinition();

				anguloIzquierdo->beginDefinition();
					anguloIzquierdo->asignarAlturaInterior(altura-espesor);
					anguloIzquierdo->asignarAnchoInterior(patin-espesor);
					anguloIzquierdo->asignarEspesor(espesor);
					anguloIzquierdo->asignarLongitud(longitud);
					anguloIzquierdo->rotate(REAR,DEGREES(180));
					anguloIzquierdo->position(ONLEFT,-espesor);
					anguloIzquierdo->position(TOP,0);
				anguloIzquierdo->endDefinition();

				anguloDerecho->beginDefinition();
					anguloDerecho->asignarAlturaInterior(altura-espesor);
					anguloDerecho->asignarAnchoInterior(patin-espesor);
					anguloDerecho->asignarEspesor(espesor);
					anguloDerecho->asignarLongitud(longitud);
					anguloDerecho->rotate(LEFT,DEGREES(180));
					anguloDerecho->position(RIGHT,-espesor);
					anguloDerecho->position(TOP,0);
				anguloDerecho->endDefinition();

				esquinaIzquierda->beginDefinition();

				esquinaIzquierda->endDefinition();


				esquinaDerecha->beginDefinition();

				esquinaDerecha->endDefinition();
			}

	};
}

#endif /* CANALLATERAL_H_ */
