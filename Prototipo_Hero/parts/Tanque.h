/*
 * Tanque.h
 *
 *  Created on: Jan 6, 2009
 *      Author: khayyam
 */

#ifndef TANQUE_H_
#define TANQUE_H_

#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "Angulo.h"
namespace NSTransformador{
	using namespace Hero;
	class Tanque: public GBox{
		public:
			Angulo *paredFrontal;
			Angulo *paredPosterior;
			Angulo *paredIzquierda;
			Angulo *paredDerecha;
			GBox *espacioInterior;
			GBox *base;
			GBox *tapa;
			double ceja;//hasta ahora las cejas de las paredes (para fijar la tapa siempre miden 50 mm.)
			double espacioParaSoldaduraEntreParedes;
			double espacioParaSoldaduraEntreParedesYTapa;
			double espacioParaSoldaduraEntreParedesYBase;
			double anchoInterior;
			double largoInterior;
			double altoInterior;
			double alturaBase;//la altura a la que se encuentra la placa de la base (C5 en exportacion)
			double espesorParedes;
			double espesorTapa;
			double espesorBase;
			Tanque(GBox &parent):GBox(parent){
				createPart(paredFrontal, Angulo);
				createPart(paredPosterior, Angulo);
				createPart(paredIzquierda, Angulo);
				createPart(paredDerecha, Angulo);
				createPart(espacioInterior, GBox);
				createPart(base, GBox);
				createPart(tapa, GBox);
				espacioParaSoldaduraEntreParedes=millimeter(10);//aparentemente solo depende de si el aparato es nacional o de exportacion
				espacioParaSoldaduraEntreParedesYTapa=millimeter(16.5);
				espacioParaSoldaduraEntreParedesYBase=millimeter(7);
				anchoInterior=millimeter(1500);
				largoInterior=millimeter(850);
				altoInterior=millimeter(1800);
				alturaBase=millimeter(380);
				espesorParedes=millimeter(6);
				espesorTapa=millimeter(6);
				espesorBase=millimeter(6);

				ceja=millimeter(50);
				setHidden(true);
				setBindParts(true);

			}

			void obtenerDimensiones(GInputs &inputs){
				inputs.addInput<double>("espacioParaSoldaduraEntreParedes",espacioParaSoldaduraEntreParedes);
				inputs.addInput<double>("espacioParaSoldaduraEntreParedesYTapa",espacioParaSoldaduraEntreParedesYTapa);
				inputs.addInput<double>("espacioParaSoldaduraEntreParedesYBase",espacioParaSoldaduraEntreParedesYBase);
				inputs.addInput<double>("anchoInterior",anchoInterior);
				inputs.addInput<double>("largoInterior",largoInterior);
				inputs.addInput<double>("altoInterior",altoInterior);
				inputs.addInput<double>("alturaBase",alturaBase);
				inputs.addInput<double>("espesorParedes",espesorParedes);
				inputs.addInput<double>("espesorTapa",espesorTapa);
				inputs.addInput<double>("espesorBase",espesorBase);
				inputs.addInput<double>("ceja",ceja);
			}

			void asignarDimensiones(const GInputs &inputs){
				espacioParaSoldaduraEntreParedes=inputs.getReal("espacioParaSoldaduraEntreParedes",espacioParaSoldaduraEntreParedes);
				espacioParaSoldaduraEntreParedesYTapa=inputs.getReal("espacioParaSoldaduraEntreParedesYTapa",espacioParaSoldaduraEntreParedesYTapa);
				espacioParaSoldaduraEntreParedesYBase=inputs.getReal("espacioParaSoldaduraEntreParedesYBase",espacioParaSoldaduraEntreParedesYBase);
				anchoInterior=inputs.getReal("anchoInterior",anchoInterior);
				largoInterior=inputs.getReal("largoInterior",largoInterior);
				altoInterior=inputs.getReal("altoInterior",altoInterior);
				alturaBase=inputs.getReal("alturaBase",alturaBase);
				espesorParedes=inputs.getReal("espesorParedes",espesorParedes);
				espesorTapa=inputs.getReal("espesorTapa",espesorTapa);
				espesorBase=inputs.getReal("espesorBase",espesorBase);
				ceja=inputs.getReal("ceja",ceja);
			}

			void actualizarDimensiones(void){
				setWidth(anchoInterior+2*(ceja+espesorParedes));
				setLength(largoInterior+2*(ceja+espesorParedes-espacioParaSoldaduraEntreParedes));
				setHeight(alturaBase+altoInterior);
			}

			void defParts(void){
				actualizarDimensiones();
				espacioInterior->beginDefinition();
				espacioInterior->setName("espacioInterior");
				espacioInterior->initposition();
					espacioInterior->setHeight(altoInterior);
					espacioInterior->setWidth(anchoInterior);
					espacioInterior->setLength(largoInterior);
					espacioInterior->position(TOP,0);
					espacioInterior->setPosition();
					espacioInterior->setHidden(true);
				espacioInterior->endDefinition();

				paredFrontal->beginDefinition();
				paredFrontal->setName("paredFrontal");
				paredFrontal->initposition();
					paredFrontal->asignarEspesor(espesorParedes);
					paredFrontal->asignarAlturaInterior(altoInterior-espesorParedes);
					paredFrontal->asignarAnchoInterior(ceja);
					paredFrontal->asignarLongitud(anchoInterior);
					paredFrontal->rotate180(LONGITUDINAL);
					paredFrontal->rotate90(BOTTOM);
					paredFrontal->position(TOP,0);
					paredFrontal->position(INFRONT,0,*espacioInterior);
					paredFrontal->setPosition();
				paredFrontal->endDefinition();

				paredPosterior->beginDefinition();
				paredPosterior->setName("paredPosterior");
				paredPosterior->initposition();
					paredPosterior->asignarEspesor(espesorParedes);
					paredPosterior->asignarAlturaInterior(altoInterior-espesorParedes);
					paredPosterior->asignarAnchoInterior(ceja);
					paredPosterior->asignarLongitud(anchoInterior);
					paredPosterior->rotate180(LONGITUDINAL);
					paredPosterior->rotate90(TOP);
					paredPosterior->position(TOP,0);
					paredPosterior->position(INREAR,0,*espacioInterior);
					paredPosterior->setPosition();
				paredPosterior->endDefinition();

				paredIzquierda->beginDefinition();
				paredIzquierda->setName("paredIzquierda");
				paredIzquierda->initposition();
					paredIzquierda->asignarEspesor(espesorParedes);
					paredIzquierda->asignarAlturaInterior(altoInterior+alturaBase);
					paredIzquierda->asignarAnchoInterior(ceja);
					paredIzquierda->asignarLongitud(largoInterior+2*(espesorParedes+espacioParaSoldaduraEntreParedes));
					paredIzquierda->rotate180(LONGITUDINAL);
					paredIzquierda->position(TOP,0);
					paredIzquierda->position(ONLEFT,0,*espacioInterior);
					paredIzquierda->setPosition();
				paredIzquierda->endDefinition();

				paredDerecha->beginDefinition();
				paredDerecha->setName("paredDerecha");
				paredDerecha->initposition();
					paredDerecha->asignarEspesor(espesorParedes);
					paredDerecha->asignarAlturaInterior(altoInterior+alturaBase);
					paredDerecha->asignarAnchoInterior(ceja);
					paredDerecha->asignarLongitud(largoInterior+2*(espesorParedes+espacioParaSoldaduraEntreParedes));

					paredDerecha->rotate180(LONGITUDINAL);
					paredDerecha->rotate180(VERTICAL);
					paredDerecha->position(TOP,0);
					paredDerecha->position(ONRIGHT,0,*espacioInterior);
					paredDerecha->setPosition();
				paredDerecha->endDefinition();

				tapa->beginDefinition();
				tapa->setName("tapa");
				tapa->initposition();
					tapa->setHeight(espesorTapa);
					tapa->setWidth(anchoInterior+2*(ceja+espesorParedes-espacioParaSoldaduraEntreParedesYTapa));
					tapa->setLength(largoInterior+2*(ceja+espesorParedes-espacioParaSoldaduraEntreParedesYTapa));
					tapa->position(ABOVE,0,*espacioInterior);
					tapa->setPosition();
				tapa->endDefinition();

				base->beginDefinition();
				base->setName("base");
				base->initposition();
					base->setHeight(espesorBase);
					base->setWidth(anchoInterior);
					base->setLength(largoInterior+2*(espesorParedes+espacioParaSoldaduraEntreParedesYBase));
					base->position(BELOW,0,*espacioInterior);
					base->setPosition();
				base->endDefinition();
			}
	};
}

#endif /* TANQUE_H_ */
