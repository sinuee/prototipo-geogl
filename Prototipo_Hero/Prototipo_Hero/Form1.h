#pragma once

#include "HeroLib.h"
#include "CTransformador.h"
#include "Consulta.h"
#include "FuncionesComunes.h"
#include "Inspector.h"
//#include "eAccionMenuTree.h"
//#pragma comment (lib "Consulta.lib")
//
//#import "C:\Program Files\Common Files\System\ADO\msado15.dll" rename_namespace("ADOCG") rename("EOF", "EndOfFile")
//using namespace ADOCG;

namespace Prototipo_Hero {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace Hero;

	
	/// <summary>
	/// Summary for Form1
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			WinHero = gcnew COpenGL(this->MuevoGrafico,this->Width/2,this->Height/2);
			Transformador = new CTransformador();
			x = 0;
			y = 0;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected: 
	private: System::Windows::Forms::ToolStripMenuItem^  archivoToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  dibujoToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  ambienteToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  reporteToolStripMenuItem;

	private: System::Windows::Forms::ToolStripMenuItem^  cargarDise˝oDeBaseDeDatosToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  guardarDise˝oDeArchivoToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  cargarDise˝oDeArchivoToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  guardarDise˝oDeBaseDeDatosToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  generarSCRToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  generarAutolispToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  specOnlyToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  specGraphicsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  specGraphicsTreeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  specTreeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  graphicsTreeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  graphicsOnlyToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  treeOnlyToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  dise˝oBaseToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  estructuraToolStripMenuItem;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::ComponentModel::IContainer^  components;
	private: System::Windows::Forms::TreeView^  treeView1;
	private: System::Windows::Forms::TabControl^  tabControl1;
	private: System::Windows::Forms::TabPage^  tabPage1;
	private: System::Windows::Forms::ComboBox^  comboBox11;
	private: System::Windows::Forms::ComboBox^  comboBox10;
	private: System::Windows::Forms::ComboBox^  comboBox9;
	private: System::Windows::Forms::ComboBox^  comboBox8;
	private: System::Windows::Forms::Panel^  panel14;
	private: System::Windows::Forms::RadioButton^  radioButton34;
	private: System::Windows::Forms::RadioButton^  radioButton33;
	private: System::Windows::Forms::Panel^  panel8;
	private: System::Windows::Forms::RadioButton^  radioButton22;
	private: System::Windows::Forms::RadioButton^  radioButton21;
	private: System::Windows::Forms::RadioButton^  radioButton20;
	private: System::Windows::Forms::RadioButton^  radioButton19;
	private: System::Windows::Forms::Panel^  panel7;
	private: System::Windows::Forms::RadioButton^  radioButton18;
	private: System::Windows::Forms::RadioButton^  radioButton17;
	private: System::Windows::Forms::Label^  label27;
	private: System::Windows::Forms::Label^  label26;
	private: System::Windows::Forms::Label^  label21;
	private: System::Windows::Forms::Label^  label20;
	private: System::Windows::Forms::Label^  label19;
	private: System::Windows::Forms::Label^  label18;
	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::Label^  label16;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::TextBox^  textBox8;
	private: System::Windows::Forms::TextBox^  textBox7;
	private: System::Windows::Forms::TextBox^  textBox6;
	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Panel^  panel13;
	private: System::Windows::Forms::RadioButton^  radioButton32;
	private: System::Windows::Forms::RadioButton^  radioButton31;
	private: System::Windows::Forms::TabPage^  tabPage2;

	private: System::Windows::Forms::ComboBox^  comboBox6;
	private: System::Windows::Forms::ComboBox^  comboBox5;
	private: System::Windows::Forms::ComboBox^  comboBox4;
	private: System::Windows::Forms::ComboBox^  comboBox3;
	private: System::Windows::Forms::ComboBox^  comboBox2;
	private: System::Windows::Forms::Label^  label35;
	private: System::Windows::Forms::Label^  label34;
	private: System::Windows::Forms::Label^  label33;
	private: System::Windows::Forms::Label^  label32;
	private: System::Windows::Forms::Label^  label31;
	private: System::Windows::Forms::Label^  label30;
	private: System::Windows::Forms::Label^  label29;
	private: System::Windows::Forms::TabPage^  tabPage3;
	private: System::Windows::Forms::ComboBox^  comboBox1;
	private: System::Windows::Forms::Panel^  panel20;
	private: System::Windows::Forms::RadioButton^  radioButton44;
	private: System::Windows::Forms::RadioButton^  radioButton43;
	private: System::Windows::Forms::Panel^  panel18;
	private: System::Windows::Forms::RadioButton^  radioButton42;
	private: System::Windows::Forms::RadioButton^  radioButton41;
	private: System::Windows::Forms::Panel^  panel16;
	private: System::Windows::Forms::RadioButton^  radioButton40;
	private: System::Windows::Forms::RadioButton^  radioButton39;
	private: System::Windows::Forms::Label^  label39;
	private: System::Windows::Forms::Label^  label38;
	private: System::Windows::Forms::Label^  label37;
	private: System::Windows::Forms::Label^  label36;
	private: System::Windows::Forms::Panel^  panel6;
	private: System::Windows::Forms::RadioButton^  radioButton16;
	private: System::Windows::Forms::RadioButton^  radioButton15;
private: System::Windows::Forms::PictureBox^  ZoomIn;
private: System::Windows::Forms::PictureBox^  ZoomOut;
private: System::Windows::Forms::PictureBox^  ArrowTop;
private: System::Windows::Forms::PictureBox^  ArrowRight;
private: System::Windows::Forms::PictureBox^  ArrowLeft;
private: System::Windows::Forms::PictureBox^  ArrowDown;
private: System::Windows::Forms::PictureBox^  ArrowTR;
private: System::Windows::Forms::PictureBox^  ArrowTL;
private: System::Windows::Forms::PictureBox^  ArrowDL;
private: System::Windows::Forms::PictureBox^  ArrowDR;

private: System::Windows::Forms::Panel^  panel2;
private: System::Windows::Forms::RadioButton^  radioButton6;
private: System::Windows::Forms::RadioButton^  radioButton5;
private: System::Windows::Forms::RadioButton^  radioButton1;
private: System::Windows::Forms::RadioButton^  radioButton2;
	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		COpenGL^ WinHero;
private: System::Windows::Forms::SaveFileDialog^  saveDibujo;
private: System::Windows::Forms::ToolStripMenuItem^  vistaToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  topToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  backToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  frontToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  leftToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  rightToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  bottomToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  topRightFrontToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  topFrontToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  topLeftFrontToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  bottomFrontToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  bottomFrontrigthToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  bottomFrontLeftToolStripMenuItem;
private: System::Windows::Forms::TextBox^  textBox5;

		 CTransformador *Transformador;
		 int x;
private: System::Windows::Forms::PictureBox^  MuevoGrafico;
private: System::Windows::Forms::Label^  lblDebugeo;
private: System::Windows::Forms::ContextMenuStrip^  MenuTree;
private: System::Windows::Forms::ToolStripMenuItem^  showToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  hidenToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  solidToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  wireframeToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  inspectorToolStripMenuItem;
		 int y;
		 Inspector^ frmInspect;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->archivoToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->cargarDise˝oDeArchivoToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->cargarDise˝oDeBaseDeDatosToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->guardarDise˝oDeArchivoToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->guardarDise˝oDeBaseDeDatosToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->dibujoToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->generarSCRToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->generarAutolispToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->vistaToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->topToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->backToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->frontToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->leftToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->rightToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->bottomToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->topRightFrontToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->topFrontToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->topLeftFrontToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->bottomFrontToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->bottomFrontrigthToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->bottomFrontLeftToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->ambienteToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->specGraphicsTreeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->specGraphicsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->specTreeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->graphicsTreeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->graphicsOnlyToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->specOnlyToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->treeOnlyToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->reporteToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->dise˝oBaseToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->estructuraToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->tabPage3 = (gcnew System::Windows::Forms::TabPage());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->panel20 = (gcnew System::Windows::Forms::Panel());
			this->radioButton44 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton43 = (gcnew System::Windows::Forms::RadioButton());
			this->panel18 = (gcnew System::Windows::Forms::Panel());
			this->radioButton42 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton41 = (gcnew System::Windows::Forms::RadioButton());
			this->panel16 = (gcnew System::Windows::Forms::Panel());
			this->radioButton40 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton39 = (gcnew System::Windows::Forms::RadioButton());
			this->label39 = (gcnew System::Windows::Forms::Label());
			this->label38 = (gcnew System::Windows::Forms::Label());
			this->label37 = (gcnew System::Windows::Forms::Label());
			this->label36 = (gcnew System::Windows::Forms::Label());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->radioButton6 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton5 = (gcnew System::Windows::Forms::RadioButton());
			this->comboBox6 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox5 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox4 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox3 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox2 = (gcnew System::Windows::Forms::ComboBox());
			this->label35 = (gcnew System::Windows::Forms::Label());
			this->label34 = (gcnew System::Windows::Forms::Label());
			this->label33 = (gcnew System::Windows::Forms::Label());
			this->label32 = (gcnew System::Windows::Forms::Label());
			this->label31 = (gcnew System::Windows::Forms::Label());
			this->label30 = (gcnew System::Windows::Forms::Label());
			this->label29 = (gcnew System::Windows::Forms::Label());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->comboBox11 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox10 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox9 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox8 = (gcnew System::Windows::Forms::ComboBox());
			this->panel14 = (gcnew System::Windows::Forms::Panel());
			this->radioButton34 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton33 = (gcnew System::Windows::Forms::RadioButton());
			this->panel8 = (gcnew System::Windows::Forms::Panel());
			this->radioButton22 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton21 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton20 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton19 = (gcnew System::Windows::Forms::RadioButton());
			this->panel7 = (gcnew System::Windows::Forms::Panel());
			this->radioButton18 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton17 = (gcnew System::Windows::Forms::RadioButton());
			this->panel6 = (gcnew System::Windows::Forms::Panel());
			this->radioButton16 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton15 = (gcnew System::Windows::Forms::RadioButton());
			this->label27 = (gcnew System::Windows::Forms::Label());
			this->label26 = (gcnew System::Windows::Forms::Label());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->textBox8 = (gcnew System::Windows::Forms::TextBox());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->panel13 = (gcnew System::Windows::Forms::Panel());
			this->radioButton32 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton31 = (gcnew System::Windows::Forms::RadioButton());
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->treeView1 = (gcnew System::Windows::Forms::TreeView());
			this->MenuTree = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
			this->showToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->hidenToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->solidToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->wireframeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->inspectorToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->radioButton1 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton2 = (gcnew System::Windows::Forms::RadioButton());
			this->ZoomIn = (gcnew System::Windows::Forms::PictureBox());
			this->ZoomOut = (gcnew System::Windows::Forms::PictureBox());
			this->ArrowTop = (gcnew System::Windows::Forms::PictureBox());
			this->ArrowRight = (gcnew System::Windows::Forms::PictureBox());
			this->ArrowLeft = (gcnew System::Windows::Forms::PictureBox());
			this->ArrowDown = (gcnew System::Windows::Forms::PictureBox());
			this->ArrowTR = (gcnew System::Windows::Forms::PictureBox());
			this->ArrowTL = (gcnew System::Windows::Forms::PictureBox());
			this->ArrowDL = (gcnew System::Windows::Forms::PictureBox());
			this->ArrowDR = (gcnew System::Windows::Forms::PictureBox());
			this->saveDibujo = (gcnew System::Windows::Forms::SaveFileDialog());
			this->MuevoGrafico = (gcnew System::Windows::Forms::PictureBox());
			this->lblDebugeo = (gcnew System::Windows::Forms::Label());
			this->menuStrip1->SuspendLayout();
			this->tabPage3->SuspendLayout();
			this->panel20->SuspendLayout();
			this->panel18->SuspendLayout();
			this->panel16->SuspendLayout();
			this->tabPage2->SuspendLayout();
			this->panel2->SuspendLayout();
			this->tabPage1->SuspendLayout();
			this->panel14->SuspendLayout();
			this->panel8->SuspendLayout();
			this->panel7->SuspendLayout();
			this->panel6->SuspendLayout();
			this->panel13->SuspendLayout();
			this->tabControl1->SuspendLayout();
			this->MenuTree->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ZoomIn))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ZoomOut))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ArrowTop))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ArrowRight))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ArrowLeft))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ArrowDown))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ArrowTR))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ArrowTL))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ArrowDL))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ArrowDR))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->MuevoGrafico))->BeginInit();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {this->archivoToolStripMenuItem, 
				this->dibujoToolStripMenuItem, this->ambienteToolStripMenuItem, this->reporteToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(926, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// archivoToolStripMenuItem
			// 
			this->archivoToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {this->cargarDise˝oDeArchivoToolStripMenuItem, 
				this->cargarDise˝oDeBaseDeDatosToolStripMenuItem, this->guardarDise˝oDeArchivoToolStripMenuItem, this->guardarDise˝oDeBaseDeDatosToolStripMenuItem});
			this->archivoToolStripMenuItem->Name = L"archivoToolStripMenuItem";
			this->archivoToolStripMenuItem->Size = System::Drawing::Size(60, 20);
			this->archivoToolStripMenuItem->Text = L"Archivo";
			// 
			// cargarDise˝oDeArchivoToolStripMenuItem
			// 
			this->cargarDise˝oDeArchivoToolStripMenuItem->Name = L"cargarDise˝oDeArchivoToolStripMenuItem";
			this->cargarDise˝oDeArchivoToolStripMenuItem->Size = System::Drawing::Size(246, 22);
			this->cargarDise˝oDeArchivoToolStripMenuItem->Text = L"Cargar dise˝o de Archivo";
			this->cargarDise˝oDeArchivoToolStripMenuItem->Visible = false;
			// 
			// cargarDise˝oDeBaseDeDatosToolStripMenuItem
			// 
			this->cargarDise˝oDeBaseDeDatosToolStripMenuItem->Name = L"cargarDise˝oDeBaseDeDatosToolStripMenuItem";
			this->cargarDise˝oDeBaseDeDatosToolStripMenuItem->Size = System::Drawing::Size(246, 22);
			this->cargarDise˝oDeBaseDeDatosToolStripMenuItem->Text = L"Cargar dise˝o de Base de Datos";
			this->cargarDise˝oDeBaseDeDatosToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::cargarDise˝oDeBaseDeDatosToolStripMenuItem_Click);
			// 
			// guardarDise˝oDeArchivoToolStripMenuItem
			// 
			this->guardarDise˝oDeArchivoToolStripMenuItem->Name = L"guardarDise˝oDeArchivoToolStripMenuItem";
			this->guardarDise˝oDeArchivoToolStripMenuItem->Size = System::Drawing::Size(246, 22);
			this->guardarDise˝oDeArchivoToolStripMenuItem->Text = L"Guardar dise˝o en Archivo";
			this->guardarDise˝oDeArchivoToolStripMenuItem->Visible = false;
			// 
			// guardarDise˝oDeBaseDeDatosToolStripMenuItem
			// 
			this->guardarDise˝oDeBaseDeDatosToolStripMenuItem->Name = L"guardarDise˝oDeBaseDeDatosToolStripMenuItem";
			this->guardarDise˝oDeBaseDeDatosToolStripMenuItem->Size = System::Drawing::Size(246, 22);
			this->guardarDise˝oDeBaseDeDatosToolStripMenuItem->Text = L"Guardar dise˝o en Base de Datos";
			this->guardarDise˝oDeBaseDeDatosToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::guardarDise˝oDeBaseDeDatosToolStripMenuItem_Click);
			// 
			// dibujoToolStripMenuItem
			// 
			this->dibujoToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->generarSCRToolStripMenuItem, 
				this->generarAutolispToolStripMenuItem, this->vistaToolStripMenuItem});
			this->dibujoToolStripMenuItem->Name = L"dibujoToolStripMenuItem";
			this->dibujoToolStripMenuItem->Size = System::Drawing::Size(54, 20);
			this->dibujoToolStripMenuItem->Text = L"Dibujo";
			// 
			// generarSCRToolStripMenuItem
			// 
			this->generarSCRToolStripMenuItem->Name = L"generarSCRToolStripMenuItem";
			this->generarSCRToolStripMenuItem->Size = System::Drawing::Size(162, 22);
			this->generarSCRToolStripMenuItem->Text = L"Generar SCR";
			this->generarSCRToolStripMenuItem->Visible = false;
			// 
			// generarAutolispToolStripMenuItem
			// 
			this->generarAutolispToolStripMenuItem->Name = L"generarAutolispToolStripMenuItem";
			this->generarAutolispToolStripMenuItem->Size = System::Drawing::Size(162, 22);
			this->generarAutolispToolStripMenuItem->Text = L"Generar Autolisp";
			this->generarAutolispToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::generarAutolispToolStripMenuItem_Click);
			// 
			// vistaToolStripMenuItem
			// 
			this->vistaToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(12) {this->topToolStripMenuItem, 
				this->backToolStripMenuItem, this->frontToolStripMenuItem, this->leftToolStripMenuItem, this->rightToolStripMenuItem, this->bottomToolStripMenuItem, 
				this->topRightFrontToolStripMenuItem, this->topFrontToolStripMenuItem, this->topLeftFrontToolStripMenuItem, this->bottomFrontToolStripMenuItem, 
				this->bottomFrontrigthToolStripMenuItem, this->bottomFrontLeftToolStripMenuItem});
			this->vistaToolStripMenuItem->Name = L"vistaToolStripMenuItem";
			this->vistaToolStripMenuItem->Size = System::Drawing::Size(162, 22);
			this->vistaToolStripMenuItem->Text = L"Vistas";
			// 
			// topToolStripMenuItem
			// 
			this->topToolStripMenuItem->Name = L"topToolStripMenuItem";
			this->topToolStripMenuItem->Size = System::Drawing::Size(180, 22);
			this->topToolStripMenuItem->Text = L"Top";
			this->topToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::topToolStripMenuItem_Click);
			// 
			// backToolStripMenuItem
			// 
			this->backToolStripMenuItem->Name = L"backToolStripMenuItem";
			this->backToolStripMenuItem->Size = System::Drawing::Size(180, 22);
			this->backToolStripMenuItem->Text = L"Back";
			this->backToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::backToolStripMenuItem_Click);
			// 
			// frontToolStripMenuItem
			// 
			this->frontToolStripMenuItem->Name = L"frontToolStripMenuItem";
			this->frontToolStripMenuItem->Size = System::Drawing::Size(180, 22);
			this->frontToolStripMenuItem->Text = L"Front";
			this->frontToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::frontToolStripMenuItem_Click);
			// 
			// leftToolStripMenuItem
			// 
			this->leftToolStripMenuItem->Name = L"leftToolStripMenuItem";
			this->leftToolStripMenuItem->Size = System::Drawing::Size(180, 22);
			this->leftToolStripMenuItem->Text = L"Left";
			this->leftToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::leftToolStripMenuItem_Click);
			// 
			// rightToolStripMenuItem
			// 
			this->rightToolStripMenuItem->Name = L"rightToolStripMenuItem";
			this->rightToolStripMenuItem->Size = System::Drawing::Size(180, 22);
			this->rightToolStripMenuItem->Text = L"Right";
			this->rightToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::rightToolStripMenuItem_Click);
			// 
			// bottomToolStripMenuItem
			// 
			this->bottomToolStripMenuItem->Name = L"bottomToolStripMenuItem";
			this->bottomToolStripMenuItem->Size = System::Drawing::Size(180, 22);
			this->bottomToolStripMenuItem->Text = L"Bottom";
			this->bottomToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::bottomToolStripMenuItem_Click);
			// 
			// topRightFrontToolStripMenuItem
			// 
			this->topRightFrontToolStripMenuItem->Name = L"topRightFrontToolStripMenuItem";
			this->topRightFrontToolStripMenuItem->Size = System::Drawing::Size(180, 22);
			this->topRightFrontToolStripMenuItem->Text = L"Top-Right-Front";
			this->topRightFrontToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::topRightFrontToolStripMenuItem_Click);
			// 
			// topFrontToolStripMenuItem
			// 
			this->topFrontToolStripMenuItem->Name = L"topFrontToolStripMenuItem";
			this->topFrontToolStripMenuItem->Size = System::Drawing::Size(180, 22);
			this->topFrontToolStripMenuItem->Text = L"Top-Front ";
			this->topFrontToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::topFrontToolStripMenuItem_Click);
			// 
			// topLeftFrontToolStripMenuItem
			// 
			this->topLeftFrontToolStripMenuItem->Name = L"topLeftFrontToolStripMenuItem";
			this->topLeftFrontToolStripMenuItem->Size = System::Drawing::Size(180, 22);
			this->topLeftFrontToolStripMenuItem->Text = L"Top-Left-Front";
			this->topLeftFrontToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::topLeftFrontToolStripMenuItem_Click);
			// 
			// bottomFrontToolStripMenuItem
			// 
			this->bottomFrontToolStripMenuItem->Name = L"bottomFrontToolStripMenuItem";
			this->bottomFrontToolStripMenuItem->Size = System::Drawing::Size(180, 22);
			this->bottomFrontToolStripMenuItem->Text = L"Bottom-Front";
			this->bottomFrontToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::bottomFrontToolStripMenuItem_Click);
			// 
			// bottomFrontrigthToolStripMenuItem
			// 
			this->bottomFrontrigthToolStripMenuItem->Name = L"bottomFrontrigthToolStripMenuItem";
			this->bottomFrontrigthToolStripMenuItem->Size = System::Drawing::Size(180, 22);
			this->bottomFrontrigthToolStripMenuItem->Text = L"Bottom-Front-Rigth";
			this->bottomFrontrigthToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::bottomFrontrigthToolStripMenuItem_Click);
			// 
			// bottomFrontLeftToolStripMenuItem
			// 
			this->bottomFrontLeftToolStripMenuItem->Name = L"bottomFrontLeftToolStripMenuItem";
			this->bottomFrontLeftToolStripMenuItem->Size = System::Drawing::Size(180, 22);
			this->bottomFrontLeftToolStripMenuItem->Text = L"Bottom-Front-Left";
			this->bottomFrontLeftToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::bottomFrontLeftToolStripMenuItem_Click);
			// 
			// ambienteToolStripMenuItem
			// 
			this->ambienteToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(7) {this->specGraphicsTreeToolStripMenuItem, 
				this->specGraphicsToolStripMenuItem, this->specTreeToolStripMenuItem, this->graphicsTreeToolStripMenuItem, this->graphicsOnlyToolStripMenuItem, 
				this->specOnlyToolStripMenuItem, this->treeOnlyToolStripMenuItem});
			this->ambienteToolStripMenuItem->Name = L"ambienteToolStripMenuItem";
			this->ambienteToolStripMenuItem->Size = System::Drawing::Size(71, 20);
			this->ambienteToolStripMenuItem->Text = L"Ambiente";
			this->ambienteToolStripMenuItem->Visible = false;
			// 
			// specGraphicsTreeToolStripMenuItem
			// 
			this->specGraphicsTreeToolStripMenuItem->Name = L"specGraphicsTreeToolStripMenuItem";
			this->specGraphicsTreeToolStripMenuItem->Size = System::Drawing::Size(174, 22);
			this->specGraphicsTreeToolStripMenuItem->Text = L"Spec Graphics Tree";
			this->specGraphicsTreeToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::specGraphicsTreeToolStripMenuItem_Click);
			// 
			// specGraphicsToolStripMenuItem
			// 
			this->specGraphicsToolStripMenuItem->Name = L"specGraphicsToolStripMenuItem";
			this->specGraphicsToolStripMenuItem->Size = System::Drawing::Size(174, 22);
			this->specGraphicsToolStripMenuItem->Text = L"Spec Graphics";
			this->specGraphicsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::specGraphicsToolStripMenuItem_Click);
			// 
			// specTreeToolStripMenuItem
			// 
			this->specTreeToolStripMenuItem->Name = L"specTreeToolStripMenuItem";
			this->specTreeToolStripMenuItem->Size = System::Drawing::Size(174, 22);
			this->specTreeToolStripMenuItem->Text = L"Spec Tree";
			this->specTreeToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::specTreeToolStripMenuItem_Click);
			// 
			// graphicsTreeToolStripMenuItem
			// 
			this->graphicsTreeToolStripMenuItem->Name = L"graphicsTreeToolStripMenuItem";
			this->graphicsTreeToolStripMenuItem->Size = System::Drawing::Size(174, 22);
			this->graphicsTreeToolStripMenuItem->Text = L"Graphics Tree";
			this->graphicsTreeToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::graphicsTreeToolStripMenuItem_Click);
			// 
			// graphicsOnlyToolStripMenuItem
			// 
			this->graphicsOnlyToolStripMenuItem->Name = L"graphicsOnlyToolStripMenuItem";
			this->graphicsOnlyToolStripMenuItem->Size = System::Drawing::Size(174, 22);
			this->graphicsOnlyToolStripMenuItem->Text = L"Graphics Only";
			this->graphicsOnlyToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::graphicsOnlyToolStripMenuItem_Click);
			// 
			// specOnlyToolStripMenuItem
			// 
			this->specOnlyToolStripMenuItem->Name = L"specOnlyToolStripMenuItem";
			this->specOnlyToolStripMenuItem->Size = System::Drawing::Size(174, 22);
			this->specOnlyToolStripMenuItem->Text = L"Spec Only";
			this->specOnlyToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::specOnlyToolStripMenuItem_Click);
			// 
			// treeOnlyToolStripMenuItem
			// 
			this->treeOnlyToolStripMenuItem->Name = L"treeOnlyToolStripMenuItem";
			this->treeOnlyToolStripMenuItem->Size = System::Drawing::Size(174, 22);
			this->treeOnlyToolStripMenuItem->Text = L"Tree Only";
			this->treeOnlyToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::treeOnlyToolStripMenuItem_Click);
			// 
			// reporteToolStripMenuItem
			// 
			this->reporteToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->dise˝oBaseToolStripMenuItem, 
				this->estructuraToolStripMenuItem});
			this->reporteToolStripMenuItem->Name = L"reporteToolStripMenuItem";
			this->reporteToolStripMenuItem->Size = System::Drawing::Size(60, 20);
			this->reporteToolStripMenuItem->Text = L"Reporte";
			// 
			// dise˝oBaseToolStripMenuItem
			// 
			this->dise˝oBaseToolStripMenuItem->Name = L"dise˝oBaseToolStripMenuItem";
			this->dise˝oBaseToolStripMenuItem->Size = System::Drawing::Size(140, 22);
			this->dise˝oBaseToolStripMenuItem->Text = L"Dise˝o  Base";
			this->dise˝oBaseToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::dise˝oBaseToolStripMenuItem_Click);
			// 
			// estructuraToolStripMenuItem
			// 
			this->estructuraToolStripMenuItem->Name = L"estructuraToolStripMenuItem";
			this->estructuraToolStripMenuItem->Size = System::Drawing::Size(140, 22);
			this->estructuraToolStripMenuItem->Text = L"Estructura";
			this->estructuraToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::estructuraToolStripMenuItem_Click);
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Interval = 10;
			this->timer1->Tick += gcnew System::EventHandler(this, &Form1::timer1_Tick);
			// 
			// tabPage3
			// 
			this->tabPage3->BackColor = System::Drawing::Color::Transparent;
			this->tabPage3->Controls->Add(this->comboBox1);
			this->tabPage3->Controls->Add(this->panel20);
			this->tabPage3->Controls->Add(this->panel18);
			this->tabPage3->Controls->Add(this->panel16);
			this->tabPage3->Controls->Add(this->label39);
			this->tabPage3->Controls->Add(this->label38);
			this->tabPage3->Controls->Add(this->label37);
			this->tabPage3->Controls->Add(this->label36);
			this->tabPage3->Location = System::Drawing::Point(4, 22);
			this->tabPage3->Name = L"tabPage3";
			this->tabPage3->Padding = System::Windows::Forms::Padding(3);
			this->tabPage3->Size = System::Drawing::Size(490, 517);
			this->tabPage3->TabIndex = 2;
			this->tabPage3->Text = L"Accesorios";
			// 
			// comboBox1
			// 
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Items->AddRange(gcnew cli::array< System::Object^  >(3) {L"NORMAL", L"BASE I", L"BASE CON RUEDAS"});
			this->comboBox1->Location = System::Drawing::Point(139, 96);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(148, 21);
			this->comboBox1->TabIndex = 69;
			this->comboBox1->Text = L"NORMAL";
			// 
			// panel20
			// 
			this->panel20->Controls->Add(this->radioButton44);
			this->panel20->Controls->Add(this->radioButton43);
			this->panel20->Location = System::Drawing::Point(139, 129);
			this->panel20->Name = L"panel20";
			this->panel20->Size = System::Drawing::Size(148, 26);
			this->panel20->TabIndex = 68;
			// 
			// radioButton44
			// 
			this->radioButton44->AutoSize = true;
			this->radioButton44->Checked = true;
			this->radioButton44->Location = System::Drawing::Point(86, 6);
			this->radioButton44->Name = L"radioButton44";
			this->radioButton44->Size = System::Drawing::Size(41, 17);
			this->radioButton44->TabIndex = 1;
			this->radioButton44->TabStop = true;
			this->radioButton44->Text = L"NO";
			this->radioButton44->UseVisualStyleBackColor = true;
			// 
			// radioButton43
			// 
			this->radioButton43->AutoSize = true;
			this->radioButton43->Location = System::Drawing::Point(21, 6);
			this->radioButton43->Name = L"radioButton43";
			this->radioButton43->Size = System::Drawing::Size(35, 17);
			this->radioButton43->TabIndex = 0;
			this->radioButton43->Text = L"SI";
			this->radioButton43->UseVisualStyleBackColor = true;
			// 
			// panel18
			// 
			this->panel18->Controls->Add(this->radioButton42);
			this->panel18->Controls->Add(this->radioButton41);
			this->panel18->Location = System::Drawing::Point(139, 56);
			this->panel18->Name = L"panel18";
			this->panel18->Size = System::Drawing::Size(148, 26);
			this->panel18->TabIndex = 66;
			// 
			// radioButton42
			// 
			this->radioButton42->AutoSize = true;
			this->radioButton42->Checked = true;
			this->radioButton42->Location = System::Drawing::Point(86, 6);
			this->radioButton42->Name = L"radioButton42";
			this->radioButton42->Size = System::Drawing::Size(41, 17);
			this->radioButton42->TabIndex = 1;
			this->radioButton42->TabStop = true;
			this->radioButton42->Text = L"NO";
			this->radioButton42->UseVisualStyleBackColor = true;
			// 
			// radioButton41
			// 
			this->radioButton41->AutoSize = true;
			this->radioButton41->Location = System::Drawing::Point(21, 6);
			this->radioButton41->Name = L"radioButton41";
			this->radioButton41->Size = System::Drawing::Size(35, 17);
			this->radioButton41->TabIndex = 0;
			this->radioButton41->Text = L"SI";
			this->radioButton41->UseVisualStyleBackColor = true;
			// 
			// panel16
			// 
			this->panel16->Controls->Add(this->radioButton40);
			this->panel16->Controls->Add(this->radioButton39);
			this->panel16->Location = System::Drawing::Point(139, 19);
			this->panel16->Name = L"panel16";
			this->panel16->Size = System::Drawing::Size(148, 26);
			this->panel16->TabIndex = 64;
			// 
			// radioButton40
			// 
			this->radioButton40->AutoSize = true;
			this->radioButton40->Checked = true;
			this->radioButton40->Location = System::Drawing::Point(86, 3);
			this->radioButton40->Name = L"radioButton40";
			this->radioButton40->Size = System::Drawing::Size(41, 17);
			this->radioButton40->TabIndex = 1;
			this->radioButton40->TabStop = true;
			this->radioButton40->Text = L"NO";
			this->radioButton40->UseVisualStyleBackColor = true;
			// 
			// radioButton39
			// 
			this->radioButton39->AutoSize = true;
			this->radioButton39->Location = System::Drawing::Point(21, 3);
			this->radioButton39->Name = L"radioButton39";
			this->radioButton39->Size = System::Drawing::Size(35, 17);
			this->radioButton39->TabIndex = 0;
			this->radioButton39->Text = L"SI";
			this->radioButton39->UseVisualStyleBackColor = true;
			// 
			// label39
			// 
			this->label39->AutoSize = true;
			this->label39->Location = System::Drawing::Point(19, 142);
			this->label39->Name = L"label39";
			this->label39->Size = System::Drawing::Size(87, 13);
			this->label39->TabIndex = 3;
			this->label39->Text = L"Apoyo para Gato";
			// 
			// label38
			// 
			this->label38->AutoSize = true;
			this->label38->Location = System::Drawing::Point(19, 104);
			this->label38->Name = L"label38";
			this->label38->Size = System::Drawing::Size(70, 13);
			this->label38->TabIndex = 2;
			this->label38->Text = L"Tipo de Base";
			// 
			// label37
			// 
			this->label37->AutoSize = true;
			this->label37->Location = System::Drawing::Point(19, 69);
			this->label37->Name = L"label37";
			this->label37->Size = System::Drawing::Size(107, 13);
			this->label37->TabIndex = 1;
			this->label37->Text = L"Tanque Conservador";
			// 
			// label36
			// 
			this->label36->AutoSize = true;
			this->label36->Location = System::Drawing::Point(19, 32);
			this->label36->Name = L"label36";
			this->label36->Size = System::Drawing::Size(30, 13);
			this->label36->TabIndex = 0;
			this->label36->Text = L"TC\'S";
			// 
			// tabPage2
			// 
			this->tabPage2->BackColor = System::Drawing::Color::Transparent;
			this->tabPage2->Controls->Add(this->textBox5);
			this->tabPage2->Controls->Add(this->panel2);
			this->tabPage2->Controls->Add(this->comboBox6);
			this->tabPage2->Controls->Add(this->comboBox5);
			this->tabPage2->Controls->Add(this->comboBox4);
			this->tabPage2->Controls->Add(this->comboBox3);
			this->tabPage2->Controls->Add(this->comboBox2);
			this->tabPage2->Controls->Add(this->label35);
			this->tabPage2->Controls->Add(this->label34);
			this->tabPage2->Controls->Add(this->label33);
			this->tabPage2->Controls->Add(this->label32);
			this->tabPage2->Controls->Add(this->label31);
			this->tabPage2->Controls->Add(this->label30);
			this->tabPage2->Controls->Add(this->label29);
			this->tabPage2->Location = System::Drawing::Point(4, 22);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(490, 517);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"Datos Mecanico";
			// 
			// textBox5
			// 
			this->textBox5->Location = System::Drawing::Point(133, 240);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(121, 20);
			this->textBox5->TabIndex = 52;
			this->textBox5->Text = L"0";
			// 
			// panel2
			// 
			this->panel2->Controls->Add(this->radioButton6);
			this->panel2->Controls->Add(this->radioButton5);
			this->panel2->Location = System::Drawing::Point(133, 201);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(147, 22);
			this->panel2->TabIndex = 51;
			// 
			// radioButton6
			// 
			this->radioButton6->AutoSize = true;
			this->radioButton6->Location = System::Drawing::Point(96, 2);
			this->radioButton6->Name = L"radioButton6";
			this->radioButton6->Size = System::Drawing::Size(85, 17);
			this->radioButton6->TabIndex = 1;
			this->radioButton6->TabStop = true;
			this->radioButton6->Text = L"radioButton6";
			this->radioButton6->UseVisualStyleBackColor = true;
			// 
			// radioButton5
			// 
			this->radioButton5->AutoSize = true;
			this->radioButton5->Location = System::Drawing::Point(4, 2);
			this->radioButton5->Name = L"radioButton5";
			this->radioButton5->Size = System::Drawing::Size(35, 17);
			this->radioButton5->TabIndex = 0;
			this->radioButton5->TabStop = true;
			this->radioButton5->Text = L"SI";
			this->radioButton5->UseVisualStyleBackColor = true;
			// 
			// comboBox6
			// 
			this->comboBox6->FormattingEnabled = true;
			this->comboBox6->Items->AddRange(gcnew cli::array< System::Object^  >(2) {L"IZQUIERDO", L"DERECHO"});
			this->comboBox6->Location = System::Drawing::Point(133, 168);
			this->comboBox6->Name = L"comboBox6";
			this->comboBox6->Size = System::Drawing::Size(121, 21);
			this->comboBox6->TabIndex = 15;
			this->comboBox6->Text = L"IZQUIERDO";
			// 
			// comboBox5
			// 
			this->comboBox5->FormattingEnabled = true;
			this->comboBox5->Location = System::Drawing::Point(133, 126);
			this->comboBox5->Name = L"comboBox5";
			this->comboBox5->Size = System::Drawing::Size(121, 21);
			this->comboBox5->TabIndex = 14;
			this->comboBox5->Text = L"GARGANTA";
			// 
			// comboBox4
			// 
			this->comboBox4->FormattingEnabled = true;
			this->comboBox4->Location = System::Drawing::Point(133, 90);
			this->comboBox4->Name = L"comboBox4";
			this->comboBox4->Size = System::Drawing::Size(121, 21);
			this->comboBox4->TabIndex = 13;
			this->comboBox4->Text = L"GARGANTA";
			// 
			// comboBox3
			// 
			this->comboBox3->FormattingEnabled = true;
			this->comboBox3->Location = System::Drawing::Point(133, 52);
			this->comboBox3->Name = L"comboBox3";
			this->comboBox3->Size = System::Drawing::Size(121, 21);
			this->comboBox3->TabIndex = 12;
			this->comboBox3->Text = L"ESTANDAR";
			// 
			// comboBox2
			// 
			this->comboBox2->FormattingEnabled = true;
			this->comboBox2->Items->AddRange(gcnew cli::array< System::Object^  >(2) {L"ESTACION", L"CANGREJO"});
			this->comboBox2->Location = System::Drawing::Point(133, 14);
			this->comboBox2->Name = L"comboBox2";
			this->comboBox2->Size = System::Drawing::Size(121, 21);
			this->comboBox2->TabIndex = 11;
			this->comboBox2->Text = L"ESTACION";
			// 
			// label35
			// 
			this->label35->AutoSize = true;
			this->label35->Location = System::Drawing::Point(6, 247);
			this->label35->Name = L"label35";
			this->label35->Size = System::Drawing::Size(80, 13);
			this->label35->TabIndex = 10;
			this->label35->Text = L"Cant Refuerzos";
			// 
			// label34
			// 
			this->label34->AutoSize = true;
			this->label34->Location = System::Drawing::Point(6, 210);
			this->label34->Name = L"label34";
			this->label34->Size = System::Drawing::Size(87, 13);
			this->label34->TabIndex = 9;
			this->label34->Text = L"Boquilla en Tapa";
			// 
			// label33
			// 
			this->label33->AutoSize = true;
			this->label33->Location = System::Drawing::Point(6, 176);
			this->label33->Name = L"label33";
			this->label33->Size = System::Drawing::Size(37, 13);
			this->label33->TabIndex = 8;
			this->label33->Text = L"Frente";
			// 
			// label32
			// 
			this->label32->AutoSize = true;
			this->label32->Location = System::Drawing::Point(6, 134);
			this->label32->Name = L"label32";
			this->label32->Size = System::Drawing::Size(65, 13);
			this->label32->TabIndex = 7;
			this->label32->Text = L"Interfase AT";
			// 
			// label31
			// 
			this->label31->AutoSize = true;
			this->label31->Location = System::Drawing::Point(6, 98);
			this->label31->Name = L"label31";
			this->label31->Size = System::Drawing::Size(65, 13);
			this->label31->TabIndex = 6;
			this->label31->Text = L"Interfase BT";
			// 
			// label30
			// 
			this->label30->AutoSize = true;
			this->label30->Location = System::Drawing::Point(6, 60);
			this->label30->Name = L"label30";
			this->label30->Size = System::Drawing::Size(80, 13);
			this->label30->TabIndex = 5;
			this->label30->Text = L"Tipo Gargantas";
			// 
			// label29
			// 
			this->label29->AutoSize = true;
			this->label29->Location = System::Drawing::Point(6, 22);
			this->label29->Name = L"label29";
			this->label29->Size = System::Drawing::Size(99, 13);
			this->label29->TabIndex = 4;
			this->label29->Text = L"Tipo Transformador";
			// 
			// tabPage1
			// 
			this->tabPage1->AutoScroll = true;
			this->tabPage1->BackColor = System::Drawing::Color::Transparent;
			this->tabPage1->Controls->Add(this->comboBox11);
			this->tabPage1->Controls->Add(this->comboBox10);
			this->tabPage1->Controls->Add(this->comboBox9);
			this->tabPage1->Controls->Add(this->comboBox8);
			this->tabPage1->Controls->Add(this->panel14);
			this->tabPage1->Controls->Add(this->panel8);
			this->tabPage1->Controls->Add(this->panel7);
			this->tabPage1->Controls->Add(this->panel6);
			this->tabPage1->Controls->Add(this->label27);
			this->tabPage1->Controls->Add(this->label26);
			this->tabPage1->Controls->Add(this->label21);
			this->tabPage1->Controls->Add(this->label20);
			this->tabPage1->Controls->Add(this->label19);
			this->tabPage1->Controls->Add(this->label18);
			this->tabPage1->Controls->Add(this->label17);
			this->tabPage1->Controls->Add(this->label16);
			this->tabPage1->Controls->Add(this->label11);
			this->tabPage1->Controls->Add(this->textBox8);
			this->tabPage1->Controls->Add(this->textBox7);
			this->tabPage1->Controls->Add(this->textBox6);
			this->tabPage1->Controls->Add(this->textBox4);
			this->tabPage1->Controls->Add(this->textBox3);
			this->tabPage1->Controls->Add(this->textBox2);
			this->tabPage1->Controls->Add(this->textBox1);
			this->tabPage1->Controls->Add(this->label8);
			this->tabPage1->Controls->Add(this->label7);
			this->tabPage1->Controls->Add(this->label6);
			this->tabPage1->Controls->Add(this->label4);
			this->tabPage1->Controls->Add(this->label3);
			this->tabPage1->Controls->Add(this->label2);
			this->tabPage1->Controls->Add(this->label1);
			this->tabPage1->Controls->Add(this->panel13);
			this->tabPage1->Location = System::Drawing::Point(4, 22);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(490, 517);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"Datos Principales";
			// 
			// comboBox11
			// 
			this->comboBox11->FormattingEnabled = true;
			this->comboBox11->Items->AddRange(gcnew cli::array< System::Object^  >(1) {L"OA"});
			this->comboBox11->Location = System::Drawing::Point(163, 389);
			this->comboBox11->Name = L"comboBox11";
			this->comboBox11->Size = System::Drawing::Size(147, 21);
			this->comboBox11->TabIndex = 59;
			this->comboBox11->Text = L"OA";
			// 
			// comboBox10
			// 
			this->comboBox10->FormattingEnabled = true;
			this->comboBox10->Items->AddRange(gcnew cli::array< System::Object^  >(4) {L"1000", L"2000", L"2300", L"2500"});
			this->comboBox10->Location = System::Drawing::Point(163, 329);
			this->comboBox10->Name = L"comboBox10";
			this->comboBox10->Size = System::Drawing::Size(148, 21);
			this->comboBox10->TabIndex = 58;
			this->comboBox10->Text = L"2300";
			// 
			// comboBox9
			// 
			this->comboBox9->FormattingEnabled = true;
			this->comboBox9->Items->AddRange(gcnew cli::array< System::Object^  >(2) {L"225", L"300"});
			this->comboBox9->Location = System::Drawing::Point(163, 267);
			this->comboBox9->Name = L"comboBox9";
			this->comboBox9->Size = System::Drawing::Size(148, 21);
			this->comboBox9->TabIndex = 57;
			this->comboBox9->Text = L"225";
			// 
			// comboBox8
			// 
			this->comboBox8->FormattingEnabled = true;
			this->comboBox8->Items->AddRange(gcnew cli::array< System::Object^  >(3) {L"N-MX-J-284", L"N-MX-J-116", L"NOM-002-SEDE"});
			this->comboBox8->Location = System::Drawing::Point(163, 210);
			this->comboBox8->Name = L"comboBox8";
			this->comboBox8->Size = System::Drawing::Size(148, 21);
			this->comboBox8->TabIndex = 56;
			this->comboBox8->Text = L"N-MX-J-284";
			// 
			// panel14
			// 
			this->panel14->Controls->Add(this->radioButton34);
			this->panel14->Controls->Add(this->radioButton33);
			this->panel14->Location = System::Drawing::Point(163, 442);
			this->panel14->Name = L"panel14";
			this->panel14->Size = System::Drawing::Size(147, 22);
			this->panel14->TabIndex = 50;
			// 
			// radioButton34
			// 
			this->radioButton34->AutoSize = true;
			this->radioButton34->Checked = true;
			this->radioButton34->Location = System::Drawing::Point(86, 3);
			this->radioButton34->Name = L"radioButton34";
			this->radioButton34->Size = System::Drawing::Size(41, 17);
			this->radioButton34->TabIndex = 1;
			this->radioButton34->TabStop = true;
			this->radioButton34->Text = L"NO";
			this->radioButton34->UseVisualStyleBackColor = true;
			// 
			// radioButton33
			// 
			this->radioButton33->AutoSize = true;
			this->radioButton33->Location = System::Drawing::Point(21, 3);
			this->radioButton33->Name = L"radioButton33";
			this->radioButton33->Size = System::Drawing::Size(35, 17);
			this->radioButton33->TabIndex = 0;
			this->radioButton33->Text = L"SI";
			this->radioButton33->UseVisualStyleBackColor = true;
			// 
			// panel8
			// 
			this->panel8->Controls->Add(this->radioButton22);
			this->panel8->Controls->Add(this->radioButton21);
			this->panel8->Controls->Add(this->radioButton20);
			this->panel8->Controls->Add(this->radioButton19);
			this->panel8->Location = System::Drawing::Point(163, 357);
			this->panel8->Name = L"panel8";
			this->panel8->Size = System::Drawing::Size(245, 22);
			this->panel8->TabIndex = 44;
			// 
			// radioButton22
			// 
			this->radioButton22->AutoSize = true;
			this->radioButton22->Location = System::Drawing::Point(123, 2);
			this->radioButton22->Name = L"radioButton22";
			this->radioButton22->Size = System::Drawing::Size(54, 17);
			this->radioButton22->TabIndex = 3;
			this->radioButton22->Text = L"55/65";
			this->radioButton22->UseVisualStyleBackColor = true;
			// 
			// radioButton21
			// 
			this->radioButton21->AutoSize = true;
			this->radioButton21->Location = System::Drawing::Point(182, 2);
			this->radioButton21->Name = L"radioButton21";
			this->radioButton21->Size = System::Drawing::Size(54, 17);
			this->radioButton21->TabIndex = 2;
			this->radioButton21->Text = L"45/55";
			this->radioButton21->UseVisualStyleBackColor = true;
			// 
			// radioButton20
			// 
			this->radioButton20->AutoSize = true;
			this->radioButton20->Checked = true;
			this->radioButton20->Location = System::Drawing::Point(74, 2);
			this->radioButton20->Name = L"radioButton20";
			this->radioButton20->Size = System::Drawing::Size(37, 17);
			this->radioButton20->TabIndex = 1;
			this->radioButton20->TabStop = true;
			this->radioButton20->Text = L"65";
			this->radioButton20->UseVisualStyleBackColor = true;
			// 
			// radioButton19
			// 
			this->radioButton19->AutoSize = true;
			this->radioButton19->Location = System::Drawing::Point(28, 2);
			this->radioButton19->Name = L"radioButton19";
			this->radioButton19->Size = System::Drawing::Size(37, 17);
			this->radioButton19->TabIndex = 0;
			this->radioButton19->Text = L"55";
			this->radioButton19->UseVisualStyleBackColor = true;
			// 
			// panel7
			// 
			this->panel7->Controls->Add(this->radioButton18);
			this->panel7->Controls->Add(this->radioButton17);
			this->panel7->Location = System::Drawing::Point(163, 295);
			this->panel7->Name = L"panel7";
			this->panel7->Size = System::Drawing::Size(148, 22);
			this->panel7->TabIndex = 43;
			// 
			// radioButton18
			// 
			this->radioButton18->AutoSize = true;
			this->radioButton18->Checked = true;
			this->radioButton18->Location = System::Drawing::Point(86, 2);
			this->radioButton18->Name = L"radioButton18";
			this->radioButton18->Size = System::Drawing::Size(37, 17);
			this->radioButton18->TabIndex = 1;
			this->radioButton18->TabStop = true;
			this->radioButton18->Text = L"60";
			this->radioButton18->UseVisualStyleBackColor = true;
			// 
			// radioButton17
			// 
			this->radioButton17->AutoSize = true;
			this->radioButton17->Location = System::Drawing::Point(21, 2);
			this->radioButton17->Name = L"radioButton17";
			this->radioButton17->Size = System::Drawing::Size(37, 17);
			this->radioButton17->TabIndex = 0;
			this->radioButton17->Text = L"50";
			this->radioButton17->UseVisualStyleBackColor = true;
			// 
			// panel6
			// 
			this->panel6->Controls->Add(this->radioButton16);
			this->panel6->Controls->Add(this->radioButton15);
			this->panel6->Location = System::Drawing::Point(163, 238);
			this->panel6->Name = L"panel6";
			this->panel6->Size = System::Drawing::Size(148, 23);
			this->panel6->TabIndex = 42;
			// 
			// radioButton16
			// 
			this->radioButton16->AutoSize = true;
			this->radioButton16->Checked = true;
			this->radioButton16->Location = System::Drawing::Point(86, 3);
			this->radioButton16->Name = L"radioButton16";
			this->radioButton16->Size = System::Drawing::Size(31, 17);
			this->radioButton16->TabIndex = 1;
			this->radioButton16->TabStop = true;
			this->radioButton16->Text = L"3";
			this->radioButton16->UseVisualStyleBackColor = true;
			// 
			// radioButton15
			// 
			this->radioButton15->AutoSize = true;
			this->radioButton15->Location = System::Drawing::Point(21, 3);
			this->radioButton15->Name = L"radioButton15";
			this->radioButton15->Size = System::Drawing::Size(31, 17);
			this->radioButton15->TabIndex = 0;
			this->radioButton15->Text = L"1";
			this->radioButton15->UseVisualStyleBackColor = true;
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->Location = System::Drawing::Point(3, 451);
			this->label27->Name = L"label27";
			this->label27->Size = System::Drawing::Size(71, 13);
			this->label27->TabIndex = 35;
			this->label27->Text = L"Zona Sismica";
			// 
			// label26
			// 
			this->label26->AutoSize = true;
			this->label26->Location = System::Drawing::Point(3, 423);
			this->label26->Name = L"label26";
			this->label26->Size = System::Drawing::Size(112, 13);
			this->label26->TabIndex = 34;
			this->label26->Text = L"Pantalla Electrostatica";
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(3, 397);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(94, 13);
			this->label21->TabIndex = 29;
			this->label21->Text = L"Tipos Enfriamiento";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(3, 366);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(147, 13);
			this->label20->TabIndex = 28;
			this->label20->Text = L"Sobreelavion de Temperatura";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(3, 337);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(72, 13);
			this->label19->TabIndex = 27;
			this->label19->Text = L"Altitud (msnm)";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(3, 304);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(60, 13);
			this->label18->TabIndex = 26;
			this->label18->Text = L"Frecuencia";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(3, 275);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(31, 13);
			this->label17->TabIndex = 25;
			this->label17->Text = L"Kvas";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(3, 244);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(35, 13);
			this->label16->TabIndex = 24;
			this->label16->Text = L"Fases";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(3, 220);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(38, 13);
			this->label11->TabIndex = 19;
			this->label11->Text = L"Norma";
			// 
			// textBox8
			// 
			this->textBox8->Location = System::Drawing::Point(163, 184);
			this->textBox8->Name = L"textBox8";
			this->textBox8->Size = System::Drawing::Size(128, 20);
			this->textBox8->TabIndex = 16;
			this->textBox8->Text = L"13.5";
			// 
			// textBox7
			// 
			this->textBox7->Location = System::Drawing::Point(163, 153);
			this->textBox7->Name = L"textBox7";
			this->textBox7->Size = System::Drawing::Size(128, 20);
			this->textBox7->TabIndex = 15;
			this->textBox7->Text = L"0";
			// 
			// textBox6
			// 
			this->textBox6->Location = System::Drawing::Point(163, 118);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(128, 20);
			this->textBox6->TabIndex = 14;
			this->textBox6->Text = L"2";
			this->textBox6->TextChanged += gcnew System::EventHandler(this, &Form1::textBox6_TextChanged);
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(166, 59);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(246, 20);
			this->textBox4->TabIndex = 12;
			this->textBox4->Text = L"1";
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(166, 89);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(246, 20);
			this->textBox3->TabIndex = 11;
			this->textBox3->Text = L"jfa";
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(166, 30);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(246, 20);
			this->textBox2->TabIndex = 10;
			this->textBox2->Text = L"is13t";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(166, 3);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(246, 20);
			this->textBox1->TabIndex = 9;
			this->textBox1->Text = L"Pro. Elect de Tijuana";
			this->textBox1->TextChanged += gcnew System::EventHandler(this, &Form1::textBox1_TextChanged);
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(3, 193);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(43, 13);
			this->label8->TabIndex = 7;
			this->label8->Text = L"Paridad";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(3, 158);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(150, 13);
			this->label7->TabIndex = 6;
			this->label7->Text = L"Cte. de Evaluacion Devanado";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(3, 127);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(115, 13);
			this->label6->TabIndex = 5;
			this->label6->Text = L"Altura Ventana nucleo ";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(6, 98);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(70, 13);
			this->label4->TabIndex = 3;
			this->label4->Text = L"Dise˝ado por";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(6, 68);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(49, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Cantidad";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(6, 37);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(91, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Numero de Orden";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(6, 12);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(39, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Cliente";
			// 
			// panel13
			// 
			this->panel13->Controls->Add(this->radioButton32);
			this->panel13->Controls->Add(this->radioButton31);
			this->panel13->Location = System::Drawing::Point(163, 414);
			this->panel13->Name = L"panel13";
			this->panel13->Size = System::Drawing::Size(147, 22);
			this->panel13->TabIndex = 49;
			// 
			// radioButton32
			// 
			this->radioButton32->AutoSize = true;
			this->radioButton32->Checked = true;
			this->radioButton32->Location = System::Drawing::Point(86, 2);
			this->radioButton32->Name = L"radioButton32";
			this->radioButton32->Size = System::Drawing::Size(41, 17);
			this->radioButton32->TabIndex = 1;
			this->radioButton32->TabStop = true;
			this->radioButton32->Text = L"NO";
			this->radioButton32->UseVisualStyleBackColor = true;
			// 
			// radioButton31
			// 
			this->radioButton31->AutoSize = true;
			this->radioButton31->Location = System::Drawing::Point(21, 2);
			this->radioButton31->Name = L"radioButton31";
			this->radioButton31->Size = System::Drawing::Size(35, 17);
			this->radioButton31->TabIndex = 0;
			this->radioButton31->Text = L"SI";
			this->radioButton31->UseVisualStyleBackColor = true;
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Controls->Add(this->tabPage3);
			this->tabControl1->Location = System::Drawing::Point(442, 27);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(498, 543);
			this->tabControl1->TabIndex = 0;
			// 
			// treeView1
			// 
			this->treeView1->ContextMenuStrip = this->MenuTree;
			this->treeView1->Location = System::Drawing::Point(0, 295);
			this->treeView1->Name = L"treeView1";
			this->treeView1->Size = System::Drawing::Size(426, 275);
			this->treeView1->TabIndex = 0;
			this->treeView1->Click += gcnew System::EventHandler(this, &Form1::treeView1_Click);
			// 
			// MenuTree
			// 
			this->MenuTree->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(5) {this->showToolStripMenuItem, 
				this->hidenToolStripMenuItem, this->solidToolStripMenuItem, this->wireframeToolStripMenuItem, this->inspectorToolStripMenuItem});
			this->MenuTree->Name = L"MenuTree";
			this->MenuTree->Size = System::Drawing::Size(130, 114);
			// 
			// showToolStripMenuItem
			// 
			this->showToolStripMenuItem->Name = L"showToolStripMenuItem";
			this->showToolStripMenuItem->Size = System::Drawing::Size(129, 22);
			this->showToolStripMenuItem->Text = L"&Show";
			this->showToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::showToolStripMenuItem_Click);
			// 
			// hidenToolStripMenuItem
			// 
			this->hidenToolStripMenuItem->Name = L"hidenToolStripMenuItem";
			this->hidenToolStripMenuItem->Size = System::Drawing::Size(129, 22);
			this->hidenToolStripMenuItem->Text = L"&Hiden";
			this->hidenToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::hidenToolStripMenuItem_Click);
			// 
			// solidToolStripMenuItem
			// 
			this->solidToolStripMenuItem->Name = L"solidToolStripMenuItem";
			this->solidToolStripMenuItem->Size = System::Drawing::Size(129, 22);
			this->solidToolStripMenuItem->Text = L"S&olid";
			this->solidToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::solidToolStripMenuItem_Click);
			// 
			// wireframeToolStripMenuItem
			// 
			this->wireframeToolStripMenuItem->Name = L"wireframeToolStripMenuItem";
			this->wireframeToolStripMenuItem->Size = System::Drawing::Size(129, 22);
			this->wireframeToolStripMenuItem->Text = L"&Wireframe";
			this->wireframeToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::wireframeToolStripMenuItem_Click);
			// 
			// inspectorToolStripMenuItem
			// 
			this->inspectorToolStripMenuItem->Name = L"inspectorToolStripMenuItem";
			this->inspectorToolStripMenuItem->Size = System::Drawing::Size(129, 22);
			this->inspectorToolStripMenuItem->Text = L"Inspector";
			this->inspectorToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::inspectorToolStripMenuItem_Click);
			// 
			// radioButton1
			// 
			this->radioButton1->AutoSize = true;
			this->radioButton1->Checked = true;
			this->radioButton1->Location = System::Drawing::Point(86, 2);
			this->radioButton1->Name = L"radioButton1";
			this->radioButton1->Size = System::Drawing::Size(41, 17);
			this->radioButton1->TabIndex = 1;
			this->radioButton1->TabStop = true;
			this->radioButton1->Text = L"NO";
			this->radioButton1->UseVisualStyleBackColor = true;
			// 
			// radioButton2
			// 
			this->radioButton2->AutoSize = true;
			this->radioButton2->Location = System::Drawing::Point(21, 2);
			this->radioButton2->Name = L"radioButton2";
			this->radioButton2->Size = System::Drawing::Size(35, 17);
			this->radioButton2->TabIndex = 0;
			this->radioButton2->Text = L"SI";
			this->radioButton2->UseVisualStyleBackColor = true;
			// 
			// ZoomIn
			// 
			this->ZoomIn->AccessibleRole = System::Windows::Forms::AccessibleRole::Caret;
			this->ZoomIn->BackColor = System::Drawing::Color::Black;
			this->ZoomIn->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"ZoomIn.Image")));
			this->ZoomIn->Location = System::Drawing::Point(425, 348);
			this->ZoomIn->Name = L"ZoomIn";
			this->ZoomIn->Size = System::Drawing::Size(18, 26);
			this->ZoomIn->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->ZoomIn->TabIndex = 1;
			this->ZoomIn->TabStop = false;
			this->ZoomIn->Visible = false;
			this->ZoomIn->Click += gcnew System::EventHandler(this, &Form1::ZoomIn_Click);
			// 
			// ZoomOut
			// 
			this->ZoomOut->AccessibleRole = System::Windows::Forms::AccessibleRole::Caret;
			this->ZoomOut->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"ZoomOut.Image")));
			this->ZoomOut->Location = System::Drawing::Point(425, 316);
			this->ZoomOut->Name = L"ZoomOut";
			this->ZoomOut->Size = System::Drawing::Size(18, 26);
			this->ZoomOut->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->ZoomOut->TabIndex = 2;
			this->ZoomOut->TabStop = false;
			this->ZoomOut->Visible = false;
			this->ZoomOut->Click += gcnew System::EventHandler(this, &Form1::ZoomOut_Click);
			// 
			// ArrowTop
			// 
			this->ArrowTop->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"ArrowTop.Image")));
			this->ArrowTop->Location = System::Drawing::Point(385, 482);
			this->ArrowTop->Name = L"ArrowTop";
			this->ArrowTop->Size = System::Drawing::Size(24, 26);
			this->ArrowTop->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->ArrowTop->TabIndex = 3;
			this->ArrowTop->TabStop = false;
			this->ArrowTop->Visible = false;
			this->ArrowTop->Click += gcnew System::EventHandler(this, &Form1::ArrowTop_Click);
			// 
			// ArrowRight
			// 
			this->ArrowRight->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"ArrowRight.Image")));
			this->ArrowRight->Location = System::Drawing::Point(407, 507);
			this->ArrowRight->Name = L"ArrowRight";
			this->ArrowRight->Size = System::Drawing::Size(24, 26);
			this->ArrowRight->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->ArrowRight->TabIndex = 4;
			this->ArrowRight->TabStop = false;
			this->ArrowRight->Visible = false;
			this->ArrowRight->Click += gcnew System::EventHandler(this, &Form1::ArrowRight_Click);
			// 
			// ArrowLeft
			// 
			this->ArrowLeft->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"ArrowLeft.Image")));
			this->ArrowLeft->Location = System::Drawing::Point(360, 507);
			this->ArrowLeft->Name = L"ArrowLeft";
			this->ArrowLeft->Size = System::Drawing::Size(24, 26);
			this->ArrowLeft->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->ArrowLeft->TabIndex = 5;
			this->ArrowLeft->TabStop = false;
			this->ArrowLeft->Visible = false;
			this->ArrowLeft->Click += gcnew System::EventHandler(this, &Form1::ArrowLeft_Click);
			// 
			// ArrowDown
			// 
			this->ArrowDown->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"ArrowDown.Image")));
			this->ArrowDown->Location = System::Drawing::Point(385, 529);
			this->ArrowDown->Name = L"ArrowDown";
			this->ArrowDown->Size = System::Drawing::Size(24, 26);
			this->ArrowDown->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->ArrowDown->TabIndex = 6;
			this->ArrowDown->TabStop = false;
			this->ArrowDown->Visible = false;
			this->ArrowDown->Click += gcnew System::EventHandler(this, &Form1::ArrowDown_Click);
			// 
			// ArrowTR
			// 
			this->ArrowTR->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"ArrowTR.Image")));
			this->ArrowTR->Location = System::Drawing::Point(407, 482);
			this->ArrowTR->Name = L"ArrowTR";
			this->ArrowTR->Size = System::Drawing::Size(24, 26);
			this->ArrowTR->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->ArrowTR->TabIndex = 7;
			this->ArrowTR->TabStop = false;
			this->ArrowTR->Visible = false;
			this->ArrowTR->Click += gcnew System::EventHandler(this, &Form1::ArrowTR_Click);
			// 
			// ArrowTL
			// 
			this->ArrowTL->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"ArrowTL.Image")));
			this->ArrowTL->Location = System::Drawing::Point(360, 482);
			this->ArrowTL->Name = L"ArrowTL";
			this->ArrowTL->Size = System::Drawing::Size(24, 26);
			this->ArrowTL->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->ArrowTL->TabIndex = 8;
			this->ArrowTL->TabStop = false;
			this->ArrowTL->Visible = false;
			this->ArrowTL->Click += gcnew System::EventHandler(this, &Form1::ArrowTL_Click);
			// 
			// ArrowDL
			// 
			this->ArrowDL->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"ArrowDL.Image")));
			this->ArrowDL->Location = System::Drawing::Point(360, 529);
			this->ArrowDL->Name = L"ArrowDL";
			this->ArrowDL->Size = System::Drawing::Size(24, 26);
			this->ArrowDL->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->ArrowDL->TabIndex = 9;
			this->ArrowDL->TabStop = false;
			this->ArrowDL->Visible = false;
			this->ArrowDL->Click += gcnew System::EventHandler(this, &Form1::ArrowDL_Click);
			// 
			// ArrowDR
			// 
			this->ArrowDR->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"ArrowDR.Image")));
			this->ArrowDR->Location = System::Drawing::Point(407, 529);
			this->ArrowDR->Name = L"ArrowDR";
			this->ArrowDR->Size = System::Drawing::Size(24, 26);
			this->ArrowDR->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->ArrowDR->TabIndex = 10;
			this->ArrowDR->TabStop = false;
			this->ArrowDR->Visible = false;
			this->ArrowDR->Click += gcnew System::EventHandler(this, &Form1::ArrowDR_Click);
			// 
			// saveDibujo
			// 
			this->saveDibujo->AddExtension = false;
			this->saveDibujo->FileName = L"D:\\Dibujo1.lsp";
			this->saveDibujo->Filter = L"*.LSP|";
			this->saveDibujo->InitialDirectory = L"D:\\";
			// 
			// MuevoGrafico
			// 
			this->MuevoGrafico->BackColor = System::Drawing::Color::Transparent;
			this->MuevoGrafico->Location = System::Drawing::Point(0, 27);
			this->MuevoGrafico->Name = L"MuevoGrafico";
			this->MuevoGrafico->Size = System::Drawing::Size(85, 60);
			this->MuevoGrafico->TabIndex = 11;
			this->MuevoGrafico->TabStop = false;
			this->MuevoGrafico->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &Form1::MuevoGrafico_MouseMove);
			// 
			// lblDebugeo
			// 
			this->lblDebugeo->AutoSize = true;
			this->lblDebugeo->Location = System::Drawing::Point(330, 46);
			this->lblDebugeo->Name = L"lblDebugeo";
			this->lblDebugeo->Size = System::Drawing::Size(35, 13);
			this->lblDebugeo->TabIndex = 12;
			this->lblDebugeo->Text = L"label5";
			this->lblDebugeo->Visible = false;
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::Control;
			this->ClientSize = System::Drawing::Size(926, 567);
			this->Controls->Add(this->lblDebugeo);
			this->Controls->Add(this->ArrowDown);
			this->Controls->Add(this->ArrowDR);
			this->Controls->Add(this->ArrowDL);
			this->Controls->Add(this->ArrowTL);
			this->Controls->Add(this->ArrowTR);
			this->Controls->Add(this->ZoomOut);
			this->Controls->Add(this->ArrowLeft);
			this->Controls->Add(this->ArrowRight);
			this->Controls->Add(this->ArrowTop);
			this->Controls->Add(this->ZoomIn);
			this->Controls->Add(this->treeView1);
			this->Controls->Add(this->tabControl1);
			this->Controls->Add(this->menuStrip1);
			this->Controls->Add(this->MuevoGrafico);
			this->Name = L"Form1";
			this->Text = L"Tipo Transformador";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->MaximizedBoundsChanged += gcnew System::EventHandler(this, &Form1::Form1_MaximizedBoundsChanged);
			this->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &Form1::Form1_MouseClick);
			this->SizeChanged += gcnew System::EventHandler(this, &Form1::Form1_SizeChanged);
			this->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &Form1::Form1_MouseMove);
			this->MaximumSizeChanged += gcnew System::EventHandler(this, &Form1::Form1_MaximumSizeChanged);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->tabPage3->ResumeLayout(false);
			this->tabPage3->PerformLayout();
			this->panel20->ResumeLayout(false);
			this->panel20->PerformLayout();
			this->panel18->ResumeLayout(false);
			this->panel18->PerformLayout();
			this->panel16->ResumeLayout(false);
			this->panel16->PerformLayout();
			this->tabPage2->ResumeLayout(false);
			this->tabPage2->PerformLayout();
			this->panel2->ResumeLayout(false);
			this->panel2->PerformLayout();
			this->tabPage1->ResumeLayout(false);
			this->tabPage1->PerformLayout();
			this->panel14->ResumeLayout(false);
			this->panel14->PerformLayout();
			this->panel8->ResumeLayout(false);
			this->panel8->PerformLayout();
			this->panel7->ResumeLayout(false);
			this->panel7->PerformLayout();
			this->panel6->ResumeLayout(false);
			this->panel6->PerformLayout();
			this->panel13->ResumeLayout(false);
			this->panel13->PerformLayout();
			this->tabControl1->ResumeLayout(false);
			this->MenuTree->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ZoomIn))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ZoomOut))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ArrowTop))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ArrowRight))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ArrowLeft))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ArrowDown))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ArrowTR))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ArrowTL))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ArrowDL))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ArrowDR))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->MuevoGrafico))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

bool IsNumber(const char* str)
{
	if (!str || !*str) return false;

	const char* p = &str[0];
	while (p && *p && isspace(*p)) p++;

	const char* start = p;
	bool dot = false;

	while (p && *p) {
		if (isdigit(*p)) ;
		else if (*p == '-') {
			if (p != start) return false;
		}
		else if (*p == '.') {
			if (dot) return false;
			dot = true;
		}
		else if (isspace(*p)) {
			p++;
			while (p && *p) {
				if (!isspace(*p++)) return false;
			}
			return true;
		}
		else
			return false;
		p++;
	}
	return true;
}
///////////////////////////////////////////////////////////////////////////////////
//! \fn		FromNetString
//! \author	Esteban Sinuee Hernßndez Sßnchez
//! \brief	convierte de System::String^ a std::string
//! \date	22/06/2009
///////////////////////////////////////////////////////////////////////////////////
std::string FromNetString(System::String^ str) 
{
	System::IntPtr ansi = System::Runtime::InteropServices::Marshal::StringToCoTaskMemAnsi(str);
    std::string retval((const char*)(void*)ansi);
    System::Runtime::InteropServices::Marshal::FreeCoTaskMem(ansi);
    return retval;
  }

 ///////////////////////////////////////////////////////////////////////////////////
//! \fn		FromCppString
//! \author	Esteban Sinuee Hernßndez Sßnchez
//! \brief	convierte de std::string a System::String^
//! \date	22/06/2009
///////////////////////////////////////////////////////////////////////////////////
System::String^ FromCppString(std::string& str) {
	return System::Runtime::InteropServices::Marshal::PtrToStringAnsi((System::IntPtr)(void*)str.c_str());
  }
private: System::Void specOnlyToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 //this->splitContainer1->Panel1Collapsed = true;
			 //this->splitContainer1 ->Panel2Collapsed =false;
		 }
private: System::Void specGraphicsTreeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 //this->splitContainer1->Panel2Collapsed =false;
			 //this->splitContainer1->Panel1Collapsed =false;
			 //this->splitContainer2 ->Panel1Collapsed =false;
			 //this->splitContainer2 ->Panel2Collapsed =false;
		 }
private: System::Void specGraphicsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 //this->splitContainer2 ->Panel2Collapsed = true;
			 //this->splitContainer2 ->Panel1Collapsed =false;
			 //this->splitContainer1 ->Panel1Collapsed =false;
			 //this->splitContainer1 ->Panel2Collapsed =false;
		 }
private: System::Void specTreeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 //this->splitContainer2 ->Panel2Collapsed =true;
			 //this->splitContainer1 ->Panel1Collapsed =false;
			 //this->splitContainer1 ->Panel2Collapsed =false;
			 //this->splitContainer2 ->Panel1Collapsed =true;
		 }
private: System::Void graphicsTreeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 //this->splitContainer1 ->Panel2Collapsed =true;
			 //this->splitContainer1 ->Panel1Collapsed =false;
			 //this->splitContainer2 ->Panel1Collapsed =false;
			 //this->splitContainer2 ->Panel2Collapsed =false;
		 }
private: System::Void graphicsOnlyToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 //this->splitContainer1 ->Panel2Collapsed =true;
			 //this->splitContainer2 ->Panel2Collapsed =true;
			 //this->splitContainer1 ->Panel1Collapsed =false;
			 //this->splitContainer2 ->Panel1Collapsed =false;
		 }
private: System::Void treeOnlyToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 //this->splitContainer1 ->Panel2Collapsed =true;
			 //this->splitContainer1 ->Panel1Collapsed =false;
			 //this->splitContainer2 ->Panel1Collapsed =true;
			 //this->splitContainer2 ->Panel2Collapsed =false;
		 }
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
			 
			// WinHero->Hijo->defParts();

			 
			 WinHero->Render(true,this->Width/2,this->Height/2," ",false, x ,y);
		//	 WinHero->SwapOpenGLBuffers();
		 }
private: void CambiaTamanos()
		 {
			 //WinHero->~COpenGL();
			 //WinHero = gcnew COpenGL(this->MuevoGrafico ,this->Width/2,(this->Height/2)-40);
			 
			 this->tabControl1->Width = this->Width/2;
			 this->tabControl1->Height= this->Height-26;
			 this->tabControl1->Location = System::Drawing::Point(this->Width/2, 26);
			 this->treeView1->Width = this->Width/2;
			 this->treeView1->Height=this->Height/2-15;
			 this->treeView1->Location = System::Drawing::Point(1, this->Height/2-15);
			 ZoomIn->Location = System::Drawing::Point(0,this->Height/2 - ZoomIn->Height);
			 ZoomOut->Location = System::Drawing::Point(this->Width/2-ZoomOut->Width,this->Height/2 - ZoomOut->Height);
			 ArrowRight->Location = System::Drawing::Point(this->Width/2-ArrowRight->Width,25+ArrowTop->Height);
			 ArrowTop->Location = System::Drawing::Point(this->Width/2-ArrowRight->Width*2,25);
			 ArrowLeft->Location = System::Drawing::Point(this->Width/2-ArrowRight->Width*3,25+ArrowTop->Height);
			 ArrowDown->Location = System::Drawing::Point(this->Width/2-ArrowRight->Width*2,25+2*ArrowTop->Height);
			 ArrowDR->Location = System::Drawing::Point(this->Width/2-ArrowDR->Width,25+2*ArrowTop->Height);
			 ArrowDL->Location = System::Drawing::Point(this->Width/2-ArrowDR->Width*3,25+2*ArrowTop->Height);
			 ArrowTR->Location = System::Drawing::Point(this->Width/2-ArrowRight->Width,25);
			 ArrowTL->Location = System::Drawing::Point(this->Width/2-ArrowRight->Width*3,25);
			 MuevoGrafico->Width = (this->Width/2);//el 10% de la Hero
			 MuevoGrafico->Height = (this->Height/2)-40;//el 10% de la Hero
		 };
//private: 
	public: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
			  /*TreeView1 tree = new TreeView();
	          tree.Parent = this;
		      tree.Dock = DockStyle.Fill;*/

			 int j=0,nivel;
			 //char cambio="->Nodes[";
			 CambiaTamanos();
			 WinHero->Render(true,this->Width/2,this->Height/2," ",false,x,y);
			 treeView1->Nodes->Add(FromCppString(WinHero->Hijo->Arbol[0].parent));//Primer Padre
			 this->CreaTree(treeView1->Nodes[0],WinHero->Hijo->Arbol[0].parent,0);
			 
		 }
private: System::Void Form1_MaximumSizeChanged(System::Object^  sender, System::EventArgs^  e) {
			 CambiaTamanos();
		 }
private: System::Void Form1_MaximizedBoundsChanged(System::Object^  sender, System::EventArgs^  e) {
			 CambiaTamanos();
		 }
private: System::Void Form1_SizeChanged(System::Object^  sender, System::EventArgs^  e) {
			  CambiaTamanos();
		 }
private: System::Void ZoomIn_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->ZoomIn();
		 }
private: System::Void ZoomOut_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->ZoomOut();
		 }
private: System::Void ArrowRight_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->RotarXMinus();
		 }
private: System::Void ArrowLeft_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->RotarXPlus();
		 }
private: System::Void ArrowTop_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->RotarZPlus();
		 }
private: System::Void ArrowDown_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->RotarZMinus();
		 }
private: System::Void ArrowTR_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->RotarXMinus();
			 WinHero->RotarZPlus();
		 }
private: System::Void ArrowTL_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->RotarXPlus();
			 WinHero->RotarZPlus(); 
		 }
private: System::Void ArrowDR_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->RotarXMinus();
			 WinHero->RotarZMinus();
		 }
private: System::Void ArrowDL_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->RotarXPlus();
			 WinHero->RotarZMinus();
		 }
private: System::Void textBox1_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void generarAutolispToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 saveDibujo->Filter = "*.LSP|";
			 if (saveDibujo->ShowDialog() == System::Windows::Forms::DialogResult::OK)
			 {
				 WinHero->root->SetFile(FromNetString(saveDibujo->FileName)); 
				 WinHero->root->Export();
				 //WinHero->Hijo1->Export();
			 }
		 }
private: System::Void topToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->dAngleRotaX=-360;			 
			 WinHero->dAngleRotaZ=270;
			 WinHero->dAngleRotaY = 0;
		 }
private: System::Void backToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->dAngleRotaX=-90;	
			 WinHero->dAngleRotaZ=180;
			 WinHero->dAngleRotaY = 0;
		 }
private: System::Void frontToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->dAngleRotaX=-90;			 
			 WinHero->dAngleRotaZ=0;
			 WinHero->dAngleRotaY = 0;
		 }
private: System::Void leftToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->dAngleRotaX=-90;
			 WinHero->dAngleRotaZ=90;
			 WinHero->dAngleRotaY = 0;
		 }
private: System::Void rightToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->dAngleRotaX=-90;
			 WinHero->dAngleRotaZ=270;
			 WinHero->dAngleRotaY = 0;
		 }
private: System::Void bottomToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->dAngleRotaX=-180;
			 WinHero->dAngleRotaZ=270;
			 WinHero->dAngleRotaY = 0;
		 }
private: System::Void topRightFrontToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->dAngleRotaX=-60;	
			 WinHero->dAngleRotaZ=120;
			 WinHero->dAngleRotaY = 0;
		 }
private: System::Void topFrontToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->dAngleRotaX=-60;	
			 WinHero->dAngleRotaZ=90;
			 WinHero->dAngleRotaY = 0;
		 }
private: System::Void topLeftFrontToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->dAngleRotaX=-60;
			 WinHero->dAngleRotaZ=45;
			 WinHero->dAngleRotaY = 0;
		 }
private: System::Void bottomFrontToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->dAngleRotaX=-120;	
			 WinHero->dAngleRotaZ=-270;
			 WinHero->dAngleRotaY = 0;
		 }
private: System::Void bottomFrontrigthToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->dAngleRotaX=-120;
			 WinHero->dAngleRotaZ=-225;
			 WinHero->dAngleRotaY = 0;
		 }
private: System::Void bottomFrontLeftToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->dAngleRotaX=-120;	
			 WinHero->dAngleRotaZ=-180;
			 WinHero->dAngleRotaY = 0;
		 }
private: void ConsultaMyBade()
 {
	 tblPrototipo Renglon;
	 if (ConsultarBD((char *) FromNetString(this->textBox2->Text).c_str(),"MYBADE","0", &Renglon))
	 {
		 this->textBox3->Text = FromCppString(Renglon.DISENADOR);
		 this->textBox1->Text = FromCppString(Renglon.CLIENTE);
		 this->textBox2->Text = FromCppString(Renglon.NODISENO);
		 this->textBox6->Text = FromCppString(Renglon.CTEEVALDEV);
		 //this->textBox7->Text = FromCppString(Renglon.CTEEVALNUC);
		 this->textBox8->Text = FromCppString(Renglon.PARIDAD);
		 this->comboBox8->Text = FromCppString(Renglon.NORMA);
		 this->radioButton15->Checked = Renglon.FASES=="1";
		 this->comboBox9->Text = FromCppString(Renglon.KVAS);
		 this->radioButton19->Checked = Renglon.FRECUENCIA=="50";
		 this->comboBox10->Text = FromCppString(Renglon.ALTITUD);
		 this->radioButton19->Checked = Renglon.SOBREELEV=="1";
		 this->radioButton20->Checked = Renglon.SOBREELEV=="2";
		 this->radioButton22->Checked  = Renglon.SOBREELEV=="3";
		 this->comboBox11->Text = FromCppString(Renglon.TIPOENFTO);
		 this->radioButton31->Checked = Renglon.PANTALLAELECTROSTATICA=="1";
		 this->radioButton33->Checked = Renglon.ZONASISMICA=="1";
		 this->comboBox2->Text = FromCppString(Renglon.TIPOTRANSFORMADOR);
		 this->comboBox3->Text = FromCppString(Renglon.TIPOGARGANTA);
		 this->comboBox4->Text = FromCppString(Renglon.INTERFASEBT);
		 this->comboBox5->Text = FromCppString(Renglon.INTERFASEAT);
		 this->radioButton5->Checked = Renglon.BOQUILLATAPA=="1";
		 this->textBox5->Text = FromCppString(Renglon.NUMRADIDADORES);
		 this->radioButton39->Checked = Renglon.TCS=="1";
		 this->radioButton41->Checked = Renglon.TANQUECONSERVADOR=="1";
		 this->comboBox1->Text = FromCppString(Renglon.TIPOBASE);
		 this->radioButton44->Checked = Renglon.APOYOPARAGATO=="1";
		 System::Windows::Forms::MessageBox::Show("SE cargo exitosamente");
	 }
	 else
		 System::Windows::Forms::MessageBox::Show("No existe este dise˝o en la Base de datos");
 }
private: System::Void cargarDise˝oDeBaseDeDatosToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 ConsultaMyBade();
		 }
private: System::Void dise˝oBaseToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 saveDibujo->Filter = "*.TXT|";
			 if (saveDibujo->ShowDialog() == System::Windows::Forms::DialogResult::OK)
			 {
				 Transformador->generaReporteElectrico(FromNetString(saveDibujo->FileName));
			 }
		 }
private: System::Void guardarDise˝oDeBaseDeDatosToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 tblPrototipo Renglon;
			 Renglon.NODISENO = FromNetString(this->textBox2->Text);
			 Renglon.CLIENTE = FromNetString(this->textBox1->Text);
			 Renglon.DISENADOR = FromNetString(this->textBox3->Text);
			 Renglon.CTEEVALDEV = FromNetString(this->textBox6->Text);
			 Renglon.CTEEVALNUC = FromNetString(this->textBox7->Text);
			 Renglon.PARIDAD = FromNetString(this->textBox8->Text);
			 Renglon.NORMA = FromNetString(this->comboBox8->Text);
			 Renglon.FASES = this->radioButton15->Checked?"1":"0";
			 Renglon.KVAS = FromNetString(this->comboBox9->Text);
			 Renglon.FRECUENCIA = this->radioButton19->Checked?"50":"60";
			 Renglon.ALTITUD = FromNetString(this->comboBox10->Text);
			 Renglon.SOBREELEV = this->radioButton19->Checked?"1":this->radioButton20->Checked?"2":this->radioButton22->Checked?"3":"4";
			 Renglon.TIPOENFTO = FromNetString(this->comboBox11->Text);
			 Renglon.PANTALLAELECTROSTATICA = this->radioButton31->Checked?"1":"0";
			 Renglon.ZONASISMICA = this->radioButton33->Checked?"1":"0";
			 Renglon.TIPOTRANSFORMADOR = FromNetString(this->comboBox2->Text);
			 Renglon.TIPOGARGANTA = FromNetString(this->comboBox3->Text);
			 Renglon.INTERFASEBT = FromNetString(this->comboBox4->Text);
			 Renglon.INTERFASEAT = FromNetString(this->comboBox5->Text);
			 Renglon.BOQUILLATAPA = this->radioButton5->Checked?"1":"0";
			 Renglon.NUMRADIDADORES = FromNetString(this->textBox5->Text);
			 Renglon.TCS = this->radioButton39->Checked?"1":"0";
			 Renglon.TANQUECONSERVADOR = this->radioButton41->Checked?"1":"0";
			 Renglon.TIPOBASE = FromNetString(this->comboBox1->Text);
			 Renglon.APOYOPARAGATO = this->radioButton44->Checked?"1":"0";
			 GrabarBD((char *)Renglon.NODISENO.c_str(),"MYBADE","0",&Renglon);
			 System::Windows::Forms::MessageBox::Show("SE Grabo exitosamente");
		 }
private: System::Void textBox6_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		/*	 WinHero->~COpenGL();
			 WinHero = gcnew COpenGL(this,this->Width/2,this->Height/2);
			 WinHero->Render(true,this->Width/2,this->Height/2," ",false);*/
			 if (IsNumber((char *)FromNetString(textBox6->Text).c_str())){
				 WinHero->Hijo->alturaVentana = System::Convert::ToInt16(textBox6->Text);
				 WinHero->Hijo->actualizarDimensiones();
				 //WinHero->Hijo->nucleoMitter->actualizarDimensiones();
				 //delete WinHero->root;
				 WinHero->Hijo->ReDefParts();
				 WinHero->Hijo->defParts();
			 }
			 WinHero->Hijo->cambio=1;
		 }
private: System::Void Form1_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {

		 }

private: System::Void estructuraToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
saveDibujo->Filter = "*.TXT|";
			 if (saveDibujo->ShowDialog() == System::Windows::Forms::DialogResult::OK)
			 {
				 WinHero->Hijo->calcpeso()/*calVolumen()*/;
				 WinHero->Hijo->generaReportePesos((char *)FromNetString(saveDibujo->FileName).c_str());				 
			 }
		 }
 private: System::Windows::Forms::TreeNode ^CreaTree(System::Windows::Forms::TreeNode ^a,string padreant,int comienzo){
			 string padreact;
			 int Hijos=-1;
			 System::Windows::Forms::TreeNode ^b = gcnew System::Windows::Forms::TreeNode;

			 for (int i =comienzo; i<WinHero->Hijo->Arbol.size();i++){
				 padreact = WinHero->Hijo->Arbol[i].parent;
				  
				 if (padreact == padreant){
					 b->Nodes->Add(FromCppString(WinHero->Hijo->Arbol[i].name));
					 Hijos++;					 
					 if (WinHero->Hijo->Arbol[i].IsParent){

						 a->Nodes->Add(CreaTree(b->Nodes[Hijos],WinHero->Hijo->Arbol[i].name,i));
					 }
					 else
						 a->Nodes->Add(FromCppString(WinHero->Hijo->Arbol[i].name));
				 }
				}
			 Hijos=-1;
			 return a;
		  }
private: System::Void Form1_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			/* if (e->Button  <0)
			 {
			 }*/
		 }
		 
private: System::Void MuevoGrafico_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			 lblDebugeo->Text = "x: " + e->X.ToString() + "\nX Ant: " + x + "\nY: " + e->Y.ToString() + "\nY Ant: "+ y +
				 "\nWinX: " + WinHero->x + "\nWinY: "+ WinHero->y ;
			 if(e->Button == System::Windows::Forms::MouseButtons::Middle)
			 {
				 if (y>=e->Y)
					 WinHero->ZoomIn();//zoom in
				 else
					 WinHero->ZoomOut();//zoom out
				 this->y = e->Y;

			 }
			 if(e->Button == System::Windows::Forms::MouseButtons::Left){
				 if (y>e->Y)//(x>e->X)
					 WinHero->dAngleRotaX -= 5;
				 else{
					 if (y ==e->Y)//(x==e->X)
						 WinHero->dAngleRotaX += 0;
					 else
						 WinHero->dAngleRotaX += 5;
				 }
				 if (x>e->X)//(y>e->Y)
					 WinHero->dAngleRotaY -= 5;
				 else{
					 if (x==e->X)//(y ==e->Y)
						 WinHero->dAngleRotaY += 0;
					 else
						 WinHero->dAngleRotaY += 5;
				 }
				 
				 this->x = e->X;
				 this->y = e->Y;
			 }
		 }		 
private: System::Void showToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {			 
			 //System::Windows::Forms::MessageBox::Show(this->treeView1->SelectedNode->Index.ToString());
			 AccionMenu(SHOW,FromNetString(this->treeView1->SelectedNode->Text));
		 }
private: System::Void AccionMenu (AccionMenuTree Accion, std::string sElemento){
			 const std::string &sAccion = EnumString<AccionMenuTree>::From(Accion);

			 std::string str = "menu selected: " + sElemento + "\nAccion: " + sAccion;
			// System::Windows::Forms::MessageBox::Show(FromCppString(str));
			 WinHero->Hijo->ejecutaAccionMenu(Accion,sElemento);
		 }
private: System::Void hidenToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 AccionMenu(HIDE,FromNetString(this->treeView1->SelectedNode->Text));
		 }
private: System::Void solidToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 AccionMenu(SOLIDO,FromNetString(this->treeView1->SelectedNode->Text));
		 }
private: System::Void wireframeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 AccionMenu(WIRE,FromNetString(this->treeView1->SelectedNode->Text));
		 }
private: System::Void inspectorToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->Hijo->calcpeso();
			 WinHero->Hijo->guardar("c:\\capauto\\Serializado.xml");
			 frmInspect = gcnew Inspector();
			 frmInspect->Parte = this->treeView1->SelectedNode->Text;
			 frmInspect->ShowDialog(this);
		 }
private: System::Void treeView1_Click(System::Object^  sender, System::EventArgs^  e) {
			//if( e->botton == Right)
			 /*System::Windows::Forms::MessageBox::Show("Mensaje","Titulo",System::Windows::Forms::MessageBoxButtons::YesNo);
			 System::Windows::Forms::DialogResult*/

		 }
};
}