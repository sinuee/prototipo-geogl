// Prototipo_Hero.cpp : main project file.

#include "stdafx.h"
#include "Form1.h"
#include "CNucleo.h"
#include "CTransformador.h"
#include "CParteViva.h"
using namespace System;
using namespace System::IO;


using namespace Prototipo_Hero;

[STAThreadAttribute]
int main(array<System::String ^> ^args)
{
 
     //CNucleo a = CNucleo(false,false);    
	CTransformador *Transformador = new CTransformador();


String^ fileName = "gauss.txt";

   StreamWriter^ sw = gcnew StreamWriter(fileName);
//   sw->WriteLine("gauss   " + Transformador->ParteViva->Nucleo->getGauss(14.113006591796875));
   sw->WriteLine("anucleo   " + Transformador->ParteViva->Nucleo->getANucleo());
   sw->WriteLine("bnucleo   " + Transformador->ParteViva->Nucleo->getBNucleo());
   sw->WriteLine("a1   " + Transformador->ParteViva->Nucleo->getA1());	   
   sw->WriteLine("a2   " + Transformador->ParteViva->Nucleo->getA2());
   sw->WriteLine("b1   " + Transformador->ParteViva->Nucleo->getB1());
	sw->WriteLine("areanucleo   " + Transformador->ParteViva->Nucleo->getAreaNucleo());
//	sw->WriteLine("gausstotal   " + Transformador->ParteViva->Nucleo->getGaussTotal(14.113006591796875));
	sw->WriteLine("areatotalnucleo   " + Transformador->ParteViva->Nucleo->getAreaTotalNucleo());
	sw->WriteLine("espesorescalon   " + Transformador->ParteViva->Nucleo->getEspesorEscalon());
	sw->WriteLine("relacionnucleo  " + Transformador->ParteViva->Nucleo->getRelacionNucleo());
   sw->WriteLine(DateTime::Now);
   sw->Close();






	// Enabling Windows XP visual effects before any controls are created
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false); 

	// Create the main window and run it
	Application::Run(gcnew Form1());
	return 0;
}
