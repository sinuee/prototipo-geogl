#pragma once
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Xml;

namespace Prototipo_Hero {

	/// <summary>
	/// Summary for Inspector
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Inspector : public System::Windows::Forms::Form
	{
	public:
		Inspector(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Inspector()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::DataGridView^  InspectorView;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Atributo;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Valor;
			 /// Declaracion de variables
	public: System::String^ Parte;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->InspectorView = (gcnew System::Windows::Forms::DataGridView());
			this->Atributo = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Valor = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->InspectorView))->BeginInit();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(13, 17);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(35, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"label1";
			// 
			// InspectorView
			// 
			this->InspectorView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->InspectorView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(2) {this->Atributo, 
				this->Valor});
			this->InspectorView->Dock = System::Windows::Forms::DockStyle::Fill;
			this->InspectorView->Location = System::Drawing::Point(0, 0);
			this->InspectorView->Name = L"InspectorView";
			this->InspectorView->RowHeadersVisible = false;
			this->InspectorView->Size = System::Drawing::Size(292, 267);
			this->InspectorView->TabIndex = 1;
			this->InspectorView->CellDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Inspector::InspectorView_CellDoubleClick);
			// 
			// Atributo
			// 
			this->Atributo->HeaderText = L"Atributo";
			this->Atributo->Name = L"Atributo";
			this->Atributo->ReadOnly = true;
			// 
			// Valor
			// 
			this->Valor->HeaderText = L"Valor";
			this->Valor->Name = L"Valor";
			this->Valor->ReadOnly = true;
			// 
			// Inspector
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(292, 267);
			this->Controls->Add(this->InspectorView);
			this->Controls->Add(this->label1);
			this->Name = L"Inspector";
			this->Text = L"Inspector";
			this->Load += gcnew System::EventHandler(this, &Inspector::Inspector_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->InspectorView))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Inspector_Load(System::Object^  sender, System::EventArgs^  e) {
				 InspectorView->Rows->Clear();
				 LlenarInspector(-1);
				 this->Text = Parte + " Inspector";
				 //this->label1->Text = " Alto \t\t25\nAncho \t\t5\nLargo \t\t2.5\n";
			 }
	private: void LlenarInspector(int i){
				 //int i = -1;
				 bool bIsThis=false;
				 bool bIsNode = false;
				 int iDepth;
				 XmlTextReader^ reader = gcnew XmlTextReader ("c:\\capauto\\Serializado.xml");
				 label1->Text ="";
				 
				 while (reader->Read()){
					 bIsThis;
					 if (reader->Name == Parte)
						 iDepth = reader->Depth+2;
					 if ((reader->Name == Parte) || (bIsThis) && (iDepth >= reader->Depth ))
					 {
						 bIsThis = true;
						 switch (reader->NodeType){
							 case XmlNodeType::Whitespace:
								 //bIsNode=true;
								 break;
							 case XmlNodeType::EndElement:
								 if (reader->Name==Parte)
									 bIsThis = false;
								 break;
							case XmlNodeType::Element: // The node is an element.
								//Console::Write("<{0}", reader->Name);
								//label1->Text +="\n"+ reader->Name	+ " ";
								if (iDepth-1 >= reader->Depth ){
									InspectorView->Rows->Add();
									i++;
									InspectorView[0,i]->Value = reader->Name;
									InspectorView[1,i]->Value = "";
									while (reader->MoveToNextAttribute()){ // Read the attributes.
											//label1->Text +="\n"+ reader->Name	+ " = " + reader->Value;//Console::Write(" {0}='{1}'", reader->Name, reader->Value);
											InspectorView->Rows->Add();
											i++;
											InspectorView[0,i]->Value = reader->Name;
											InspectorView[1,i]->Value = reader->Value;
									}
								}
								//Console::WriteLine(">");
								break;
							case XmlNodeType::Text: //Display the text in each element.
								//Console::WriteLine (reader->Value);
								InspectorView[1,i]->Value = reader->Value;
								//label1->Text +=" = " +reader->Value;
								break;
						 }//switch
					 }//if
				 }//while
				 reader->Close();
		 }
	private: System::Void InspectorView_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
				 System::String ^Aux;
				 if (InspectorView[1,e->RowIndex]->Value == ""){
					 Parte = InspectorView[0,e->RowIndex]->Value->ToString();
					 Aux=BuscaPadre(Parte);
					 InspectorView->Rows->Clear();
					 InspectorView->Rows->Add();
					 InspectorView[0,0]->Value = Aux;
					 InspectorView[1,0]->Value = "";
					 LlenarInspector(0);
					 //if (InspectorView[0,0]->Value == InspectorView[0,1]->Value)
						// InspectorView->Rows[0]->Visible =false;
				 }
			 }
	private: System::String ^BuscaPadre(System::String ^Parte){
				 System::String ^Padre;
				 System::String ^Aux;
				 System::String ^Ant="";
				 XmlTextReader^ reader = gcnew XmlTextReader ("c:\\capauto\\Serializado.xml");
				 while (reader->Read()){
					 if (reader->Name == Parte)
						 return Padre;
					 switch (reader->NodeType){
						 case XmlNodeType::Element:
							 Aux = reader->Name;
							 break;
						 case XmlNodeType::Whitespace:
							 if (Ant==""){
								 Padre = Aux;
								 Ant="";
							 }
							 break;
						 case XmlNodeType::Text:
							 Ant=reader->Value;
							 break;
						 /*case XmlNodeType::EndElement:
							 Ant="";
							 break;*/
					 }
				 }
				 reader->Close();
				 return Padre;
			 }
};
}
