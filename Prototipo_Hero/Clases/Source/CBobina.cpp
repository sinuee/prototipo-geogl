///////////////////////////////////////////////////////////////////////////////////
//! \file	    CBobina.cpp
//! \author     
//! \brief      Defincion de la clase CBobina
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////


//Revisiones
//  Fecha      	      Descripción					   Persona  	  Folio
//dd/mmm/aa  Descripción de revisión o modificación 	 XXX  		 XXXYYY

//#include "StdAfx.h"
#include "CBobina.h"
#include "CParteViva.h"
#include "CTransformador.h"
#include "CNucleo.h"
#include "String.h"
#include "Math.h"

CBobina::CBobina(CParteViva *_ParteViva)
{
	ParteViva = _ParteViva;
	iVueltasBt = 9;
	cConexionBt = "ESTRELLA";
	iVoltajeLineaBt = 220;
	dFases = 3.0;
	iToleranciaDropeo =3;
	dEspesorDuctoHor = 6.4;
	cTipoBobina = "NORMAL";
	cTipoConductorBt="LAMINA"; 
	bSpst = ParteViva->Transformador->obtenerSpst();
	b69Kv = ParteViva->Transformador->obtener69Kv();
	iAnchoAislado = 340;
	iAnchoDesnudo = 340;
	iCabezalAtIni = 16;
	dAnchoAisladoAt = 2.98;
	iConductoresEnParAt = 2;
	iCondXCapa = 56;
	iD69 = 8;
	iEntreSeccionesBt =0;
	iConductoresEnPar =1;
	iVueltasXCapaBt=1;
	iCabezalBtIni = 0;
	iCabezal =10;
	iCabezalPaquetes = 12;
	iCabezalBtSpst =12;
	iVueltasBt = 9;

}

int CBobina::getAlturaVentana()
{
	if(bSpst)
		return (getAlturaBobina()+50.8);//redondeasup checar funcion
	else
	{
		if(b69Kv)
			return (getAlturaBobina() + dEspesorDuctoHor);
		else
			return getCabezalAlturaVentana(1);
	}
}


int CBobina::getAlturaBobina()
{
	if(bSpst)
		return getAlturaBobinaSpst();
	else
	{
		if(b69Kv)
			return getAlturaBobinaPrev69();
		else
			return getCabezalAlturaVentana(2);
	}
}


double CBobina::getAlturaBobinaSpst()
{
	double iVal1=0,iVal2=0;
	iVal1=getAlturaTotalAtSpst()+(2 * iCabezalAtIni);

	if(strcmp(cTipoConductorBt,"SOLERA")==0)
		iVal2 = getLongitudTotalBob() + (2 * iCabezalAtIni);
	else
		iVal2 = getLongitudTotalBob() + (2 * getCabezalAlturaVentana(0));

	if(iVal1<=iVal2)
		return getAlturaTotalAtSpst() + (2 * iCabezalAtIni);//redondeasup 2
	else
	{
		if(strcmp(cTipoConductorBt,"SOLERA")==0)
			return getLongitudTotalBob() + (2 * iCabezalAtIni);//redondeasup 2
		else 
			return getLongitudTotalBob() + (2 * getCabezalAlturaVentana(0));
	}
}

double CBobina::getLongitudTotalBob()
{
	char *cArregloBt=ParteViva->Transformador->cArregloBt;

	if(strcmp(cTipoConductorBt,"LAMINA")==0)
	{
		if((strcmp(cTipoBobina,"NORMAL")==0)&&(strcmp(cArregloBt,"RADIAL")==0))
			return iAnchoDesnudo;
		else
			return (2 * iAnchoDesnudo) + (2 * getCabezalBt1()) + (iEntreSeccionesBt);//redondea sub 1
	}
	else
		return (iVueltasXCapaBt + 1) * iConductoresEnPar * (iAnchoAislado + 0.1) ;
		//redondea sub if spst  1  2
}


double CBobina::getAlturaTotalAtSpst()
{
	return getVueltaFalsa() + getAlturaEfectASpst();
}

double CBobina::getVueltaFalsa()
{
	return iConductoresEnParAt * dAnchoAisladoAt;
}

double CBobina::getAlturaBobinaPrev69()
{
	if(iD69!=0)
		return getLongitudTotalBob() + (2 * getCabezalBt());
	else
		return getCabezalAlturaVentana(2);//checar cabezal-altura-ventana
}



double CBobina::getAlturaEfectASpst()
{
	return (iCondXCapa * (dAnchoAisladoAt + 0.1) * iConductoresEnParAt);// redondea sup 1
}

double CBobina::getAnchoAislado()
{
	char *cConductorBt = ParteViva->Transformador->cConductorBt;

	if(strcmp(cConductorBt,"OTRO")==0)
	{
		if(strcmp(cTipoConductorBt,"LAMINA")==0)
			return (ParteViva->Transformador->obtenerOtroAnchoBt());
		else if(strcmp(cTipoConductorBt,"MAGNETO")==0) 
			return (ParteViva->Transformador->obtenerOtroDiametroAislBt());
		else	
			return ((ParteViva->Transformador->obtenerOtroAnchoBt()) + 0.06);
	}else
		return iAnchoAislado;
}


double CBobina::getAnchoDesnudo()
{
	char *cConductorBt = ParteViva->Transformador->cConductorBt;

	if(strcmp(cConductorBt,"OTRO")==0)
	{
		if(strcmp(cTipoConductorBt,"LAMINA")==0)
			return (ParteViva->Transformador->obtenerOtroAnchoBt());
		else if(strcmp(cTipoConductorBt,"MAGNETO")==0) 
			return (ParteViva->Transformador->obtenerOtroDiametroDesBt());
		else	
			return ParteViva->Transformador->obtenerOtroAnchoBt();
	}else
		return iAnchoDesnudo;
}


double CBobina::getLadoCortoMolde()
{	
	char *cTipoNucleo=ParteViva->Nucleo->cTipoNucleo;

	if(bSpst)
		return (ParteViva->Nucleo->getAnchoLamina1() + (2*iToleranciaDropeo));
	else
	{
		if (strcmp(cTipoNucleo,"ENROLLADO")==0)
			return ceil(2 * ParteViva->Nucleo->getANucleo()) + 6.35 + 1.5;
		else
			return (ParteViva->Nucleo->getANucleo() + 6.35 + 1.5);
	}
}

double CBobina::getCabezalBt1()
{
	if(bSpst && (strcmp(cTipoConductorBt,"SOLERA")==0))
		return iCabezalBtIni;
	else
	{
		if(strcmp(cTipoBobina,"NORMAL")==0)
		{
			if(strcmp(cTipoConductorBt,"SOLERA")==0)
				return iCabezal;
			else
				return iCabezal;
		}
		else
			return iCabezalPaquetes;
	}

}

double CBobina::getCabezalBt()
{
	if(bSpst)
		return iCabezalBtSpst;
	else
	{
		if(b69Kv)
		{
			if(iD69!=0)
			{
				if(dAnchoAisladoAt<=7.6)
					return ((dAnchoAisladoAt + 0.052) * (iD69/2) * iConductoresEnParAt) + getCabezalAlturaVentana(0); //cabezal altura venta 0
				else if (dAnchoAisladoAt<=12.8&&dAnchoAisladoAt>7.6)
					return ((dAnchoAisladoAt + 0.103) * (iD69/2) * iConductoresEnParAt) + getCabezalAlturaVentana(0);//cabezal altura venta 0
				else if (dAnchoAisladoAt>12.8)
					return ((dAnchoAisladoAt + 0.206) * (iD69/2) * iConductoresEnParAt) + getCabezalAlturaVentana(0);//cabezal altura venta 0
			}
			else
				return getCabezalAlturaVentana(0);////cabezal altura venta 0
		}
		else
			return getCabezalAlturaVentana(0);//cabezal altura venta 0
	}
}
double CBobina::getCabezalAlturaVentana(int i)
{
	if(i==0)
		return getCabezalBt1();
	else if (i==1)
		return getLongitudTotalBob();
	else if(i==2)
		return dEspesorDuctoHor;
	else 
		return 0;
}

double CBobina::getVoltXVuelta()
{
	return getVoltajeFaseBt()/iVueltasBt;
}

double CBobina::getVoltajeFaseBt()
{
	if(strcmp(cConexionBt,"ESTRELLA")==0)
		return iVoltajeLineaBt / sqrt(dFases);
	else
		return iVoltajeLineaBt;
}