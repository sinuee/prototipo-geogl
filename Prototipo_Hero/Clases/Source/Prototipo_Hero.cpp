// Prototipo_Hero.cpp : main project file.

#include "stdafx.h"
#include "Form1.h"
#include "CNucleo.h"
#include "CTransformador.h"
#include "CParteViva.h"

using namespace System;
using namespace System::IO;


using namespace Prototipo_Hero;

[STAThreadAttribute]
int main(array<System::String ^> ^args)
{

	// Enabling Windows XP visual effects before any controls are created
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false); 

	// Create the main window and run it
	Application::Run(gcnew Form1());
	return 0;
}
