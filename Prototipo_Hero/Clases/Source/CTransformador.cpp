///////////////////////////////////////////////////////////////////////////////////
//! \file	    CTransformador.cpp
//! \author     
//! \brief      Defincion de la clase CTransformador
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////


//Revisiones
//  Fecha      	      Descripci�n					   Persona  	  Folio
//dd/mmm/aa  Descripci�n de revisi�n o modificaci�n 	 XXX  		 XXXYYY

//#include "StdAfx.h"
#include "CTransformador.h"
#include "CTanque.h"
#include "CParteViva.h"
#include <string>

CTransformador::CTransformador(void)
{

	cConductorBt="LAMINA 1.65 x 340";
	iOtroAnchoBt=1245;
	iOtroDiametroAislBt=700;
	dOtroDiametroDesBt=0.5766;
	cArregloBt="RADIAL";
	bIsight =false;
	bSpst=false;
	cTipoAccesorios ="ESTANDAR";
	iKvas=225;
	strcpy(cCliente,"Prod. Elec de Tijuana");
	strcpy(cNumOrden,"is13t");
	iCantidad=1;
	strcpy(cDise�adoPor,"jfa");
	iCteEvaluacionNucleo=0;
	iCteEvaluacionDevanado=0;
	strcpy(cNorma,"N-MX-J-284");
	dParidad=13.5;
	iFases=3;
	iFrecuencia=60;
	iAltitud=2300;
	SobreElevacionTemperatura=65;
	strcpy(cTipoEnfriamiento,"OA");
	strcpy(cPantallaElectrostatica,"NO");
	strcpy(cZonaSismica,"NO");
	
	strcpy(cTipoTransformador,"L.CORTOS");
	strcpy(cTipoGargantas,"ESTANDAR");
	strcpy(cInterfazBt,"GARGANTA");
	strcpy(cInterfazAt,"GARGANTA");
	strcpy(cFrente,"IZQUIERDO");
	strcpy(cTiposRadiador,"NO");
	strcpy(cBoquillaEnTapa,"SOLDADOS");
	
	strcpy(cTCS,"NO");
	strcpy(cTanqueConservador,"NO");
	strcpy(cTipoBase,"NORMAL");
	strcpy(cApoyoGato,"NO");
		
//	Tanque = new CTanque(this);
	ParteViva = new CParteViva(this);
}



bool CTransformador::obtenerIsight()
{
	return bIsight;
}
bool CTransformador::obtenerSpst()
{
	return bSpst;
}

bool CTransformador::obtener69Kv()
{
	return b69Kv;
}

int CTransformador::obtenerOtroAnchoBt()
{
	return iOtroAnchoBt;
}

int CTransformador::obtenerOtroDiametroAislBt()
{
	return iOtroDiametroAislBt;
}

double CTransformador::obtenerOtroDiametroDesBt()
{
	return dOtroDiametroDesBt;
}

int CTransformador::obtenerKvas()
{
	return iKvas;
}

void CTransformador::generaReporteElectrico(std::string NombreArch)
{

	System::String^ fileName = FromCppString(NombreArch);

	System::IO::StreamWriter^ sw = gcnew System::IO::StreamWriter(fileName);
	sw->WriteLine("                                                         P R O L E C   G E                                                    REV. 0");
	sw->WriteLine("                                              INGENIERIA-TRANSFORMADORES INDUSTRIALES                              GENERADO POR AUDI");
	sw->WriteLine("                                                                                                                        D09.12.01.00");
	sw->WriteLine("");
	std::string Aux = "        " + std::string(cCliente) +"                                    NIL";
	//strcat(Aux,"        ");
	/*strcat(Aux,cCliente);
	strcat(Aux,"                                    NIL");*/
	sw->WriteLine(FromCppString(Aux));
	//sw->WriteLine("        " +     cCliente  +  "                                    NIL");
	sw->WriteLine("                                                                  NIL");
	sw->WriteLine("     NUM. ORDEN: *****                                                                                          TIPO TRANS: ********");
	sw->WriteLine("     FECHA: */**/****                                                                                                FRENTE: *******");
	sw->WriteLine("     DISENADO PARA COMPRESSION BONDING");
	sw->WriteLine("     TIPO:**            KVA: **      FASES: " + iFases    + "  FREC: **      TIPO COND AT: ******* **      VOLTS A.T: *****          *****");
	sw->WriteLine("     **    ***          MSNM: ****    TIPO BOB BT: *****         TIPO COND BT: ****** **       VOLTS B.T: ***           ********");
	sw->WriteLine("     -------------------------------------------------------------------------------------------------------------------------------");
	sw->WriteLine("     VOLTS AT1 | VUELTAS AT1 | VOLTS AT2 | VUELTAS AT2 |                           A= ****          | PARTE  PESO  COSTO      COSTO");
	sw->WriteLine("               |             |           |             |                          B1= ****  B2=  ***|        NETO  /KG             ");
	sw->WriteLine("      *******  |   ***       |           |             | NUCLEO:   *********       C= ****          | NUCLEO ***** *****      *****");
	sw->WriteLine("               |             |           |             |                          B1= ****  B2=  ***|        NETO  /KG             ");
	sw->WriteLine("      *******  |   ***       |           |             | NUCLEO:   *********       C= ****          | NUCLEO ***** *****      *****");
	sw->WriteLine("      *******  |   *** **    |           |             | LAMINA:   **             D1=  ***  D2= ****| MAT BT ****  *****      *****");
	sw->WriteLine("      *******  |   *** **    |           |             | DENSIDAD: ********       F1= ****  F2= ****| MAT AT ***** *****      *****");
	sw->WriteLine("      *******  |   *** **    |           |             | AREA:     ********        H= ****  DL= ****| AISLAM *****            *****");
	sw->WriteLine("      *******  |   *** **    |           |             | REL NUC:  ****        DC KG1= **  DC KG2= *|        ***** EVALUACION *****");
	sw->WriteLine("               |             |           |             |                      DG KG1= ***  DG KG2= *| TOTAL        TOTAL      *****");
	sw->WriteLine("     -------------------------------------------------------------------------------------------------------------------------------");
	sw->WriteLine("                  |      B.T.       |        A.T.          |   A.T.2    |       |VENTANA|LADO BT|LADO AT|PERDIDAS (WATTS) |GARANTIAS");
	sw->WriteLine("     KV NBAI      |       **        |         **           |            | CASQ. |  ***  |  ***  |  ***  | NUC 100%  ***** |      ***");
	sw->WriteLine("     VOLTS/VUELTA |      *****      |        *****         |            | B.T.  | ****  | ****  | ****  | NUC 110%  ***** |         ");
	sw->WriteLine("     VOLTAJE      |     ******      |   **************     |            | DUCTOS|  ***  | ***** | ***** | BT NOM    ***** |         ");
	sw->WriteLine("     CORRIENTE    |      *****      |     ***********      |            | PUNTAS| ****  | ****  |  ***  | EDDY BT     *** |         ");
	sw->WriteLine("     DIM. COND    |   **********    |   ***************    |            | E.B.A |  ***  |  ***  |  ***  | PUNTAS     **** |         ");
	sw->WriteLine("     COND. AISL.  |   **********    |       *******        |            | A.T. 1| ****  | ****  | ****  | AT 1 NOM  ***** |         ");
	sw->WriteLine("     AREA COND.   |      *****      |        ****          |            | DUCTOS|  ***  | ***** | ***** | AT 1 MAX  ***** |         ");
	sw->WriteLine("     DENSIDAD     |      *****      |     ***********      |            | PUNTAS|  ***  |  ***  | ****  | EDDY AT1   **** |         ");
	sw->WriteLine("     COND. EN PAR |      *****      |        *****         |            | A.T. 2|       |       |       | MAX AT1    **** |         ");
	sw->WriteLine("     VUELTAS/COL. |        *        |         ***          |            | DUCTOS|       |       |       | AT 2 NOM    *** |         ");
	sw->WriteLine("     BOBINAS      |        *        |          *           |            | CIERRE|  ***  |  ***  |  ***  | AT 2 MAX    *** |         ");
	sw->WriteLine("     VUELTAS/BOB  |        *        |         ***          |            | FLARE |  ***  |  ***  |  ***  | EDDY AT2        |         ");
	sw->WriteLine("     CAPAS        |       ***       |      ********        |            | SUMA  | ****  | ****  | ***** | MAX AT2         |         ");
	sw->WriteLine("     VUELTAS/CAPA |       ***       |     **********       |            | MOLDE | ***** |       | ***** | EXTRANAS  ***** |         ");
	sw->WriteLine("     ENTRE SECC.  |        *        |          *           |            | --------------|---------------| FAC CORR  ****  |         ");
	sw->WriteLine("     ENTRE CAPAS  |    *********    |      *********       |            | ANCHO  ***    |  LARGO ***    | W.BOB    ****** |        *");
	sw->WriteLine("     RES. A 20 oC |    ********     |      ********        |            | --------------|---------------| W.TOTAL  ****** |     ****");
	sw->WriteLine("     H. RADIAL    |      *****      |        *****         |            | PERIMETROS: mm|GRADIENTES: oC |-----------------|         ");
	sw->WriteLine("     H. AXIAL     |     ******      |       ******         |            |               |BT =  ***(    )|     BT-AT1      |---------");
	sw->WriteLine("     L. TOT. BOB. |     ******      |       ******         |            | MOLDE   ******|AT1= ****(    )|                 |         ");
	sw->WriteLine("     CABEZAL      | **************  |     ***********      |            | CASQ.   ******|AT2=     (    )|  % Z   =  ****  |         ");
	sw->WriteLine("     ALT. BOBINA  |      *****      |        *****         |            | B.T.    ******|---------------|  % Z:      **   |  MIN ***");
	sw->WriteLine("     DUCTOS       |  *************  |    *************     |            | E.B.A. *******|CORTO CIRCUITO:|     BT-AT2      |  MAX ***");
	sw->WriteLine("     ALT. VENTANA |      *****      |        ******        |            | AT 1   *******|FZA. RAD ******|                 |         ");
	sw->WriteLine("                  |                 |                      |            | AT 2   *******|FZA. TAN ******|  % Z   =        |         ");
	sw->WriteLine("                  |                 |                      |            | FINAL  *******|TOTAL =  ******|  % Iex =  ****  |         ");
	sw->WriteLine("     -------------------------------------------------------------------------------------------------------------------------------");
	sw->WriteLine("      E F I C I E N C I A     |  R E G U L A C I O N    |   R E A C T A N C I A   |   I M P E D A N C I A    |  WATTS ENFRIAMIENTO");
	sw->WriteLine("                              |                         |                         |                          |                    ");
	sw->WriteLine("            A F.P. = *        |  FC    BT-AT1   BT-AT2  |                         |       %R=   ****         |         ****       ");
	sw->WriteLine("       FC    BT-AT1   BT-AT2  |  ***    ****%        %  |       %Xa=  ****        |       %X=   ****         |    NIVEL DE RUIDO  ");
	sw->WriteLine("      ****   *****%        %  |  ***    ****%        %  |       %Xl=  ****        |       %Z=   ****         |       ***** Db     ");
	sw->WriteLine("      ****   *****%        %  |  ***    ****%        %  |       %Xt=  ****        |   Factor Corr = ****     |                    ");
	sw->WriteLine("      ****   *****%        %  |                         |                         | %Z final=   ***********  |                    ");
	sw->WriteLine("      ****   *****%        %  |                         |                         |                          |                   ");
	sw->WriteLine("          Ef DOE  *****%      |                         |                         |                          |                   ");
	sw->WriteLine("     -------------------------------------------------------------------------------------------------------------------------------");
	sw->WriteLine("");
	sw->WriteLine("     ************************************************************************************************");
	sw->WriteLine("");
	sw->WriteLine("       TIEMPO DE FABRICACION (POR BOBINA)");
	sw->WriteLine("         BT = ****  *                     ");
	sw->WriteLine("         AT = ******                       ");
	sw->WriteLine("         FACTOR DE IMPEDANCIA DADO POR APARATOS EN PLANTA TI 2005 (1)");
	sw->WriteLine("          ");
	sw->WriteLine("        ");
	sw->WriteLine("        ");
	sw->WriteLine("     ************************************************************************************************");
	sw->WriteLine("");
	sw->WriteLine("     NEMA DOE (AL 50% DE CARGA) 99.06% (EL REQUERIMIENTO MINIMO DE EFICIENCIA NEMA DOE ES 99.19%)");
	sw->WriteLine("");
	sw->WriteLine("     ***************************************************");
	sw->WriteLine("     * EL CONTENIDO DE MATERIALES ES -VER INFO-% VS 50%*");
	sw->WriteLine("     ***************************************************");
	sw->WriteLine("");
	sw->WriteLine("");
	sw->WriteLine("");
	sw->WriteLine("");
	sw->WriteLine("                    ELABORO: __________________    REVISO: ____________________    VERIFICO: ___________________");
	sw->WriteLine("                                     jfa                           jfa   ");
	sw->Close();
}
 void CTransformador::LlamaBD()
 {
	 //grabarBD("AA","MYBADE","0");
 }
 ///////////////////////////////////////////////////////////////////////////////////
//! \fn		FromNetString
//! \author	Esteban Sinuee Hern�ndez S�nchez
//! \brief	convierte de System::String^ a std::string
//! \date	22/06/2009
///////////////////////////////////////////////////////////////////////////////////
 std::string CTransformador::FromNetString(System::String^ str) 
{
	System::IntPtr ansi = System::Runtime::InteropServices::Marshal::StringToCoTaskMemAnsi(str);
    std::string retval((const char*)(void*)ansi);
    System::Runtime::InteropServices::Marshal::FreeCoTaskMem(ansi);
    return retval;
  }

 ///////////////////////////////////////////////////////////////////////////////////
//! \fn		FromCppString
//! \author	Esteban Sinuee Hern�ndez S�nchez
//! \brief	convierte de std::string a System::String^
//! \date	22/06/2009
///////////////////////////////////////////////////////////////////////////////////
 System::String^ CTransformador::FromCppString(std::string& str) {
	return System::Runtime::InteropServices::Marshal::PtrToStringAnsi((System::IntPtr)(void*)str.c_str());
  }