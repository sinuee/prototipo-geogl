///////////////////////////////////////////////////////////////////////////////////
//! \file	    CPared.cpp
//! \author     
//! \brief      Defincion de la clase CPared
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////


//Revisiones
//  Fecha      	      Descripción					   Persona  	  Folio
//dd/mmm/aa  Descripción de revisión o modificación 	 XXX  		 XXXYYY

//#include "StdAfx.h"
#include "CPared.h"
#include "CTanque.h"
#include "CParteViva.h"
#include "CTransformador.h"


CPared::CPared(CTanque *_Tanque)
{
	Tanque = _Tanque;
	iAltoInterior=920;
	iHeight=920;
	iLength=560;
	iAnchoInterior=560;
	dPesoTotal=58.810;
	dPesoTotalRad = 35.020;
}

int CPared::getAltoInterior()
{
	return iAltoInterior;
}

int CPared::getHeight()
{
	return iHeight;
}

int CPared::getLength()
{
	return iLength;
}

int CPared::getAnchoInterior()
{
	return iAnchoInterior;
}
double CPared::getPesoTotal()
{
	return dPesoTotal;
}

double CPared::getPesoTotalRad()
{
	return dPesoTotalRad;
}