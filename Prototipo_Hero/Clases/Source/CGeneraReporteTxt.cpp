#include "StdAfx.h"
#include "CGeneraReporteTxt.h"
using namespace System;
using namespace System::IO;

CGeneraReporteTxt::CGeneraReporteTxt(void)
{}

int Escribe() 
{
	String^ fileName = "ReporteHero.txt";

   StreamWriter^ sw = gcnew StreamWriter(fileName);
   sw->WriteLine("Aext file is born!");
   sw->Write("You can use WriteLine");
   sw->WriteLine("...or just Write");
   sw->WriteLine("and do {0} output too.", "formatted");
   sw->WriteLine("You can also send non-text objects:");
   sw->WriteLine(DateTime::Now);
   sw->Close();
   Console::WriteLine("a new file ('{0}') has been written", fileName);

   return 0;
}
