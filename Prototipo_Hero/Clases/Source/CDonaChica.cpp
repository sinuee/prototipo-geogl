///////////////////////////////////////////////////////////////////////////////////
//! \file	    CDonaChica.cpp
//! \author     
//! \brief      Defincion de la clase CDonaChica
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////


//Revisiones
//  Fecha      	      Descripción					   Persona  	  Folio
//dd/mmm/aa  Descripción de revisión o modificación 	 XXX  		 XXXYYY


//#include "StdAfx.h"
#include "CDonaChica.h"
#include "CNucleo.h"

CDonaChica::CDonaChica(CNucleo *_CNucleo)
{
	Nucleo = _CNucleo;
	dDNucleo = 99.293;//valor de prueba
}

double CDonaChica::getDNucleo()
{
	return dDNucleo;
}