///////////////////////////////////////////////////////////////////////////////////
//! \file	    CBase.cpp
//! \author     
//! \brief      Defincion de la clase CBase
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////


//Revisiones
//  Fecha      	      Descripción					   Persona  	  Folio
//dd/mmm/aa  Descripción de revisión o modificación 	 XXX  		 XXXYYY

//#include "StdAfx.h"
#include "CBase.h"
#include "CTanque.h"
#include "CParteViva.h"
#include "CTransformador.h"


CBase::CBase(CTanque *_Tanque)
{
	Tanque = _Tanque;
	dAlturaBase = 83.25;
	iAlturaUsa = 1373;
	dWidth = 619.52;
	dHeight = 83.26;
	dEspesorPared = 4.76;
	dPesoTotal = 46.0537;
	cPlacaBase = "BASE_SOLID_CR";
	iKvas = Tanque->Transformador->obtenerKvas();

}

double CBase::getEspesorBase()
{
	if(iKvas<=500)
		return 4.76;
	else
		return 6.35;
}

double CBase::getAlturaBase()
{
	return dAlturaBase;
}

double CBase::getAlturaUsa()
{
	return iAlturaUsa;
}

double CBase::getWidth()
{
	return dWidth;
}

double CBase::getHeight()
{
	return dHeight;
}

double CBase::getEspesorPared()
{
	return dEspesorPared;
}

double CBase::getPesoTotal()
{
	return dPesoTotal;
}

