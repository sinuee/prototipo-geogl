///////////////////////////////////////////////////////////////////////////////////
//! \file	    CTapa.cpp
//! \author     
//! \brief      Defincion de la clase CTapa
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////


//Revisiones
//  Fecha      	      Descripción					   Persona  	  Folio
//dd/mmm/aa  Descripción de revisión o modificación 	 XXX  		 XXXYYY

//#include "StdAfx.h"
#include "CTapa.h"
#include "CTanque.h"
#include "CParteViva.h"
#include "CTransformador.h"



CTapa::CTapa(CTanque *_Tanque)
{
	Tanque = _Tanque;
	iAnchoInterior = 560;
	iAnchoTapa=640;
	iWidth =1140;
	dEspesorTapa = 3.2;
	dPesoTotal = 249.149;
	iLargoInterior =1140;
	cMaterial ="PLACA FE";
}

int CTapa::getAnchoInterior()
{
	return iAnchoInterior;
}

int CTapa::getAnchoTapa()
{
	return iAnchoTapa;
}

int CTapa::getWidth()
{
	return iWidth;
}

double CTapa::getEspesorTapa()
{
	return dEspesorTapa;
}

double CTapa::getPesoTotal()
{
	return dPesoTotal;
}

int CTapa::getLargoInterior()
{
	return iLargoInterior;
}
