///////////////////////////////////////////////////////////////////////////////////
//! \file	    CNucleo.cpp
//! \author     
//! \brief      Defincion de la clase CNucleo
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////


//Revisiones
//  Fecha      	      Descripción					   Persona  	  Folio
//dd/mmm/aa  Descripción de revisión o modificación 	 XXX  		 XXXYYY

//#include "StdAfx.h"
#include "CNucleo.h"
#include "String.h"
#include "Math.h"
#include "CTransformador.h"
#include "CParteViva.h"
#include "CBobina.h"
#include "CDonaGrande.h"
#include "CDonaChica.h"

CNucleo::CNucleo(/*bool bI,bool bS*/CParteViva *_ParteViva)
{
	ParteViva = _ParteViva;
	DonaChica = new CDonaChica(this);
	DonaGrande = new CDonaGrande(this);

	iFrecuencia=60;
	dFactorApilamiento=0.955;
	iEspesorNucleo=71;
	iAnchoNucleo=240;
	iB2=20;
	cTipoNucleo="ENROLLADO";
	bIsight = ParteViva->Transformador->obtenerIsight();
	bSpst = ParteViva->Transformador->obtenerSpst();//bS;
	iAnchoLamina1=240;
	cDobleNucleo="NO";
	bAislPvSta1705=false;
	iToleranciaMitter=10;
	iDistanciaEntreFases=12;
	iAnchoLamina2=120;
	dVoltXVuelta = ParteViva->Bobina->getVoltXVuelta();
	dAlturaVentana =ParteViva->Bobina->getAlturaVentana();
	dDonaGDNucleo = DonaGrande->getDNucleo();

}

double CNucleo::getA2()
{
 return (getA1()-getEspesorEscalon());
}

double CNucleo::getA1()
{
 return getANucleo();
}

double CNucleo::getB1()
{
	return (getBNucleo() - 40);
}

double CNucleo::getEspesorEscalon()
{
	return (iB2 * 2);
}

double CNucleo::getGaussTotal() 
{
	return ((dVoltXVuelta * 10 * 10000000)/(4.44 * iFrecuencia * getAreaTotalNucleo()));
}

double CNucleo::getAreaTotalNucleo()
{
	return (((getA1() * getB1()) + (2 * getA2() * iB2)) / 100) * dFactorApilamiento;
}

double CNucleo::getBNucleo()
{
	if (bIsight)
	{
		if(strcmp(cTipoNucleo,"ENROLLADO")==0)
			return 1;//checar the :root :a-nucleo-e  y :b-nucleo-e
	}
	else
	{
		if(strcmp(cTipoNucleo,"ENROLLADO")==0)
			return iAnchoNucleo;
		else
			return iEspesorNucleo;
	}
}

double CNucleo::getANucleo()
{
	if (bIsight)
	{
		if(strcmp(cTipoNucleo,"ENROLLADO")==0)
			return 1;//checar the :root :a-nucleo-e  y :b-nucleo-e
	}
	else
	{
		if(strcmp(cTipoNucleo,"ENROLLADO")==0)
			return iEspesorNucleo;
		else
			return iAnchoNucleo;
	}
}

double CNucleo::getAreaNucleo()
{
	if (bSpst)
		return getAreaTotalNucleo();
	else
	{
		if (strcmp(cTipoNucleo,"ENROLLADO")==0)
			return ((((getANucleo() * getBNucleo()) / 1) * 2) * dFactorApilamiento);
		else
			return ((getANucleo() * getBNucleo() * dFactorApilamiento) / 100);
	}
}

double CNucleo::getGauss()
{
	double dNum =2.522522522E7;
	const double PI = 3.1415926535;
	if (bSpst)
		return getGaussTotal();
	else
	{
		if(false)//checar  the :root :menor?
			return  (((dNum/iFrecuencia)/(getAreaNucleo() / 1000))*dVoltXVuelta*100)/1000;
		else
		{
			if(strcmp(cTipoNucleo,"ENROLLADO")==0)
				return (sqrt(2.0)*100000000*(dVoltXVuelta/(2*PI*iFrecuencia*(getAreaNucleo()/100))));
			else
				return (sqrt(2.0)*100000000*(dVoltXVuelta/(2*PI*iFrecuencia*(getAreaNucleo()))));
		}
	}
}

double CNucleo::getRelacionNucleo()
{
	if(strcmp(cTipoNucleo,"ENROLLADO")==0)
		return (getBNucleo()/(2*getANucleo()));
	else
		return (getBNucleo()*getANucleo());
}

double CNucleo::getB11()//checar nombre B1
{
	return  (iEspesorNucleo - (2 * iB2));
}

double CNucleo::getC1()//verificar funcion redondea sup
{
	return (ceil(dAlturaVentana + 10));
}

double getF2Nucleo(/*double dDimRadialLv,double dLadoCortoMolde,int iDistanciaEntreFases*/)
{/*
	if (strcmp(cTipoNucleo,"ENROLLADO"==0))
		return (getD2Nucleo()+(2*iEspesorNucleo));
	else
		return (floor((((2*dDimRadialLv) + dLadoCortoMolde + iDistanciaEntreFases)/10)+0.9))*10;
*/
	return 0;
}

int CNucleo::getD2Nucleo()
{
	if(strcmp(cTipoNucleo,"ENROLLADO")==0)
	{
		if(bAislPvSta1705)
			return ceil(dDonaGDNucleo - 5);
		else
			return ceil(dDonaGDNucleo);
	}else
		return ceil(/*getF2Nucleo()-*/getANucleo());
}

int CNucleo::getB1Nucleo()
{
	if(strcmp(cDobleNucleo,"NO")==0)
		return getBNucleo();
	else
		return iAnchoLamina1;
}

int CNucleo::getCNucleo()
{
	if(bSpst)
		return getC1();
	else
	{
		if(strcmp(cTipoNucleo,"ENROLLADO")==0)
		{
			if(bAislPvSta1705)
				return floor(dAlturaVentana + 5);
			else
				return floor(dAlturaVentana);
		}else
			return floor(dAlturaVentana + iToleranciaMitter);
	}
}

double CNucleo::getF1Nucleo(double dDonasCDNucleo)
{
	if(strcmp(cTipoNucleo,"ENROLLADO")==0)
		return (getD1Nucleo(dDonasCDNucleo)+(2*iEspesorNucleo));
	else
		return 0;
}

double CNucleo::getD1Nucleo(double dDonasCDNucleo)
{
	if(strcmp(cTipoNucleo,"ENROLLADO")==0)
		return dDonasCDNucleo;
	else
		return 0;
}

double CNucleo::getENucleo()
{
	return (getCNucleo()+(2*getANucleo())+getToleranciaNucleo());
}

double CNucleo::getToleranciaNucleo()
{
	if(bSpst)
		return 0;
	else
		if(false)//chechar :root :menor?
			return 0;
		else
			return 5;
}

int CNucleo::getB2Nucleo()
{
	if(strcmp(cDobleNucleo,"NO")==0)
		return 0;
	else
		return iAnchoLamina2;
}
/*
double CNucleo::getC2()
{
	return (getC1()+(2*iB2));
}*/

double CNucleo::getF2()
{
	return 1;//((2*dDimRadialLv) + dLadoCortoMolde + iDistanciaEntreFases);//redondea sup
}

double CNucleo::getD2()
{
	return (getD1() + (2*iB2));
}

double CNucleo::getD1()
{
	return 0/*(getF2() + getA1())*/;
}

double CNucleo::getL1()
{
	return (2/*getF2()*/)+getA1();
}

double CNucleo::getL2()
{
	return getL1() - (2*iB2);
}

double CNucleo::getH2()
{
	return getH1() - (2 * iB2);
}

double CNucleo::getH1()
{
	return /* getC1() + */(2 * getA1());
}

int CNucleo::getAnchoLamina1()
{ 
	return iAnchoLamina1;
}

