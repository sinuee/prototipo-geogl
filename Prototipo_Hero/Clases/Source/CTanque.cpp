///////////////////////////////////////////////////////////////////////////////////
//! \file	    CTanque.cpp
//! \author     
//! \brief      Defincion de la clase CTanque
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////


//Revisiones
//  Fecha      	      Descripción					   Persona  	  Folio
//dd/mmm/aa  Descripción de revisión o modificación 	 XXX  		 XXXYYY

//#include "StdAfx.h"
#include "CTanque.h"
#include "CPared.h"
#include "CBase.h"
#include "CTapa.h"

CTanque::CTanque(CTransformador *_Transformador)
{

	Pared = new CPared(this);
	Tapa = new CTapa(this);
	Base = new CBase(this);
	dPesoTotal = getPesoTotal();
	dVolumen = 4.77E7;
	cLiquido = "ACEITE";
	iPresion =12;
}

double CTanque::getPesoTotal()
{
	return ((Base->getPesoTotal())+(Pared->getPesoTotal())+(Tapa->getPesoTotal()))-(Pared->getPesoTotalRad());
}

double CTanque::getVolumen()
{
	return dVolumen;
}

int CTanque::getPresion()
{
	return iPresion;
}