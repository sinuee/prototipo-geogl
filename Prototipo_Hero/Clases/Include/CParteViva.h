///////////////////////////////////////////////////////////////////////////////////
//! \file	    CParteViva.h
//! \author     
//! \brief      Definicion de la clase CParteViva
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
//////////////////////////////////////////////////////////////////////////////////

//Revisiones
//   Fecha      	     Descripción					Persona  	 Folio
//dd/mmm/aa  Descripción de revisión o modificación 	  XXX  		 XXXYYY


class CTransformador;
class CBobina;
class CNucleo;

#if !defined(__PARTEVIVA_H_)
#define __PARTEVIVA_H_

///////////////////////////////////////////////////////////////////////////////////
//! \class		CParteViva
//! \author     
//! \brief      Definicion de la clase CParteViva
//! \date       
//////////////////////////////////////////////////////////////////////////////////

class CParteViva{
	protected:
		
	public:
		CTransformador *Transformador;
		CNucleo *Nucleo;
		CBobina *Bobina;
		//DECLARACION DE ATRIBUTOS
	public:	
		//TABLAS
		//FUNCIONES EXCLUSIVAS DE LA CLASE
		CParteViva(CTransformador *_Transformador);
		//FUNCIONES OBTENER
		//FUNCIONES ASIGNAR
		//FUNCIONES CALCULAR

};

#endif //#if !defined(__PARTEVIVA_H_)