///////////////////////////////////////////////////////////////////////////////////
//! \file	    CBobina.h
//! \author     
//! \brief      Definicion de la clase CBobina
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
//////////////////////////////////////////////////////////////////////////////////

//Revisiones
//   Fecha      	     Descripción					Persona  	 Folio
//dd/mmm/aa  Descripción de revisión o modificación 	  XXX  		 XXXYYY


class CParteViva;

#if !defined(__BOBINA_H_)
#define __BOBINA_H_

///////////////////////////////////////////////////////////////////////////////////
//! \class		CBobina
//! \author     
//! \brief      Definicion de la clase CBobina
//! \date       
//////////////////////////////////////////////////////////////////////////////////

class CBobina{
	protected:
		
	private:
		CParteViva *ParteViva;
		//DECLARACION DE ATRIBUTOS
		int iVueltasBt;
		char *cConexionBt;
		int iVoltajeLineaBt;
		double dFases;
		bool bSpst;
		bool b69Kv;
		int iToleranciaDropeo;
		double dEspesorDuctoHor;
		char *cTipoBobina;
		double *dCabezalAlturanVentana;
		int iConductoresEnPar;
		char *cTipoConductorBt;
		int iAnchoAislado;
		int iAnchoDesnudo;
		int iCabezalAtIni;
		double dAnchoAisladoAt;
		int iConductoresEnParAt;
		int iCondXCapa;
		int iD69;
		int iEntreSeccionesBt;
	    int iVueltasXCapaBt;
		int iCabezalBtIni;
		int iCabezal;
		int iCabezalPaquetes;
		int iCabezalBtSpst;



	public:	


		//TABLAS
		//FUNCIONES EXCLUSIVAS DE LA CLASE
		CBobina(CParteViva *_ParteViva);
		//FUNCIONES OBTENER
		int getAlturaVentana();
		int getAlturaBobina();
		double getCabezalAlturaVentana(int i);
		double getAlturaBobinaSpst();
		double getAlturaBobinaPrev69();
		double getAnchoAislado();
		double getAnchoDesnudo();
		double getLadoCortoMolde();
		double getAlturaTotalAtSpst();
		double getLongitudTotalBob();
		double getVueltaFalsa();
		double getAlturaEfectASpst();
		double getCabezalBt();
		double getCabezalBt1();
		double getVoltXVuelta();
		double getVoltajeFaseBt();

		//FUNCIONES ASIGNAR

		//FUNCIONES CALCULAR

};

#endif //#if !defined(__BOBINA_H_)