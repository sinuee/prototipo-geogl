///////////////////////////////////////////////////////////////////////////////////
//! \file	    CTanque.h
//! \author     
//! \brief      Definicion de la clase CTanque
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
//////////////////////////////////////////////////////////////////////////////////

//Revisiones
//   Fecha      	     Descripción					Persona  	 Folio
//dd/mmm/aa  Descripción de revisión o modificación 	  XXX  		 XXXYYY

class CTransformador;
class CPared;
class CBase;
class CTapa;

#if !defined(__TANQUE_H_)
#define __TANQUE_H_

///////////////////////////////////////////////////////////////////////////////////
//! \class		CTanque
//! \author     
//! \brief      Definicion de la clase CTanque
//! \date       
//////////////////////////////////////////////////////////////////////////////////

class CTanque{
	protected:
		
	private:
		double dPesoTotal;
		double dVolumen;
		int iPresion;


		//DECLARACION DE ATRIBUTOS
	public:
		CTanque(CTransformador *_Transformador);
		CTransformador *Transformador;
		CTapa *Tapa;
		CBase *Base;
		CPared *Pared;
		char *cLiquido;
		double getPesoTotal();
		double getVolumen();
		int getPresion();
		//TABLAS
		//FUNCIONES EXCLUSIVAS DE LA CLASE
		//FUNCIONES OBTENER
		//FUNCIONES ASIGNAR
		//FUNCIONES CALCULAR

};

#endif //#if !defined(__TANQUE_H_)