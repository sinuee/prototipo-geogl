///////////////////////////////////////////////////////////////////////////////////
//! \file	    CTransformador.h
//! \author     
//! \brief      Definicion de la clase CTransformador
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
//////////////////////////////////////////////////////////////////////////////////

//Revisiones
//   Fecha      	     Descripci�n					Persona  	 Folio
//dd/mmm/aa  Descripci�n de revisi�n o modificaci�n 	  XXX  		 XXXYYY

class CParteViva;
class CTanque;

#include <string>
#if !defined(__TRANSFORMADOR_H_)
#define __TRANSFORMADOR_H_

///////////////////////////////////////////////////////////////////////////////////
//! \class		CTransformador
//! \author     
//! \brief      Definicion de la clase CTransformador
//! \date       
//////////////////////////////////////////////////////////////////////////////////

class CTransformador{
	protected:
		
	public:
		
		//DECLARACION DE ATRIBUTOS
		bool bIsight;
		bool bSpst;
		bool b69Kv;
		int iOtroDiametroAislBt;
		int iOtroAnchoBt;
		double dOtroDiametroDesBt;
		char cCliente[50];
		char cNumOrden[50];
		int iCantidad;
		double dParidad;
		int iFases;
		int iKvas;
		int iFrecuencia;
		int iAltitud;
		int iSobreElevacion;
		char cTipoEnfriamiento[50];
		char cPantallaElectrostatica[50];
		char cZonaSismica[50];
		char cDise�adoPor[50];
		int iCteEvaluacionNucleo;
		int iCteEvaluacionDevanado;
		char cNorma[50];

		char cTipoTransformador[50];
		char cTipoGargantas[50];
		char cInterfazBt[50];
		char cInterfazAt[50];
		char cFrente[50];
		char cTiposRadiador[50];
		char cBoquillaEnTapa[50];

		char cTCS[50];
		char cTanqueConservador[50];
		char cTipoBase[50];
		char cApoyoGato[50];
		double SobreElevacionTemperatura;
		
		


	public:
		CParteViva *ParteViva;
		CTanque *Tanque;
		char *cConductorBt;
		char *cArregloBt;
		char *cTipoAccesorios;
		//TABLAS
		//FUNCIONES EXCLUSIVAS DE LA CLASE

		//FUNCIONES OBTENER
		void generaReporteElectrico(std::string NombreArch);
		bool obtenerIsight();
		bool obtenerSpst();
	    bool obtener69Kv();
		int obtenerOtroDiametroAislBt();
		int obtenerOtroAnchoBt();
		double obtenerOtroDiametroDesBt();
		int obtenerKvas();
		CTransformador(void);
		//FUNCIONES ASIGNAR
		//FUNCIONES CALCULAR
		void LlamaBD();
		///////////////////////////////////////////////////////////////////////////////////
//! \fn		FromNetString
//! \author	Esteban Sinuee Hern�ndez S�nchez
//! \brief	convierte de System::String^ a std::string
//! \date	22/06/2009
///////////////////////////////////////////////////////////////////////////////////
std::string FromNetString(System::String^ str);

 ///////////////////////////////////////////////////////////////////////////////////
//! \fn		FromCppString
//! \author	Esteban Sinuee Hern�ndez S�nchez
//! \brief	convierte de std::string a System::String^
//! \date	22/06/2009
///////////////////////////////////////////////////////////////////////////////////
System::String^ FromCppString(std::string& str);


};

#endif //#if !defined(__TRANSFORMADOR_H_)