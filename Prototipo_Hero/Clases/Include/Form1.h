#pragma once

#include "HeroLib.h"
#include "CTransformador.h"

namespace Prototipo_Hero {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace Hero;

	
	/// <summary>
	/// Summary for Form1
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			WinHero = gcnew COpenGL(this,this->Width/2,this->Height/2);
			Transformador = new CTransformador();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected: 
	private: System::Windows::Forms::ToolStripMenuItem^  archivoToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  dibujoToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  ambienteToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  reporteToolStripMenuItem;

	private: System::Windows::Forms::ToolStripMenuItem^  cargarDise˝oDeBaseDeDatosToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  guardarDise˝oDeArchivoToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  cargarDise˝oDeArchivoToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  guardarDise˝oDeBaseDeDatosToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  generarSCRToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  generarAutolispToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  specOnlyToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  specGraphicsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  specGraphicsTreeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  specTreeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  graphicsTreeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  graphicsOnlyToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  treeOnlyToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  dise˝oBaseToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  estructuraToolStripMenuItem;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::ComponentModel::IContainer^  components;
	private: System::Windows::Forms::TreeView^  treeView1;
	private: System::Windows::Forms::TabControl^  tabControl1;
	private: System::Windows::Forms::TabPage^  tabPage1;
	private: System::Windows::Forms::ComboBox^  comboBox11;
	private: System::Windows::Forms::ComboBox^  comboBox10;
	private: System::Windows::Forms::ComboBox^  comboBox9;
	private: System::Windows::Forms::ComboBox^  comboBox8;







	private: System::Windows::Forms::Panel^  panel14;
	private: System::Windows::Forms::RadioButton^  radioButton34;
	private: System::Windows::Forms::RadioButton^  radioButton33;












	private: System::Windows::Forms::Panel^  panel8;
	private: System::Windows::Forms::RadioButton^  radioButton22;
	private: System::Windows::Forms::RadioButton^  radioButton21;
	private: System::Windows::Forms::RadioButton^  radioButton20;
	private: System::Windows::Forms::RadioButton^  radioButton19;
	private: System::Windows::Forms::Panel^  panel7;
	private: System::Windows::Forms::RadioButton^  radioButton18;
	private: System::Windows::Forms::RadioButton^  radioButton17;




















	private: System::Windows::Forms::Label^  label27;
	private: System::Windows::Forms::Label^  label26;




	private: System::Windows::Forms::Label^  label21;
	private: System::Windows::Forms::Label^  label20;
	private: System::Windows::Forms::Label^  label19;
	private: System::Windows::Forms::Label^  label18;
	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::Label^  label16;




	private: System::Windows::Forms::Label^  label11;


	private: System::Windows::Forms::TextBox^  textBox8;
	private: System::Windows::Forms::TextBox^  textBox7;
	private: System::Windows::Forms::TextBox^  textBox6;

	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::TextBox^  textBox1;

	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label6;

	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;



	private: System::Windows::Forms::Panel^  panel13;
	private: System::Windows::Forms::RadioButton^  radioButton32;
	private: System::Windows::Forms::RadioButton^  radioButton31;
	private: System::Windows::Forms::TabPage^  tabPage2;
	private: System::Windows::Forms::ComboBox^  comboBox7;
	private: System::Windows::Forms::ComboBox^  comboBox6;
	private: System::Windows::Forms::ComboBox^  comboBox5;
	private: System::Windows::Forms::ComboBox^  comboBox4;
	private: System::Windows::Forms::ComboBox^  comboBox3;
	private: System::Windows::Forms::ComboBox^  comboBox2;
	private: System::Windows::Forms::Label^  label35;
	private: System::Windows::Forms::Label^  label34;
	private: System::Windows::Forms::Label^  label33;
	private: System::Windows::Forms::Label^  label32;
	private: System::Windows::Forms::Label^  label31;
	private: System::Windows::Forms::Label^  label30;
	private: System::Windows::Forms::Label^  label29;




	private: System::Windows::Forms::TabPage^  tabPage3;
	private: System::Windows::Forms::ComboBox^  comboBox1;
	private: System::Windows::Forms::Panel^  panel20;
	private: System::Windows::Forms::RadioButton^  radioButton44;
	private: System::Windows::Forms::RadioButton^  radioButton43;
	private: System::Windows::Forms::Panel^  panel18;
	private: System::Windows::Forms::RadioButton^  radioButton42;
	private: System::Windows::Forms::RadioButton^  radioButton41;
	private: System::Windows::Forms::Panel^  panel16;
	private: System::Windows::Forms::RadioButton^  radioButton40;
	private: System::Windows::Forms::RadioButton^  radioButton39;




	private: System::Windows::Forms::Label^  label39;
	private: System::Windows::Forms::Label^  label38;
	private: System::Windows::Forms::Label^  label37;
	private: System::Windows::Forms::Label^  label36;
private: System::Windows::Forms::Panel^  panel6;
private: System::Windows::Forms::RadioButton^  radioButton16;
private: System::Windows::Forms::RadioButton^  radioButton15;
private: System::Windows::Forms::Panel^  panel2;
private: System::Windows::Forms::RadioButton^  radioButton6;
private: System::Windows::Forms::RadioButton^  radioButton5;
private: System::Windows::Forms::RadioButton^  radioButton1;
private: System::Windows::Forms::RadioButton^  radioButton2;
	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		COpenGL^ WinHero;
			CTransformador *Transformador;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->archivoToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->cargarDise˝oDeArchivoToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->cargarDise˝oDeBaseDeDatosToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->guardarDise˝oDeArchivoToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->guardarDise˝oDeBaseDeDatosToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->dibujoToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->generarSCRToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->generarAutolispToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->ambienteToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->specGraphicsTreeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->specGraphicsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->specTreeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->graphicsTreeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->graphicsOnlyToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->specOnlyToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->treeOnlyToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->reporteToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->dise˝oBaseToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->estructuraToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->tabPage3 = (gcnew System::Windows::Forms::TabPage());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->panel20 = (gcnew System::Windows::Forms::Panel());
			this->radioButton44 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton43 = (gcnew System::Windows::Forms::RadioButton());
			this->panel18 = (gcnew System::Windows::Forms::Panel());
			this->radioButton42 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton41 = (gcnew System::Windows::Forms::RadioButton());
			this->panel16 = (gcnew System::Windows::Forms::Panel());
			this->radioButton40 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton39 = (gcnew System::Windows::Forms::RadioButton());
			this->label39 = (gcnew System::Windows::Forms::Label());
			this->label38 = (gcnew System::Windows::Forms::Label());
			this->label37 = (gcnew System::Windows::Forms::Label());
			this->label36 = (gcnew System::Windows::Forms::Label());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->radioButton6 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton5 = (gcnew System::Windows::Forms::RadioButton());
			this->comboBox7 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox6 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox5 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox4 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox3 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox2 = (gcnew System::Windows::Forms::ComboBox());
			this->label35 = (gcnew System::Windows::Forms::Label());
			this->label34 = (gcnew System::Windows::Forms::Label());
			this->label33 = (gcnew System::Windows::Forms::Label());
			this->label32 = (gcnew System::Windows::Forms::Label());
			this->label31 = (gcnew System::Windows::Forms::Label());
			this->label30 = (gcnew System::Windows::Forms::Label());
			this->label29 = (gcnew System::Windows::Forms::Label());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->comboBox11 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox10 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox9 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox8 = (gcnew System::Windows::Forms::ComboBox());
			this->panel14 = (gcnew System::Windows::Forms::Panel());
			this->radioButton34 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton33 = (gcnew System::Windows::Forms::RadioButton());
			this->panel8 = (gcnew System::Windows::Forms::Panel());
			this->radioButton22 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton21 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton20 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton19 = (gcnew System::Windows::Forms::RadioButton());
			this->panel7 = (gcnew System::Windows::Forms::Panel());
			this->radioButton18 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton17 = (gcnew System::Windows::Forms::RadioButton());
			this->panel6 = (gcnew System::Windows::Forms::Panel());
			this->radioButton16 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton15 = (gcnew System::Windows::Forms::RadioButton());
			this->label27 = (gcnew System::Windows::Forms::Label());
			this->label26 = (gcnew System::Windows::Forms::Label());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->textBox8 = (gcnew System::Windows::Forms::TextBox());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->panel13 = (gcnew System::Windows::Forms::Panel());
			this->radioButton32 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton31 = (gcnew System::Windows::Forms::RadioButton());
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->treeView1 = (gcnew System::Windows::Forms::TreeView());
			this->radioButton1 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton2 = (gcnew System::Windows::Forms::RadioButton());
			this->menuStrip1->SuspendLayout();
			this->tabPage3->SuspendLayout();
			this->panel20->SuspendLayout();
			this->panel18->SuspendLayout();
			this->panel16->SuspendLayout();
			this->tabPage2->SuspendLayout();
			this->panel2->SuspendLayout();
			this->tabPage1->SuspendLayout();
			this->panel14->SuspendLayout();
			this->panel8->SuspendLayout();
			this->panel7->SuspendLayout();
			this->panel6->SuspendLayout();
			this->panel13->SuspendLayout();
			this->tabControl1->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {this->archivoToolStripMenuItem, 
				this->dibujoToolStripMenuItem, this->ambienteToolStripMenuItem, this->reporteToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(926, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// archivoToolStripMenuItem
			// 
			this->archivoToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {this->cargarDise˝oDeArchivoToolStripMenuItem, 
				this->cargarDise˝oDeBaseDeDatosToolStripMenuItem, this->guardarDise˝oDeArchivoToolStripMenuItem, this->guardarDise˝oDeBaseDeDatosToolStripMenuItem});
			this->archivoToolStripMenuItem->Name = L"archivoToolStripMenuItem";
			this->archivoToolStripMenuItem->Size = System::Drawing::Size(55, 20);
			this->archivoToolStripMenuItem->Text = L"Archivo";
			// 
			// cargarDise˝oDeArchivoToolStripMenuItem
			// 
			this->cargarDise˝oDeArchivoToolStripMenuItem->Name = L"cargarDise˝oDeArchivoToolStripMenuItem";
			this->cargarDise˝oDeArchivoToolStripMenuItem->Size = System::Drawing::Size(245, 22);
			this->cargarDise˝oDeArchivoToolStripMenuItem->Text = L"Cargar dise˝o de Archivo";
			// 
			// cargarDise˝oDeBaseDeDatosToolStripMenuItem
			// 
			this->cargarDise˝oDeBaseDeDatosToolStripMenuItem->Name = L"cargarDise˝oDeBaseDeDatosToolStripMenuItem";
			this->cargarDise˝oDeBaseDeDatosToolStripMenuItem->Size = System::Drawing::Size(245, 22);
			this->cargarDise˝oDeBaseDeDatosToolStripMenuItem->Text = L"Cargar dise˝o de Base de Datos";
			// 
			// guardarDise˝oDeArchivoToolStripMenuItem
			// 
			this->guardarDise˝oDeArchivoToolStripMenuItem->Name = L"guardarDise˝oDeArchivoToolStripMenuItem";
			this->guardarDise˝oDeArchivoToolStripMenuItem->Size = System::Drawing::Size(245, 22);
			this->guardarDise˝oDeArchivoToolStripMenuItem->Text = L"Guardar dise˝o en Archivo";
			// 
			// guardarDise˝oDeBaseDeDatosToolStripMenuItem
			// 
			this->guardarDise˝oDeBaseDeDatosToolStripMenuItem->Name = L"guardarDise˝oDeBaseDeDatosToolStripMenuItem";
			this->guardarDise˝oDeBaseDeDatosToolStripMenuItem->Size = System::Drawing::Size(245, 22);
			this->guardarDise˝oDeBaseDeDatosToolStripMenuItem->Text = L"Guardar dise˝o en Base de Datos";
			// 
			// dibujoToolStripMenuItem
			// 
			this->dibujoToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->generarSCRToolStripMenuItem, 
				this->generarAutolispToolStripMenuItem});
			this->dibujoToolStripMenuItem->Name = L"dibujoToolStripMenuItem";
			this->dibujoToolStripMenuItem->Size = System::Drawing::Size(49, 20);
			this->dibujoToolStripMenuItem->Text = L"Dibujo";
			// 
			// generarSCRToolStripMenuItem
			// 
			this->generarSCRToolStripMenuItem->Name = L"generarSCRToolStripMenuItem";
			this->generarSCRToolStripMenuItem->Size = System::Drawing::Size(165, 22);
			this->generarSCRToolStripMenuItem->Text = L"Generar SCR";
			// 
			// generarAutolispToolStripMenuItem
			// 
			this->generarAutolispToolStripMenuItem->Name = L"generarAutolispToolStripMenuItem";
			this->generarAutolispToolStripMenuItem->Size = System::Drawing::Size(165, 22);
			this->generarAutolispToolStripMenuItem->Text = L"Generar Autolisp";
			// 
			// ambienteToolStripMenuItem
			// 
			this->ambienteToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(7) {this->specGraphicsTreeToolStripMenuItem, 
				this->specGraphicsToolStripMenuItem, this->specTreeToolStripMenuItem, this->graphicsTreeToolStripMenuItem, this->graphicsOnlyToolStripMenuItem, 
				this->specOnlyToolStripMenuItem, this->treeOnlyToolStripMenuItem});
			this->ambienteToolStripMenuItem->Name = L"ambienteToolStripMenuItem";
			this->ambienteToolStripMenuItem->Size = System::Drawing::Size(64, 20);
			this->ambienteToolStripMenuItem->Text = L"Ambiente";
			// 
			// specGraphicsTreeToolStripMenuItem
			// 
			this->specGraphicsTreeToolStripMenuItem->Name = L"specGraphicsTreeToolStripMenuItem";
			this->specGraphicsTreeToolStripMenuItem->Size = System::Drawing::Size(177, 22);
			this->specGraphicsTreeToolStripMenuItem->Text = L"Spec Graphics Tree";
			this->specGraphicsTreeToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::specGraphicsTreeToolStripMenuItem_Click);
			// 
			// specGraphicsToolStripMenuItem
			// 
			this->specGraphicsToolStripMenuItem->Name = L"specGraphicsToolStripMenuItem";
			this->specGraphicsToolStripMenuItem->Size = System::Drawing::Size(177, 22);
			this->specGraphicsToolStripMenuItem->Text = L"Spec Graphics";
			this->specGraphicsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::specGraphicsToolStripMenuItem_Click);
			// 
			// specTreeToolStripMenuItem
			// 
			this->specTreeToolStripMenuItem->Name = L"specTreeToolStripMenuItem";
			this->specTreeToolStripMenuItem->Size = System::Drawing::Size(177, 22);
			this->specTreeToolStripMenuItem->Text = L"Spec Tree";
			this->specTreeToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::specTreeToolStripMenuItem_Click);
			// 
			// graphicsTreeToolStripMenuItem
			// 
			this->graphicsTreeToolStripMenuItem->Name = L"graphicsTreeToolStripMenuItem";
			this->graphicsTreeToolStripMenuItem->Size = System::Drawing::Size(177, 22);
			this->graphicsTreeToolStripMenuItem->Text = L"Graphics Tree";
			this->graphicsTreeToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::graphicsTreeToolStripMenuItem_Click);
			// 
			// graphicsOnlyToolStripMenuItem
			// 
			this->graphicsOnlyToolStripMenuItem->Name = L"graphicsOnlyToolStripMenuItem";
			this->graphicsOnlyToolStripMenuItem->Size = System::Drawing::Size(177, 22);
			this->graphicsOnlyToolStripMenuItem->Text = L"Graphics Only";
			this->graphicsOnlyToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::graphicsOnlyToolStripMenuItem_Click);
			// 
			// specOnlyToolStripMenuItem
			// 
			this->specOnlyToolStripMenuItem->Name = L"specOnlyToolStripMenuItem";
			this->specOnlyToolStripMenuItem->Size = System::Drawing::Size(177, 22);
			this->specOnlyToolStripMenuItem->Text = L"Spec Only";
			this->specOnlyToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::specOnlyToolStripMenuItem_Click);
			// 
			// treeOnlyToolStripMenuItem
			// 
			this->treeOnlyToolStripMenuItem->Name = L"treeOnlyToolStripMenuItem";
			this->treeOnlyToolStripMenuItem->Size = System::Drawing::Size(177, 22);
			this->treeOnlyToolStripMenuItem->Text = L"Tree Only";
			this->treeOnlyToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::treeOnlyToolStripMenuItem_Click);
			// 
			// reporteToolStripMenuItem
			// 
			this->reporteToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->dise˝oBaseToolStripMenuItem, 
				this->estructuraToolStripMenuItem});
			this->reporteToolStripMenuItem->Name = L"reporteToolStripMenuItem";
			this->reporteToolStripMenuItem->Size = System::Drawing::Size(58, 20);
			this->reporteToolStripMenuItem->Text = L"Reporte";
			// 
			// dise˝oBaseToolStripMenuItem
			// 
			this->dise˝oBaseToolStripMenuItem->Name = L"dise˝oBaseToolStripMenuItem";
			this->dise˝oBaseToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->dise˝oBaseToolStripMenuItem->Text = L"Dise˝o  Base";
			this->dise˝oBaseToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::dise˝oBaseToolStripMenuItem_Click);
			// 
			// estructuraToolStripMenuItem
			// 
			this->estructuraToolStripMenuItem->Name = L"estructuraToolStripMenuItem";
			this->estructuraToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->estructuraToolStripMenuItem->Text = L"Estructura";
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Interval = 10;
			this->timer1->Tick += gcnew System::EventHandler(this, &Form1::timer1_Tick);
			// 
			// tabPage3
			// 
			this->tabPage3->BackColor = System::Drawing::Color::Transparent;
			this->tabPage3->Controls->Add(this->comboBox1);
			this->tabPage3->Controls->Add(this->panel20);
			this->tabPage3->Controls->Add(this->panel18);
			this->tabPage3->Controls->Add(this->panel16);
			this->tabPage3->Controls->Add(this->label39);
			this->tabPage3->Controls->Add(this->label38);
			this->tabPage3->Controls->Add(this->label37);
			this->tabPage3->Controls->Add(this->label36);
			this->tabPage3->Location = System::Drawing::Point(4, 22);
			this->tabPage3->Name = L"tabPage3";
			this->tabPage3->Padding = System::Windows::Forms::Padding(3);
			this->tabPage3->Size = System::Drawing::Size(490, 517);
			this->tabPage3->TabIndex = 2;
			this->tabPage3->Text = L"Accesorios";
			// 
			// comboBox1
			// 
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Items->AddRange(gcnew cli::array< System::Object^  >(3) {L"NORMAL", L"BASE I", L"BASE CON RUEDAS"});
			this->comboBox1->Location = System::Drawing::Point(139, 96);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(148, 21);
			this->comboBox1->TabIndex = 69;
			this->comboBox1->Text = L"NORMAL";
			this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox1_SelectedIndexChanged);
			// 
			// panel20
			// 
			this->panel20->Controls->Add(this->radioButton44);
			this->panel20->Controls->Add(this->radioButton43);
			this->panel20->Location = System::Drawing::Point(139, 129);
			this->panel20->Name = L"panel20";
			this->panel20->Size = System::Drawing::Size(148, 26);
			this->panel20->TabIndex = 68;
			// 
			// radioButton44
			// 
			this->radioButton44->AutoSize = true;
			this->radioButton44->Checked = true;
			this->radioButton44->Location = System::Drawing::Point(86, 6);
			this->radioButton44->Name = L"radioButton44";
			this->radioButton44->Size = System::Drawing::Size(41, 17);
			this->radioButton44->TabIndex = 1;
			this->radioButton44->TabStop = true;
			this->radioButton44->Text = L"NO";
			this->radioButton44->UseVisualStyleBackColor = true;
			this->radioButton44->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton44_CheckedChanged);
			// 
			// radioButton43
			// 
			this->radioButton43->AutoSize = true;
			this->radioButton43->Location = System::Drawing::Point(21, 6);
			this->radioButton43->Name = L"radioButton43";
			this->radioButton43->Size = System::Drawing::Size(35, 17);
			this->radioButton43->TabIndex = 0;
			this->radioButton43->Text = L"SI";
			this->radioButton43->UseVisualStyleBackColor = true;
			this->radioButton43->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton43_CheckedChanged);
			// 
			// panel18
			// 
			this->panel18->Controls->Add(this->radioButton42);
			this->panel18->Controls->Add(this->radioButton41);
			this->panel18->Location = System::Drawing::Point(139, 56);
			this->panel18->Name = L"panel18";
			this->panel18->Size = System::Drawing::Size(148, 26);
			this->panel18->TabIndex = 66;
			// 
			// radioButton42
			// 
			this->radioButton42->AutoSize = true;
			this->radioButton42->Checked = true;
			this->radioButton42->Location = System::Drawing::Point(86, 6);
			this->radioButton42->Name = L"radioButton42";
			this->radioButton42->Size = System::Drawing::Size(41, 17);
			this->radioButton42->TabIndex = 1;
			this->radioButton42->TabStop = true;
			this->radioButton42->Text = L"NO";
			this->radioButton42->UseVisualStyleBackColor = true;
			this->radioButton42->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton42_CheckedChanged);
			// 
			// radioButton41
			// 
			this->radioButton41->AutoSize = true;
			this->radioButton41->Location = System::Drawing::Point(21, 6);
			this->radioButton41->Name = L"radioButton41";
			this->radioButton41->Size = System::Drawing::Size(35, 17);
			this->radioButton41->TabIndex = 0;
			this->radioButton41->Text = L"SI";
			this->radioButton41->UseVisualStyleBackColor = true;
			this->radioButton41->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton41_CheckedChanged);
			// 
			// panel16
			// 
			this->panel16->Controls->Add(this->radioButton40);
			this->panel16->Controls->Add(this->radioButton39);
			this->panel16->Location = System::Drawing::Point(139, 19);
			this->panel16->Name = L"panel16";
			this->panel16->Size = System::Drawing::Size(148, 26);
			this->panel16->TabIndex = 64;
			// 
			// radioButton40
			// 
			this->radioButton40->AutoSize = true;
			this->radioButton40->Checked = true;
			this->radioButton40->Location = System::Drawing::Point(86, 3);
			this->radioButton40->Name = L"radioButton40";
			this->radioButton40->Size = System::Drawing::Size(41, 17);
			this->radioButton40->TabIndex = 1;
			this->radioButton40->TabStop = true;
			this->radioButton40->Text = L"NO";
			this->radioButton40->UseVisualStyleBackColor = true;
			this->radioButton40->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton40_CheckedChanged);
			// 
			// radioButton39
			// 
			this->radioButton39->AutoSize = true;
			this->radioButton39->Location = System::Drawing::Point(21, 3);
			this->radioButton39->Name = L"radioButton39";
			this->radioButton39->Size = System::Drawing::Size(35, 17);
			this->radioButton39->TabIndex = 0;
			this->radioButton39->Text = L"SI";
			this->radioButton39->UseVisualStyleBackColor = true;
			this->radioButton39->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton39_CheckedChanged);
			// 
			// label39
			// 
			this->label39->AutoSize = true;
			this->label39->Location = System::Drawing::Point(19, 142);
			this->label39->Name = L"label39";
			this->label39->Size = System::Drawing::Size(87, 13);
			this->label39->TabIndex = 3;
			this->label39->Text = L"Apoyo para Gato";
			// 
			// label38
			// 
			this->label38->AutoSize = true;
			this->label38->Location = System::Drawing::Point(19, 104);
			this->label38->Name = L"label38";
			this->label38->Size = System::Drawing::Size(70, 13);
			this->label38->TabIndex = 2;
			this->label38->Text = L"Tipo de Base";
			// 
			// label37
			// 
			this->label37->AutoSize = true;
			this->label37->Location = System::Drawing::Point(19, 69);
			this->label37->Name = L"label37";
			this->label37->Size = System::Drawing::Size(107, 13);
			this->label37->TabIndex = 1;
			this->label37->Text = L"Tanque Conservador";
			// 
			// label36
			// 
			this->label36->AutoSize = true;
			this->label36->Location = System::Drawing::Point(19, 32);
			this->label36->Name = L"label36";
			this->label36->Size = System::Drawing::Size(30, 13);
			this->label36->TabIndex = 0;
			this->label36->Text = L"TC\'S";
			// 
			// tabPage2
			// 
			this->tabPage2->BackColor = System::Drawing::Color::Transparent;
			this->tabPage2->Controls->Add(this->panel2);
			this->tabPage2->Controls->Add(this->comboBox7);
			this->tabPage2->Controls->Add(this->comboBox6);
			this->tabPage2->Controls->Add(this->comboBox5);
			this->tabPage2->Controls->Add(this->comboBox4);
			this->tabPage2->Controls->Add(this->comboBox3);
			this->tabPage2->Controls->Add(this->comboBox2);
			this->tabPage2->Controls->Add(this->label35);
			this->tabPage2->Controls->Add(this->label34);
			this->tabPage2->Controls->Add(this->label33);
			this->tabPage2->Controls->Add(this->label32);
			this->tabPage2->Controls->Add(this->label31);
			this->tabPage2->Controls->Add(this->label30);
			this->tabPage2->Controls->Add(this->label29);
			this->tabPage2->Location = System::Drawing::Point(4, 22);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(490, 517);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"Datos Mecanico";
			// 
			// panel2
			// 
			this->panel2->Controls->Add(this->radioButton6);
			this->panel2->Controls->Add(this->radioButton5);
			this->panel2->Location = System::Drawing::Point(133, 201);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(147, 22);
			this->panel2->TabIndex = 51;
			// 
			// radioButton6
			// 
			this->radioButton6->AutoSize = true;
			this->radioButton6->Checked = true;
			this->radioButton6->Location = System::Drawing::Point(89, 2);
			this->radioButton6->Name = L"radioButton6";
			this->radioButton6->Size = System::Drawing::Size(41, 17);
			this->radioButton6->TabIndex = 1;
			this->radioButton6->TabStop = true;
			this->radioButton6->Text = L"NO";
			this->radioButton6->UseVisualStyleBackColor = true;
			this->radioButton6->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton6_CheckedChanged);
			// 
			// radioButton5
			// 
			this->radioButton5->AutoSize = true;
			this->radioButton5->Location = System::Drawing::Point(20, 2);
			this->radioButton5->Name = L"radioButton5";
			this->radioButton5->Size = System::Drawing::Size(35, 17);
			this->radioButton5->TabIndex = 0;
			this->radioButton5->Text = L"SI";
			this->radioButton5->UseVisualStyleBackColor = true;
			this->radioButton5->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton5_CheckedChanged);
			// 
			// comboBox7
			// 
			this->comboBox7->FormattingEnabled = true;
			this->comboBox7->Items->AddRange(gcnew cli::array< System::Object^  >(1) {L"SOLDADOS"});
			this->comboBox7->Location = System::Drawing::Point(133, 239);
			this->comboBox7->Name = L"comboBox7";
			this->comboBox7->Size = System::Drawing::Size(121, 21);
			this->comboBox7->TabIndex = 16;
			this->comboBox7->Text = L"SOLDADOS";
			this->comboBox7->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox7_SelectedIndexChanged);
			// 
			// comboBox6
			// 
			this->comboBox6->FormattingEnabled = true;
			this->comboBox6->Items->AddRange(gcnew cli::array< System::Object^  >(2) {L"IZQUIERDO", L"DERECHO"});
			this->comboBox6->Location = System::Drawing::Point(133, 168);
			this->comboBox6->Name = L"comboBox6";
			this->comboBox6->Size = System::Drawing::Size(121, 21);
			this->comboBox6->TabIndex = 15;
			this->comboBox6->Text = L"IZQUIERDO";
			this->comboBox6->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox6_SelectedIndexChanged);
			// 
			// comboBox5
			// 
			this->comboBox5->FormattingEnabled = true;
			this->comboBox5->Location = System::Drawing::Point(133, 126);
			this->comboBox5->Name = L"comboBox5";
			this->comboBox5->Size = System::Drawing::Size(121, 21);
			this->comboBox5->TabIndex = 14;
			this->comboBox5->Text = L"GARGANTA";
			this->comboBox5->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox5_SelectedIndexChanged);
			// 
			// comboBox4
			// 
			this->comboBox4->FormattingEnabled = true;
			this->comboBox4->Location = System::Drawing::Point(133, 90);
			this->comboBox4->Name = L"comboBox4";
			this->comboBox4->Size = System::Drawing::Size(121, 21);
			this->comboBox4->TabIndex = 13;
			this->comboBox4->Text = L"GARGANTA";
			this->comboBox4->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox4_SelectedIndexChanged);
			// 
			// comboBox3
			// 
			this->comboBox3->FormattingEnabled = true;
			this->comboBox3->Location = System::Drawing::Point(133, 52);
			this->comboBox3->Name = L"comboBox3";
			this->comboBox3->Size = System::Drawing::Size(121, 21);
			this->comboBox3->TabIndex = 12;
			this->comboBox3->Text = L"ESTANDAR";
			this->comboBox3->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox3_SelectedIndexChanged);
			// 
			// comboBox2
			// 
			this->comboBox2->FormattingEnabled = true;
			this->comboBox2->Items->AddRange(gcnew cli::array< System::Object^  >(3) {L"ESTACION", L"CANGREJO", L"L.CORTOS"});
			this->comboBox2->Location = System::Drawing::Point(133, 14);
			this->comboBox2->Name = L"comboBox2";
			this->comboBox2->Size = System::Drawing::Size(121, 21);
			this->comboBox2->TabIndex = 11;
			this->comboBox2->Text = L"L.CORTOS";
			this->comboBox2->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox2_SelectedIndexChanged);
			// 
			// label35
			// 
			this->label35->AutoSize = true;
			this->label35->Location = System::Drawing::Point(6, 247);
			this->label35->Name = L"label35";
			this->label35->Size = System::Drawing::Size(74, 13);
			this->label35->TabIndex = 10;
			this->label35->Text = L"Tipo Radiador";
			// 
			// label34
			// 
			this->label34->AutoSize = true;
			this->label34->Location = System::Drawing::Point(6, 210);
			this->label34->Name = L"label34";
			this->label34->Size = System::Drawing::Size(87, 13);
			this->label34->TabIndex = 9;
			this->label34->Text = L"Boquilla en Tapa";
			// 
			// label33
			// 
			this->label33->AutoSize = true;
			this->label33->Location = System::Drawing::Point(6, 176);
			this->label33->Name = L"label33";
			this->label33->Size = System::Drawing::Size(37, 13);
			this->label33->TabIndex = 8;
			this->label33->Text = L"Frente";
			// 
			// label32
			// 
			this->label32->AutoSize = true;
			this->label32->Location = System::Drawing::Point(6, 134);
			this->label32->Name = L"label32";
			this->label32->Size = System::Drawing::Size(65, 13);
			this->label32->TabIndex = 7;
			this->label32->Text = L"Interfase AT";
			// 
			// label31
			// 
			this->label31->AutoSize = true;
			this->label31->Location = System::Drawing::Point(6, 98);
			this->label31->Name = L"label31";
			this->label31->Size = System::Drawing::Size(65, 13);
			this->label31->TabIndex = 6;
			this->label31->Text = L"Interfase BT";
			// 
			// label30
			// 
			this->label30->AutoSize = true;
			this->label30->Location = System::Drawing::Point(6, 60);
			this->label30->Name = L"label30";
			this->label30->Size = System::Drawing::Size(80, 13);
			this->label30->TabIndex = 5;
			this->label30->Text = L"Tipo Gargantas";
			// 
			// label29
			// 
			this->label29->AutoSize = true;
			this->label29->Location = System::Drawing::Point(6, 22);
			this->label29->Name = L"label29";
			this->label29->Size = System::Drawing::Size(99, 13);
			this->label29->TabIndex = 4;
			this->label29->Text = L"Tipo Transformador";
			// 
			// tabPage1
			// 
			this->tabPage1->AutoScroll = true;
			this->tabPage1->BackColor = System::Drawing::Color::Transparent;
			this->tabPage1->Controls->Add(this->comboBox11);
			this->tabPage1->Controls->Add(this->comboBox10);
			this->tabPage1->Controls->Add(this->comboBox9);
			this->tabPage1->Controls->Add(this->comboBox8);
			this->tabPage1->Controls->Add(this->panel14);
			this->tabPage1->Controls->Add(this->panel8);
			this->tabPage1->Controls->Add(this->panel7);
			this->tabPage1->Controls->Add(this->panel6);
			this->tabPage1->Controls->Add(this->label27);
			this->tabPage1->Controls->Add(this->label26);
			this->tabPage1->Controls->Add(this->label21);
			this->tabPage1->Controls->Add(this->label20);
			this->tabPage1->Controls->Add(this->label19);
			this->tabPage1->Controls->Add(this->label18);
			this->tabPage1->Controls->Add(this->label17);
			this->tabPage1->Controls->Add(this->label16);
			this->tabPage1->Controls->Add(this->label11);
			this->tabPage1->Controls->Add(this->textBox8);
			this->tabPage1->Controls->Add(this->textBox7);
			this->tabPage1->Controls->Add(this->textBox6);
			this->tabPage1->Controls->Add(this->textBox4);
			this->tabPage1->Controls->Add(this->textBox3);
			this->tabPage1->Controls->Add(this->textBox2);
			this->tabPage1->Controls->Add(this->textBox1);
			this->tabPage1->Controls->Add(this->label8);
			this->tabPage1->Controls->Add(this->label7);
			this->tabPage1->Controls->Add(this->label6);
			this->tabPage1->Controls->Add(this->label4);
			this->tabPage1->Controls->Add(this->label3);
			this->tabPage1->Controls->Add(this->label2);
			this->tabPage1->Controls->Add(this->label1);
			this->tabPage1->Controls->Add(this->panel13);
			this->tabPage1->Location = System::Drawing::Point(4, 22);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(490, 517);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"Datos Principales";
			// 
			// comboBox11
			// 
			this->comboBox11->FormattingEnabled = true;
			this->comboBox11->Items->AddRange(gcnew cli::array< System::Object^  >(1) {L"OA"});
			this->comboBox11->Location = System::Drawing::Point(163, 389);
			this->comboBox11->Name = L"comboBox11";
			this->comboBox11->Size = System::Drawing::Size(147, 21);
			this->comboBox11->TabIndex = 59;
			this->comboBox11->Text = L"OA";
			// 
			// comboBox10
			// 
			this->comboBox10->FormattingEnabled = true;
			this->comboBox10->Items->AddRange(gcnew cli::array< System::Object^  >(4) {L"1000", L"2000", L"2300", L"2500"});
			this->comboBox10->Location = System::Drawing::Point(163, 329);
			this->comboBox10->Name = L"comboBox10";
			this->comboBox10->Size = System::Drawing::Size(148, 21);
			this->comboBox10->TabIndex = 58;
			this->comboBox10->Text = L"2300";
			this->comboBox10->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox10_SelectedIndexChanged);
			// 
			// comboBox9
			// 
			this->comboBox9->FormattingEnabled = true;
			this->comboBox9->Items->AddRange(gcnew cli::array< System::Object^  >(2) {L"225", L"300"});
			this->comboBox9->Location = System::Drawing::Point(163, 267);
			this->comboBox9->Name = L"comboBox9";
			this->comboBox9->Size = System::Drawing::Size(148, 21);
			this->comboBox9->TabIndex = 57;
			this->comboBox9->Text = L"225";
			this->comboBox9->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox9_SelectedIndexChanged);
			// 
			// comboBox8
			// 
			this->comboBox8->FormattingEnabled = true;
			this->comboBox8->Items->AddRange(gcnew cli::array< System::Object^  >(3) {L"N-MX-J-284", L"N-MX-J-116", L"NOM-002-SEDE"});
			this->comboBox8->Location = System::Drawing::Point(163, 210);
			this->comboBox8->Name = L"comboBox8";
			this->comboBox8->Size = System::Drawing::Size(148, 21);
			this->comboBox8->TabIndex = 56;
			this->comboBox8->Text = L"N-MX-J-284";
			this->comboBox8->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox8_SelectedIndexChanged);
			// 
			// panel14
			// 
			this->panel14->Controls->Add(this->radioButton34);
			this->panel14->Controls->Add(this->radioButton33);
			this->panel14->Location = System::Drawing::Point(163, 442);
			this->panel14->Name = L"panel14";
			this->panel14->Size = System::Drawing::Size(147, 22);
			this->panel14->TabIndex = 50;
			// 
			// radioButton34
			// 
			this->radioButton34->AutoSize = true;
			this->radioButton34->Checked = true;
			this->radioButton34->Location = System::Drawing::Point(86, 3);
			this->radioButton34->Name = L"radioButton34";
			this->radioButton34->Size = System::Drawing::Size(41, 17);
			this->radioButton34->TabIndex = 1;
			this->radioButton34->TabStop = true;
			this->radioButton34->Text = L"NO";
			this->radioButton34->UseVisualStyleBackColor = true;
			this->radioButton34->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton34_CheckedChanged);
			// 
			// radioButton33
			// 
			this->radioButton33->AutoSize = true;
			this->radioButton33->Location = System::Drawing::Point(21, 3);
			this->radioButton33->Name = L"radioButton33";
			this->radioButton33->Size = System::Drawing::Size(35, 17);
			this->radioButton33->TabIndex = 0;
			this->radioButton33->Text = L"SI";
			this->radioButton33->UseVisualStyleBackColor = true;
			this->radioButton33->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton33_CheckedChanged);
			// 
			// panel8
			// 
			this->panel8->Controls->Add(this->radioButton22);
			this->panel8->Controls->Add(this->radioButton21);
			this->panel8->Controls->Add(this->radioButton20);
			this->panel8->Controls->Add(this->radioButton19);
			this->panel8->Location = System::Drawing::Point(163, 357);
			this->panel8->Name = L"panel8";
			this->panel8->Size = System::Drawing::Size(245, 22);
			this->panel8->TabIndex = 44;
			// 
			// radioButton22
			// 
			this->radioButton22->AutoSize = true;
			this->radioButton22->Location = System::Drawing::Point(123, 2);
			this->radioButton22->Name = L"radioButton22";
			this->radioButton22->Size = System::Drawing::Size(54, 17);
			this->radioButton22->TabIndex = 3;
			this->radioButton22->Text = L"55/65";
			this->radioButton22->UseVisualStyleBackColor = true;
			// 
			// radioButton21
			// 
			this->radioButton21->AutoSize = true;
			this->radioButton21->Location = System::Drawing::Point(182, 2);
			this->radioButton21->Name = L"radioButton21";
			this->radioButton21->Size = System::Drawing::Size(54, 17);
			this->radioButton21->TabIndex = 2;
			this->radioButton21->Text = L"45/55";
			this->radioButton21->UseVisualStyleBackColor = true;
			// 
			// radioButton20
			// 
			this->radioButton20->AutoSize = true;
			this->radioButton20->Checked = true;
			this->radioButton20->Location = System::Drawing::Point(74, 2);
			this->radioButton20->Name = L"radioButton20";
			this->radioButton20->Size = System::Drawing::Size(37, 17);
			this->radioButton20->TabIndex = 1;
			this->radioButton20->TabStop = true;
			this->radioButton20->Text = L"65";
			this->radioButton20->UseVisualStyleBackColor = true;
			// 
			// radioButton19
			// 
			this->radioButton19->AutoSize = true;
			this->radioButton19->Location = System::Drawing::Point(28, 2);
			this->radioButton19->Name = L"radioButton19";
			this->radioButton19->Size = System::Drawing::Size(37, 17);
			this->radioButton19->TabIndex = 0;
			this->radioButton19->Text = L"55";
			this->radioButton19->UseVisualStyleBackColor = true;
			this->radioButton19->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton19_CheckedChanged);
			// 
			// panel7
			// 
			this->panel7->Controls->Add(this->radioButton18);
			this->panel7->Controls->Add(this->radioButton17);
			this->panel7->Location = System::Drawing::Point(163, 295);
			this->panel7->Name = L"panel7";
			this->panel7->Size = System::Drawing::Size(148, 22);
			this->panel7->TabIndex = 43;
			// 
			// radioButton18
			// 
			this->radioButton18->AutoSize = true;
			this->radioButton18->Checked = true;
			this->radioButton18->Location = System::Drawing::Point(86, 2);
			this->radioButton18->Name = L"radioButton18";
			this->radioButton18->Size = System::Drawing::Size(37, 17);
			this->radioButton18->TabIndex = 1;
			this->radioButton18->TabStop = true;
			this->radioButton18->Text = L"60";
			this->radioButton18->UseVisualStyleBackColor = true;
			this->radioButton18->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton18_CheckedChanged);
			// 
			// radioButton17
			// 
			this->radioButton17->AutoSize = true;
			this->radioButton17->Location = System::Drawing::Point(21, 2);
			this->radioButton17->Name = L"radioButton17";
			this->radioButton17->Size = System::Drawing::Size(37, 17);
			this->radioButton17->TabIndex = 0;
			this->radioButton17->Text = L"50";
			this->radioButton17->UseVisualStyleBackColor = true;
			this->radioButton17->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton17_CheckedChanged);
			// 
			// panel6
			// 
			this->panel6->Controls->Add(this->radioButton16);
			this->panel6->Controls->Add(this->radioButton15);
			this->panel6->Location = System::Drawing::Point(163, 238);
			this->panel6->Name = L"panel6";
			this->panel6->Size = System::Drawing::Size(148, 23);
			this->panel6->TabIndex = 42;
			// 
			// radioButton16
			// 
			this->radioButton16->AutoSize = true;
			this->radioButton16->Checked = true;
			this->radioButton16->Location = System::Drawing::Point(86, 3);
			this->radioButton16->Name = L"radioButton16";
			this->radioButton16->Size = System::Drawing::Size(31, 17);
			this->radioButton16->TabIndex = 1;
			this->radioButton16->TabStop = true;
			this->radioButton16->Text = L"3";
			this->radioButton16->UseVisualStyleBackColor = true;
			this->radioButton16->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton16_CheckedChanged);
			// 
			// radioButton15
			// 
			this->radioButton15->AutoSize = true;
			this->radioButton15->Location = System::Drawing::Point(21, 3);
			this->radioButton15->Name = L"radioButton15";
			this->radioButton15->Size = System::Drawing::Size(31, 17);
			this->radioButton15->TabIndex = 0;
			this->radioButton15->Text = L"1";
			this->radioButton15->UseVisualStyleBackColor = true;
			this->radioButton15->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton15_CheckedChanged);
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->Location = System::Drawing::Point(3, 451);
			this->label27->Name = L"label27";
			this->label27->Size = System::Drawing::Size(71, 13);
			this->label27->TabIndex = 35;
			this->label27->Text = L"Zona Sismica";
			// 
			// label26
			// 
			this->label26->AutoSize = true;
			this->label26->Location = System::Drawing::Point(3, 423);
			this->label26->Name = L"label26";
			this->label26->Size = System::Drawing::Size(112, 13);
			this->label26->TabIndex = 34;
			this->label26->Text = L"Pantalla Electrostatica";
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(3, 397);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(94, 13);
			this->label21->TabIndex = 29;
			this->label21->Text = L"Tipos Enfriamiento";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(3, 366);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(147, 13);
			this->label20->TabIndex = 28;
			this->label20->Text = L"Sobreelavion de Temperatura";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(3, 337);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(72, 13);
			this->label19->TabIndex = 27;
			this->label19->Text = L"Altitud (msnm)";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(3, 304);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(60, 13);
			this->label18->TabIndex = 26;
			this->label18->Text = L"Frecuencia";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(3, 275);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(31, 13);
			this->label17->TabIndex = 25;
			this->label17->Text = L"Kvas";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(3, 244);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(35, 13);
			this->label16->TabIndex = 24;
			this->label16->Text = L"Fases";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(3, 220);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(38, 13);
			this->label11->TabIndex = 19;
			this->label11->Text = L"Norma";
			// 
			// textBox8
			// 
			this->textBox8->Location = System::Drawing::Point(163, 184);
			this->textBox8->Name = L"textBox8";
			this->textBox8->Size = System::Drawing::Size(128, 20);
			this->textBox8->TabIndex = 16;
			this->textBox8->Text = L"13.5";
			this->textBox8->TextChanged += gcnew System::EventHandler(this, &Form1::textBox8_TextChanged);
			// 
			// textBox7
			// 
			this->textBox7->Location = System::Drawing::Point(163, 153);
			this->textBox7->Name = L"textBox7";
			this->textBox7->Size = System::Drawing::Size(128, 20);
			this->textBox7->TabIndex = 15;
			this->textBox7->Text = L"0";
			this->textBox7->TextChanged += gcnew System::EventHandler(this, &Form1::textBox7_TextChanged);
			// 
			// textBox6
			// 
			this->textBox6->Location = System::Drawing::Point(163, 118);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(128, 20);
			this->textBox6->TabIndex = 14;
			this->textBox6->Text = L"0";
			this->textBox6->TextChanged += gcnew System::EventHandler(this, &Form1::textBox6_TextChanged);
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(166, 59);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(246, 20);
			this->textBox4->TabIndex = 12;
			this->textBox4->Text = L"1";
			this->textBox4->TextChanged += gcnew System::EventHandler(this, &Form1::textBox4_TextChanged);
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(166, 89);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(246, 20);
			this->textBox3->TabIndex = 11;
			this->textBox3->Text = L"jfa";
			this->textBox3->TextChanged += gcnew System::EventHandler(this, &Form1::textBox3_TextChanged);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(166, 30);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(246, 20);
			this->textBox2->TabIndex = 10;
			this->textBox2->Text = L"is13t";
			this->textBox2->TextChanged += gcnew System::EventHandler(this, &Form1::textBox2_TextChanged);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(166, 3);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(246, 20);
			this->textBox1->TabIndex = 9;
			this->textBox1->Text = L"Pro. Elect de Tijuana";
			this->textBox1->TextChanged += gcnew System::EventHandler(this, &Form1::textBox1_TextChanged);
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(3, 193);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(43, 13);
			this->label8->TabIndex = 7;
			this->label8->Text = L"Paridad";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(3, 158);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(150, 13);
			this->label7->TabIndex = 6;
			this->label7->Text = L"Cte. de Evaluacion Devanado";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(3, 127);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(134, 13);
			this->label6->TabIndex = 5;
			this->label6->Text = L"Cte. de Evaluacion Nucleo";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(6, 98);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(70, 13);
			this->label4->TabIndex = 3;
			this->label4->Text = L"Dise˝ado por";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(6, 68);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(49, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Cantidad";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(6, 37);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(91, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Numero de Orden";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(6, 12);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(39, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Cliente";
			// 
			// panel13
			// 
			this->panel13->Controls->Add(this->radioButton32);
			this->panel13->Controls->Add(this->radioButton31);
			this->panel13->Location = System::Drawing::Point(163, 414);
			this->panel13->Name = L"panel13";
			this->panel13->Size = System::Drawing::Size(147, 22);
			this->panel13->TabIndex = 49;
			this->panel13->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &Form1::panel13_Paint);
			// 
			// radioButton32
			// 
			this->radioButton32->AutoSize = true;
			this->radioButton32->Checked = true;
			this->radioButton32->Location = System::Drawing::Point(86, 2);
			this->radioButton32->Name = L"radioButton32";
			this->radioButton32->Size = System::Drawing::Size(41, 17);
			this->radioButton32->TabIndex = 1;
			this->radioButton32->TabStop = true;
			this->radioButton32->Text = L"NO";
			this->radioButton32->UseVisualStyleBackColor = true;
			this->radioButton32->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton32_CheckedChanged);
			// 
			// radioButton31
			// 
			this->radioButton31->AutoSize = true;
			this->radioButton31->Location = System::Drawing::Point(21, 2);
			this->radioButton31->Name = L"radioButton31";
			this->radioButton31->Size = System::Drawing::Size(35, 17);
			this->radioButton31->TabIndex = 0;
			this->radioButton31->Text = L"SI";
			this->radioButton31->UseVisualStyleBackColor = true;
			this->radioButton31->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton31_CheckedChanged);
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Controls->Add(this->tabPage3);
			this->tabControl1->Location = System::Drawing::Point(442, 27);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(498, 543);
			this->tabControl1->TabIndex = 0;
			// 
			// treeView1
			// 
			this->treeView1->Location = System::Drawing::Point(0, 295);
			this->treeView1->Name = L"treeView1";
			this->treeView1->Size = System::Drawing::Size(426, 275);
			this->treeView1->TabIndex = 0;
			// 
			// radioButton1
			// 
			this->radioButton1->AutoSize = true;
			this->radioButton1->Checked = true;
			this->radioButton1->Location = System::Drawing::Point(86, 2);
			this->radioButton1->Name = L"radioButton1";
			this->radioButton1->Size = System::Drawing::Size(41, 17);
			this->radioButton1->TabIndex = 1;
			this->radioButton1->TabStop = true;
			this->radioButton1->Text = L"NO";
			this->radioButton1->UseVisualStyleBackColor = true;
			// 
			// radioButton2
			// 
			this->radioButton2->AutoSize = true;
			this->radioButton2->Location = System::Drawing::Point(21, 2);
			this->radioButton2->Name = L"radioButton2";
			this->radioButton2->Size = System::Drawing::Size(35, 17);
			this->radioButton2->TabIndex = 0;
			this->radioButton2->Text = L"SI";
			this->radioButton2->UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(926, 567);
			this->Controls->Add(this->treeView1);
			this->Controls->Add(this->tabControl1);
			this->Controls->Add(this->menuStrip1);
			this->Name = L"Form1";
			this->Text = L"Tipo Transformador";
			this->SizeChanged += gcnew System::EventHandler(this, &Form1::Form1_SizeChanged);
			this->MaximizedBoundsChanged += gcnew System::EventHandler(this, &Form1::Form1_MaximizedBoundsChanged);
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->MaximumSizeChanged += gcnew System::EventHandler(this, &Form1::Form1_MaximumSizeChanged);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->tabPage3->ResumeLayout(false);
			this->tabPage3->PerformLayout();
			this->panel20->ResumeLayout(false);
			this->panel20->PerformLayout();
			this->panel18->ResumeLayout(false);
			this->panel18->PerformLayout();
			this->panel16->ResumeLayout(false);
			this->panel16->PerformLayout();
			this->tabPage2->ResumeLayout(false);
			this->tabPage2->PerformLayout();
			this->panel2->ResumeLayout(false);
			this->panel2->PerformLayout();
			this->tabPage1->ResumeLayout(false);
			this->tabPage1->PerformLayout();
			this->panel14->ResumeLayout(false);
			this->panel14->PerformLayout();
			this->panel8->ResumeLayout(false);
			this->panel8->PerformLayout();
			this->panel7->ResumeLayout(false);
			this->panel7->PerformLayout();
			this->panel6->ResumeLayout(false);
			this->panel6->PerformLayout();
			this->panel13->ResumeLayout(false);
			this->panel13->PerformLayout();
			this->tabControl1->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

private: System::Void specOnlyToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 //this->splitContainer1->Panel1Collapsed = true;
			 //this->splitContainer1 ->Panel2Collapsed =false;
		 }
private: System::Void specGraphicsTreeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 //this->splitContainer1->Panel2Collapsed =false;
			 //this->splitContainer1->Panel1Collapsed =false;
			 //this->splitContainer2 ->Panel1Collapsed =false;
			 //this->splitContainer2 ->Panel2Collapsed =false;
		 }
private: System::Void specGraphicsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 //this->splitContainer2 ->Panel2Collapsed = true;
			 //this->splitContainer2 ->Panel1Collapsed =false;
			 //this->splitContainer1 ->Panel1Collapsed =false;
			 //this->splitContainer1 ->Panel2Collapsed =false;
		 }
private: System::Void specTreeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 //this->splitContainer2 ->Panel2Collapsed =true;
			 //this->splitContainer1 ->Panel1Collapsed =false;
			 //this->splitContainer1 ->Panel2Collapsed =false;
			 //this->splitContainer2 ->Panel1Collapsed =true;
		 }
private: System::Void graphicsTreeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 //this->splitContainer1 ->Panel2Collapsed =true;
			 //this->splitContainer1 ->Panel1Collapsed =false;
			 //this->splitContainer2 ->Panel1Collapsed =false;
			 //this->splitContainer2 ->Panel2Collapsed =false;
		 }
private: System::Void graphicsOnlyToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 //this->splitContainer1 ->Panel2Collapsed =true;
			 //this->splitContainer2 ->Panel2Collapsed =true;
			 //this->splitContainer1 ->Panel1Collapsed =false;
			 //this->splitContainer2 ->Panel1Collapsed =false;
		 }
private: System::Void treeOnlyToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 //this->splitContainer1 ->Panel2Collapsed =true;
			 //this->splitContainer1 ->Panel1Collapsed =false;
			 //this->splitContainer2 ->Panel1Collapsed =true;
			 //this->splitContainer2 ->Panel2Collapsed =false;
		 }
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
			 WinHero->Render(true,this->Width/2,this->Height/2,"Hola",false);
			 WinHero->SwapOpenGLBuffers();
		 }
private: void CambiaTamanos()
		 {
			 WinHero->~COpenGL();
			 WinHero = gcnew COpenGL(this,this->Width/2,this->Height/2);
			 this->tabControl1->Width = this->Width/2;
			 this->tabControl1->Height= this->Height-26;
			 this->tabControl1->Location = System::Drawing::Point(this->Width/2, 26);
			 this->treeView1->Width = this->Width/2;
			 this->treeView1->Height=this->Height/2;
			 this->treeView1->Location = System::Drawing::Point(1, this->Height/2);
		 };
private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
			 CambiaTamanos();
		 }
private: System::Void Form1_MaximumSizeChanged(System::Object^  sender, System::EventArgs^  e) {
			 CambiaTamanos();
		 }
private: System::Void Form1_MaximizedBoundsChanged(System::Object^  sender, System::EventArgs^  e) {
			 CambiaTamanos();
		 }
private: System::Void Form1_SizeChanged(System::Object^  sender, System::EventArgs^  e) {
			  CambiaTamanos();
		 }

private: System::Void textBox1_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->cCliente=this->textBox1->Text;
		 }
private: System::Void textBox2_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->cNumOrden=this->textBox2->Text;
		 }
private: System::Void textBox4_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->iCantidad=this->textBox4->Text;
		 }
private: System::Void textBox3_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->cDise˝adoPor=this->textBox3->Text;
		 }
private: System::Void textBox6_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->iCteEvaluacionNucleo=this->textBox6->Text;
		 }
private: System::Void textBox7_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->iCteEvaluacionDevanado=this->textBox7->Text;
		 }
private: System::Void textBox8_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->dParidad=this->textBox8->Text;
		 }
private: System::Void comboBox8_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->cNorma=this->comboBox8->Text;
		 }
private: System::Void radioButton15_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(this->radioButton15->Checked)
				 Transformador->iFases=1;
		 }
private: System::Void radioButton16_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(this->radioButton16->Checked)
				 Transformador->iFases=3;
		 }
private: System::Void comboBox9_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->iKvas=this->comboBox9->Text;
		 }
private: System::Void comboBox10_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->iAltitud=this->comboBox10->Text;
		 }
private: System::Void radioButton17_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(this->radioButton17->Checked)
				Transformador->iFrecuencia=50;
		 }
private: System::Void radioButton18_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(this->radiobutto18->Checked)
				 Transformador->iFrecuencia=60;
		 }
private: System::Void panel13_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
			//
		 }
private: System::Void radioButton31_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(this->radioButton31->Checked)
				 Transformador->cPantallaElectrostatica="SI";
		 }
private: System::Void radioButton32_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(this->radioButton32->Checked)
				 Transformador->cPantallaElectrostatica="NO";
		 }
private: System::Void comboBox2_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->cTipoTransformador = this->comboBox2->Text;
		 }
private: System::Void comboBox3_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->cTipoGargantas=this->comboBox3->Text;
		 }
private: System::Void comboBox4_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->cInterfazBt=this->comboBox4->Text;
		 }
private: System::Void comboBox5_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->cInterfazAt=this->comboBox5->Text;
		 }
private: System::Void comboBox6_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->cFrente=this->comboBox6->Text;
		 }
private: System::Void comboBox7_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->cTiposRadiador=this->comboBox7->Text;
		 }
private: System::Void radioButton5_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(this->radioButton5->Checked)
				 Transformador->cBoquillaEnTapa="SI";
		 }
private: System::Void radioButton6_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(this->radioButton6->Checked)
				 Transformador->cBoquillaEnTapa="NO";
		 }
private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->cTipoBase=this->comboBox1->Text;
		 }
private: System::Void radioButton39_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(this->radioButton39->Checked)
				Transformador->cTCS="SI";
		 }
private: System::Void radioButton40_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(this->radioButton40->checked)
				 Transformador->cTCS="NO";
		 }
private: System::Void radioButton41_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(this->radioButton41->Checked)
				 Transformador->cTanqueConservador="SI";
		 }
private: System::Void radioButton42_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(this->radioButton42->Checked)
				 Transformador->cTanqueConservador="NO";
		 }
private: System::Void radioButton43_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(this->radioButton43->Checked)
				 Transformador->cApoyoGato="SI";
		 }
private: System::Void radioButton44_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(this->radioButton43->Checked)
				 Transformador->cApoyoGato="NO";
		 }
private: System::Void radioButton33_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(this->radioButton33->Checked)
				 Transformador->cZonaSismica="SI";
		 }
private: System::Void radioButton34_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(this->radioButton34->Checked)
				 Transformador->cZonaSismica="NO";
		 }
private: System::Void radioButton19_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void dise˝oBaseToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 Transformador->generaReporteElectrico();
		 }
};
}

