///////////////////////////////////////////////////////////////////////////////////
//! \file	    CDonaChica.h
//! \author     
//! \brief      Definicion de la clase CDonaChica
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
//////////////////////////////////////////////////////////////////////////////////

//Revisiones
//   Fecha      	     Descripción					Persona  	 Folio
//dd/mmm/aa  Descripción de revisión o modificación 	  XXX  		 XXXYYY



class CNucleo;

#if !defined(__DONACHICA_H_)
#define __DONACHICA_H_

///////////////////////////////////////////////////////////////////////////////////
//! \class		CDonaChica
//! \author     
//! \brief      Definicion de la clase CDonaChica
//! \date       
//////////////////////////////////////////////////////////////////////////////////

class CDonaChica{
	protected:

		
	private:
		//CDonaChica();


	public:	
		CDonaChica(CNucleo *_Nucleo);
		CNucleo *Nucleo;
		double dDNucleo;

public:
		double getDNucleo();



};

#endif //#if !defined(__DONACHICA_H_)