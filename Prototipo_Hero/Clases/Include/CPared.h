///////////////////////////////////////////////////////////////////////////////////
//! \file	    CPared.h
//! \author     
//! \brief      Definicion de la clase CPared
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
//////////////////////////////////////////////////////////////////////////////////

//Revisiones
//   Fecha      	     Descripción					Persona  	 Folio
//dd/mmm/aa  Descripción de revisión o modificación 	  XXX  		 XXXYYY


class CTanque;

#if !defined(__PARED_H_)
#define __PARED_H_

///////////////////////////////////////////////////////////////////////////////////
//! \class		CPared
//! \author     
//! \brief      Definicion de la clase CPared
//! \date       
//////////////////////////////////////////////////////////////////////////////////

class CPared{
	protected:
		
	private:
		int iAltoInterior;
		int iHeight;
		int iLength;
		int iAnchoInterior;
		double dPesoTotal;
		double dPesoTotalRad;
		//DECLARACION DE ATRIBUTOS
	public:
		CPared(CTanque *_Tanque);
		CTanque *Tanque;

		int getAltoInterior();
		int getHeight();
		int getLength();
		int getAnchoInterior();
		double getPesoTotal();
		double getPesoTotalRad();

		//TABLAS
		//FUNCIONES EXCLUSIVAS DE LA CLASE
		//FUNCIONES OBTENER
		//FUNCIONES ASIGNAR
		//FUNCIONES CALCULAR

};

#endif //#if !defined(__Pared_H_)