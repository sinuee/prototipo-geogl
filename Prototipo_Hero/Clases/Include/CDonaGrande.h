///////////////////////////////////////////////////////////////////////////////////
//! \file	    CDonaGrande.h
//! \author     
//! \brief      Definicion de la clase CDonaGrande
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
//////////////////////////////////////////////////////////////////////////////////

//Revisiones
//   Fecha      	     Descripción					Persona  	 Folio
//dd/mmm/aa  Descripción de revisión o modificación 	  XXX  		 XXXYYY




class CNucleo;

#if !defined(__DONAGRANDE_H_)
#define __DONAGRANDE_H_

///////////////////////////////////////////////////////////////////////////////////
//! \class		CDonaGrande
//! \author     
//! \brief      Definicion de la clase CDonaGrande
//! \date       
//////////////////////////////////////////////////////////////////////////////////

class CDonaGrande{
	protected:

		
	private:
		//CDonaGrande();
		double dDonasGDNucleo;
		CNucleo *Nucleo;

	public:	
		CDonaGrande(CNucleo *_Nucleo);
		double getDNucleo();




};

#endif //#if !defined(__DONAGRANDE_H_)