///////////////////////////////////////////////////////////////////////////////////
//! \file	    CTapa.h
//! \author     
//! \brief      Definicion de la clase CTapa
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
//////////////////////////////////////////////////////////////////////////////////

//Revisiones
//   Fecha      	     Descripción					Persona  	 Folio
//dd/mmm/aa  Descripción de revisión o modificación 	  XXX  		 XXXYYY


class CTanque;

#if !defined(__TAPA_H_)
#define __TAPA_H_

///////////////////////////////////////////////////////////////////////////////////
//! \class		CTapa
//! \author     
//! \brief      Definicion de la clase CTapa
//! \date       
//////////////////////////////////////////////////////////////////////////////////

class CTapa{
	protected:
		
	private:
		int iAnchoInterior;
		int iAnchoTapa;
		int iWidth;
		double dEspesorTapa;
		double dPesoTotal;
		int iLargoInterior;
		char *cMaterial;

		//DECLARACION DE ATRIBUTOS
	public:	
		CTapa(CTanque *_Tanque);
		CTanque *Tanque;

		int getAnchoInterior();
		int getAnchoTapa();
		int getWidth();
		double getEspesorTapa();
		double getPesoTotal();
		int getLargoInterior();
		//TABLAS
		//FUNCIONES EXCLUSIVAS DE LA CLASE
		//FUNCIONES OBTENER
		//FUNCIONES ASIGNAR
		//FUNCIONES CALCULAR

};

#endif //#if !defined(__TAPA_H_)