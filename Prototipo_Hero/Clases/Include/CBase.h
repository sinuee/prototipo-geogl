///////////////////////////////////////////////////////////////////////////////////
//! \file	    CBase.h
//! \author     
//! \brief      Definicion de la clase CBase
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
//////////////////////////////////////////////////////////////////////////////////

//Revisiones
//   Fecha      	     Descripción					Persona  	 Folio
//dd/mmm/aa  Descripción de revisión o modificación 	  XXX  		 XXXYYY

class CTanque;

#if !defined(__BASE_H_)
#define __BASE_H_

///////////////////////////////////////////////////////////////////////////////////
//! \class		CBase
//! \author     
//! \brief      Definicion de la clase CBase
//! \date       
//////////////////////////////////////////////////////////////////////////////////

class CBase{
	protected:
		
	private:
		double dAlturaBase;
		double dWidth;
		int iAlturaUsa;
		double dHeight;
		double dEspesorPared;
		double dPesoTotal;
		//DECLARACION DE ATRIBUTOS

	public:	
		char *cPlacaBase;
		int iKvas;
		double getEspesorBase();
		CBase(CTanque *_Tanque);
		CTanque *Tanque;
		double getAlturaBase();
		double getAlturaUsa();
		double getWidth();
		double getHeight();
		double getEspesorPared();
		double getPesoTotal();

		//TABLAS
		//FUNCIONES EXCLUSIVAS DE LA CLASE
		//FUNCIONES OBTENER
		//FUNCIONES ASIGNAR
		//FUNCIONES CALCULAR

};

#endif //#if !defined(__BASE_H_)