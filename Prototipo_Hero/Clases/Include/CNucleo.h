///////////////////////////////////////////////////////////////////////////////////
//! \file	    CNucleo.h
//! \author     
//! \brief      Definicion de la clase CNucleo
//! \date       
//
//			Informacion privilegiada de Prolec-GE. 
//          La informacion contenida en este documento es informacion 
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a 
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential 
//			and Prolec-GE property. Must not be used, reproduced or 
//			revealed to others without written consent of Prolec-GE.
//////////////////////////////////////////////////////////////////////////////////

//Revisiones
//   Fecha      	     Descripción					Persona  	 Folio
//dd/mmm/aa  Descripción de revisión o modificación 	  XXX  		 XXXYYY



class CParteViva;
class CDonaGrande;
class CDonaChica;

#if !defined(__NUCLEO_H_)
#define __NUCLEO_H_

///////////////////////////////////////////////////////////////////////////////////
//! \class		CNucleo
//! \author     
//! \brief      Definicion de la clase CNucleo
//! \date       
//////////////////////////////////////////////////////////////////////////////////

class CNucleo{
	protected:
		
	private:
		CParteViva *ParteViva;
		CDonaGrande *DonaGrande;
		CDonaChica *DonaChica;

		int iFrecuencia;
		double dFactorApilamiento;
		int iEspesorNucleo;
		int iAnchoNucleo;
		int iB2;
		bool bIsight;
		bool bSpst;
		bool bAislPvSta1705;
	    int iAnchoLamina1;
		char *cDobleNucleo;
		int iToleranciaMitter;
		int iDistanciaEntreFases;
		int iAnchoLamina2;
		double dVoltXVuelta;
		double dAlturaVentana;
		double dDonaGDNucleo;

	public:	
		//CNucleo(bool bI,bool bS);
		CNucleo(CParteViva *_ParteViva);
		char *cTipoNucleo;

		double getGauss();
		double getAreaNucleo();
		double getGaussTotal();
		double getAreaTotalNucleo();
		double getA1();
		double getA2();
		double getEspesorEscalon();
		double getANucleo();
		double getBNucleo();
		double getB1();
		double getRelacionNucleo();

		double getB11();//checar nombre B1
		double getC1();
		int getD2Nucleo();
		int getB1Nucleo();
		int getCNucleo();
		double getF1Nucleo(double dDonasCDNucleo);
		double getD1Nucleo(double dDonasCDNucleo);
		double getToleranciaNucleo();
		double getENucleo();
		double getF2Nucleo();
		
		int getB2Nucleo();
		double getC2();
		double getF2();
		double getD2();
		double getD1();
		double getL1();
		double getL2();
		double getH2();
		double getH1();


		int getAnchoLamina1();





};

#endif //#if !defined(__NUCLEO_H_)