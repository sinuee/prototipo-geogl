/*
 * GAsymmetricRoundedHollowBox.h
 *
 *  Created on: Dec 17, 2008
 *      Author: khayyam
 */

#ifndef GASYMMETRICROUNDEDHOLLOWBOX_H_
#define GASYMMETRICROUNDEDHOLLOWBOX_H_
#include "GAsymmetricHollowBox.h"
namespace gapi{
	class GAsymmetricRoundedHollowBox: public GAsymmetricHollowBox{
		protected:
			int slices;
		public:
			GAsymmetricRoundedHollowBox();
			GAsymmetricRoundedHollowBox(GBox &parent);
			virtual ~GAsymmetricRoundedHollowBox();
			virtual void doDraw(void);
	};
}
#endif /* GASYMMETRICROUNDEDHOLLOWBOX_H_ */
