/*
 * GPrimitives.h
 *
 *  Created on: Dec 15, 2008
 *      Author: khayyam
 */

#ifndef GPRIMITIVES_H_
#define GPRIMITIVES_H_
#include "gapila.h"
#ifdef USE_GAPI_GRAPHICS
#include "GL/glut.h"
namespace gapi{
	void drawPartialHollowCylinder(double innerRadius, double outerRadius, double height,
						  double startAngle, double sweepAngle, int slices, GLenum displayType);

	void drawPartialHollowCylinder(double innerRadius, double outerRadius, double height,
							   double startAngle, double sweepAngle, double px, double py, double pz, int slices, GLenum displayType);


	void drawBox(double width, double length, double height, GLenum displayType);
	void drawBox(double width, double length, double height, double px, double py, double pz, GLenum displayType);
}
#endif
#endif /* GPRIMITIVES_H_ */
