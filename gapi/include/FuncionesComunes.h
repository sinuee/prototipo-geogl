///////////////////////////////////////////////////////////////////////////////////
//! \file		FuncionesComunes.h
//! \author		Judith Janett Valdez Medrano
//! \date		10/Abr/2009
//! \brief		Declaracion de las funciones de uso general utilizadas en el proyecto
//
//				Informacion privilegiada de Prolec-GE.
//				La informacion contenida en este documento es informacion
//				privilegiada de  Prolec-GE y es revelada de manera 
//				confidencial. No debera ser utilizada, reproducida o 
//				revelada a otros sin el consentimiento escrito de Prolec-GE.
//
//				Prolec-GE Privileged information.
//				The information reveled in this document is confidential and
//				Prolec-GE property. Must not be used, reproduced or revealed 
//				to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////

//Revisiones

//Fecha			Descripcion										Persona		Folio
//dd/mmm/aa		Descipcion de revision o modificacion			XXX			XXXYYY
#include <string>
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <sstream>
#include <iomanip>
//#include <windows.h>

//using namespace std;
#pragma warning(disable:4996) //evitar warnings por templates de stl

#ifndef FUNCIONESCOMUNES_H
#define FUNCIONESCOMUNES_H
/******************   BD  ***********************************************************/
#import "C:\Program Files\Common Files\System\ADO\msado15.dll" rename_namespace("ADOCG") rename("EOF", "EndOfFile")

using namespace ADOCG;
/******************   BD  ***********************************************************/
#define EPSILON (1e-6)
//REAL_EQUALS: regresa true si |a-b|<EPSILON, false de otro modo
#define REAL_EQUALS(a,b) ((((b)>(a))?((b)-(a)):(((a)-(b))))<EPSILON) //CJFZ 
//LESS_COMP: regresa true si a+EPSILON<b, false de otro modo
#define REAL_LESS(a,b) (((a)+EPSILON)<(b))
//GREATER_COMP: regresa true si a>b+EPSILON, false de otro modo
#define REAL_GREATER(a,b) ((a)>(b)+EPSILON)




//////////////////////////////////////////////////////////////////////////////////////////////
//! \author Omar Ocegueda
//! \brief Template para convertir una variable (de tipo estandar) a cadena
//! \date Sep-02-2009
//////////////////////////////////////////////////////////////////////////////////////////////
template<typename T>std::string convertToString(T x, int precision=-1){
	std::ostringstream os;
	if(0<=precision){
		os<<std::fixed<<std::setprecision(precision);
	}else{
		os<<std::scientific;
	}
	os<<x;
	return os.str();
}
///////////////////////////////////////////////////////////////////////////////////
//! \fn			ROUND
//! \author			Diana Carolina Osuna Arceo
//! \brief		Funcion que redondea al entero mas proximo y multiplo del para-
//					metro div es el que corresponde a la funcion redondea-entero-sup 
//					de ICAD
//! \date			20/Mar/07
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)int ROUND (double valor, int div);

///////////////////////////////////////////////////////////////////////////////////
//! \fn			ROUND
//! \author			Diana Carolina Osuna Arceo
//! \brief		Sobrecarga de la funcion que redondea al entero mas proximo 
//! \date			02/May/07
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)int ROUND (double valor);

///////////////////////////////////////////////////////////////////////////////////
//! \fn			ROUND
//! \author			Alejandra Guzman Osteguin
//! \brief		Redondea al entero inferior 
//! \date			10-Diciembre-08
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)double REDONDEAINF(double val, double redondeo);

///////////////////////////////////////////////////////////////////////////////////
//! \fn			REDONDEASUP1
//! \author			Diana Carolina Osuna Arceo
//! \brief		Funcion que redondea al superior inmediato
//! \date			17/05/07
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport) double REDONDEASUP1(double val, double redondeo);

///////////////////////////////////////////////////////////////////////////////////
//! \fn			reemplazar
//! \author     Judith Janett Valdez Medrano
//! \brief      Funcion que reemplaza un caracter por otro en un arreglo de caracteres
//! \date       17/Abr/09
//!    @param 	sDescripcion		Arreglo de caracteres
//!    @param 	iTamanio			Tamanio del arreglo
//!    @param 	cCambiar			Caracter a reemplazar
//!    @param 	cCambiarPor			Caracter de reemplazo
//!	@return     	Ningun valor
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)void reemplazar (char *sDescripcion, size_t iTamanio, char *cCambiar, char *cCambiarPor);

///////////////////////////////////////////////////////////////////////////////////
//! \fn			MergeList
//! \author     Esteban Sinuee Hernandez Sanchez
//! \brief      Mezcla 2 arreglos devolviendo el resultado en forma de arreglo
//! \date       23/Abr/09
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)void MergeList (double *Lista1, double *Lista2, int I, int J, char forma[1], double dMergeList[]);
///////////////////////////////////////////////////////////////////////////////////
//! \fn			MaxMin
//! \author     Esteban Sinuee Hernandez Sanchez
//! \brief      Crea un arreglo llenandolo con valores incrementales desde el minimo hasta el maximo dado
//! \date       23/Abr/09
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)int* MaxMin (int iMin, int iMax, int iIncremento);
__declspec(dllexport)void MaxMin (int iMin, int iMax, double iIncremento,std::vector <int> Arregloi);
__declspec(dllexport)void MaxMin (int iMin, int iMax, int iIncremento
			 , int *a);

__declspec(dllexport)void fixNum (double *pdValor, int Precision);

///////////////////////////////////////////////////////////////////////////////////
//! \fn			member
//! \author			Diana Carolina Osuna Arceo
//! \brief		Funcion que busca un elemento en un arreglo y devuelve 1 si lo 
//					encuentra, 0 caso contrario
//! \date			28/03/07
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)bool member (int iElemento, int iNumElementos, int* piValores);

///////////////////////////////////////////////////////////////////////////////////
//! \fn			dPintNueva
//! \author     Rocio Yazmin Hernandez Ibarra
//! \brief      Calcula la cantidad de Pintura.
//! \date       086/Jun/09
//! @param 		double dValor Valor 
//! @param 		double dDecim Decimales
////////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)double RedondeaDecimRgcg(double dValor, double dDecim);

 ///////////////////////////////////////////////////////////////////////////////////
//! \fn		nearTo
//! \author	Esteban Sinuee Hern�ndez S�nchez
//! \brief	funcion que dice si un valor esta cerca de otro en base a una tolerancia
//! \date	22/07/2009
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)bool nearTo (double Val1, double Val2, double Tolerance);
///////////////////////////////////////////////////////////////////////////////////
//! \fn		Instr
//! \author	Emilio Santos
//! \brief	encuentra una subcadena dentro de otra cadena
//! \date	15/Jul/2009
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)int Instr(const char *SearchString, const char *SearchTerm);
////////////////////////////////////////////////////////////////////////////////////////////////////////
//! \author Omar Ocegueda
//! \brief  "Corta" la cadena de entrada \ref cadena en fragmentos definidos por los \ref separadores
//! dados. Si \ref descartarVacias es true, entonces  el vector de salida no contendra cadenas vacias. Por default
//! se permiten las cadenas vacias.
//! 
//! \date Aug-31-2009
//!
////////////////////////////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport) std::vector<std::string> cortarCadena(std::string cadena, const std::string separadores, bool descartarVacias=false);
////////////////////////////////////////////////////////////////////////////////////////////////////////
//! \author Esteban Sinuee Hernandez Sanchez
//! \brief  Convierte un string a minusculas
//! 
//! \date Aug-31-2009
//!
////////////////////////////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)std::string  LowerCase (char *str);

__declspec(dllexport)std::string itos(int iEntrada);

__declspec(dllexport)std::string ltos(long lEntrada);

__declspec(dllexport)std::string dtos(double dEntrada, int iDecimales);


//JOOG: regresa la copia del arreglo (usando el operador de asignacion de T, asegurarse de que esto es lo que se quiere hacer)
template<typename T> T* arrayCopy(T *original, unsigned size){
	T *copy=new T[size];
	for(unsigned i=0;i<size;++i){
		copy[i]=original[i];
	}
	return copy;
}
__declspec(dllexport)std::string funcRTrim(std::string sVar);

/////////////////////////////////////////////////////
//! \author Omar Ocegueda
//! \brief regresa el nombre del archivo, dada su ruta completa
//! 
//! \date 23-Julio-2009
/////////////////////////////////////////////////////
__declspec(dllexport) std::string extraerNombreArchivo(std::string fullPath);


#define ESPACIOS " \t\r\n"

//////////////////////////////////////////////////////////////////////////////////
//! \brief sinCaracteresDerecha
//! \brief 
//! \author Omar Ocegueda
//! \date 01-Junio-07
//! @param 
//! @return 
////////////////////////////////////////////////////////////////////////////////// 
__declspec(dllexport)inline std::string sinCaracteresDerecha(const std::string &s, const std::string &indeseables=ESPACIOS);

//////////////////////////////////////////////////////////////////////////////////
//! \brief sinCaracteresIzquierda
//! \brief 
//! \author Omar Ocegueda
//! \date 01-Junio-07
//! @param 
//! @return 
////////////////////////////////////////////////////////////////////////////////// 
__declspec(dllexport)inline std::string sinCaracteresIzquierda(const std::string &s, const std::string &indeseables=ESPACIOS);

//////////////////////////////////////////////////////////////////////////////////
//! \brief sinCaracteresExtremos
//! \brief 
//! \author Omar Ocegueda
//! \date 01-Junio-07
//! @param 
//! @return 
////////////////////////////////////////////////////////////////////////////////// 
__declspec(dllexport)inline std::string sinCaracteresExtremos(const std::string &s, const std::string &indeseables=ESPACIOS);

///////////////////////////////////////////////////////////////////////////////////
//! \brief itos
//! \brief Convierte un entero en string
//! \author	Jos� Fernando Reyes Salda�a
//! \date		01/Jun/06
//! @param int iEntrada Entero a convertir a string
//! @return 
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)std::string itos(int iEntrada);

///////////////////////////////////////////////////////////////////////////////////
//! \fn		ltos
//! \author		Jos� Fernando Reyes Salda�a
//! \brief	Convierte un entero long en string
//! \date		01/Jun/06
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)std::string ltos(long lEntrada);

///////////////////////////////////////////////////////////////////////////////////
//! \brief FechaYHoraToString
//! \brief Convierte un string formato ddmmyyyyhhmiss en long
//! \author	Diana Carolina Osuna Arceo
//! \date	28/Jul/09
//! @param string sFecha a convertir a long
//! @return long lFecha
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)long FechaYHoraToLong(std::string sFecha);

///////////////////////////////////////////////////////////////////////////////////
//! \brief FechaYHoraToString
//! \brief Convierte un long a string formato ddmmyyyyhhmiss para queries
//! \author	Diana Carolina Osuna Arceo
//! \date	28/Jul/09
//! @param long lFecha a convertir
//! @return string formato ddmmyyyyhhmiss
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)std::string FechaYHoraToString(long lFecha);

///////////////////////////////////////////////////////////////////////////////////
//! \brief formatearFechaYHora
//! \brief Convierte un long a string formato ddmmyyyyhhmiss
//! \author	Diana Carolina Osuna Arceo
//! \date	28/Jul/09
//! @param long lFecha a convertir
//! @return string formato ddmmyyyyhhmiss
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)std::string formatearFechaYHora(long lFecha);

///////////////////////////////////////////////////////////////////////////////////
//! \brief inicializarHoraInicio
//! \brief Asigna un objeto time_t con el primer segundo del dia
//! \author	Diana Carolina Osuna Arceo
//! \date	2/Sep/09
//! @param time_t &tFecha objeto time por referencia
//! @return void
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)void inicializarHoraInicio(time_t &tFecha);

///////////////////////////////////////////////////////////////////////////////////
//! \brief inicializarHoraFin
//! \brief Asigna un objeto time_t con el ultimo segundo del dia
//! \author	Diana Carolina Osuna Arceo
//! \date	2/Sep/09
//! @param time_t &tFecha objeto time por referencia
//! @return void
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)void inicializarHoraFin(time_t &tFecha);

///////////////////////////////////////////////////////////////////////////////////
//Funci�n:            FormateaFecha
//Autor:              Jose Fernando Reyes Salda�a
//Descripci�n:        Construye una fecha en formato aaaammdd, a partir de un long
//Fecha:              01 de Junio de 2007
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)std::string formatearFecha(long lFecha);

///////////////////////////////////////////////////////////////////////////////////
//Funci�n:            formatearHora
//Autor:              Diana Carolina Osuna Arceo
//Descripci�n:        Construye una hora en formato hh:mm:ss, a partir de un long
//Fecha:              2/Sep/09
///////////////////////////////////////////////////////////////////////////////////
__declspec(dllexport)std::string formatearHora(long lFecha);

/////////////////////////////////////////////////////
//! \author Omar Ocegueda
//! \brief "Convierte una cadena de texto a un tipo de dato especifico
//! 
//! \date 23-Julio-2009
/////////////////////////////////////////////////////
template <typename T> T convertirCadena(const std::string &s){
	std::istringstream is(s);
	T valor;
	is>>valor;
	return valor;
};
__declspec(dllexport)void construirDirectorioAplicacion(std::string& CFileName);

#define REAL_EQUALS(a,b) ((((b)>(a))?((b)-(a)):(((a)-(b))))<EPSILON) //CJFZ 
#define REAL_LESS(a,b) (((a)+EPSILON)<(b))
#define REAL_GREATER(a,b) ((a)>(b)+EPSILON)

__declspec(dllexport) std::vector<std::string> obtenerListaArchivos(std::string patron, __time64_t from=-1, __time64_t to=-1);

__declspec(dllexport)std::string cambiarExtension(std::string s, std::string nuevaExtension);

#endif //FUNCIONESCOMUNES_H


