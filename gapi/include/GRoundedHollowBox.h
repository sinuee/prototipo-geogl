/*
 * GRoundedHollowBox.h
 *
 *  Created on: Dec 15, 2008
 *      Author: khayyam
 */

#ifndef GROUNDEDHOLLOWBOX_H_
#define GROUNDEDHOLLOWBOX_H_
#include "GHollowBox.h"
namespace gapi{
	class GRoundedHollowBox:public GHollowBox{
		protected:
			int slices;
		public:
			GRoundedHollowBox();
			GRoundedHollowBox(GBox &parent);
			virtual ~GRoundedHollowBox();
			virtual void doDraw(void);
	};
}

#endif /* GROUNDEDHOLLOWBOX_H_ */
