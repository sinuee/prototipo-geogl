/*
 * HollowCylinder.h
 *
 *  Created on: Dec 11, 2008
 *      Author: khayyam
 */

#ifndef GHOLLOWCYLINDER_H_
#define GHOLLOWCYLINDER_H_
#include "GCylinder.h"
namespace gapi{
	class GHollowCylinder:public GCylinder{
		protected:
			double innerRadius;
			//char *name;
		public:
			GHollowCylinder();
			GHollowCylinder(GBox &parent);
			//void Export();
			virtual void doExport(void);
			char *getName();
			virtual ~GHollowCylinder();
			virtual void doDraw(void);
			virtual void setInnerRadius(double innerRadius);
			virtual void setRadius(double radius);
			virtual void setName(char *data);
			double getIneerRadius(void);
	};
}

#endif /* HOLLOWCYLINDER_H_ */
