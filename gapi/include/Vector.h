///////////////////////////////////////////////////////////////////////////////
//! \class 					Vector.h
//! \brief					Definicion de la clase Vector
//! \author					J. Omar Ocegueda Glez.
//! \author					CTA, Prolec-GE
//! \version				1.0
//! \date					3-Diciembre-2008
//			Informacion privilegiada de Prolec-GE.
//                 	La informacion contenida en este documento es informacion
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential
//			and Prolec-GE property. Must not be used, reproduced or
//			revealed to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////

#ifndef VECTOR_H_
#define VECTOR_H_
#include "NTuple.h"
namespace gapi{
	typedef NTuple<double, 4> Vector;
}

#endif /* VECTOR_H_ */
