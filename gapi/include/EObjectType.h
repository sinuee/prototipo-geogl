///////////////////////////////////////////////////////////////////////////////
//! \class 					EObjectType.h
//! \brief					Enumeracion de los tipos de objetos de la api de modelacion
//! \author					J. Omar Ocegueda Glez.
//! \author					CTA, Prolec-GE
//! \version				1.0
//! \date					3-Diciembre-2008
//			Informacion privilegiada de Prolec-GE.
//                 	La informacion contenida en este documento es informacion
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential
//			and Prolec-GE property. Must not be used, reproduced or
//			revealed to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////
#ifndef EOBJECTTYPE_H_
#define EOBJECTTYPE_H_
namespace gapi{
	enum EObjectType{default_EObjectType=-1,
				 OT_GBase=0,
				 OT_GScalar,
				 OT_GConstant,
				 OT_GMutableBase,
				 OT_GCompoundBase,
				 OT_GCompoundScalar,
				 OT_GFunction,
				 OT_GObject,
				 OT_GCompoundObject,
				 OT_GPoint,
				 OT_GArc,
				 OT_GPolyLine,
				 OT_GSegment,
				 OT_GContainer,
				 OT_GPlanarObject,
				 OT_GCircle,
				 OT_GCircumference,
				 OT_GPlane,
				 OT_GFace,
				 OT_GCone,
				 OT_GCilinder,
				 OT_GBox};
}
#endif /* EOBJECTTYPE_H_ */
