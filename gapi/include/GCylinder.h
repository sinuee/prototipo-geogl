/*
 * GCylinder.h
 *
 *  Created on: Dec 10, 2008
 *      Author: khayyam
 */

#ifndef GCYLINDER_H_
#define GCYLINDER_H_
#include "GBox.h"
namespace gapi{
	class GCylinder:public GBox{
		protected:
			int slices, stacks;
			double radius;
			//char *name;
		public:
			GCylinder();
			GCylinder(GBox &parent);
			//void Export(void);
			virtual void doExport(void);
			virtual ~GCylinder();
			virtual void doDraw();
			double getRadius(void);
			double getHeight(void);
			char* getName();
			virtual void setRadius(double r);
			virtual void setLength(double length);
			virtual void setWidth(double width);
			virtual void setName(char* data);
	};
}
#endif /* GCYLINDER_H_ */
