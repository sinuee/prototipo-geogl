#ifndef GMACROS_H_
#define GMACROS_H_
//#define USE_GAPI_NAMES
#define USE_GAPI_GRAPHICS
#if defined(USE_GAPI_NAMES)
	#include <map>
	#include "GBox.h"
	extern std::map<gapi::GBox*, std::string> GAPI_NAMES;
#endif

#ifdef USE_GAPI_GRAPHICS
#include <GL/glut.h>
#endif

namespace gapi{
	#if defined(USE_GAPI_NAMES)
		#define createPart(part, type) {part=new type(*this);GAPI_NAMES[part]=#part;}
		#define createChild(part, type, parent) {part=new type(parent);GAPI_NAMES[part]=#part;}
		#define createRoot(part, type) {part=new type();GAPI_NAMES[part]=#part;}
	#else
		#define createPart(part, type) {part=new type(*this);}
		#define createChild(part, type, parent) {part=new type(parent);}
		#define createRoot(part, type) {part=new type();}
	#endif

	#define ABS(x) ((x)<0?(-(x)):(x))
	#define SQR(x) ((x)*(x))
	#define MAX(a,b) ((a)<(b)?(b):(a))
	#define MIN(a,b) ((a)<(b)?(a):(b))
//	#define EPSILON 1e-9
	#define DEGREES(x) (M_PI*(x)/180.0)
	#ifndef M_PI
		#define M_PI 3.1415926535897932384626433832795
	#endif
	#define inch(x) ((x)*.0254)
	#define feet(x) ((x)*.3048)
	#define metersToFeet(x)   ((x)/.3048)
	#define metersToInches(x) ((x)/.0254)
}
#endif

