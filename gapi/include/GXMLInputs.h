#ifndef GXMLINPUTS_H_
#define GXMLINPUTS_H_
#include "GInputs.h"
namespace gapi{
	class GXMLInputs:public GInputs{
		protected:
			void printNode(const char *nodeName, std::fstream &outputFile, int cummulativeIndent=0, int indent=4);
		public:
			static const unsigned int MAX_BUFFER_SIZE=1024;
			GXMLInputs();
			GXMLInputs(const char* inputFileName);
			virtual ~GXMLInputs();
			GInputsResult loadFromFile(const char* inputFileName);
			GInputsResult saveToFile(const char* outputFileName);
			virtual GInputs* createNewInstance(void);
	};
}

#endif
