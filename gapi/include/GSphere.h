/*
 * GSphere.h
 *
 *  Created on: Dec 11, 2008
 *      Author: khayyam
 */

#ifndef GSPHERE_H_
#define GSPHERE_H_
#include "GBox.h"
namespace gapi{
	class GSphere:public GBox{
		protected:
			double radius;
			int slices, stacks;
			//char *name;
		public:
			GSphere();
			//void Export(void);
			virtual void doExport(void);
			GSphere(GBox &parent);
			char* getName();				
			virtual ~GSphere();
			virtual void setRadius(double radius);
			virtual void doDraw(void);
			virtual void setName(char *data);
	};
}
#endif /* GSPHERE_H_ */
