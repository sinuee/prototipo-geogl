///////////////////////////////////////////////////////////////////////////////
//! \class 					NTupla.h
//! \brief					Template para la representacion de una N-tupla
//! \author					J. Omar Ocegueda Glez.
//! \author					CTA, Prolec-GE
//! \version				1.0
//! \date					3-Diciembre-2008
//			Informacion privilegiada de Prolec-GE.
//                 	La informacion contenida en este documento es informacion
//			privilegiada de Prolec-GE y es revelada de manera confide-
//			ncial. No debera ser utilizada, reproducida o revelada a
//			otros sin el consentimiento escrito de Prolec-GE.
//
//			Prolec-GE Privileged information.
//			The information reveled in this document is confidential
//			and Prolec-GE property. Must not be used, reproduced or
//			revealed to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////

#ifndef NTUPLE_H_
#define NTUPLE_H_
#include <fstream>
#include <iostream>
#include <math.h>
namespace gapi{
	template<typename T, int N, int M> class Matrix;
	template <typename T, int N> class NTuple{
		protected:
			T val[N];
		public:
			NTuple<T,N>(){
			}

			NTuple<T,N>(const NTuple<T, N> &v){
				for(int i=0;i<N;i++)
					val[i]=v.val[i];
			}

			NTuple<T,N>(T v[N]){
				for(int i=0;i<N;i++)
					val[i]=v[i];
			}

			T* getData(void){
				return val;
			}

			virtual ~NTuple<T,N>(){
			}

		    T& operator[](const int i){
				return val[i];
			}

		    const T& operator[](const int i)const{
				return val[i];
			}


		    NTuple<T,N> operator+(const NTuple<T, N> &x)const{
				NTuple<T,N> sum;
				for(int i=0;i<N;i++)
					sum[i]=val[i]+x.val[i];
				return sum;
			}

		    NTuple<T,N> operator-(const NTuple<T, N> &x)const{
		    	NTuple<T,N> sum;
		    	for(int i=0;i<N;i++)
		    		sum[i]=val[i]-x.val[i];
		    	return sum;
		    }



		    T operator*(const NTuple<T, N> &x)const{
				T retVal=0;
				for(int i=0;i<N;i++)
					retVal+=val[i]*x.val[i];
				return retVal;
			}

		    NTuple<T,N> operator+(const T x[N])const{
		    	NTuple<T,N> sum;
		    	for(int i=0;i<N;i++)
		    		sum[i]=val[i]+x[i];
		    	return sum;
		    }

		    NTuple<T,N> operator-(const T x[N])const{
		    	NTuple<T,N> diff;
		    	for(int i=0;i<N;i++)
		    		diff[i]=val[i]-x[i];
		    	return diff;
		    }

		    T operator*(T const (&x)[N])const{
		    	T retVal=0;
		    	for(int i=0;i<N;i++)
		    		retVal+=val[i]*x[i];
		    	return retVal;
		    }

			NTuple<T,N>& operator=(const NTuple<T, N>& v){
				for(int i=0;i<N;i++)
					val[i]=v.val[i];
				return *this;
			}

			NTuple<T,N>& operator=(T (&v)[N]){
				for(int i=0;i<N;i++)
					val[i]=v[i];
			    return *this;
			}

			NTuple<T,N> operator^(const NTuple<T,N> &v)const{
				NTuple<T,N> crossProd;
				if(N<3)
				{
					crossProd=0;
					return crossProd;
				}
				crossProd.val[0]=val[1]*v.val[2]-val[2]*v.val[1];
				crossProd.val[1]=-(val[0]*v.val[2]-val[2]*v.val[0]);
				crossProd.val[2]=val[0]*v.val[1]-val[1]*v.val[0];
				if(3<N)
					crossProd.val[3]=val[3]*v.val[3];
				else
					crossProd.val[3]=1;
				return crossProd;
			}

			NTuple<T,N> operator^(const T (&v)[N])const{
				NTuple<T,N> crossProd;
				if(N<3)
					{
						crossProd.zero();
						return crossProd;
					}
				crossProd.val[0]=val[1]*v[2]-val[2]*v[1];
				crossProd.val[1]=-(val[0]*v[2]-val[2]*v[0]);
				crossProd.val[2]=val[0]*v[1]-val[1]*v[0];
				if(3<N)
					crossProd.val[3]=val[3]*v[3];
				else
					crossProd.val[3]=1;
				return crossProd;
			}

			bool operator==(const NTuple<T, N>& v)const{
				for(int i=0;i<N;i++)
					if(!(val[i]==v.val[i]))
						return false;
				return true;
			}

			bool operator <(const NTuple<T, N>&v)const{
				for(int i=0;i<N;i++)
					if(val[i]<v.val[i])
						return true;
					else if(v.val[i]<val[i])
						return false;
				return false;
			}

			bool operator<(const T &x)const{
				for(int i=0;i<N;i++)
					if(!(val[i]<x))
						return false;
				return true;
			}

			bool operator<(const T v[N])const{
				for(int i=0;i<N;i++)
					if(val[i]<v.val[i])
						return true;
					else if(v[i]<val[i])
						return false;
				return false;
			}

			bool operator !=(const NTuple<T, N> &v)const{
				for(int i=0;i<N;i++)
					if(!(val[i]==v.val[i]))
						return true;
				return false;
			}

			NTuple<T,N> operator+(const T x)const{
				NTuple<T,N> sum;
				for(int i=0;i<N;i++)
					sum[i]=val[i]+x;
				return sum;
			}

			NTuple<T,N> operator-(const T x)const{
				NTuple<T,N> diff;
				for(int i=0;i<N;i++)
					diff[i]=val[i]-x;
				return diff;
			}

			NTuple<T,N> operator*(const T x)const{
				NTuple<T,N> retVal;
				for(int i=0;i<N;i++)
					retVal.val[i]=val[i]*x;
				return retVal;
			}

			NTuple<T,N>& operator+=(const T x){
				for(int i=0;i<N;i++)
					val[i]+=x;
				return *this;
			}

			NTuple<T,N>& operator-=(const T x){
				for(int i=0;i<N;i++)
					val[i]-=x;
				return *this;
			}

			NTuple<T,N>& operator*=(const T x){
				for(int i=0;i<N;i++)
					val[i]*=x;
				return *this;
			}
			NTuple<T,N>& operator/=(const T x){
				for(int i=0;i<N;i++)
					val[i]/=x;
				return *this;
			}

			NTuple<T,N>& operator=(const T x){
				for(int i=0;i<N;i++)
					val[i]=x;
				return *this;
			}
			double distance(const NTuple<T,N> &v)const{
				double dist=0;
				for(int i=0;i<N;i++)
					dist+=(val[i]-v.val[i])*(val[i]-v.val[i]);
				return sqrt(dist);
			}

			double distance(const T (&v)[N])const{
				double dist=0;
				for(int i=0;i<N;i++)
					dist+=SQR(val[i]-v[i]);
				return sqrt(dist);
			}

			T normL0(void)const{
				T m=val[0];
				for(int i=1;i<N;i++)
					if(m<val[i])
						m=val[i];
				return m;
			}

			T normL1(void)const{
				T s=0;
				for(int i=0;i<N;i++)
					s+=ABS(val[i]);
				return s;
			}

			double normL2(void)const{
				double s=0;
				for(int i=0;i<N;i++)
					s+=val[i]*val[i];
				return sqrt(s);
			}

			double norm(void)const{
				return normL2();
			}

			NTuple<T,N> operator/(const T &x)const{
				NTuple<T,N> retVal;
				for(int i=0;i<N;i++)
					retVal[i]=val[i]/x;
				return retVal;
			}

			void copyTo(T (&v)[N])const{
				for(int i=0;i<N;i++)
					v[i]=val[i];
			}

			template<typename TT, int NN, int MM>  friend class gapi::Matrix;
			template<typename TT, int NN> friend std::ostream& operator<< (std::ostream&, const NTuple<TT,NN> &);
			template<typename TT, int NN> friend std::istream& operator>> (std::istream&, NTuple<TT,NN> &);
			template<typename TT, int NN> friend NTuple<T,N> operator-(const NTuple<T, N> &x);
			template<typename TT, int nn, int mm> friend NTuple<TT,mm> operator*(const NTuple<TT,nn>&v, const Matrix<TT,nn,mm> &M);
	};

	template<typename T, int N> std::ostream& operator<< (std::ostream& os, const NTuple<T,N> &v){
			os<<"[";
			for(int i=0;i<N;i++)
			{
				os<<v.val[i];
				if(i<N-1)
					os<<", ";
			}
			os<<"]";
			return os;
	}
	template<typename T, int N> std::istream& operator>> (std::istream& is, NTuple<T,N> &v){
		for(int i=0;i<N;i++)
			is>>v.val[i];
		return is;
	}

	template<typename T, int N> NTuple<T,N> operator+(const T x, const NTuple<T,N> &val){
		NTuple<T,N> retVal=val;
		retVal+=x;
		return retVal;
	}

	template<typename T, int N> NTuple<T,N> operator-(const T x, const NTuple<T,N> &val){
		NTuple<T,N> retVal=val;
		retVal-=x;
		return retVal;
	}

	template<typename T, int N> NTuple<T,N> operator*(const T x, const NTuple<T,N> &val){
		NTuple<T,N> retVal=val;
		retVal*=x;
		return retVal;
	}

	template<typename T, int N> NTuple<T,N> operator-(const NTuple<T, N> &x){
		NTuple<T,N> neg;
		for(int i=0;i<N;i++)
			neg[i]=-x[i];
		return neg;
	}
}
#endif /* NTUPLE_H_ */
