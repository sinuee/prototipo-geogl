#ifndef GPLAININPUTS_H_
#define GPLAININPUTS_H_
#include "GInputs.h"
namespace gapi{
	class GPlainInputs:public GInputs{
		protected:
		public:
			GPlainInputs();
			GPlainInputs(const char* inputFileName);
			virtual ~GPlainInputs();
			GInputsResult loadFromFile(const char* inputFileName);
			GInputsResult saveToFile(const char* outputFileName);
	};
}
#endif
