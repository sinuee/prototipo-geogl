/*
 * GFunctions.h
 *
 *  Created on: Jan 6, 2009
 *      Author: khayyam
 */
#ifndef GFUNCTIONS_H_
#define GFUNCTIONS_H_
#include "Point.h"
#include "Vector.h"
namespace gapi{
	double distanceAlongVector(const Point &P, const Point &Q, const Vector &v);
}

#endif /* GFUNCTIONS_H_ */
