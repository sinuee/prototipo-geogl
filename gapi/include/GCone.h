/*
 * GCone.h
 *
 *  Created on: Dec 11, 2008
 *      Author: khayyam
 */

#ifndef GCONE_H_
#define GCONE_H_
#include "GCylinder.h"
namespace gapi{
	class GCone:public GCylinder{
		protected:
		public:
			GCone();
			GCone(GBox &parent);
//			void Export();
			virtual void doExport(void);
			char* getName();
			~GCone();
			virtual void doDraw();
			virtual void setName(char *data);

	};
}
#endif /* GCONE_H_ */
