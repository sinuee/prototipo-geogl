/*
 * GCone.cpp
 *
 *  Created on: Dec 11, 2008
 *      Author: khayyam
 */
#include "GCone.h"
#include "GMacros.h"
#include <iostream>
gapi::GCone::GCone():GCylinder(){

}
gapi::GCone::GCone(GBox &parent):GCylinder(parent){

}
gapi::GCone::~GCone(){

}
void gapi::GCone::doExport(void){

		char nombreArchivo[100]= "D:\\cubo.lsp";
		char buffer[255],suba[30];
		char *str=new char[8];
					
		std::string sAux;
		char centro[15];
		//Centro ? **********
		double _points[1][4];
		points[0].copyTo(_points[0]);
		sAux = "";
		for (int k=0;k<3;k++)
		if(this->radius!=0)
		{
		sAux = sAux + convertToString<int >((int)(points[k][0]/ points[k][3])) + " ";
		strcpy(centro,sAux.c_str());
					

		FILE *fp;

		fp = fopen(nombreArchivo, "a");   /* Abrir archivo para escritura */

		strcpy(buffer,"");
		strcat(buffer,"(COMMAND \"_.CONE\" ");	
		strcat(buffer,"'(");
		strcat(buffer,centro);
		strcat(buffer,") ");		
		sprintf(str, "%.6f", this->getRadius());   
		strcat(buffer,str);
		strcat(buffer," \"T\" ");
		sprintf(str, "%.6f", 0); 
		strcat(buffer,str);
		strcat(buffer," ");
		sprintf(str, "%.6f", this->getHeight());  //alto
		strcat(buffer,str);
		strcat(buffer,")\n");
		fprintf(fp, buffer);
		strcpy(suba,"");
		strcat(suba,"(SETQ ");
		strcat(suba,this->getName());
		strcat(suba," (ENTLAST))");					
		fprintf(fp,buffer);
		if(!strcmp(this->rotar.c_str(),""))
			fprintf(fp,this->rotar.c_str());
		if(!strcmp(this->posi.c_str(),""))
			fprintf(fp,this->posi.c_str());	
						
		//(COMMAND "_.CONE" centroc radioc "_Top" radiosup  alturac)
				    

		fclose(fp);    /* Cerrar el archivo antes de terminar el programa */
		}
		//printf("Se ha creado el archivo: \n");	
}

/*void gapi::GCone::Export()
{
for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->Export();
	if(!hidden)
		this->doExport();
}*/

char* gapi::GCone::getName()
{
	return name;
}

void gapi::GCone::setName(char *data)
{
	name=data;
}

#ifdef USE_GAPI_GRAPHICS
void gapi::GCone::doDraw(){
	if(!visible)
		return;
	GLUquadricObj *quadObj=gluNewQuadric();
	gluQuadricDrawStyle(quadObj, ((displayType==WIRED)?GLU_LINE:GLU_FILL));
	glPushMatrix();
	Vector desp=normals[BOTTOM]*(height*0.5);

	//glTranslated(desp[0], desp[1], desp[2]);
	glMultMatrixd(T.getData());
	gluQuadricOrientation(quadObj, GLU_OUTSIDE);
	gluCylinder(quadObj, radius, 0, height, slices, stacks);

	gluQuadricOrientation(quadObj, GLU_INSIDE);
	gluDisk(quadObj, 0, radius, slices, stacks);
	glPopMatrix();
	gluDeleteQuadric(quadObj);
}
#else
void gapi::GCone::doDraw(){
	if(!visible)
		return;
	std::cerr<<"Object: GCone"<<std::endl;
}
#endif

