/*
 * GBox.cpp
 *
 *  Created on: Dec 4, 2008
 *      Author: khayyam
 */
#include "GBox.h"
#include "Matrix.h"
#include "GMacros.h"
#include "GPrimitives.h"
#include "GFunctions.h"
#include <fstream>
#include <string>
#include <iostream>
gapi::GBox::GBox(){
	objectType=OT_GBox;
	parent=NULL;
	restoreDefaults();
	bindParts=false;
	hidden=false;
}

gapi::GBox::GBox(GBox &parent){
	objectType=OT_GBox;
	this->parent=&parent;
	inheritFromParent();
	this->parent->addChild(*this);
}

void gapi::GBox::inheritFromParent(void){
	if(parent==NULL)
	{
		restoreDefaults();
		return;
	}
	/*for(int i=0;i<3;i++)
		dimmensions[i]=parent->dimmensions[i];*/
	this->width=parent->width;
	this->height=parent->height;
	this->length=parent->length;

	for(int i=0;i<15;i++)
		this->points[i]=parent->points[i];
	for(int i=0;i<6;i++)
		this->normals[i]=parent->normals[i];
	T=parent->T;
	visible=true;
	this->displayType=WIRED;
	bindParts=parent->bindParts;
	this->hidden=false;
}

void gapi::GBox::restoreDefaults(void){
	this->width=1;
	this->height=1;
	this->length=1;
	/*for(int i=0;i<3;i++)
		dimmensions[i]=1;*/
	T.identity();
	build();
	visible=true;
	this->displayType=WIRED;
	bindParts=false;
	this->hidden=false;
}

void gapi::GBox::build(void){
	 double _points[15][4]={
			{ 0,  0,  0, 1},
			{-1, -1, -1, 1},
			{-1, -1,  1, 1},
			{-1,  1,  1, 1},
			{-1,  1, -1, 1},
			{ 1, -1, -1, 1},
			{ 1, -1,  1, 1},
			{ 1,  1,  1, 1},
			{ 1,  1, -1, 1},
			{-1,  0,  0, 1},
			{ 0,  1,  0, 1},
			{ 1,  0,  0, 1},
			{ 0, -1,  0, 1},
			{ 0,  0,  1, 1},
			{ 0,  0, -1, 1}
	};
	for(int i=0;i<15;i++)
	{
		_points[i][0]*=(width*0.5);
		_points[i][1]*=(length*0.5);
		_points[i][2]*=(height*0.5);
	}

	double _vectors[6][4]={
			{-1,  0,  0, 0},
			{ 0,  1,  0, 0},
			{ 1,  0,  0, 0},
			{ 0, -1,  0, 0},
			{ 0,  0,  1, 0},
			{ 0,  0, -1, 0}
	};

	for(int i=0;i<15;i++)
	{
		points[i]=T*_points[i];
	}
	for(int i=0;i<6;i++)
	{
		normals[i]=T*_vectors[i];
	}

}

gapi::GBox::~GBox(){
	if(this->parent!=NULL)
	{
		std::cout<<"Precaucion: un objeto fue borrado fuera de su padre.\n";
	}
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
	{
		(*it)->parent=NULL;
		delete *it;
	}
}

gapi::Point& gapi::GBox::getPoint(EBoxPoint point){
	return points[point];
}

gapi::Vector& gapi::GBox::getNormal(EFace face){
	return normals[face];
}



void gapi::GBox::draw(void){
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->draw();
	if(!hidden)
		this->doDraw();
}

void gapi::GBox::doExport(void){
	
	char nombreArchivo[100]= "D:\\cubo.lsp";
	char buffer[255],suba[30];
	char *str=new char[8];
			
	//Codigo de Alfredo
	std::string sAux;
	char centro[8];
	//Centro ? **********
	double _points[1][4];
	points[0].copyTo(_points[0]);
	sAux = "";
	for (int k=0;k<3;k++)
	sAux = sAux + convertToString<int >((int)(points[k][0]/ points[k][3])) + " ";
	strcpy(centro,sAux.c_str());
			

	FILE *fp;

	fp = fopen(nombreArchivo, "a");   /* Abrir archivo para escritura */

	strcpy(buffer,"");
	strcat(buffer,"(COMMAND \"_.BOX\" \"_C\" ");	
	strcat(buffer,"'(");
	strcat(buffer,centro);
	strcat(buffer,")");
	strcat(buffer," \"_LENGTH\"");
	strcat(buffer," ");
	sprintf(str, "%.6f", this->getWidth());//largo 	
	strcat(buffer,str);
	strcat(buffer," ");
	sprintf(str, "%.6f", this->getLength()); // ancho 
	strcat(buffer,str);
	strcat(buffer," ");
	sprintf(str, "%.6f", this->getHeight());  //alto
	strcat(buffer,str);
	strcat(buffer,")\n");
	fprintf(fp, buffer);
	//(COMMAND "_.BOX" "_C" '(0 0 0) "_LENGTH" 1000.000000 1000.000000 1000.000000)
	strcpy(suba,"");
	strcat(suba,"(SETQ ");
	strcat(suba,this->getName());
	strcat(suba," (ENTLAST))\n");
	fprintf(fp,suba);
	if(!strcmp(this->rotar.c_str(),""))
		fprintf(fp,this->rotar.c_str());	
	if(!strcmp(this->posi.c_str(),""))
		fprintf(fp,this->posi.c_str());	
	/*if(strcmp(posi," "))
	{	if(strcmp(rotar," "))
	
	fprintf(fp,posi);
	fprintf(fp,rotar);
	}*/
	fclose(fp);    /* Cerrar el archivo antes de terminar el programa */
	printf("Se ha creado el archivo: \n");	
}

void gapi::GBox::Export()
{
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->Export();
	if(!hidden)
		this->doExport();
}

char *gapi::GBox::getName()
{
	return name;
}
/*char gapi::GBox::getPosition()
{
//	return posi.c_str();
}

char gapi::GBox::getOrientation()
{
//	return rotar.c_str();
}*/

void gapi::GBox::setName(char *data)
{
	name=data;	
}

void gapi::GBox::setPosition(Vector reference, float dis)
{
 char *str=new char[15];
 char poss[100],paso[35];
 float flt;
  strcpy(poss,"");
  strcat(poss,"(COMMAND \"_.MOVE\" ");
  strcat(poss,name);
  strcat(poss," \"\" '(");
  strcpy(paso, "");	
	flt=0;//reference[0];	
	sprintf(str, "%.6g",flt);
	strcat(paso,str);	
	flt=0;//reference[1];
	sprintf(str, " %.6g",flt);
	strcat(paso,str);	
	flt=0;//reference[2];
	sprintf(str, " %.6g",flt);
	strcat(paso,str);
	strcat(paso,") ");
	strcat(poss,paso);
	strcpy(paso,"");
	if((reference[0]/reference[3])==0)
		flt=0;
	else
		flt=dis*reference[0];
	
	sprintf(str, "'(%.6g",flt);
	strcat(paso,str);	
	if((reference[1]/reference[3])==0)
		flt=0;
	else
		flt=dis*reference[1];

	sprintf(str, " %.6g",flt);
	strcat(paso,str);	
	if((reference[2]/reference[3])==0)
		flt=0;
	else
		flt=dis*reference[2];

	sprintf(str, " %.6g)",flt);
	strcat(paso,str);
	strcat(paso,")");

	strcat(poss,paso);
	/*flt=dis;
	sprintf(str, " %.6g)",flt);
	strcat(paso,str);
	strcat(poss,paso);*/
	posi=poss;
}

void gapi::GBox::setOrientation(Vector reference ,float angle)
{
 char *str=new char[15];
 char paso[25],rotar1[100];
 float flt;
  
  strcpy(rotar1,"");
  strcat(rotar1,"(COMMAND \"_ROTATE3D\" ");
  strcat(rotar1,this->getName());
  strcat(rotar1," \"\" '(");
  strcpy(paso, "");	
  flt=reference[0];	
  sprintf(str, "%.6g",flt);
  strcat(paso,str);	
  flt=reference[1];
  sprintf(str, " %.6g",flt);
  strcat(paso,str);	
  flt=reference[2];
  sprintf(str, " %.6g)",flt);
  strcat(paso,str);	
  strcat(rotar1,paso);

  strcpy(paso, "");	
  flt=0;	
  sprintf(str," '(%.6g",flt);
  strcat(paso,str);	
  flt=0;
  sprintf(str, " %.6g",flt);
  strcat(paso,str);	
  flt=0;
  sprintf(str, " %.6g)",flt);
  strcat(paso,str);	
  strcat(rotar1,paso);

  strcpy(paso,"");
  flt=angle*180/3.1416;
  sprintf(str, " %.6g",flt);
  strcat(paso,str);

  strcat(rotar1,paso);
  strcat(rotar1,")");
  rotar=rotar1;
}

void gapi::GBox::subtract(char *name1,char *name2)
{char sub[50];
strcpy(sub,"");
strcat(sub,"(COMMAND \"_.SUBTRACT\" ");
strcat(sub,name1);
strcat(sub," \"\" ");
strcat(sub,name2);
strcat(sub," \"\" )");
std::string pcNombreArch1 = "d:\\cubo.lsp";
FILE *pArchivo1;
pArchivo1 = fopen(pcNombreArch1.c_str(),"a");
fprintf(pArchivo1,"%s\n",sub);			
fclose(pArchivo1);
}


void gapi::GBox::uni(char *name1, char *name2)
{char u[50];
strcpy(u,"");
strcat(u,"(COMMAND \"_.UNION\" ");
strcat(u,name1);
strcat(u," ");
strcat(u,name2);
strcat(u," \"\" )");
std::string pcNombreArch1 = "d:\\cubo.lsp";
FILE *pArchivo1;
pArchivo1 = fopen(pcNombreArch1.c_str(),"a");
fprintf(pArchivo1,"%s\n",u);			
fclose(pArchivo1);
}

void gapi::GBox::intersect(char *name1,char *name2)
{char inte[50];
strcpy(inte,"");
strcat(inte,"(COMMAND \"_.INTERSECT\" ");
strcat(inte,name1);
strcat(inte," ");
strcat(inte,name2);
strcat(inte," \"\" )");
std::string pcNombreArch1 = "d:\\cubo.lsp";
FILE *pArchivo1;
pArchivo1 = fopen(pcNombreArch1.c_str(),"a");
fprintf(pArchivo1,"%s\n",inte);			
fclose(pArchivo1);
}

void gapi::GBox::rotate(const double angleInRadians, const double x, const double y, const double z)
{
	double cosT=cos(angleInRadians);
	double sinT=sin(angleInRadians);

	double _T0[4][4]={
    	 {1,  0,  0, T[3][0]},
    	 {0,  1,  0, T[3][1]},
    	 {0,  0,  1, T[3][2]},
    	 {0,  0,  0,  1}};
	Matrix<double, 4, 4> A(_T0);

    double _R[4][4]={
    	{cosT+(1-cosT)*x*x  , (1-cosT)*x*y-sinT*z, (1-cosT)*x*z+sinT*y, 	0},
    	{(1-cosT)*y*x+sinT*z, cosT+(1-cosT)*y*y  , (1-cosT)*y*z-sinT*x, 	0},
    	{(1-cosT)*z*x-sinT*y, (1-cosT)*z*y+sinT*x, cosT+(1-cosT)*z*z  , 	0},
    	{0				   , 0					, 0				  	 ,  1}
    };
    A*=_R;
    double _T1[4][4]={
    	 {1,  0,  0, -T[3][0]},
    	 {0,  1,  0, -T[3][1]},
    	 {0,  0,  1, -T[3][2]},
    	 {0,  0,  0,  1}};
    A*=_T1;
    T=A*T;
    build();



}

void gapi::GBox::rotate90(const double x, const double y, const double z)
{
    double _T[4][4]={
    	{x*x  , x*y-z, x*z+y, 	0},
    	{y*x+z, y*y  , y*z-x, 	0},
    	{z*x-y, z*y+x, z*z  , 	0},
    	{0	  , 0	 , 0	,	1}
    };
    Matrix<double, 4, 4> A(_T);
    T=A*T;
    build();
}

void gapi::GBox::rotate180(const double x, const double y, const double z)
{
    double _T[4][4]={
    	{2*x*x-1, 2*x*y,	2*x*z, 		0},
    	{2*y*x,   2*y*y-1,	2*y*z, 		0},
    	{2*z*x,   2*z*y, 	2*z*z-1, 	0},
    	{0	  ,   0,		0,			1}
    };
    Matrix<double, 4, 4> A(_T);
    T=A*T;
    build();
}


void gapi::GBox::rotateX(double angleInRadians)
{
    double _T[4][4]={
    	{1, 0, 					  0, 				   0},
    	{0, cos(angleInRadians),  sin(angleInRadians), 0},
    	{0, -sin(angleInRadians), cos(angleInRadians), 0},
    	{0, 0, 					  0, 				   1}
    };
    Matrix<double, 4, 4> A(_T);
    T=A*T;
    build();

}

void gapi::GBox::rotateY(double angleInRadians)
{
	double _T[4][4]={
    	{cos(angleInRadians), 0, -sin(angleInRadians), 0},
    	{0, 				  1,  0, 				   0},
    	{sin(angleInRadians), 0,  cos(angleInRadians), 0},
    	{0, 				  0,  0, 				   1}};
    Matrix<double, 4, 4> A(_T);
    T=A*T;
    build();
}

void gapi::GBox::rotateZ(double angleInRadians)
{
	double _T[4][4]={
    	{cos(angleInRadians), sin(angleInRadians), 0, 0},
    	{-sin(angleInRadians), cos(angleInRadians), 0, 0},
    	{0, 				   0,  					1, 0},
    	{0, 				   0,  					0, 1}};
	Matrix<double, 4, 4> A(_T);
    T=A*T;
    build();
}

void gapi::GBox::rotate90(EFace faceNormal){
	if(faceNormal<0)
		return;
	Vector reference=normals[faceNormal];
	rotate90(reference[0],reference[1],reference[2]);
	build();
}

void gapi::GBox::rotate180(EAxis axis){
	if(axis<0)
		return;
	Vector reference;
	switch(axis)
	{
		case LATERAL:
			reference=normals[2];
			break;
		case LONGITUDINAL:
			reference=normals[1];
			break;
		case VERTICAL:
			reference=normals[4];
			break;
		default:
			return;
	}
	rotate180(reference[0],reference[1],reference[2]);
	build();
	return;
	build();
}

void gapi::GBox::rotate(EFace faceNormal, double angleRadians){
	float n[2]= {5,4};
	if(faceNormal<0)
		return;
	Vector reference=normals[faceNormal];
	rotate(angleRadians, reference[0],reference[1],reference[2]);
	build();  
	setOrientation(reference, angleRadians);
}

void gapi::GBox::translate(double dx, double dy, double dz)
{
    double _T[4][4]={
    	 {1,  0,  0, dx},
    	 {0,  1,  0, dy},
    	 {0,  0,  1, dz},
    	 {0,  0,  0,  1}};
	Matrix<double, 4, 4> A(_T);
    T=A*T;
    build();
}

void gapi::GBox::translate(const Vector &v)
{
	double _T[4][4]={
    	 {1,  0,  0, (*(const_cast<Vector*>(&v)))[0]},
    	 {0,  1,  0, (*(const_cast<Vector*>(&v)))[1]},
    	 {0,  0,  1, (*(const_cast<Vector*>(&v)))[2]},
    	 {0,  0,  0,  1}};
	Matrix<double, 4, 4> A(_T);
    T=A*T;
    build();
}

void gapi::GBox::translate(const Vector &v, double factor)
{
	double n=v.norm();
	if(ABS(n)<EPSILON)
		return;
	Vector w=v/n;
	double _T[4][4]={
    	 {1,  0,  0, w[0]*factor},
    	 {0,  1,  0, w[1]*factor},
    	 {0,  0,  1, w[2]*factor},
    	 {0,  0,  0, 1}};
	Matrix<double, 4, 4> A(_T);
    T=A*T;
    build();
}

void gapi::GBox::beginDefinition(void){
	if(parent==NULL)
		T.identity();
	else
		T=parent->T;
	defSelf();
	build();
}

void gapi::GBox::endDefinition(void){
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->inheritFromParent();
	defParts();
}


void gapi::GBox::addChild(GBox &child){
	parts.insert(&child);
}

double gapi::GBox::getWidth(void){
	return width;
}

double gapi::GBox::getLength(void){
	return length;
}

double gapi::GBox::getHeight(void){
	return height;
}


//int gapi::GBox::main(int argc, char* argv[]){
//	return 0;
//}

gapi::GBox& gapi::GBox::getPart(unsigned index){
	if(index<0 || parts.size()<=index)
		return *this;
	std::set<GBox*>::iterator it;
	unsigned count;
	for(count=0, it=parts.begin(); count<index;++count, ++it);
	return *(*(it));
}

unsigned gapi::GBox::partsCount(void){
	return (unsigned)parts.size();
}

std::set<gapi::GBox*>::iterator gapi::GBox::getFirstPart(void){
	return parts.begin();
}

std::set<gapi::GBox*>::iterator gapi::GBox::getLastPart(void){
	return parts.end();
}

void gapi::GBox::setVisible(bool visible){
	this->visible=visible;
	if(bindParts)
		for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
			(*it)->setVisible(visible);

}

bool gapi::GBox::getVisible(void){
	return this->visible;
}

void gapi::GBox::setBindParts(bool bindParts){
	this->bindParts=bindParts;
}

bool gapi::GBox::getBindParts(void){
	return bindParts;
}

void gapi::GBox::setHidden(bool hidden){
	this->hidden=hidden;
}

bool gapi::GBox::getHidden(void){
	return hidden;
}

void gapi::GBox::setDisplayType(gapi::EDisplayType displayType){
	this->displayType=displayType;
	if(bindParts)
		for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
			(*it)->setDisplayType(displayType);
}

gapi::EDisplayType gapi::GBox::getDisplayType(void){
	return this->displayType;
}

void gapi::GBox::setDimmensions(double length, double width, double height)
{
	this->width=width;
	this->length=length;
	this->height=height;
	build();
}


void gapi::GBox::setWidth(double width){
	this->width=width;
	build();
}

void gapi::GBox::setLength(double length){
	this->length=length;
	build();
}

void gapi::GBox::setHeight(double height){
	this->height=height;
	build();
}

void gapi::GBox::defSelf(void){
}

void gapi::GBox::defParts(void){
}

double gapi::GBox::distanceFromCenterToFaceCenter(EFace face){
	switch(face){
		case LEFT:case RIGHT:
			return width*0.5;
		case FRONT:case REAR:
			return length*0.5;
		case TOP:case BOTTOM:
			return height*0.5;
		default:
			return -1;
	}
}

double gapi::GBox::distanceFromCenterToFaceCenter(const Vector &v){
	double best=-1;
	int index=-1;
	for(int i=0;i<6;i++)
	{
		double op=this->normals[i]*v;
		if(op>best)
		{
			best=op;
			index=i;
		}
	}
	if(index<0)
		return -1;
	return distanceFromCenterToFaceCenter(EFace(index));
}

void gapi::GBox::position(EFace pos, double dist){

	if(parent==NULL)
	{
		return;
	}
	Vector parentDisplacementVector=this->parent->getNormal(pos);
	double parentDisplacementDistance=this->parent->distanceFromCenterToFaceCenter(pos);

	double childDisplacementDistance=this->distanceFromCenterToFaceCenter(parentDisplacementVector);
	this->translate(parentDisplacementVector, parentDisplacementDistance-(childDisplacementDistance+dist));	
		setPosition(parentDisplacementVector, parentDisplacementDistance-(childDisplacementDistance+dist));
   }

void gapi::GBox::position(EFaceReference pos, double dist){
	if(parent==NULL)
	{
		return;
	}
	Vector parentDisplacementVector=this->parent->getNormal((EFace)pos);
	double parentDisplacementDistance=this->parent->distanceFromCenterToFaceCenter(EFace(pos));

	double childDisplacementDistance=this->distanceFromCenterToFaceCenter(-parentDisplacementVector);
	this->translate(parentDisplacementVector, parentDisplacementDistance+(childDisplacementDistance+dist));
	
	//if(pos==INREAR||pos==INFRONT||pos==BELOW)
		setPosition(parentDisplacementVector,(parentDisplacementDistance+(childDisplacementDistance+dist)));
	
}

void gapi::GBox::position(EFace pos, double dist, GBox &reference){
	if(reference.parent==NULL)
		return;
	//this->translate(reference.points[CENTER]-this->points[CENTER]);
	Vector parentDisplacementVector=reference.parent->getNormal(pos);
	double referenceDisplacementDistance=reference.distanceFromCenterToFaceCenter(parentDisplacementVector);
	referenceDisplacementDistance+=distanceAlongVector(this->points[CENTER], reference.points[CENTER], parentDisplacementVector);

	double childDisplacementDistance=this->distanceFromCenterToFaceCenter(parentDisplacementVector);
	this->translate(parentDisplacementVector, referenceDisplacementDistance-(childDisplacementDistance+dist));
	//if(pos==REAR||pos==FRONT||pos==BOTTOM)
		setPosition(parentDisplacementVector,referenceDisplacementDistance-(childDisplacementDistance+dist));
	//else
	//	setPosition(parentDisplacementVector,referenceDisplacementDistance-(childDisplacementDistance+dist));
}

void gapi::GBox::position(EFaceReference pos, double dist, GBox &reference){
	if(reference.parent==NULL)
		return;
	//this->translate(reference.points[CENTER]-this->points[CENTER]);
	Vector parentDisplacementVector=reference.parent->getNormal((EFace)pos);
	double referenceDisplacementDistance=reference.distanceFromCenterToFaceCenter(parentDisplacementVector);
	referenceDisplacementDistance+=distanceAlongVector(this->points[CENTER], reference.points[CENTER], parentDisplacementVector);

	double childDisplacementDistance=this->distanceFromCenterToFaceCenter(parentDisplacementVector);
	this->translate(parentDisplacementVector, referenceDisplacementDistance+(childDisplacementDistance+dist));
	//if(pos==INREAR||pos==INFRONT||pos==BELOW)
		setPosition(parentDisplacementVector, referenceDisplacementDistance-(childDisplacementDistance+dist));
	//else		setPosition(parentDisplacementVector,referenceDisplacementDistance+(childDisplacementDistance+dist));
}
/*void gapi::GBox::printPoints()
{
	double _points[15][4];
	std::string pcNombreArch = "d:\\Matriz.txt";
	FILE *pArchivo;
	
	if( (pArchivo = fopen(pcNombreArch.c_str(),"wt")) == NULL ) {
		printf("No se pudo crear el archivo de salida para el comparador %s\n", pcNombreArch.c_str());
		 return;	
	}
	for(int i=0;i<15;i++)
	{
		//_points[i] = points[i];
		if(_points[i][3]==0)
		{
		fprintf(pArchivo,"\n");
		}
		else
		{
		points[i].copyTo(_points[i]);
		_points[i][0]= _points[i][0]/_points[i][3];
		_points[i][1]= _points[i][1]/_points[i][3];
		_points[i][2]= _points[i][2]/_points[i][3];
		fprintf(pArchivo,"%lf\t %lf \t %lf \n",_points[i][0],_points[i][1],_points[i][2]);
		}
		
	}	
	fclose(pArchivo);
}*/


#ifdef USE_GAPI_GRAPHICS
void gapi::GBox::doDraw(void){
	if(!visible)
		return;
	GLenum displayType=this->displayType==WIRED?GL_LINE_LOOP:GL_TRIANGLE_FAN;
	glPushMatrix();
	glMultMatrixd(T.getData());
	drawBox(width, length, height,displayType);
	glPopMatrix();
}
#else
void gapi::GBox::doDraw(void){
	if(!visible)
		return;
	std::cerr<<"Object: GBox"<<std::endl;
}
#endif
