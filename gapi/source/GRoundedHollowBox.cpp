/*
 * GRoundedHollowBox.cpp
 *
 *  Created on: Dec 15, 2008
 *      Author: khayyam
 */
#include "GRoundedHollowBox.h"
#include "GMacros.h"
#include "GPrimitives.h"
#include <iostream>
gapi::GRoundedHollowBox::GRoundedHollowBox():GHollowBox(){
	slices=20;
	displayType=WIRED;
}

gapi::GRoundedHollowBox::GRoundedHollowBox(GBox &parent):GHollowBox(parent){
	slices=20;
	displayType=WIRED;
}

gapi::GRoundedHollowBox::~GRoundedHollowBox(){

}
#ifdef USE_GAPI_GRAPHICS
void gapi::GRoundedHollowBox::doDraw(void){
	GLenum displayType=this->displayType==WIRED?GL_LINE_LOOP:GL_TRIANGLE_FAN;
	glPushMatrix();
	glMultMatrixd(T.getData());
	drawBox(width-2*thickness, length, thickness,0,0,0.5*(height-thickness),displayType);
	drawBox(width-2*thickness, length, thickness,0,0,-0.5*(height-thickness),displayType);
	drawBox(thickness,length,height-2*thickness,-0.5*(width-thickness), 0, 0, displayType);
	drawBox(thickness,length,height-2*thickness,0.5*(width-thickness), 0, 0, displayType);
	glPushMatrix();
	glTranslated(-0.5*width+thickness, length*0.5, 0.5*height-thickness);
	glRotated(90,1, 0,0);
	drawPartialHollowCylinder(0, thickness, length,
							   DEGREES(90), DEGREES(90), slices, displayType);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0.5*width-thickness, length*0.5, 0.5*height-thickness);
	glRotated(90,1, 0,0);
	drawPartialHollowCylinder(0, thickness, length,
							   DEGREES(0), DEGREES(90), slices, displayType);
	glPopMatrix();

	glPushMatrix();
	glTranslated(-0.5*width+thickness, length*0.5, -0.5*height+thickness);
	glRotated(90,1, 0,0);
	drawPartialHollowCylinder(0, thickness, length,
							   DEGREES(180), DEGREES(90), slices, displayType);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0.5*width-thickness, length*0.5, -0.5*height+thickness);
	glRotated(90,1, 0,0);
	drawPartialHollowCylinder(0, thickness, length,
							   DEGREES(270), DEGREES(90), slices, displayType);
	glPopMatrix();

	glPopMatrix();

}
#else
void gapi::GRoundedHollowBox::doDraw(void){
	std::cerr<<"Object: GRoundedHollowBox"<<std::endl;
}
#endif

