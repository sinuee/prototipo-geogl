/*
 * GFunctions.cpp
 *
 *  Created on: Jan 6, 2009
 *      Author: khayyam
 */
#include "GMacros.h"
#include "GFunctions.h"
#include "Point.h"
#include "Vector.h"
double gapi::distanceAlongVector(const Point &P, const Point &Q, const Vector &v){
	Vector w=Q-P;
	double dist=v*w/v.norm();
	return dist;
}
