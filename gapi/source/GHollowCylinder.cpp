/*
 * HollowCylinder.cpp
 *
 *  Created on: Dec 11, 2008
 *      Author: khayyam
 */
#include "GHollowCylinder.h"
#include "GMacros.h"
#include <iostream>
gapi::GHollowCylinder::GHollowCylinder(GBox &parent):GCylinder(parent){
	slices=20;
	stacks=5;
	radius=MIN(width, length)*0.5;
	innerRadius=radius*0.5;
}


gapi::GHollowCylinder::GHollowCylinder():GCylinder(){
	slices=20;
	stacks=5;
	radius=MIN(width, length)*0.5;
	innerRadius=radius*0.5;
}

gapi::GHollowCylinder::~GHollowCylinder(){

}



void gapi::GHollowCylinder::setInnerRadius(double innerRadius){
	this->innerRadius=innerRadius;
}

void gapi::GHollowCylinder::setRadius(double radius){
	this->radius=radius;
	width=length=2*this->radius;
	build();
}

double gapi::GHollowCylinder::getIneerRadius(void){
	return innerRadius;
}


void gapi::GHollowCylinder::doExport(void){
	
	char *str=new char[8];	
	char centro[15];
	char valor[100],cil2[80],suba[30],subb[30],subc[50];
	std::string sAux;
	//Centro ? **********
	double _points[1][4], radioint;	
	points[0].copyTo(_points[0]);
	sAux = "";
	for (int k=0;k<3;k++)
		sAux = sAux +  convertToString<int >((int)(points[k][0]/ points[k][3])) + " ";
	strcpy(centro,sAux.c_str());
	//*_points[0] = points[0];
	//***************/
	double flt;		
	/*Vector desp=normals[BOTTOM]*(height*0.5);
	strcpy(centro, "");	
	flt=desp[0];	
	sprintf(str, "%.6g",flt);
	strcat(centro,str);
	strcat(centro," ");
	flt=desp[1];
	sprintf(str, "%.6g",flt);
	strcat(centro,str);
	strcat(centro," ");
	flt=desp[2];
	sprintf(str, "%.6g",flt);
	strcat(centro,str);*/
	//se definen los dos cilindros puesto q solo se modifica el radio en el segundo cilindro
	//solo se necesita el radio
	strcpy(valor,"");
	strcat(valor,"(COMMAND \"_.CYLINDER\" '(");	
	strcat(valor,centro);
	strcat(valor, ")");
	strcpy(cil2,"");
	strcpy(cil2,valor);	
	sprintf(str, " %.6g",this->radius);
	strcat(valor,str);	
	//strcat(valor, " \"_TOP\"");
	//strcat(valor,str);	
	sprintf(str, " %.6g",this->height);
	strcat(valor,str);	
	strcat(valor,") ");

	sprintf(str, " %.6g",this->getIneerRadius());
	strcat(cil2,str);	
	//strcat(cil2, " \"_TOP\"");
	//strcat(cil2,str);	
	sprintf(str, " %.6g",this->height);
	strcat(cil2,str);	
	strcat(cil2,") ");

	//se usa para hacer el subtract de los dos cinlindros 
	strcpy(suba,"");
	strcat(suba,"(SETQ ");
	strcat(suba,name);
	strcat(suba," (ENTLAST))");
	strcpy(subb,"");
	strcat(subb,"(SETQ ");
	strcat(subb,name);
	strcat(subb,"c2 (ENTLAST)) ");
	strcpy(subc,"");
	strcat(subc,"(COMMAND \"_.SUBTRACT\" ");
	strcat(subc,name);
	strcat(subc,"\"\" ");
	strcat(subc,name);
	strcat(subc,"c2 \"\")");
	if(this->radius!=0&&this->innerRadius!=0)
		{
		std::string pcNombreArch1 = "d:\\cubo.lsp";
		FILE *pArchivo1;		
		pArchivo1 = fopen(pcNombreArch1.c_str(),"a");
		fprintf(pArchivo1,"%s\n%s\n%s\n%s\n%s\n",valor, suba, cil2, subb,subc);			
		if(strcmp(this->rotar.c_str(),"") != 0)
			fprintf(pArchivo1,"%s\n",this->rotar.c_str());	
		if(strcmp(this->posi.c_str(),"") != 0)
			fprintf(pArchivo1,"%s\n",this->posi.c_str());	
					
		fclose(pArchivo1);	
       }
}

/*void gapi::GHollowCylinder::Export()
{
for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->Export();
	if(!hidden)
		this->doExport();
}*/

char* gapi::GHollowCylinder::getName()
{
	return name;
}

void gapi::GHollowCylinder::setName(char *data)
{
	name=data;
}

#ifdef USE_GAPI_GRAPHICS
void gapi::GHollowCylinder::doDraw(void){
	if(!visible)
		return;
	GLUquadricObj *quadObj=gluNewQuadric();
	gluQuadricDrawStyle(quadObj, (((displayType)==WIRED)?GLU_LINE:GLU_FILL));
	glPushMatrix();
	Vector desp=normals[BOTTOM]*(height*0.5);

	//glTranslated(desp[0], desp[1], desp[2]);
	glMultMatrixd(T.getData());
	gluQuadricOrientation(quadObj, GLU_INSIDE);
	gluCylinder(quadObj, innerRadius, innerRadius, height, slices, stacks);
	gluQuadricOrientation(quadObj, GLU_OUTSIDE);
	gluCylinder(quadObj, radius, radius, height, slices, stacks);


	gluQuadricOrientation(quadObj, GLU_INSIDE);
	gluDisk(quadObj, innerRadius, radius, slices, stacks);

	glPopMatrix();
	glPushMatrix();
	glTranslated(-desp[0], -desp[1], -desp[2]);
	glMultMatrixd(T.getData());
	gluQuadricOrientation(quadObj, GLU_OUTSIDE);
	gluDisk(quadObj, innerRadius, radius, slices, stacks);
	glPopMatrix();
	gluDeleteQuadric(quadObj);

}
#else
void gapi::GHollowCylinder::doDraw(void){
	if(!visible)
		return;
	std::cerr<<"Object: GHollowCylinder"<<std::endl;
}
#endif
