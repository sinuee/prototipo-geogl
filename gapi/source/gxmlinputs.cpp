#include "GXMLInputs.h"
#include "expat.h"
#include <fstream>
#include <stack>
#include "GMacros.h"


gapi::GXMLInputs::GXMLInputs(){
}

gapi::GXMLInputs::GXMLInputs(const char* inputFileName){
	loadFromFile(inputFileName);
}

gapi::GXMLInputs::~GXMLInputs(){
}

void documentTypeHandler(void *inputsStackVoidPointer, const XML_Char *doctypeName, const XML_Char *sysid, const XML_Char *pubid, int has_internal_subset){
}

void startTagHandler(void *inputsStackVoidPointer, const XML_Char *name,const XML_Char **atts){
	std::stack<gapi::GInputs*> *inputsStack=(std::stack<gapi::GInputs*> *)inputsStackVoidPointer;
	if(inputsStack->size()==1){//duplicate first tag (its the caller node, so no new group has to be added)
		inputsStack->push(inputsStack->top());
		return;
	}
	gapi::GXMLInputs *newGroup=new gapi::GXMLInputs();
	inputsStack->push(newGroup);
}



void endTagHandler(void *inputsStackVoidPointer, const XML_Char *name){
	std::stack<gapi::GInputs*> *inputsStack=(std::stack<gapi::GInputs*> *)inputsStackVoidPointer;
	gapi::GInputs* topInputs=inputsStack->top();
	inputsStack->pop();
	gapi::GInputs* parent=inputsStack->top();

	if(!topInputs->hasGroups() && topInputs->getInputsCount()<=1){//then it's not a group but a single input
		std::string value=topInputs->getLiteral("text","");
		if(!value.empty()){
			parent->addInput(name,value);
			delete topInputs;
		}
		else if(topInputs!=parent){
			parent->addGroup(name, topInputs);
		}
	}
	else if(topInputs!=parent){
		parent->addGroup(name, topInputs);
	}
}

void customTrim(std::string &value, const char *setToTrim){
	size_t startpos = value.find_first_not_of(setToTrim); // Find the first character position after excluding leading blank spaces
    size_t endpos = value.find_last_not_of(setToTrim); // Find the first character position from reverse af
	if(( std::string::npos == startpos ) || ( std::string::npos == endpos)){
		value = "";
	}
	else{
		value = value.substr( startpos, endpos-startpos+1 );
	}
}
void textHandler(void *inputsStackVoidPointer,const XML_Char *s,int len){
	std::stack<gapi::GInputs*> *inputsStack=(std::stack<gapi::GInputs*> *)inputsStackVoidPointer;
	gapi::GInputs* topInputs=inputsStack->top();
	char buff[gapi::GXMLInputs::MAX_BUFFER_SIZE];
	int infoSize=MIN((unsigned)len,gapi::GXMLInputs::MAX_BUFFER_SIZE-1);
	memcpy(buff, s, infoSize);
	buff[infoSize]='\0';
	std::string value=buff;
	customTrim(value," \t\n\r");
	if(!value.empty())
		topInputs->addInput("text",buff);
}


gapi::GInputsResult gapi::GXMLInputs::loadFromFile(const char* inputFileName){
	char buff[gapi::GXMLInputs::MAX_BUFFER_SIZE];

	std::fstream inputFile;
	inputFile.open(inputFileName,std::ios_base::in);
	if(!inputFile.is_open()){
		return IR_ErrorReadingFile;
	}
	std::stack<gapi::GXMLInputs*> inputsStack;
	inputsStack.push(this);
	XML_Parser expatParser=XML_ParserCreate("UTF-8");
	XML_SetStartElementHandler    (expatParser, startTagHandler);
	XML_SetEndElementHandler      (expatParser, endTagHandler);
	XML_SetCharacterDataHandler   (expatParser, textHandler);
	XML_SetStartDoctypeDeclHandler(expatParser, documentTypeHandler);
	XML_SetUserData               (expatParser, &inputsStack);

	inputFile.read(buff, gapi::GXMLInputs::MAX_BUFFER_SIZE);
	std::streamsize count=inputFile.gcount();
	XML_Status status=XML_Parse(expatParser,buff,count,inputFile.eof());
	while(!inputFile.eof()){
		inputFile.read(buff, gapi::GXMLInputs::MAX_BUFFER_SIZE);
		count=inputFile.gcount();
		status=XML_Parse(expatParser,buff,count,inputFile.eof());
	}

	if(status!=XML_STATUS_OK){
		XML_ParserFree(expatParser);
		return gapi::IR_ErrorReadingFile;
	}
	XML_ParserFree(expatParser);
	return gapi::IR_Ok;
}

gapi::GInputsResult gapi::GXMLInputs::saveToFile(const char* outputFileName){
	if(inputs.empty() && groups.empty()){
		return IR_Ok;
	}
	std::fstream outputFile;
	outputFile.open(outputFileName,std::ios_base::out);
	if(!outputFile.is_open()){
		return IR_ErrorWritingFile;
	}
	outputFile<<"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"<<std::endl;
	this->printNode("ModelInputs", outputFile);
	outputFile.close();
	return IR_Ok;
}


void gapi::GXMLInputs::printNode(const char *nodeName, std::fstream &outputFile, int cummulativeIndent, int indent){
	std::string indentString;
	for(int i=0;i<cummulativeIndent;++i)
		indentString+=' ';
	outputFile<<indentString<<"<"<<nodeName<<">"<<std::endl;
	for(int i=0;i<indent;++i)
		indentString+=' ';
	for(std::map<std::string, std::string>::iterator it=inputs.begin();it!=inputs.end();++it){
		outputFile<<indentString<<"<"<<it->first<<">"<<it->second<<"</"<<it->first<<">"<<std::endl;
	}
	for(std::map<std::string, gapi::GInputs*>::iterator it=groups.begin();it!=groups.end();++it){
		GXMLInputs *group=(GXMLInputs *)it->second;
		group->printNode(it->first.c_str(), outputFile, cummulativeIndent+indent, indent);
	}
	outputFile<<indentString.substr(0,cummulativeIndent)<<"</"<<nodeName<<">"<<std::endl;
}

gapi::GInputs* gapi::GXMLInputs::createNewInstance(void){
	return new GXMLInputs;
}
