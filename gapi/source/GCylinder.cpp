/*
 * GCylinder.cpp
 *
 *  Created on: Dec 10, 2008
 *      Author: khayyam
 */
#include "GCylinder.h"
#include "GMacros.h"
#include <iostream>
gapi::GCylinder::GCylinder():GBox(){
	slices=20;
	stacks=5;
	radius=MIN(width, length)*0.5;
}

gapi::GCylinder::GCylinder(GBox &parent):GBox(parent){
	slices=20;
	stacks=5;
	radius=MIN(width, length)*0.5;
}

gapi::GCylinder::~GCylinder(){

}

double gapi::GCylinder::getRadius(void){
	return radius;
}

double gapi::GCylinder::getHeight(void){
	return height;
}

void gapi::GCylinder::setRadius(double r){
	radius=r;
	width=length=2*r;
	build();

}

void gapi::GCylinder::setWidth(double width){
	this->width=this->length=width;
	radius=width*0.5;
	build();
}

void gapi::GCylinder::setLength(double length){
	this->width=this->length=length;
	radius=length*0.5;
	build();
}


void gapi::GCylinder::doExport(void)
{
	char *str=new char[15];	
	char centro[40];
	char valor[50],suba[30],Revol[50],subb[30],rot[50];
	std::string sAux;
	//Centro ? **********
	double _points[1][4];	
	points[0].copyTo(_points[0]);
	sAux = "";
	for (int k=0;k<3;k++)
	sAux = sAux +  convertToString<int >((int)(points[k][0]/ points[k][3])) + " ";
	strcpy(centro,sAux.c_str());

	
	//*_points[0] = points[0];
	//***************
	//int j,i;
	/*strcpy(arch,"");
	strcat(arch,"d:"\\\\\");
	strcat(arch,name*/
	double flt;			
	//Vector desp=normals[BOTTOM]*(height*0.5);
	//se asigna el nombre del objeto
	strcpy(suba,"");
	strcat(suba,"(SETQ ");
	strcat(suba,name);
	strcat(suba," (ENTLAST))");
	strcpy(subb,"");
	strcat(subb,"(SETQ c1 (ENTLAST))");
	/*strcpy(centro, "");	
	flt=desp[0];	
	sprintf(str, "%.6g",flt);
	strcat(centro,str);
	//strcat(centro," ");
	flt=desp[1];
	sprintf(str, " %.6g",flt);
	strcat(centro,str);
	//strcat(centro," ");
	flt=desp[2];
	sprintf(str, " %.6g",flt);
	strcat(centro,str);*/
			
	//se genera el cilindro tomando su radio y su altura
	strcpy(valor,"");
	strcat(valor,"(COMMAND \"._RECTANG\" '(");	
	strcat(valor,centro);
	strcat(valor, ")");
	sprintf(str, " '(%.6g",this->radius);
	strcat(valor,str);	
	//strcat(valor, " \"_TOP\"");
	strcat(valor,str);	
	sprintf(str, " %.6g) ",this->height);
	strcat(valor,str);	
	strcat(valor,")");
	strcpy(Revol,"");
	strcat(Revol,"(COMMAND \"._REVOLVE\" c1 "\"\ \"x\" 360)");	
	strcat(rot,"(COMMAND \"ROTATED3D\" '(0 0 0)  '(0 1) 90)");
	if(this->radius!=0)
		{
	std::string pcNombreArch1 = "d:\\cubo.lsp";
	FILE *pArchivo1;	
	pArchivo1 = fopen(pcNombreArch1.c_str(),"a");
	fprintf(pArchivo1,"%s\n%s\n",valor,subb,Revol,suba,rot);
	if(strcmp(this->rotar.c_str(),""))
		fprintf(pArchivo1,"%s\n",this->rotar.c_str());
	if(strcmp(this->posi.c_str(),""))
		fprintf(pArchivo1,"%s\n",this->posi.c_str());	
	

	fclose(pArchivo1);	
	}
}

/*void gapi::GCylinder::Export(void){
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->Export();
	if(!hidden)
		this->doExport();
}*/

char* gapi::GCylinder::getName()
{
	return name;
}

void gapi::GCylinder::setName(char *data)
{
	name = data;
	//delete data;
}


#ifdef USE_GAPI_GRAPHICS
void gapi::GCylinder::doDraw(){
	if(!visible)
		return;
	GLUquadricObj *quadObj=gluNewQuadric();
	gluQuadricDrawStyle(quadObj, ((displayType==WIRED)?GLU_LINE:GLU_FILL));
	glPushMatrix();
	Vector desp=normals[BOTTOM]*(height*0.5);

	//glTranslated(desp[0], desp[1], desp[2]);
	glMultMatrixd(T.getData());
	gluQuadricOrientation(quadObj, GLU_OUTSIDE);
	gluCylinder(quadObj, radius, radius, height, slices, stacks);

	gluQuadricOrientation(quadObj, GLU_INSIDE);
	gluDisk(quadObj, 0, radius, slices, stacks);

	glPopMatrix();
	glPushMatrix();
	glTranslated(-desp[0], -desp[1], -desp[2]);
	glMultMatrixd(T.getData());
	gluQuadricOrientation(quadObj, GLU_OUTSIDE);
	gluDisk(quadObj, 0, radius, slices, stacks);
	glPopMatrix();
	gluDeleteQuadric(quadObj);
}
#else
void gapi::GCylinder::doDraw(){
	std::cerr<<"Object: GCylinder"<<std::endl;
}
#endif

