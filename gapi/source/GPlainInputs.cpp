#include "GPlainInputs.h"
#include <string>
#include <map>
#include <fstream>
gapi::GPlainInputs::GPlainInputs():GInputs(){
}

gapi::GPlainInputs::GPlainInputs(const char* inputFileName):GInputs(){
	loadFromFile(inputFileName);
}

gapi::GPlainInputs::~GPlainInputs(){
}

gapi::GInputsResult gapi::GPlainInputs::loadFromFile(const char* inputFileName){
	std::fstream inputFile;
	inputFile.open(inputFileName,std::ios_base::in);
	if(!inputFile.is_open()){
		return IR_ErrorReadingFile;
	}
	std::string item, value;
	while(inputFile>>item){
		inputFile>>value;
		inputs[item]=value;
	}
	inputFile.close();
	return IR_Ok;
}

gapi::GInputsResult gapi::GPlainInputs::saveToFile(const char* outputFileName){
	if(inputs.empty())
		return IR_Ok;
	std::fstream outputFile;
	outputFile.open(outputFileName,std::ios_base::out);
	if(!outputFile.is_open()){
		return IR_ErrorWritingFile;
	}
	for(std::map<std::string, std::string>::iterator it=inputs.begin();it!=inputs.end();++it){
		outputFile<<it->first<<it->second;
	}
	outputFile.close();
	return IR_Ok;
}
#undef BUFFER_SIZE
