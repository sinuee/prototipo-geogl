/*
 * GPrimitives.cpp
 *
 *  Created on: Dec 15, 2008
 *      Author: khayyam
 */
#include "GMacros.h"
#ifdef USE_GAPI_GRAPHICS
#include <math.h>
#include "GPrimitives.h"
void gapi::drawPartialHollowCylinder(double innerRadius, double outerRadius, double height,
							   double startAngle, double sweepAngle, int slices, GLenum displayType){

	double angleStep=sweepAngle/slices;
	double theta=startAngle;
	double cosTheta=cos(theta);
	double sinTheta=sin(theta);
	double xIn=innerRadius*cosTheta,
	       yIn=innerRadius*sinTheta;
	double xOut=outerRadius*cosTheta,
	       yOut=outerRadius*sinTheta;

	glPushMatrix();
	glBegin(displayType);
		glNormal3d(sinTheta, -cosTheta, 0);
		glVertex3d(xIn, yIn, 0);
		glVertex3d(xOut, yOut, 0);
		glVertex3d(xOut, yOut, height);
		glVertex3d(xIn, yIn, height);
	glEnd();

	for(int i=0;i<slices;i++)
	{
		double newTheta=theta+angleStep;
		double cosNewTheta=cos(newTheta);
		double sinNewTheta=sin(newTheta);
		double newXOut=outerRadius*cosNewTheta,
			   newYOut=outerRadius*sinNewTheta;
		double newXIn=innerRadius*cosNewTheta,
			   newYIn=innerRadius*sinNewTheta;
		double normalX=cos(0.5*(theta+newTheta));
		double normalY=sin(0.5*(theta+newTheta));
		glBegin(displayType);
		glNormal3d(normalX, normalY, 0);
		glVertex3d(xOut, yOut, 0);
		glVertex3d(newXOut, newYOut, 0);
		glVertex3d(newXOut, newYOut, height);
		glVertex3d(xOut, yOut, height);
		glEnd();

		glBegin(displayType);
		glNormal3d(-normalX, -normalY, 0);
		glVertex3d(xIn, yIn, 0);
		glVertex3d(xIn, yIn, height);
		glVertex3d(newXIn, newYIn, height);
		glVertex3d(newXIn, newYIn, 0);
		glEnd();

		glBegin(displayType);
		glNormal3d(0, 0, 1);
		glVertex3d(xIn, yIn, height);
		glVertex3d(xOut, yOut, height);
		glVertex3d(newXOut, newYOut, height);
		glVertex3d(newXIn, newYIn, height);
		glEnd();

		glBegin(displayType);
		glNormal3d(0, 0, -1);
		glVertex3d(newXIn, newYIn, 0);
		glVertex3d(newXOut, newYOut, 0);
		glVertex3d(xOut, yOut, 0);
		glVertex3d(xIn, yIn, 0);
		glEnd();

		theta=newTheta;
		xOut=newXOut;
		yOut=newYOut;
		xIn=newXIn;
		yIn=newYIn;
		sinTheta=sinNewTheta;
		cosTheta=cosNewTheta;
	}

	glBegin(displayType);
		glNormal3d(-sinTheta, cosTheta, 0);
		glVertex3d(xIn, yIn, 0);
		glVertex3d(xIn, yIn, height);
		glVertex3d(xOut, yOut, height);
		glVertex3d(xOut, yOut, 0);
	glEnd();
	glPopMatrix();
}


void gapi::drawPartialHollowCylinder(double innerRadius, double outerRadius, double height,
							   double startAngle, double sweepAngle, double px, double py, double pz, int slices, GLenum displayType){
		glPushMatrix();
		glTranslated(px, py, pz);
		drawPartialHollowCylinder(innerRadius, outerRadius, height, startAngle, sweepAngle, slices, displayType);
		glPopMatrix();
	}


void gapi::drawBox(double width, double length, double height, GLenum displayType){
	width*=0.5;
	length*=0.5;
	height*=0.5;
	double vertices[8][3]={
		{-width, -length, -height},
		{-width, -length,  height},
		{-width,  length,  height},
		{-width,  length, -height},
		{ width, -length, -height},
		{ width, -length,  height},
		{ width,  length,  height},
		{ width,  length, -height}
	};
	static int faces[6][4] =
	{
		{0, 1, 2, 3},
		{3, 2, 6, 7},
		{7, 6, 5, 4},
		{4, 5, 1, 0},
		{5, 6, 2, 1},
		{7, 4, 0, 3}
	};
	static double normals[6][3]={
		{-1,  0,  0},
		{ 0,  1,  0},
		{ 1,  0,  0},
		{ 0, -1,  0},
		{ 0,  0,  1},
		{ 0,  0, -1}};
	for (int i = 5; i >= 0; i--) {
		glBegin(displayType);
			glNormal3dv(normals[i]);
			glVertex3dv(vertices[faces[i][0]]);
			glVertex3dv(vertices[faces[i][1]]);
			glVertex3dv(vertices[faces[i][2]]);
			glVertex3dv(vertices[faces[i][3]]);
		glEnd();
	}
}


void gapi::drawBox(double width, double length, double height, double px, double py, double pz, GLenum displayType=GL_TRIANGLE_FAN){
	glPushMatrix();
	glTranslated(px, py, pz);
	drawBox(width, length, height, displayType);
	glPopMatrix();
}
#endif

