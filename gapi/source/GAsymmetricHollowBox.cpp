/*
 * GAsymmetricHollowBox.cpp
 *
 *  Created on: Dec 17, 2008
 *      Author: khayyam
 */
#include "GAsymmetricHollowBox.h"
gapi::GAsymmetricHollowBox::GAsymmetricHollowBox():GHollowBox(){
	topThickness=bottomThickness=thickness;

}

gapi::GAsymmetricHollowBox::GAsymmetricHollowBox(GBox &parent):GHollowBox(parent){
	topThickness=bottomThickness=thickness;
}

gapi::GAsymmetricHollowBox::~GAsymmetricHollowBox(){

}

void gapi::GAsymmetricHollowBox::setTopThickness(double topThickness){
	this->topThickness=topThickness;
	build();
}

void gapi::GAsymmetricHollowBox::setBottomThickness(double bottomThickness){
	this->bottomThickness=bottomThickness;
	build();
}

double gapi::GAsymmetricHollowBox::getTopThickness(void){
	return topThickness;
}

double gapi::GAsymmetricHollowBox::getBottomThickness(void){
	return bottomThickness;
}

void gapi::GAsymmetricHollowBox::build(void){
	double _points[15][4]={
			{ 0,  0,  0, 1},
			{-1, -1, -1, 1},
			{-1, -1,  1, 1},
			{-1,  1,  1, 1},
			{-1,  1, -1, 1},
			{ 1, -1, -1, 1},
			{ 1, -1,  1, 1},
			{ 1,  1,  1, 1},
			{ 1,  1, -1, 1},
			{-1,  0,  0, 1},
			{ 0,  1,  0, 1},
			{ 1,  0,  0, 1},
			{ 0, -1,  0, 1},
			{ 0,  0,  1, 1},
			{ 0,  0, -1, 1}
	};

	double _vectors[6][4]={
			{-1,  0,  0, 0},
			{ 0,  1,  0, 0},
			{ 1,  0,  0, 0},
			{ 0, -1,  0, 0},
			{ 0,  0,  1, 0},
			{ 0,  0, -1, 0}
	};


	for(int i=0;i<15;i++)
	{
		points[i]=_points[i];
		points[i][0]*=(width*0.5);
		points[i][1]*=(length*0.5);
		points[i][2]*=(height*0.5);
		points[i]=T*points[i];

		innerPoints[i]=_points[i];
		innerPoints[i][0]*=(innerWidth*0.5);
		innerPoints[i][1]*=(length*0.5);
		innerPoints[i][2]*=(innerHeight*0.5);

		innerPoints[i][2]+=(bottomThickness-topThickness)*0.5;//asymmetry adjustment
		innerPoints[i]=T*innerPoints[i];
	}
	for(int i=0;i<6;i++)
	{
		normals[i]=T*_vectors[i];
		innerNormals[i]=T*_vectors[i];
		innerNormals[i]*=-1;
	}
}
