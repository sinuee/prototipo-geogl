/*
 * GTruncatedCone.cpp
 *
 *  Created on: Dec 11, 2008
 *      Author: khayyam
 */
#include "GTruncatedCone.h"
#include "GMacros.h"
#include <iostream>
gapi::GTruncatedCone::GTruncatedCone():GCylinder(){
	topRadius=0;
}

gapi::GTruncatedCone::GTruncatedCone(GBox &parent):GCylinder(parent){
	topRadius=0;
}

gapi::GTruncatedCone::~GTruncatedCone(){

}

void gapi::GTruncatedCone::setTopRadius(double topRadius){
	this->topRadius=topRadius;
	width=length=2*MAX(this->topRadius, this->radius);
}

double gapi::GTruncatedCone::getTopRadius(void){
	return topRadius;

}

void gapi::GTruncatedCone::doExport(void){
	
	char nombreArchivo[100]= "D:\\cubo.lsp";
	char buffer[255];
	char *str=new char[8];
	if(this->radius!=0&&this->topRadius!=0)
		{				
	std::string sAux;
	char centro[15];
	//Centro ? **********
	double _points[1][4];
	points[0].copyTo(_points[0]);
	sAux = "";
	for (int k=0;k<3;k++)
		sAux = sAux + convertToString<int >((int)(points[k][0]/ points[k][3])) + " ";
	strcpy(centro,sAux.c_str());
	
		FILE *fp;

		fp = fopen(nombreArchivo, "w");   /* Abrir archivo para escritura */

	strcpy(buffer,"");
	strcat(buffer,"(COMMAND \"_.CONE\" ");	
	strcat(buffer,"'(");
	strcat(buffer,centro);
	strcat(buffer,") ");
	sprintf(str, "%.6f", this->getRadius());   
	strcat(buffer,str);
	strcat(buffer," \"T\" ");
	sprintf(str, "%.6f", this->getTopRadius()); 
	strcat(buffer,str);
	strcat(buffer," ");
	sprintf(str, "%.6f", this->getHeight());  //alto
	strcat(buffer,str);
	strcat(buffer,")\n");
	strcat(buffer,"(SETQ ");
	strcat(buffer,this->getName());
	strcat(buffer," (ENTLAST))");
	fprintf(fp, buffer);
					
	//(COMMAND "_.CONE" centroc radioc "_Top" radiosup  alturac)
	
	fclose(fp);    /* Cerrar el archivo antes de terminar el programa */
	}
	//return 1;
	
}

/*void gapi::GTruncatedCone::Export()
{
for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->Export();
	if(!hidden)
		this->doExport();
}*/

char *gapi::GTruncatedCone::getName()
{
	return name;
}

void gapi::GTruncatedCone::setName(char *data)
{
	name=data;
}

#ifdef USE_GAPI_GRAPHICS
void gapi::GTruncatedCone::doDraw(void){
	if(!visible)
		return;
	GLUquadricObj *quadObj=gluNewQuadric();
	gluQuadricDrawStyle(quadObj, ((displayType==WIRED)?GLU_LINE:GLU_FILL));
	glPushMatrix();
	Vector desp=normals[BOTTOM]*(height*0.5);

	glTranslated(desp[0], desp[1], desp[2]);
	glMultMatrixd(T.getData());
	gluQuadricOrientation(quadObj, GLU_OUTSIDE);
	gluCylinder(quadObj, radius, topRadius, height, slices, stacks);

	gluQuadricOrientation(quadObj, GLU_INSIDE);
	gluDisk(quadObj, 0, radius, slices, stacks);

	gluQuadricOrientation(quadObj, GLU_OUTSIDE);

	glPopMatrix();
	glPushMatrix();
	glTranslated(-desp[0], -desp[1], -desp[2]);
	glMultMatrixd(T.getData());
	gluDisk(quadObj, 0, topRadius, slices, stacks);
	glPopMatrix();
	gluDeleteQuadric(quadObj);
}
#else
void gapi::GTruncatedCone::doDraw(void){
	std::cerr<<"Object: GTruncatedCone"<<std::endl;
}
#endif

