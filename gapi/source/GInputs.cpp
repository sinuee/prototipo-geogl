/*
 * GInputs.cpp
 *
 *  Created on: Dec 5, 2008
 *      Author: khayyam
 */
#include "GInputs.h"
#include <string>
#include <cstdlib>
#include <sstream>

gapi::GInputs::GInputs(){
}

gapi::GInputs::~GInputs(){
	for(std::map<std::string, gapi::GInputs*>::iterator it=groups.begin();it!=groups.end();++it)
		delete it->second;
}

gapi::GInputs* gapi::GInputs::getGroup(const char *name){
	std::string s=name;
	size_t pos=s.find(INPUT_PATH_SEPARATOR);
	if(pos!=std::string::npos){
		std::map<std::string, GInputs*>::iterator it=groups.find(s.substr(0,pos));
		if(it==groups.end()){
			return NULL;
		}
		else{
			return it->second->getGroup(s.substr(pos+1,s.size()-(pos+1)).c_str());
		}
	}
	std::map<std::string, GInputs*>::iterator it=groups.find(name);
	if(it==groups.end()){
		return NULL;
	}
	return it->second;
}

const gapi::GInputs* gapi::GInputs::getGroup(const char *name)const{
	std::string s=name;
	size_t pos=s.find(INPUT_PATH_SEPARATOR);
	if(pos!=std::string::npos){
		std::map<std::string, GInputs*>::const_iterator it=groups.find(s.substr(0,pos));
		if(it==groups.end()){
			return NULL;
		}
		else{
			return it->second->getGroup(s.substr(pos+1,s.size()-(pos+1)).c_str());
		}
	}
	std::map<std::string, GInputs*>::const_iterator it=groups.find(name);
	if(it==groups.end()){
		return NULL;
	}
	return it->second;
}

gapi::GInputsResult gapi::GInputs::getInput(const char *name, std::string &destination)const{
	std::string s=name;
	size_t pos=s.find_last_of(INPUT_PATH_SEPARATOR);
	if(pos!=std::string::npos){
		const gapi::GInputs *group=getGroup(s.substr(0,pos).c_str());
		if(group==NULL)
			return IR_NotFound;
		return getInput(s.substr(pos+1, s.size()-(pos+1)).c_str(), destination);
	}
	std::map<std::string, std::string>::const_iterator it=inputs.find(name);
	if(it==inputs.end())
		return IR_NotFound;
	destination=it->second;
	return IR_Ok;
}

std::string gapi::GInputs::getLiteral(const char* name, const char *defaultValue)const{
	std::string literalValue;
	gapi::GInputsResult result=getInput(name, literalValue);
	if(result!=IR_Ok)
		return defaultValue;
	return literalValue;
}

long int gapi::GInputs::getInteger(const char* name, int defaultValue)const{
	std::string literalValue;
	gapi::GInputsResult result=getInput(name, literalValue);
	if(result!=IR_Ok)
		return defaultValue;
	return strtol(literalValue.c_str(), NULL, 10);
}

double gapi::GInputs::getReal(const char* name, double defaultValue)const{
	std::string literalValue;
	gapi::GInputsResult result=getInput(name, literalValue);
	if(result!=IR_Ok)
		return defaultValue;
	return strtod(literalValue.c_str(), NULL);
}

gapi::GInputsResult gapi::GInputs::addGroup(const char *groupName, GInputs *group){
	std::string s=groupName;
	size_t pos=s.find(INPUT_PATH_SEPARATOR);
	if(pos!=std::string::npos){
		std::map<std::string, GInputs*>::iterator it=groups.find(s.substr(0,pos));
		if(it==groups.end()){
			return IR_NotFound;
		}
		else{
			return it->second->addGroup(s.substr(pos+1,s.size()-(pos+1)).c_str(), group);
		}
	}
	if(inputs.find(groupName)!=inputs.end())
		return IR_Duplicated;
	if(groups.find(groupName)!=this->groups.end())
		return IR_Duplicated;
	this->groups[groupName]=group;
	return IR_Ok;
}


gapi::GInputsResult gapi::GInputs::createGroup(const char *groupName){
	std::string s=groupName;
	size_t pos=s.find_last_of(INPUT_PATH_SEPARATOR);
	if(pos!=std::string::npos){
		GInputs *parentGroup=getGroup(s.substr(0,pos).c_str());
		if(parentGroup==NULL)
			return IR_NotFound;
		return parentGroup->createGroup(s.substr(pos+1,s.size()-(pos+1)).c_str());
	}
	GInputs *newGroup=this->createNewInstance();
	if(newGroup==NULL)
		return IR_CreateNewInstanceNotDefined;
	groups[groupName]=newGroup;
	return IR_Ok;

}

gapi::GInputs* gapi::GInputs::createNewInstance(void){
	return NULL;
}

unsigned gapi::GInputs::getInputsCount(void)const{
	return (unsigned)this->inputs.size();
}

unsigned gapi::GInputs::getGroupsCount(void)const{
	return (unsigned)this->groups.size();
}

bool gapi::GInputs::hasInputs(void)const{
	return !this->inputs.empty();
}

bool gapi::GInputs::hasGroups(void)const{
	return !this->groups.empty();
}

bool gapi::GInputs::hasInput(const char*inputName)const{
	std::string s=inputName;
	size_t pos=s.find_last_of(INPUT_PATH_SEPARATOR);
	const GInputs *group=getGroup(s.substr(0,pos-1).c_str());
	if(group==NULL)
		return false;
	return group->hasInput(s.substr(pos+1,s.size()-(pos+1)).c_str());
}

bool gapi::GInputs::hasGroup(const char*groupName)const{
	std::string s=groupName;
	size_t pos=s.find(INPUT_PATH_SEPARATOR);
	if(pos!=std::string::npos){
		std::string sub=s.substr(0,pos);
		std::map<std::string, GInputs*>::const_iterator it=groups.find(sub);
		if(it==groups.end())
			return false;
		return it->second->hasGroup(s.substr(pos+1, s.size()-(pos+1)).c_str());
	}
	return groups.find(s)!=groups.end();
}
