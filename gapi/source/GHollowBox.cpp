/*
 * GHollowBox.cpp
 *
 *  Created on: Dec 15, 2008
 *      Author: khayyam
 */
#include "GHollowBox.h"
#include "GMacros.h"
#include <iostream>
gapi::GHollowBox::GHollowBox():GBox(){
	thickness=MIN(height, width)/3.0;
	this->innerHeight=height-2*thickness;
	this->innerWidth=width-2*thickness;
	build();
}

gapi::GHollowBox::GHollowBox(GBox &parent):GBox(parent){
	thickness=MIN(height, width)/3.0;
	this->innerHeight=height-2*thickness;
	this->innerWidth=width-2*thickness;
	build();
}

gapi::GHollowBox::~GHollowBox(){

}

void gapi::GHollowBox::setInnerWidth(double innerWidth){
	this->innerWidth=innerWidth;
	build();
}

void gapi::GHollowBox::setInnerHeight(double innerHeight){
	this->innerHeight=innerHeight;
	build();
}

void gapi::GHollowBox::setThickness(double thickness){
	this->thickness=thickness;
	build();
}

double  gapi::GHollowBox::getInnerWidth(void){
	return this->innerWidth;
}

double gapi::GHollowBox::getInnerHeight(void){
	return this->innerHeight;
}

double gapi::GHollowBox::getThickness(void){
	return this->thickness;
}

void gapi::GHollowBox::build(void){
	 double _points[15][4]={
			{ 0,  0,  0, 1},
			{-1, -1, -1, 1},
			{-1, -1,  1, 1},
			{-1,  1,  1, 1},
			{-1,  1, -1, 1},
			{ 1, -1, -1, 1},
			{ 1, -1,  1, 1},
			{ 1,  1,  1, 1},
			{ 1,  1, -1, 1},
			{-1,  0,  0, 1},
			{ 0,  1,  0, 1},
			{ 1,  0,  0, 1},
			{ 0, -1,  0, 1},
			{ 0,  0,  1, 1},
			{ 0,  0, -1, 1}
	};

	double _vectors[6][4]={
			{-1,  0,  0, 0},
			{ 0,  1,  0, 0},
			{ 1,  0,  0, 0},
			{ 0, -1,  0, 0},
			{ 0,  0,  1, 0},
			{ 0,  0, -1, 0}
	};


	for(int i=0;i<15;i++)
	{
		points[i]=_points[i];
		points[i][0]*=(width*0.5);
		points[i][1]*=(length*0.5);
		points[i][2]*=(height*0.5);
		points[i]=T*points[i];

		innerPoints[i]=_points[i];
		innerPoints[i][0]*=(innerWidth*0.5);
		innerPoints[i][1]*=(length*0.5);
		innerPoints[i][2]*=(innerHeight*0.5);
		innerPoints[i]=T*innerPoints[i];
	}
	for(int i=0;i<6;i++)
	{
		normals[i]=T*_vectors[i];
		innerNormals[i]=T*_vectors[i];
		innerNormals[i]*=-1;
	}

}
void gapi::GHollowBox::doExport(void){
	char nombreArchivo[100]= "D:\\cubo.lsp";
	char buffer[255];
	char *str=new char[8];
			
	//Codigo de Alfredo
	std::string sAux;
	char centro[8];
	//Centro ? **********
	double _points[1][4];
	points[0].copyTo(_points[0]);
	sAux = "";
	for (int k=0;k<3;k++)
	sAux = sAux + convertToString<int >((int)(points[k][0]/ points[k][3])) + " ";
	strcpy(centro,sAux.c_str());
			

	FILE *fp;

	fp = fopen(nombreArchivo, "a");   /* Abrir archivo para escritura */

	strcpy(buffer,"");
	strcat(buffer,"(COMMAND \"_.BOX\" \"_C\" ");	
	strcat(buffer,"'(");
	strcat(buffer,centro);
	strcat(buffer,")");
	strcat(buffer," \"_LENGTH\"");
	strcat(buffer," ");
	sprintf(str, "%.6f", this->getLength());//largo 	
	strcat(buffer,str);
	strcat(buffer," ");
	sprintf(str, "%.6f", this->getWidth()); // ancho 
	strcat(buffer,str);
	strcat(buffer," ");
	sprintf(str, "%.6f", this->getHeight());  //alto
	strcat(buffer,str);
	strcat(buffer,")\n");
	strcat(buffer,"(SETQ ");
	strcat(buffer,name);
	strcat(buffer," (ENTLAST))\n");

	fprintf(fp, buffer);
	//(COMMAND "_.BOX" "_C" '(0 0 0) "_LENGTH" 1000.000000 1000.000000 1000.000000)

	strcpy(buffer,"");
	strcat(buffer,"(COMMAND \"_.BOX\" \"_C\" ");	
	strcat(buffer,"'(");
	strcat(buffer,centro);
	strcat(buffer,")");
	strcat(buffer," \"_LENGTH\"");
	strcat(buffer," ");
	sprintf(str, "%.6f", this->getLength());//largo 	
	strcat(buffer,str);
	strcat(buffer," ");
	sprintf(str, "%.6f", this->getInnerWidth()); // ancho 
	strcat(buffer,str);
	strcat(buffer," ");
	sprintf(str, "%.6f", this->getInnerHeight());  //alto
	strcat(buffer,str);
	strcat(buffer,")\n");
	strcat(buffer,"(SETQ" );
	strcat(buffer,name);
	strcat(buffer,"c2 (ENTLAST))\n");
	fprintf(fp, buffer);

	strcpy(buffer,"");
	strcat(buffer,"(COMMAND \"_.SUBTRACT\"");
	strcat(buffer,this->getName());
	strcat(buffer," \"\" ");
	strcat(buffer,name);
	strcat(buffer,"c2 \"\")\n");
	fprintf(fp, buffer);
	if(!strcmp(this->rotar.c_str(),""))
		fprintf(fp,this->rotar.c_str());	
	if(!strcmp(this->posi.c_str(),""))
		fprintf(fp,this->posi.c_str());							

	fclose(fp);    /* Cerrar el archivo antes de terminar el programa */				
}

/*void gapi::GHollowBox::Export()
{
for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->Export();
	if(!hidden)
		this->doExport();
}*/

char* gapi::GHollowBox::getName()
{
	return name;
}

void gapi::GHollowBox::setName(char *data)
{
	name=data;
}

#ifdef USE_GAPI_GRAPHICS
void gapi::GHollowBox::doDraw(void){
	if(!visible)
		return;
	GLenum displayType=this->displayType==WIRED?GL_LINE_LOOP:GL_TRIANGLE_FAN;
	static int faces[6][4] =
	{
		{1, 2, 3, 4},
		{4, 3, 7, 8},
		{8, 7, 6, 5},
		{5, 6, 2, 1},
		{6, 7, 3, 2},
		{8, 5, 1, 4}
	};
		for (int i = 5; i >= 0; i--) if(i!=1 && i!=3){
			glBegin(displayType);
			glNormal3dv(normals[i].getData());
			glVertex3dv(points[faces[i][0]].getData());
			glVertex3dv(points[faces[i][1]].getData());
			glVertex3dv(points[faces[i][2]].getData());
			glVertex3dv(points[faces[i][3]].getData());
			glEnd();
		}

		for (int i = 5; i >= 0; i--) if(i!=1 && i!=3){
			glBegin(displayType);
			glNormal3dv(innerNormals[i].getData());
			glVertex3dv(innerPoints[faces[i][3]].getData());
			glVertex3dv(innerPoints[faces[i][2]].getData());
			glVertex3dv(innerPoints[faces[i][1]].getData());
			glVertex3dv(innerPoints[faces[i][0]].getData());
			glEnd();
		}
		//rear face
		if(displayType==GL_TRIANGLE_FAN)
		{
			glBegin(displayType);
				glNormal3dv(normals[1].getData());
				glVertex3dv(points[7].getData());
				glVertex3dv(innerPoints[3].getData());
				glVertex3dv(points[3].getData());

				glVertex3dv(points[7].getData());
				glVertex3dv(innerPoints[7].getData());
				glVertex3dv(innerPoints[3].getData());

				glVertex3dv(points[7].getData());
				glVertex3dv(innerPoints[8].getData());
				glVertex3dv(innerPoints[7].getData());

				glVertex3dv(points[7].getData());
				glVertex3dv(points[8].getData());
				glVertex3dv(innerPoints[8].getData());
			glEnd();
			glBegin(displayType);
				glNormal3dv(normals[1].getData());
				glVertex3dv(points[4].getData());
				glVertex3dv(points[3].getData());
				glVertex3dv(innerPoints[3].getData());

				glVertex3dv(points[4].getData());
				glVertex3dv(innerPoints[3].getData());
				glVertex3dv(innerPoints[4].getData());

				glVertex3dv(points[4].getData());
				glVertex3dv(innerPoints[4].getData());
				glVertex3dv(innerPoints[8].getData());

				glVertex3dv(points[4].getData());
				glVertex3dv(innerPoints[8].getData());
				glVertex3dv(points[8].getData());
			glEnd();
		}

		//front face
		if(displayType==GL_TRIANGLE_FAN)
		{
			glBegin(displayType);
				glNormal3dv(normals[3].getData());
				glVertex3dv(points[2].getData());
				glVertex3dv(innerPoints[6].getData());
				glVertex3dv(points[6].getData());

				glVertex3dv(points[2].getData());
				glVertex3dv(innerPoints[2].getData());
				glVertex3dv(innerPoints[6].getData());

				glVertex3dv(points[2].getData());
				glVertex3dv(innerPoints[1].getData());
				glVertex3dv(innerPoints[2].getData());

				glVertex3dv(points[2].getData());
				glVertex3dv(points[1].getData());
				glVertex3dv(innerPoints[1].getData());
			glEnd();
			glBegin(displayType);
			glNormal3dv(normals[3].getData());
				glVertex3dv(points[5].getData());
				glVertex3dv(points[6].getData());
				glVertex3dv(innerPoints[6].getData());

				glVertex3dv(points[5].getData());
				glVertex3dv(innerPoints[6].getData());
				glVertex3dv(innerPoints[5].getData());

				glVertex3dv(points[5].getData());
				glVertex3dv(innerPoints[5].getData());
				glVertex3dv(innerPoints[1].getData());

				glVertex3dv(points[5].getData());
				glVertex3dv(innerPoints[1].getData());
				glVertex3dv(points[1].getData());
			glEnd();
		}

}
#else
void gapi::GHollowBox::doDraw(void){
	std::cerr<<"Object: GHollowBox"<<std::endl;
}
#endif

