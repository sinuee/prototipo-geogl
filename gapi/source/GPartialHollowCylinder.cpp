/*
 * GPartialHollowCylinder.cpp
 *
 *  Created on: Dec 15, 2008
 *      Author: khayyam
 */
#include "GPartialHollowCylinder.h"
#include "GMacros.h"
#include "GPrimitives.h"


gapi::GPartialHollowCylinder::GPartialHollowCylinder():GHollowCylinder(){
	this->startAngle=0;
	this->sweepAngle=M_PI;
	parent=NULL;
}
gapi::GPartialHollowCylinder::GPartialHollowCylinder(GBox &parent):GHollowCylinder(parent){
	this->startAngle=0;
	this->sweepAngle=M_PI;
}

gapi::GPartialHollowCylinder::~GPartialHollowCylinder(){

}

void gapi::GPartialHollowCylinder::setStartAngle(double angle){
	this->startAngle=angle;
}

void gapi::GPartialHollowCylinder::setSweepAngle(double angle){
	this->sweepAngle=angle;
}

double gapi::GPartialHollowCylinder::getStartAngle(void){
	return startAngle;
}

double gapi::GPartialHollowCylinder::getSweepAngle(void){
	return sweepAngle;
}



//void gapi::GPartialHollowCylinder::Export()
//{
//for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
//		(*it)->Export();
//	if(!hidden)
//		this->doExport();
//}

char *gapi::GPartialHollowCylinder::getName()
{
	return name;
}

void gapi::GPartialHollowCylinder::setName(char *data)
{
	name=data;
}

#ifdef USE_GAPI_GRAPHICS
void gapi::GPartialHollowCylinder::doDraw(void){
	if(!visible)
		return;
	GLenum displayType=this->displayType==WIRED?GL_LINE_LOOP:GL_TRIANGLE_FAN;

	Vector desp=normals[BOTTOM]*(height*0.5);

	glPushMatrix();
	//glTranslated(desp[0], desp[1], desp[2]);
	glMultMatrixd(T.getData());
	drawPartialHollowCylinder(this->innerRadius, this->radius, this->height, startAngle, sweepAngle, slices, displayType);
	glPopMatrix();
}
void gapi::GPartialHollowCylinder::doExport(void)
{
	char *str=new char[15];	
	char centro[40];
	char valor[80],cil2[90],suba[30],subb[30],subc[50],cubo[100],sub2b[30],sub2c[50];
	//Vector desp=normals[BOTTOM]*(height*0.5);
	double flt,p1c[3],p2c[3];			
	//centro se utiliza para generar los cambios de variables 
	std::string sAux;
	//Centro ? **********
	double _points[1][4], radioint;//,paso[3];	
	points[0].copyTo(_points[0]);
	sAux = "";
	for (int k=0;k<3;k++)
			sAux = sAux +  convertToString<int >((int)(points[k][0]/ points[k][3])) + " ";
		
	strcpy(centro,sAux.c_str());
	/*strcpy(centro, "");	
	flt=desp[0];	
	sprintf(str, "%.6g",flt);
	strcat(centro,str);
	strcat(centro," ");
	flt=desp[1];
	sprintf(str, "%.6g",flt);
	strcat(centro,str);
	strcat(centro," ");
	flt=desp[2];
	sprintf(str, "%.6g",flt);
	strcat(centro,str);*/
	//se hace un cuadro el cual quitara la parte que no queremos del cilindro hueco
	//se calculan sus puntos contrapuestos correspondientes a el cilindro
	 /*paso[0]=_points[1][0]/ _points[1][3];
	  paso[1]=_points[1][1]/ _points[1][3];
	   paso[2]=_points[1][2]/ _points[1][3];*/
	p1c[0]=0;//this->radius/2;
	p1c[1]=-this->radius;
	p1c[2]=0;//this->length/2;

	p2c[0]=this->radius;
	p2c[1]=this->radius;
	p2c[2]=this->height;

	/*p1c[0]=this->radius/2;
	p1c[1]=0;
	p1c[2]=this->length/2;*/

	strcpy(valor,"");
	strcat(valor,"(COMMAND \"_.CYLINDER\" '(");	
	strcat(valor,centro);
	strcat(valor, ")");
	strcpy(cil2,"");
	strcpy(cil2,valor);	
	sprintf(str, " %.6g",this->radius);
	strcat(valor,str);	
	//strcat(valor, " \"_TOP\"");	strcat(valor,str);	
	sprintf(str, " %.6g",this->height);
	strcat(valor,str);	
	strcat(valor,")");

	sprintf(str, " %.6g",this->getIneerRadius());
	strcat(cil2,str);	
	//strcat(cil2, " \"_TOP\"");	strcat(cil2,str);	
	sprintf(str, " %.6g",this->height);
	strcat(cil2,str);	
	strcat(cil2,")");

	strcpy(suba,"");
	strcat(suba,"(SETQ ");
	strcat(suba,name);
	strcat(suba," (ENTLAST))");
	strcpy(subb,"");
	strcat(subb,"(SETQ ");
	strcat(subb,name);
	strcat(subb,"c1 (ENTLAST))");
	strcpy(subc,"");
	strcat(subc,"(COMMAND \"_.SUBTRACT\" ");
	strcat(subc,name);
	strcat(subc," \"\" ");
	strcat(subc,name);
	strcat(subc,"c1 \"\")");

	strcpy(centro, "");	
	flt=p1c[0];	
	sprintf(str, "%.6g",flt);
	strcat(centro,str);
	strcat(centro," ");
	flt=p1c[1];
	sprintf(str, " %.6g",flt);
	strcat(centro,str);
	strcat(centro," ");
	flt=p1c[2];
	sprintf(str, " %.6g)",flt);
	strcat(centro,str);
	//se genera el cubo
	strcpy(cubo,"");        
	strcat(cubo,"(COMMAND \"_.BOX\" '(");
	//strcat(cubo,"(COMMAND \"_.BOX\" \"C\" '(");
	
	strcat(cubo,centro);
	//strcat(cubo," \"_LENGTH\"");
	
		    
	strcpy(centro, "");	
	flt=p2c[0];	
	sprintf(str, " '(%.6g",flt);
	strcat(centro,str);
	strcat(centro," ");
	flt=p2c[1];
	sprintf(str, " %.6g",flt);
	strcat(centro,str);
	strcat(centro," ");
	flt=p2c[2];
	sprintf(str, " %.6g)",flt);
	strcat(centro,str);
	strcat(cubo,centro);
	strcat(cubo,")");

	/*strcpy(centro, "");	
	flt=this->radius;	
	sprintf(str, " %.6g",flt);
	strcat(centro,str);
	strcat(centro," ");
	flt=this->radius*2;
	sprintf(str, " %.6g",flt);
	strcat(centro,str);
	strcat(centro," ");
	flt=this->height*2;
	sprintf(str, " %.6g",flt);
	strcat(centro,str);
	strcat(cubo,centro);
	strcat(cubo,")");*/

	//se realiza la subtract del nuevo cubo	
	strcpy(sub2b,"");
	strcat(sub2b,"(SETQ ");
	strcat(sub2b,name);
	strcat(sub2b,"c3 (ENTLAST))");
	strcpy(sub2c,"");
	strcat(sub2c,"(COMMAND \"_.SUBTRACT\" ");
	strcat(sub2c,name);
	strcat(sub2c,"  \"\" ");
	strcat(sub2c,name);
	strcat(sub2c,"c3 \"\")");
			
	if(this->radius!=0&&this->innerRadius!=0)
		{		
	std::string pcNombreArch1 = "d:\\cubo.lsp";
	FILE *pArchivo1;			
	pArchivo1 = fopen(pcNombreArch1.c_str(),"a");
	fprintf(pArchivo1,"%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n",valor,suba,cil2,subb,subc,cubo,sub2b,sub2c);					
	if(strcmp(this->rotar.c_str(),"") != 0)
		fprintf(pArchivo1,"%s\n",this->rotar.c_str());	
	if(strcmp(this->posi.c_str(),"") != 0)
		fprintf(pArchivo1,"%s\n",this->posi.c_str());
	fclose(pArchivo1);
	}
}
#else
void gapi::GPartialHollowCylinder::doDraw(void){
	if(!visible)
		return;
	std::cerr<<"Object: GPartialHollowCylinder"<<std::endl;
}
void gapi::GPartialHollowCylinder::doExport(){
}
#endif

