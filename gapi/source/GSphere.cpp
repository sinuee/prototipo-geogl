/*
 * GSphere.cpp
 *
 *  Created on: Dec 11, 2008
 *      Author: khayyam
 */
#include "GSphere.h"
#include "GMacros.h"
#include <iostream>
#include <fstream>
#include <string>
//#include <stdio>

float x[4];
gapi::GSphere::GSphere():GBox(){
	slices=20;
	stacks=20;
	radius=MIN(MIN(width, length), height)*0.5;
	width=length=height=radius;
}

gapi::GSphere::GSphere(GBox &parent):GBox(parent){
	slices=20;
	stacks=20;
	radius=MIN(MIN(width, length), height)*0.5;
	width=length=height=radius;
}

gapi::GSphere::~GSphere(){

}

void gapi::GSphere::setRadius(double radius){
	this->radius=radius;
	width=length=height=2*radius;
}

void gapi::GSphere::doExport(void){	
	
	char *str=new char[8];	
	char centro[15],valor[60],suba[30];	
	double flt;
	Vector desp=normals[BOTTOM]*(height*0.5);
	std::string sAux;
	//Centro ? **********
	double _points[1][4], radioint;	
	points[0].copyTo(_points[0]);
	sAux = "";
	for (int k=0;k<3;k++)
		sAux = sAux +  convertToString<int >((int)(points[k][0]/ points[k][3])) + " ";
	strcpy(centro,sAux.c_str());	
	strcpy(suba,"");
	strcat(suba,"(SETQ ");
	strcat(suba,this->getName());
	strcat(suba," (ENTLAST))");

	/*strcpy(centro, "");
	flt=desp[0];
	sprintf(str, "%.6g",flt);
	strcat(centro,str);
	strcat(centro," ");
	flt=desp[1];
	sprintf(str, "%.6g",flt);
	strcat(centro,str);
	strcat(centro," ");
	flt=desp[2];
	sprintf(str, "%.6g",flt);
	strcat(centro,str);*/

	strcpy(valor,"");
	strcat(valor,"(COMMAND \"_.SPHERE\" '(");	
	strcat(valor,centro);
			
	sprintf(str, ") %.6g",this->radius);
	strcat(valor,str);			
	strcat(valor,")");

	//std::string pcNombreArch1 = "d:\\esfera.lsp";
	if(this->radius!=0)
		{
	std::string pcNombreArch1 = "d:\\cubo.lsp";

	FILE *pArchivo1;			
    pArchivo1 = fopen(pcNombreArch1.c_str(),"a");
	fprintf(pArchivo1,"%s\n%s\n",valor,suba);	
	if(strcmp(this->rotar.c_str(),""))
		fprintf(pArchivo1,"%s\n",this->rotar.c_str());	
	if(strcmp(this->posi.c_str(),""))
		fprintf(pArchivo1,"%s\n",this->posi.c_str());
	fclose(pArchivo1);
	}
}

/*void gapi::GSphere::Export()
{
	for(std::set<GBox*>::iterator it=parts.begin(); it!=parts.end();++it)
		(*it)->Export();		
	if(!hidden)
		this->doExport();
}*/

char* gapi::GSphere::getName()
{
	return name;
}

void gapi::GSphere::setName(char *data)
{
	name = data;
//	delete data;
}

#ifdef USE_GAPI_GRAPHICS
void gapi::GSphere::doDraw(void){
	if(!visible)
		return;
	GLUquadricObj *quadObj=gluNewQuadric();
	gluQuadricDrawStyle(quadObj, ((displayType==WIRED)?GLU_LINE:GLU_FILL));
	glPushMatrix();
	Vector desp=normals[BOTTOM]*(height*0.5);

	//glTranslated(desp[0], desp[1], desp[2]);
	glMultMatrixd(T.getData());
	gluQuadricOrientation(quadObj, GLU_OUTSIDE);
	gluSphere(quadObj, radius, slices, stacks);
	glPopMatrix();
	gluDeleteQuadric(quadObj);
}
#else
void gapi::GSphere::doDraw(void){
	std::cerr<<"Object: GSphere"<<std::endl;
}
#endif