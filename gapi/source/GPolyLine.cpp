/*
 * GPolyLine.cpp
 *
 *  Created on: Jan 4, 2009
 *      Author: khayyam
 */
#include <iostream>
#include "GPolyLine.h"

gapi::GPolyLine::GPolyLine(GBox &parent):GBox(parent){

}

gapi::GPolyLine::~GPolyLine(){

}



void gapi::GPolyLine::addPoint(const Point &point){
	points.push_back(point);
	if(points.size()<=1)
		return;
	L.push_back(GPolyLineSegment());
	normals.push_back(GPolyLineSegmentNormals());
	double halfWidth=lineWidth*0.5;
	double halfThick=lineThickness*0.5;
	int numPoints=points.size();

	for(int i=1;i<numPoints;i++){
		Point center;
		Point nl;
		Point nh;
		center=(points[i-1]+points[i])/2.0;
		nl=points[i]-points[i-1];
		nl/=nl.norm();
		nh=nl^nw;
		double halfLen=points[i-1].distance(points[i])*0.5;

		//vertices
		(center-(nl*halfLen)-(nw*halfWidth)-(nh*halfThick)).copyTo(L[i-1].points[0]);
		(center-(nl*halfLen)-(nw*halfWidth)+(nh*halfThick)).copyTo(L[i-1].points[1]);
		(center-(nl*halfLen)+(nw*halfWidth)+(nh*halfThick)).copyTo(L[i-1].points[2]);
		(center-(nl*halfLen)+(nw*halfWidth)-(nh*halfThick)).copyTo(L[i-1].points[3]);

		(center+(nl*halfLen)-(nw*halfWidth)-(nh*halfThick)).copyTo(L[i-1].points[4]);
		(center+(nl*halfLen)-(nw*halfWidth)+(nh*halfThick)).copyTo(L[i-1].points[5]);
		(center+(nl*halfLen)+(nw*halfWidth)+(nh*halfThick)).copyTo(L[i-1].points[6]);
		(center+(nl*halfLen)+(nw*halfWidth)-(nh*halfThick)).copyTo(L[i-1].points[7]);
		//normals
		(-nl).copyTo(normals[i-1].normals[0]);
		nw.copyTo(normals[i-1].normals[1]);
		nl.copyTo(normals[i-1].normals[2]);
		(-nw).copyTo(normals[i-1].normals[3]);
		nh.copyTo(normals[i-1].normals[4]);
		(-nh).copyTo(normals[i-1].normals[5]);
	}

}

void gapi::GPolyLine::setNw(Vector &nw){
	this->nw=nw;
}

void gapi::GPolyLine::setLineWidth(double width){
	this->lineWidth=width;

}

void gapi::GPolyLine::setLineThickness(double thickness){
	this->lineThickness=thickness;
}

#ifdef USE_GAPI_GRAPHICS
void gapi::GPolyLine::doDraw(void){
	static int faces[6][4] =
		{
			{0, 1, 2, 3},
			{3, 2, 6, 7},
			{7, 6, 5, 4},
			{4, 5, 1, 0},
			{5, 6, 2, 1},
			{7, 4, 0, 3}
		};
	for(unsigned i=0;i<L.size();i++){
		for (int j = 5; j >= 0; j--) {
			glBegin(GL_TRIANGLE_FAN);
				glNormal3dv(normals[i].normals[j]);
				glVertex3dv(L[i].points[faces[j][0]]);
				glVertex3dv(L[i].points[faces[j][1]]);
				glVertex3dv(L[i].points[faces[j][2]]);
				glVertex3dv(L[i].points[faces[j][3]]);
			glEnd();
		}
	}
}
#else
void doDraw(void){
	std::cout<<"Object: GPolyLine"<<std::endl;
}
#endif
