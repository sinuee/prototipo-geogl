#ifndef EJEMPLOGHollowTruncatedCone_H_
#define EJEMPLOGHollowTruncatedCone_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
namespace NSTransformador{
	using namespace Hero;
	class EjemploGHollowTruncatedCone: public GBox{
		public:
			//Declaración del objeto tipo Caja
			GHollowTruncatedCone *ConoTruncadoHueco;
			//Declaración de variables que se usaran en esta clase
		double dVertical;
		double dRadio;
		double dRadioSuperior;
		double dRadioInterior;
		double dRadioSuperiorInterior;

			//Crea las formas que se desen dibujar
			EjemploGHollowTruncatedCone(GBox &parent):GBox(parent){
				createPart(ConoTruncadoHueco, GHollowTruncatedCone);
			//Declaración de variables default de la clase
				dVertical = millimeter(200);
				dRadio = millimeter(100);
				dRadioSuperior = millimeter(20);
				dRadioInterior = millimeter(40);
				dRadioSuperiorInterior = millimeter(5);

				//oculta el contraste de las dimenciones
				this->setHidden(true);
				// oculta el borde de la figura
				this->setBindParts(true);
				actualizarDimensiones();
			}

			void actualizarDimensiones(){
				// Dimención de la figura (Limites)
				this->setWidth(dRadio *2);
				this->setHeight(dVertical);
				this->setLength(dRadio*2);
			}
			void defSelf(void){
			}

			void asignarDimensiones(const GInputs &inputs){
				// Son variables que se envian a un txt
				this->dRadio=inputs.getReal("Radio",dRadio);
				this->dVertical=inputs.getReal("Vertical",dVertical);
				this->dRadioSuperior=inputs.getReal("RadioSuperior",dRadioSuperior);
				this->dRadioInterior=inputs.getReal("RadioInterior",dRadioInterior);
				this->dRadioSuperiorInterior=inputs.getReal("RadioSuperiorInterior",dRadioSuperiorInterior);


				actualizarDimensiones();
			}
			void obtenerDimensiones(GInputs &inputs){
				// Son variables que se obtienen de un txt
				inputs.addInput<double>("Radio",dRadio);
				inputs.addInput<double>("Vertical",dVertical);
				inputs.addInput<double>("RadioSuperior",dRadioSuperior);
				inputs.addInput<double>("RadioInterior",dRadioInterior);
				inputs.addInput<double>("RadioSuperiorInterior",dRadioSuperiorInterior);


			}
			void defParts(void){
				actualizarDimensiones();
				// Espesificaciones de la figura, esto pueden ser:
				// width,Length, Height, position,rotate ...
				ConoTruncadoHueco->beginDefinition();
					ConoTruncadoHueco->setRadius(dRadio);
					ConoTruncadoHueco->setHeight(dVertical);
					ConoTruncadoHueco->setTopRadius(dRadioSuperior);
					ConoTruncadoHueco->setInnerRadius(dRadioInterior);
					ConoTruncadoHueco->setTopInnerRadius(dRadioSuperiorInterior);

	

				ConoTruncadoHueco->endDefinition();


			}
	};
}

#endif
