#ifndef SOPORTEDEANGULO_H_
#define SOPORTEDEANGULO_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "Definiciones.h"
namespace NSTransformador{
	using namespace Hero;
	class SoporteDeAngulo: public GBox{
		public:
			GBox *Parte2;
			GBox *Parte3;

			enum TipoArticuloHerraje tArt;
			double Espesor;
			double Width;
			double Length;
			double Height;


			SoporteDeAngulo(GBox &parent):GBox(parent){
				createPart(Parte2, GBox);
				createPart(Parte3, GBox);
				tArt =UF24;
				

				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}
			void asignarArticulo(enum TipoArticuloHerraje tArt){
				this-> tArt = tArt;
				actualizarDimensiones();
			}
			void actualizarDimensiones(){


				
					if(tArt == UF97){
						Width = millimeter(63.5);
						Length = millimeter(63.5);
						Height = millimeter(120);
						Espesor = millimeter(6.35);
					}
					else if((tArt == UF24) || (tArt == UE06)){
						Width = millimeter(63.5);
						Length = millimeter(63.5);
						Height = millimeter(40);
							if((tArt == UF08) || (tArt == EX85))
								Espesor = millimeter(6.36);
							else
								Espesor = millimeter(6.35);

					}
					else if((tArt == EX00) || (tArt == A515)){
						Width = millimeter(38.1);
						Length = millimeter(38.1);
						Height = millimeter(38.1);
						Espesor = millimeter(4.76);
					}
					else if(tArt == UE35){
						Width = millimeter(38.1);
						Length = millimeter(15);
						Height = millimeter(39);
						Espesor = millimeter(6.35);
					}
					else{
						Width = millimeter(63.5);
						Length = millimeter(63.5);
						Height = millimeter(60);
						Espesor = millimeter(6.35);
					}


				this->setWidth(Width);
				this->setHeight(Height);
				this->setLength(Length);
			}
			void defSelf(void){
			}

			void asignarDimensiones(const GInputs &inputs){
				this->Width=inputs.getReal("Width",Width);
				this->Length=inputs.getReal("Length",Length);
				this->Height=inputs.getReal("Height",Height);
				this->Espesor=inputs.getReal("Espesor", Espesor);

				actualizarDimensiones();
			}
			void obtenerDimensiones(GInputs &inputs){
				inputs.addInput<double>("Width", Width);
				inputs.addInput<double>("Length", Length);
				inputs.addInput<double>("Height", Height);
				inputs.addInput<double>("Espesor", Espesor);

			}
			void defParts(void){
				actualizarDimensiones();

				Parte2->beginDefinition();
					Parte2->setWidth(Espesor);
					Parte2->position(LEFT, 0);
				Parte2->endDefinition();

				Parte3->beginDefinition();
					Parte3->setLength(Espesor);
					Parte3->position(REAR,0);
				Parte3->endDefinition();





			}
	};
}

#endif
