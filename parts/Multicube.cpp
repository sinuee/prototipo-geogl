#include "GBox.h"
#include "GMacros.h"
#include <iostream>
using namespace gapi;
using namespace std;
class MultiCube:public GBox{
	public:
		GBox* boxes[9];
		MultiCube(GBox &parent):GBox(parent){
			boxes[0]=this;
			createPart(boxes[BOTTOMLEFTFRONT], GBox);
			createPart(boxes[TOPLEFTFRONT], GBox);
			createPart(boxes[TOPLEFTREAR], GBox);
			createPart(boxes[BOTTOMLEFTREAR], GBox);
			createPart(boxes[BOTTOMRIGHTFRONT], GBox);
			createPart(boxes[TOPRIGHTFRONT], GBox);
			createPart(boxes[TOPRIGHTREAR], GBox);
			createPart(boxes[BOTTOMRIGHTREAR], GBox);
		}
		void defParts()
		{
			for(int i=1;i<=8;i++)
			{
				boxes[i]->beginDefinition();
					boxes[i]->setDimmensions(boxes[0]->getLength()*0.5, boxes[0]->getWidth()*0.5, boxes[0]->getHeight()*0.5);
					Vector dir=boxes[0]->getPoint(EBoxPoint(i))-boxes[i]->getPoint(CENTER);
					boxes[i]->translate(dir);
				boxes[i]->endDefinition();
			}
		}

		int main(int argc, char* argv[])
		{
			cout<<"multi-cube with 8 sub-cubes\n";
			return 0;
		}
};


void multiCubeDemo(GBox *&root)
	{
		MultiCube *subMultiCube=NULL;
		MultiCube *multiCube=NULL;
		createRoot(root, GBox);
		root->beginDefinition();
			//root->rotateX(angle);
			//root->rotateY(angle);
			//root->rotateZ(angle);
			root->setDimmensions(2,2,2);
		root->endDefinition();

		createChild(multiCube, MultiCube, *root);
		multiCube->beginDefinition();
		multiCube->endDefinition();

		createChild(subMultiCube, MultiCube, (root->getPart(0).getPart(0)));
		subMultiCube->beginDefinition();
		subMultiCube->endDefinition();
	}
