/*
 * Canal.h
 *
 *  Created on: Jan 5, 2009
 *      Author: khayyam
 */

#ifndef CANAL_H_
#define CANAL_H_
#include "GBox.h"
#include "GPartialHollowCylinder.h"
namespace NSTransformador{
	using namespace Hero;
	class Canal:public GBox{
		public:
			GBox *laminaIzquierda;
			GBox *laminaDerecha;
			GBox *laminaInferior;
			GPartialHollowCylinder *esquinaIzquierda;
			GPartialHollowCylinder *esquinaDerecha;
			double espesor;
			double anchoInterior;
			double alturaCanal;
			double longitudCanal;
			Canal(GBox &parent):GBox(parent){
				createPart(laminaIzquierda, GBox);
				createPart(laminaDerecha, GBox);
				createPart(laminaInferior, GBox);
				createPart(esquinaIzquierda, GPartialHollowCylinder);
				createPart(esquinaDerecha, GPartialHollowCylinder);
				espesor=inch(1);
				longitudCanal=feet(4);
				anchoInterior=feet(1);
				alturaCanal=feet(0.25);
				actualizarDimensiones();
				this->setBindParts(true);
				this->setHidden(true);

			}

			void actualizarDimensiones(void){
				this->setWidth(2*espesor+anchoInterior);
				this->setHeight(alturaCanal);
				this->setLength(longitudCanal);
			}

			void asignarDimensiones(const GInputs &inputs){
				this->espesor=inputs.getReal("espesor",espesor);
				this->anchoInterior=inputs.getReal("anchoInterior",anchoInterior);
				this->alturaCanal=inputs.getReal("alturaCanal",alturaCanal);
				this->longitudCanal=inputs.getReal("longitudCanal", longitudCanal);
				actualizarDimensiones();
			}

			void obtenerDimensiones(GInputs &inputs){
				inputs.addInput<double>("espesor",espesor);
				inputs.addInput<double>("anchoInterior",anchoInterior);
				inputs.addInput<double>("alturaCanal",alturaCanal);
				inputs.addInput<double>("longitudCanal",longitudCanal);
			}

			void defParts(void){
				actualizarDimensiones();
				laminaIzquierda->beginDefinition();
					laminaIzquierda->setHeight(alturaCanal-espesor);
					laminaIzquierda->setWidth(espesor);
					laminaIzquierda->setLength(longitudCanal);
					laminaIzquierda->position(LEFT,0);
					laminaIzquierda->position(TOP,0);
				laminaIzquierda->endDefinition();

				laminaDerecha->beginDefinition();
					laminaDerecha->setHeight(alturaCanal-espesor);
					laminaDerecha->setWidth(espesor);
					laminaDerecha->setLength(longitudCanal);
					laminaDerecha->position(RIGHT,0);
					laminaDerecha->position(TOP,0);
				laminaDerecha->endDefinition();

				laminaInferior->beginDefinition();
					laminaInferior->setHeight(espesor);
					laminaInferior->setWidth(anchoInterior);
					laminaInferior->setLength(longitudCanal);
					laminaInferior->position(BOTTOM,0);
				laminaInferior->endDefinition();

				esquinaIzquierda->beginDefinition();
					esquinaIzquierda->setInnerRadius(0);
					esquinaIzquierda->setRadius(espesor);
					esquinaIzquierda->setHeight(longitudCanal);
					esquinaIzquierda->rotate(RIGHT,DEGREES(90));
					esquinaIzquierda->position(LEFT,0);
					esquinaIzquierda->position(BOTTOM,0);
					esquinaIzquierda->setStartAngle(DEGREES(180));
					esquinaIzquierda->setSweepAngle(DEGREES(90));
				esquinaIzquierda->endDefinition();

				esquinaDerecha->beginDefinition();
					esquinaDerecha->setInnerRadius(0);
					esquinaDerecha->setRadius(espesor);
					esquinaDerecha->setHeight(longitudCanal);
					esquinaDerecha->rotate(RIGHT,DEGREES(90));
					esquinaDerecha->position(RIGHT,0);
					esquinaDerecha->position(BOTTOM,0);
					esquinaDerecha->setStartAngle(DEGREES(270));
					esquinaDerecha->setSweepAngle(DEGREES(90));
				esquinaDerecha->endDefinition();
			}
	};
}

#endif /* CANAL_H_ */
