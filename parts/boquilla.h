#ifndef BOQUILLA_H_
#define BOQUILLA_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "Boq.h"
#include "Conector.h"
#include "Definiciones.h"

namespace NSTransformador{
	using namespace Hero;
	class Boquilla: public GBox{
		public:
			Conector *conector;
			Boq *boq;
			GBox *tanque ;

double DiametroCola;
double Largocola;
double DimC;
double DimA;
double DimD;
double DimB;
double DiametroCampana;
double AlturaCampana;
enum TipoArticulo tArt;
enum TipoConector tConector;
double d_DatosConector[4];
double PosicionaConector;

			Boquilla(GBox &parent):GBox(parent){
				createPart(conector, Conector);
				createPart(boq, Boq);
				//createPart(tanque, GBox);


//S2715

AlturaCampana = millimeter(70);
tArt = CH76;//F704;
tConector = PERNO1;
DiametroCampana = millimeter(80);
DiametroCola = millimeter(70);
DimA = millimeter(0);
DimB = millimeter(0);
DimC = millimeter(0);
DimD = millimeter(0);
Largocola = millimeter(120);

//SC1236
/*
AlturaCampana = millimeter(60);
tArt = CU25;
tConector = ZAPATA;
DiametroCampana = millimeter(106);
DiametroCola = millimeter(68);
DimA = millimeter(0);
DimB = millimeter(0);
DimC = millimeter(0);
DimD = millimeter(0);
Largocola = millimeter(68);
*/

/*
AlturaCampana = millimeter(153);
tArt = VZ30;
tConector = ZAPATA;
DiametroCampana = millimeter(120);
DiametroCola = millimeter(52);
DimA = millimeter(25);
DimB = millimeter(50);
DimC = millimeter(86);
DimD = millimeter(50);
Largocola = millimeter(130);
*/
/*
AlturaCampana = millimeter(165);
tArt = CH76;//E191;
tConector = CLEMAAT1;
DiametroCampana = millimeter(120);
DiametroCola = millimeter(76.2);
DimA = millimeter(25);
DimB = millimeter(50);
DimC = millimeter(124);
DimD = millimeter(76.2);
Largocola = millimeter(350);
*/


	d_DatosConector[0] = 0;
	d_DatosConector[1] = 0;
	d_DatosConector[2] = 0;
	d_DatosConector[3] = 0;
double Length = Largocola + AlturaCampana + millimeter(44.75);

if(tArt == EM29)
	PosicionaConector = millimeter(31.7);
else if(tArt == CH77)
	PosicionaConector = millimeter(-27);
else if(tArt == CH76)
	PosicionaConector = millimeter(-84);
else if((tConector == PERNO1) || (tConector == PERNO2))
PosicionaConector =   millimeter(44.75);//(d_DatosConector[3] - Length);
else if(tConector == ZAPATA)
PosicionaConector = d_DatosConector[3];
else
	PosicionaConector = 0;


if(tConector == ZAPATA)
{
	d_DatosConector[0] = millimeter(355);
	d_DatosConector[1] = millimeter(46);
	d_DatosConector[2] = millimeter(89);
	d_DatosConector[3] = millimeter(129.5);
	PosicionaConector = d_DatosConector[3];
}
else if(tConector == CLEMAAT1)
{
	d_DatosConector[0] = millimeter(49);
	d_DatosConector[1] = millimeter(56);
	d_DatosConector[2] = millimeter(53.2);
	d_DatosConector[3] = millimeter(56);
	PosicionaConector = d_DatosConector[3];
}
else if(tConector == CLEMAAT2)
{
	d_DatosConector[0] = millimeter(46.5);
	d_DatosConector[1] = millimeter(69.2);
	d_DatosConector[2] = millimeter(45.5);
	d_DatosConector[3] = millimeter(69.2);
	PosicionaConector = d_DatosConector[3];
}
else if(tConector == ZAPATAB4)
{
	d_DatosConector[0] = millimeter(460.85);
	d_DatosConector[1] = millimeter(101.6);
	d_DatosConector[2] = millimeter(89);
	d_DatosConector[3] = millimeter(129.9);
	PosicionaConector = d_DatosConector[3];
}
else if(tConector == CLEMABT750U)
{
	d_DatosConector[0] = millimeter(87.5);
	d_DatosConector[1] = millimeter(45);
	d_DatosConector[2] = millimeter(251.9);
	d_DatosConector[3] = millimeter(83);
	PosicionaConector = d_DatosConector[3];
}



				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}

			void actualizarDimensiones(){
				this->setWidth(.3);
				this->setHeight(.7);
				this->setLength(.3);
			}
			void defSelf(void){
			}

			void asignarDimensiones(const GInputs &inputs){
				this->DiametroCola=inputs.getReal("DiametroCola",DiametroCola);
				this->Largocola=inputs.getReal("Largocola",Largocola);
				this->DimA=inputs.getReal("DimA",DimA);
				this->DimB=inputs.getReal("DimB",DimB);
				this->DimC=inputs.getReal("DimC",DimC);
				this->DimD=inputs.getReal("DimD",DimD);
				this->DiametroCampana=inputs.getReal("DiametroCampana",DiametroCampana);
				this->AlturaCampana=inputs.getReal("AlturaCampana",AlturaCampana);
//				this->tArt=inputs.getReal("tArt",tArt);
				actualizarDimensiones();
			}
			void obtenerDimensiones(GInputs &inputs){
				inputs.addInput<double>("DiametroCola", DiametroCola);
				inputs.addInput<double>("Largocola", Largocola);
				inputs.addInput<double>("DimA", DimA);
				inputs.addInput<double>("DimB", DimB);
				inputs.addInput<double>("DimC", DimC);
				inputs.addInput<double>("DimD", DimD);
				inputs.addInput<double>("DiametroCampana", DiametroCampana);
				inputs.addInput<double>("AlturaCampana", AlturaCampana);
				//inputs.addInput<int>("tArt", tArt);

			}
			void defParts(void){
				actualizarDimensiones();

    			/*tanque->beginDefinition();
				tanque->setName("tanque");
					tanque->setWidth(0);
					tanque->setHeight(0);
					tanque->setLength(0);
					tanque->position(TOP, 0);
				tanque->endDefinition();
				tanque->setHidden(true);*/

				boq->beginDefinition();
				boq->setName("boq");
					boq->asignarAlturaCampana(AlturaCampana);
					boq->asignarDiametroCampana(DiametroCampana);
					boq->asignarDiametroCola(DiametroCola);
					boq->asignarDimA(DimA);
					boq->asignarDimB(DimB);
					boq->asignarDimC(DimC);
					boq->asignarDimD(DimD);
					boq->asignarLargocola(Largocola);
					boq->setHidden(true);
					boq->setBindParts(true);					
					boq->position(FRONT,0);
				    boq->rotate(RIGHT,DEGREES(180));				
					//boq->position(FRONT,- Largocola/2,*tanque);
				boq->endDefinition();

				conector->beginDefinition();
				conector->setName("conector1");
					conector->asignarHeight(d_DatosConector[1]);
					conector->asignarLargo(d_DatosConector[3]);
					conector->asignarLength(d_DatosConector[2]);
					conector->asignarWidth(d_DatosConector[0]);
					conector->asignarConector(tConector);
					conector->asignarArticulo(tArt);
					conector->setHidden(true);
					conector->setBindParts(true);
					conector->position(FRONT,-PosicionaConector,*boq); //PosicionaConector);
				conector->endDefinition();

			}
	};
}

#endif
