#ifndef BASE_H_
#define BASE_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "Boquilla.h"
namespace NSTransformador{
	using namespace Hero;
	class Base: public GBox{
		public:
			GBox *BaseSuperior;
			GBox *piernaIzquierda;
			GBox *piernaDerecha;
			GPartialHollowCylinder *esquinaIzquierda;
			GPartialHollowCylinder *esquinaDerecha;
			GBox *Herraje1;
			GBox *Herraje2;
			GBox *ApoyoPPalanqueo10;
			GBox *ApoyoPPalanqueo11;
			GBox *ApoyoPPalanqueo20;
			GBox *ApoyoPPalanqueo21;
			//Boquilla * Prueba;

			double EspesorBase;
			double AnchoBase;
			double LargoBase;
			double AlturaBase;
			double EspesorHerraje;
			double AnchoHerraje;
			double LargoHerraje;
			double EntreCentrosRanura;

			Base(GBox &parent):GBox(parent){
				createPart(BaseSuperior, GBox);
				createPart(piernaIzquierda, GBox);
				createPart(piernaDerecha, GBox);
				createPart(esquinaIzquierda, GPartialHollowCylinder);
			    createPart(esquinaDerecha, GPartialHollowCylinder);
				createPart(Herraje1, GBox);
				createPart(Herraje2, GBox);
				createPart(ApoyoPPalanqueo10, GBox);
				createPart(ApoyoPPalanqueo11, GBox);
				createPart(ApoyoPPalanqueo20, GBox);
			    createPart(ApoyoPPalanqueo21, GBox);
				//createPart(Prueba, Boquilla);



				EspesorBase=.0032;
				AnchoBase= .610;
				LargoBase=1.280;
				AlturaBase=.0817;

				EspesorHerraje=.0127;
				AnchoHerraje=.0762;
				LargoHerraje=.810;
				EntreCentrosRanura = .672;


				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}

			void actualizarDimensiones(){
				this->setWidth(2*EspesorBase + AnchoBase);
				this->setHeight(AlturaBase);
				this->setLength(LargoBase);
			}
			void defSelf(void){
			}

			void asignarDimensiones(const GInputs &inputs){
				this->EspesorBase=inputs.getReal("EspesorBase",EspesorBase);
				this->AnchoBase=inputs.getReal("AnchoBase",AnchoBase);
				this->LargoBase=inputs.getReal("LargoBase",LargoBase);
				this->AlturaBase=inputs.getReal("AlturaBase", AlturaBase);

				this->EspesorHerraje=inputs.getReal("EspesorHerraje",EspesorHerraje);
				this->AnchoHerraje=inputs.getReal("AnchoHerraje",AnchoHerraje);
				this->LargoHerraje=inputs.getReal("LargoHerraje",LargoHerraje);
				this->EntreCentrosRanura=inputs.getReal("EntreCentrosRanura", EntreCentrosRanura);

				actualizarDimensiones();
			}
			void obtenerDimensiones(GInputs &inputs){
				inputs.addInput<double>("EspesorBase", EspesorBase);
				inputs.addInput<double>("AnchoBase", AnchoBase);
				inputs.addInput<double>("LargoBase", LargoBase);
				inputs.addInput<double>("AlturaBase", AlturaBase);

				inputs.addInput<double>("EspesorHerraje", EspesorHerraje);
				inputs.addInput<double>("AnchoHerraje", AnchoHerraje);
				inputs.addInput<double>("LargoHerraje", LargoHerraje);
				inputs.addInput<double>("EntreCentrosRanura", EntreCentrosRanura);

			}
			void defParts(void){
				actualizarDimensiones();

				BaseSuperior->beginDefinition();
					BaseSuperior->setWidth(AnchoBase);
					BaseSuperior->setHeight(EspesorBase);
					BaseSuperior->position(TOP, 0);
				BaseSuperior->endDefinition();

				piernaIzquierda->beginDefinition();
					piernaIzquierda->setWidth(EspesorBase);
					piernaIzquierda->position(LEFT,-EspesorBase,*BaseSuperior);
					piernaIzquierda->position(TOP,EspesorBase);
				piernaIzquierda->endDefinition();

				piernaDerecha->beginDefinition();
					piernaDerecha->setWidth(EspesorBase);
					piernaDerecha->position(RIGHT,-EspesorBase,*BaseSuperior);
					piernaDerecha->position(TOP,EspesorBase);
				piernaDerecha->endDefinition();

				esquinaIzquierda->beginDefinition();
					esquinaIzquierda->setInnerRadius(0);
					esquinaIzquierda->setRadius(EspesorBase);
					esquinaIzquierda->setHeight(LargoBase);
					esquinaIzquierda->rotate(RIGHT,DEGREES(90));
					esquinaIzquierda->position(LEFT,-EspesorBase,*BaseSuperior);
					esquinaIzquierda->position(TOP,0);
					esquinaIzquierda->setStartAngle(DEGREES(90));
					esquinaIzquierda->setSweepAngle(DEGREES(90));
				esquinaIzquierda->endDefinition();

				esquinaDerecha->beginDefinition();
					esquinaDerecha->setInnerRadius(0);
					esquinaDerecha->setRadius(EspesorBase);
					esquinaDerecha->setHeight(LargoBase);
					esquinaDerecha->rotate(RIGHT,DEGREES(90));
					esquinaDerecha->position(RIGHT,-EspesorBase,*BaseSuperior);
					esquinaDerecha->position(TOP,0);
					esquinaDerecha->setStartAngle(DEGREES(0));
					esquinaDerecha->setSweepAngle(DEGREES(90));
				esquinaDerecha->endDefinition();

				Herraje1->beginDefinition();
					Herraje1->setWidth(LargoHerraje);
					Herraje1->setLength(EspesorHerraje);
					Herraje1->setHeight(AnchoHerraje);
					Herraje1->position(FRONT, ((LargoBase - EntreCentrosRanura - EspesorHerraje)/2) ,*BaseSuperior);
					Herraje1->position(TOP,EspesorBase);
				Herraje1->endDefinition();

				Herraje2->beginDefinition();
					Herraje2->setWidth(LargoHerraje);
					Herraje2->setLength(EspesorHerraje);
					Herraje2->setHeight(AnchoHerraje);
					Herraje2->position(REAR,((LargoBase - EntreCentrosRanura - EspesorHerraje)/2)  ,*BaseSuperior);
					Herraje2->position(TOP,EspesorBase);
				Herraje2->endDefinition();

				ApoyoPPalanqueo10->beginDefinition();
					ApoyoPPalanqueo10->setWidth(.1);
					ApoyoPPalanqueo10->setLength(.1);
					ApoyoPPalanqueo10->setHeight(.0127);
					ApoyoPPalanqueo10->position(FRONT,-.05,*BaseSuperior);
					ApoyoPPalanqueo10->position(TOP,EspesorBase);
					ApoyoPPalanqueo10->position(LEFT,EspesorBase);
				ApoyoPPalanqueo10->endDefinition();

				ApoyoPPalanqueo11->beginDefinition();
					ApoyoPPalanqueo11->setWidth(.1);
					ApoyoPPalanqueo11->setLength(.1);
					ApoyoPPalanqueo11->setHeight(.0127);
					ApoyoPPalanqueo11->position(FRONT,-.05,*BaseSuperior);
					ApoyoPPalanqueo11->position(TOP,EspesorBase);
					ApoyoPPalanqueo11->position(RIGHT,EspesorBase);
				ApoyoPPalanqueo11->endDefinition();

				ApoyoPPalanqueo20->beginDefinition();
					ApoyoPPalanqueo20->setWidth(.1);
					ApoyoPPalanqueo20->setLength(.1);
					ApoyoPPalanqueo20->setHeight(.0127);
					ApoyoPPalanqueo20->position(REAR,-.05,*BaseSuperior);
					ApoyoPPalanqueo20->position(TOP,EspesorBase);
					ApoyoPPalanqueo20->position(LEFT,EspesorBase);
				ApoyoPPalanqueo20->endDefinition();

				ApoyoPPalanqueo21->beginDefinition();
					ApoyoPPalanqueo21->setWidth(.1);
					ApoyoPPalanqueo21->setLength(.1);
					ApoyoPPalanqueo21->setHeight(.0127);
					ApoyoPPalanqueo21->position(REAR,-.05,*BaseSuperior);
					ApoyoPPalanqueo21->position(TOP,EspesorBase);
					ApoyoPPalanqueo21->position(RIGHT,EspesorBase);
				ApoyoPPalanqueo21->endDefinition();

				/*Prueba->beginDefinition();
					Prueba->position(ABOVE,EspesorBase,*BaseSuperior);
				Prueba->setHidden(true);
				Prueba->setBindParts(true);
				Prueba->endDefinition();*/





			}
	};
}

#endif
