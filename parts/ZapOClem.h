#ifndef ZAPOCLEM_H_
#define ZAPOCLEM_H_
#include "GBox.h"
#include "GMacros.h"
#include "GInputs.h"
#include "GPartialHollowCylinder.h"
#include "Definiciones.h"

namespace NSTransformador{
	using namespace Hero;
	class ZapOClem: public GBox{
		public:
			GPartialHollowCylinder *Cilindro;

double Width;
double Height;
double Length;
double Largo;
enum TipoConector tConector;
			ZapOClem(GBox &parent):GBox(parent){
				createPart(Cilindro, GPartialHollowCylinder);

//DiametroPerno = millimeter(19.05);

				this->setHidden(true);
				this->setBindParts(true);
				actualizarDimensiones();
			}
			void asignarConector(enum TipoConector tConector){
				this-> tConector = tConector;
				actualizarDimensiones();
			}
				void actualizarDimensiones(){
				this->setWidth(millimeter(10));
				this->setHeight(millimeter(10));
				this->setLength(Length);
			}

			/*void asignarWidth(double Width){
				this->Width=Width;
				actualizarDimensiones();
			}
			void asignarHeight(double Height){
				this->Height=Height;
				actualizarDimensiones();
			}
			void asignarLength(double Length){
				this->Length=Length;
				actualizarDimensiones();
			}
			void asignarLargo(double Largo){
				this->Largo=Largo;
				actualizarDimensiones();
			}

			void actualizarDimensiones(){
				this->setWidth(Width);
				this->setHeight(Height);
				this->setLength(Length);
			}
			void defSelf(void){
			}

			void asignarDimensiones(const GInputs &inputs){
				this->Width=inputs.getReal("Width",Width);
				this->Height=inputs.getReal("Height",Height);
				this->Length=inputs.getReal("Length",Length);
				this->Largo=inputs.getReal("Largo",Largo);
				actualizarDimensiones();
			}
			void obtenerDimensiones(GInputs &inputs){
				inputs.addInput<double>("Width", Width);
				inputs.addInput<double>("Height", Height);
				inputs.addInput<double>("Length", Length);
				inputs.addInput<double>("Largo", Largo);
			}*/


			void defParts(void){
				actualizarDimensiones();
	Width = 0;
	Height = 0;
	Length = 0;
	Largo = 0;

if(tConector == ZAPATA)
{
	Length = millimeter(355);
	//Height = millimeter(46);
	//Length = millimeter(89);
	//Largo = millimeter(129.5);
}
else if(tConector == CLEMAAT1)
{
	//Width = millimeter(49);
	Length = millimeter(56);
	//Length = millimeter(53.2);
	//Largo = millimeter(56);
}
else if(tConector == CLEMAAT2)
{
	//Width = millimeter(46.5);
	Length = millimeter(69.2);
	//Length = millimeter(45.5);
	//Largo = millimeter(69.2);
}
else if(tConector == ZAPATAB4)
{
	Length = millimeter(460.85);
	//Height = millimeter(101.6);
	//Length = millimeter(89);
	//Largo = millimeter(129.9);
}
else if(tConector == CLEMABT750U)
{
	//Width = millimeter(87.5);
	//Height = millimeter(45);
	Length = millimeter(251.9);
	//Largo = millimeter(83);
}
				Cilindro->beginDefinition();
					Cilindro->setInnerRadius(0);
					Cilindro->setRadius(millimeter(10));
					Cilindro->setHeight(Length);
					Cilindro->rotate(RIGHT,DEGREES(90));
					Cilindro->setStartAngle(DEGREES(0));
					Cilindro->setSweepAngle(DEGREES(360));
				Cilindro->endDefinition();
				
			}
	};
}

#endif
