/*
 * gapiDrawingTest.h
 *
 *  Created on: Dec 1, 2008
 *      Author: khayyam
 */

#ifndef GAPIDRAWINGTEST_H_
#define GAPIDRAWINGTEST_H_
#include <GL/glut.h>
#include "gapila.h"
#include "vector"
using namespace std;
namespace gapi{
	#define SQR(x) ((x)*(x))
	#define EPSILON 1e-9

	static const GLfloat GAPIBlue[4] = {0.2, 0.2, 1.0, 1};
	static const GLfloat GAPIRed[4] = {1.0, 0.2, 0.2, 1};
	static const GLfloat GAPIGreen[4] = {0.2, 1.0, 0.2, 1};
	static const GLfloat GAPIGray[4] = {0.5, 0.5, 0.5, 1};


	void GAPIdrawIrregularBox(GLdouble n[][3], int faces[][4], GLdouble v[][3], GLenum type);
	void GAPIdrawBox(GLdouble length, GLdouble width, GLdouble height, GLenum type);
	void GAPIdrawBox(GLdouble length, GLdouble width, GLdouble height, GLdouble cx, GLdouble cy, GLdouble cz, GLenum type);
	void GAPIdrawAxes(const GLdouble len, const GLdouble thick);

	void GAPIdrawLead(GLdouble L[][8][3],GLdouble normals[][6][3],int n);

	template<typename T> class Lead{
		protected:
			vector<NTuple<T,3> > points;
		public:
			Lead(){}
			~Lead(){}
			void addPoint(const NTuple<T,3> &point){
				points.push_back(point);
			}
			void buildPoints(NTuple<T,3> &nw, GLdouble ancho, GLdouble grosor, GLdouble L[][8][3], GLdouble normals[][6][3]){
				double halfWidth=ancho*0.5;
				double halfThick=grosor*0.5;

				for(int i=1;i<points.size();i++)
				{
					NTuple<T,3> center;
					NTuple<T,3> nl;
					NTuple<T,3> nh;
					center=(points[i-1]+points[i])/2.0;
					nl=points[i]-points[i-1];
					nl/=nl.norm();
					nh=nl^nw;
					double halfLen=points[i-1].distance(points[i])*0.5;

					//vertices
					(center-(nl*halfLen)-(nw*halfWidth)-(nh*halfThick)).copyTo(L[i-1][0]);
					(center-(nl*halfLen)-(nw*halfWidth)+(nh*halfThick)).copyTo(L[i-1][1]);
					(center-(nl*halfLen)+(nw*halfWidth)+(nh*halfThick)).copyTo(L[i-1][2]);
					(center-(nl*halfLen)+(nw*halfWidth)-(nh*halfThick)).copyTo(L[i-1][3]);

					(center+(nl*halfLen)-(nw*halfWidth)-(nh*halfThick)).copyTo(L[i-1][4]);
					(center+(nl*halfLen)-(nw*halfWidth)+(nh*halfThick)).copyTo(L[i-1][5]);
					(center+(nl*halfLen)+(nw*halfWidth)+(nh*halfThick)).copyTo(L[i-1][6]);
					(center+(nl*halfLen)+(nw*halfWidth)-(nh*halfThick)).copyTo(L[i-1][7]);
					//normales
					(-nl).copyTo(normals[i-1][0]);
					nw.copyTo(normals[i-1][1]);
					nl.copyTo(normals[i-1][2]);
					(-nw).copyTo(normals[i-1][3]);
					nh.copyTo(normals[i-1][4]);
					(-nh).copyTo(normals[i-1][5]);
				}
			}
	};


}

#endif /* GAPIDRAWINGTEST_H_ */
