//#include "ArmadoDeBt.h"

class Peine
{
private:
	int iAltoRanura;
	int iAltoPeine;
	int iAnchoPeine;
	int iLargoPeine;
	int iRanuraPeineK;
	int iRanuraPeineRg;
	int iRan1;
	int iRan2;
	int iRan4;
	int iSeparadorRp;
	int iB20;
	int iDistVerticalHerrSup;

	double dPosXPeineLc;
	double dPosYPeineLc;
	double dDistLateralHerrSup;//
	double dDistLongitudinalHerrSup;

public:
	Peine();
	void calcularPeine (int iLvpst,
					   int iNumLaminas,
					   int iCambiadorCapt,
					   double dA,			//(the :parte-viva :armado-at :soporteria-para-puntas :a)	PTE??
					   double dEspesorPunta,
					   double dEspesorCarton,
					   double dEspesorNucleo,
					   double dBNucleo,
					   double dNucleoF2,
					   double dDimRadialAt,
					   double dDimRadialBt,
					   double dEntreCentrosBoq,
					   TipoNucleo tTipoNucleo,
					   TipoConexion tTipoConexion,
					   TipoAparato tTipoAparato,
					   TipoArticuloHerraje tArt,	//articulo del angulos-2 de herraje superior
					   bool bZonaSismica
					   );
	
	void asignarAltoRanura(int p) {iAltoRanura = p;};
	void asignarAltoPeine(int p) {iAltoPeine = p;};
	void asignarAnchoPeine(int p) {iAnchoPeine = p;};
	void asignarLargoPeine(int p) {iLargoPeine = p;};
	void asignarRanuraPeineK(int p) {iRanuraPeineK = p;};
	void asignarRanuraPeineRg(int p) {iRanuraPeineRg = p;};
	void asignarRan1(int p) {iRan1 = p;};
	void asignarRan2(int p) {iRan2 = p;};
	void asignarRan4(int p) {iRan4 = p;};
	void asignarSeparadorRp(int p) {iSeparadorRp = p;};
	void asignarDistVerticalHerrSup(int p) {iDistVerticalHerrSup = p;};
	void asignarDistLateralHerrSup(double p) {dDistLateralHerrSup = p;};
	void asignarDistLongitudinalHerrSupc(double p) {dDistLongitudinalHerrSup = p;};	

	int obtenerAltoRanura(void) const {return iAltoRanura;};
	int obtenerAltoPeine(void) const {return iAltoPeine;};
	int obtenerAnchoPeine(void) const {return iAnchoPeine;};
	int obtenerLargoPeine(void) const {return iLargoPeine;};
	int obtenerRanuraPeineK(void) const {return iRanuraPeineK;};
	int obtenerRanuraPeineRg(void) const {return iRanuraPeineRg;};
	int obtenerRan1(void) const {return iRan1;};
	int obtenerRan2(void) const {return iRan2;};
	int obtenerRan4(void) const {return iRan4;};
	int obtenerSeparadorRp(void) const {return iSeparadorRp;};
	int obtenerDistVerticalHerrSup(void) const {return iDistVerticalHerrSup;};
	double obtenerDistLateralHerrSup(void) const {return dDistLateralHerrSup;};
	double obtenerDistLongitudinalHerrSup(void) const {return dDistLongitudinalHerrSup;};
};