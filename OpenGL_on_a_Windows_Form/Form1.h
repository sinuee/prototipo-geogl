#pragma once

#include "OpenGL.h"
#include "HipLib.h"

namespace OpenGL_on_a_Windows_Form {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace OpenGLForm;

	

	/// <summary>
	/// Summary for Form1
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			OpenGL = gcnew COpenGL(this, 640, 480);
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::ComponentModel::IContainer^  components;
	protected: 
	private: System::Windows::Forms::Timer^  timer1;

	private: System::Windows::Forms::CheckBox^  checkBox1;
	private: System::Windows::Forms::TextBox^  textBox1;
	public: System::Windows::Forms::HScrollBar^  ScrolZoom;
	private: 

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  enviromentToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  specGraphicsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  onlyGraficsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  onlySpecToolStripMenuItem;


	private: System::Windows::Forms::Panel^  panel1;

	private: System::Windows::Forms::CheckBox^  ckbCambiaAltura;
	private: System::Windows::Forms::Button^  cmdImprimeMatriz;








	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>

		OpenGLForm::COpenGL ^ OpenGL;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->ScrolZoom = (gcnew System::Windows::Forms::HScrollBar());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->enviromentToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->specGraphicsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->onlyGraficsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->onlySpecToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->ckbCambiaAltura = (gcnew System::Windows::Forms::CheckBox());
			this->cmdImprimeMatriz = (gcnew System::Windows::Forms::Button());
			this->menuStrip1->SuspendLayout();
			this->panel1->SuspendLayout();
			this->SuspendLayout();
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Interval = 10;
			this->timer1->Tick += gcnew System::EventHandler(this, &Form1::timer1_Tick);
			// 
			// checkBox1
			// 
			this->checkBox1->AutoSize = true;
			this->checkBox1->Checked = true;
			this->checkBox1->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBox1->Location = System::Drawing::Point(28, 79);
			this->checkBox1->Name = L"checkBox1";
			this->checkBox1->Size = System::Drawing::Size(48, 17);
			this->checkBox1->TabIndex = 1;
			this->checkBox1->Text = L"Girar";
			this->checkBox1->UseVisualStyleBackColor = true;
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(22, 22);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(111, 20);
			this->textBox1->TabIndex = 2;
			this->textBox1->Text = L"Hero Proyect";
			// 
			// ScrolZoom
			// 
			this->ScrolZoom->Location = System::Drawing::Point(0, 470);
			this->ScrolZoom->Name = L"ScrolZoom";
			this->ScrolZoom->Size = System::Drawing::Size(570, 22);
			this->ScrolZoom->TabIndex = 3;
			this->ScrolZoom->Scroll += gcnew System::Windows::Forms::ScrollEventHandler(this, &Form1::ScrolZoom_Scroll);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(25, 99);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(34, 13);
			this->label1->TabIndex = 4;
			this->label1->Text = L"Zoom";
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->enviromentToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(795, 24);
			this->menuStrip1->TabIndex = 5;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// enviromentToolStripMenuItem
			// 
			this->enviromentToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->specGraphicsToolStripMenuItem, 
				this->onlyGraficsToolStripMenuItem, this->onlySpecToolStripMenuItem});
			this->enviromentToolStripMenuItem->Name = L"enviromentToolStripMenuItem";
			this->enviromentToolStripMenuItem->Size = System::Drawing::Size(80, 20);
			this->enviromentToolStripMenuItem->Text = L"Enviroment";
			// 
			// specGraphicsToolStripMenuItem
			// 
			this->specGraphicsToolStripMenuItem->Name = L"specGraphicsToolStripMenuItem";
			this->specGraphicsToolStripMenuItem->Size = System::Drawing::Size(153, 22);
			this->specGraphicsToolStripMenuItem->Text = L"Spec & Graphics";
			this->specGraphicsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::specGraphicsToolStripMenuItem_Click);
			// 
			// onlyGraficsToolStripMenuItem
			// 
			this->onlyGraficsToolStripMenuItem->Name = L"onlyGraficsToolStripMenuItem";
			this->onlyGraficsToolStripMenuItem->Size = System::Drawing::Size(153, 22);
			this->onlyGraficsToolStripMenuItem->Text = L"Only Grafics";
			this->onlyGraficsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::onlyGraficsToolStripMenuItem_Click);
			// 
			// onlySpecToolStripMenuItem
			// 
			this->onlySpecToolStripMenuItem->Name = L"onlySpecToolStripMenuItem";
			this->onlySpecToolStripMenuItem->Size = System::Drawing::Size(153, 22);
			this->onlySpecToolStripMenuItem->Text = L"Only Spec";
			this->onlySpecToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::onlySpecToolStripMenuItem_Click);
			// 
			// panel1
			// 
			this->panel1->Controls->Add(this->cmdImprimeMatriz);
			this->panel1->Controls->Add(this->ckbCambiaAltura);
			this->panel1->Controls->Add(this->textBox1);
			this->panel1->Controls->Add(this->checkBox1);
			this->panel1->Controls->Add(this->label1);
			this->panel1->Location = System::Drawing::Point(641, 43);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(142, 422);
			this->panel1->TabIndex = 8;
			// 
			// ckbCambiaAltura
			// 
			this->ckbCambiaAltura->AutoSize = true;
			this->ckbCambiaAltura->Location = System::Drawing::Point(22, 132);
			this->ckbCambiaAltura->Name = L"ckbCambiaAltura";
			this->ckbCambiaAltura->Size = System::Drawing::Size(98, 17);
			this->ckbCambiaAltura->TabIndex = 9;
			this->ckbCambiaAltura->Text = L"Duplica Altura\?";
			this->ckbCambiaAltura->UseVisualStyleBackColor = true;
			// 
			// cmdImprimeMatriz
			// 
			this->cmdImprimeMatriz->Location = System::Drawing::Point(14, 166);
			this->cmdImprimeMatriz->Name = L"cmdImprimeMatriz";
			this->cmdImprimeMatriz->Size = System::Drawing::Size(105, 22);
			this->cmdImprimeMatriz->TabIndex = 10;
			this->cmdImprimeMatriz->Text = L"&Imprimir Matriz";
			this->cmdImprimeMatriz->UseVisualStyleBackColor = true;
			this->cmdImprimeMatriz->Click += gcnew System::EventHandler(this, &Form1::cmdImprimeMatriz_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(795, 489);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->ScrolZoom);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->Text = L"OpenGL on a Windows Form using Managed C++";
			this->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::Form1_KeyPress);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e)
			 {
				 std::string var;
				 UNREFERENCED_PARAMETER(sender);
				 UNREFERENCED_PARAMETER(e);
				 System::IntPtr ansi = System::Runtime::InteropServices::Marshal::StringToCoTaskMemAnsi(this->textBox1->Text);
				 std::string retval((const char*)(void*)ansi);
				 System::Runtime::InteropServices::Marshal::FreeCoTaskMem(ansi);
				 var = retval; 
				 OpenGL->Render(this->checkBox1->Checked,this->Width,this->Height,var,ckbCambiaAltura->Checked);
				 OpenGL->SwapOpenGLBuffers();					 
			 }
	private: System::Void Form1_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
				 OpenGL->SpecialKeys((int)e->KeyChar,0,0);
			 }
	private: System::Void ScrolZoom_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e) {
				 if (OpenGL->iZoom > ScrolZoom->Value)
					 OpenGL->SpecialKeys(GLUT_KEY_UP ,0,0);
				 else
					 OpenGL->SpecialKeys(GLUT_KEY_DOWN ,0,0);
				 OpenGL->iZoom = ScrolZoom->Value;
			 }
private: System::Void numericUpDown1_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void onlySpecToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 OpenGL->~COpenGL();
			 System::Drawing::Point Punto;
			 Punto.X = 317;
			 Punto.Y = 43;
			 this->panel1->Location::set(Punto);
			 panel1->Visible = true;
		 }
private: System::Void specGraphicsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->panel1->Location.X = 641;
			 if (System::IntPtr::operator int(OpenGL->Handle) == 0)
				 OpenGL = gcnew COpenGL(this,640,480);
			 System::Drawing::Point Punto;
			 Punto.X = 641;
			 Punto.Y = 43;
			 this->panel1->Location::set(Punto);
			 panel1->Visible = true;
		 }
private: System::Void onlyGraficsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (System::IntPtr::operator int(OpenGL->Handle) == 0)
				 OpenGL = gcnew COpenGL(this,640,480);
			 panel1->Visible = false;
		 }
private: System::Void cmdImprimeMatriz_Click(System::Object^  sender, System::EventArgs^  e) {

		//OpenGL->transformador->Export();			 
		// OpenGL->sphere->Export();
		//OpenGL->boquilla->Export();			 
		OpenGL->cylinder->uni("Cyl1","esf");
			// OpenGL->cylinder->Export();
			//OpenGL->cylinder->subtract("Cyl1","esf"); 
		//	OpenGL->cylinder->intersect("Cyl1","esf");
         //OpenGL->hcylinder->Export();		 
		  // OpenGL->phcylinder->Export();
			 //OpenGL->cone->Export();
			 //OpenGL->tcone->Export();
			// OpenGL->htcone->Export();
			 //OpenGL->box->Export();
			 //OpenGL->htcone->Export();
		 }
};
}

