/*
 * gapiDrawingTest.cpp
 *
 *  Created on: Dec 1, 2008
 *      Author: khayyam
 */
#include "gapiDrawingTest.h"
#include <math.h>
#include <GL/glut.h>
#include "gapila.h"


void gapi::GAPIdrawIrregularBox(GLdouble n[][3], int faces[][4], GLdouble v[][3], GLenum type){
	for (int i = 5; i >= 0; i--) {
	    glBegin(type);
	    glNormal3dv(&n[i][0]);
	    glVertex3dv(&v[faces[i][0]][0]);
	    glVertex3dv(&v[faces[i][1]][0]);
	    glVertex3dv(&v[faces[i][2]][0]);
	    glVertex3dv(&v[faces[i][3]][0]);
	    glEnd();
	  }
}


void gapi::GAPIdrawBox(GLdouble length/*x*/, GLdouble width/*y*/, GLdouble height/*z*/, GLenum type)
{
  static GLdouble n[6][3] =
  {
    {-1.0, 0.0, 0.0},
    {0.0, 1.0, 0.0},
    {1.0, 0.0, 0.0},
    {0.0, -1.0, 0.0},
    {0.0, 0.0, 1.0},
    {0.0, 0.0, -1.0}
  };
  static int faces[6][4] =
  {
    {0, 1, 2, 3},
    {3, 2, 6, 7},
    {7, 6, 5, 4},
    {4, 5, 1, 0},
    {5, 6, 2, 1},
    {7, 4, 0, 3}
  };
  GLdouble v[8][3];

  //v[0][0] = v[1][0] = v[2][0] = v[3][0] = -length / 2;
  v[0][0] = v[1][0] = v[2][0] = v[3][0] = 0;
  v[4][0] = v[5][0] = v[6][0] = v[7][0] = length / 2;

  //v[0][1] = v[1][1] = v[4][1] = v[5][1] = -width / 2;
  v[0][1] = v[1][1] = v[4][1] = v[5][1] = 0;
  v[2][1] = v[3][1] = v[6][1] = v[7][1] = width / 2;

  //v[0][2] = v[3][2] = v[4][2] = v[7][2] = -height / 2;
  v[0][2] = v[3][2] = v[4][2] = v[7][2] = 0;
  v[1][2] = v[2][2] = v[5][2] = v[6][2] = height / 2;
  GAPIdrawIrregularBox(n,faces,v, type);

}

void gapi::GAPIdrawBox(GLdouble length, GLdouble width, GLdouble height, GLdouble cx, GLdouble cy, GLdouble cz, GLenum type)
	{
		glPushMatrix();
		glTranslatef(cx,cy,cz);
		GAPIdrawBox(length, width, height, type);
		glPopMatrix();
	}

void gapi::GAPIdrawAxes(const GLdouble len, const GLdouble thick)
{
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GAPIRed);
		GAPIdrawBox(len,thick,thick,GL_TRIANGLE_FAN);
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GAPIGreen);
		GAPIdrawBox(thick,len,thick,GL_TRIANGLE_FAN);
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GAPIBlue);
		GAPIdrawBox(thick,thick,len,GL_TRIANGLE_FAN);
}

void gapi::GAPIdrawLead(GLdouble L[][8][3],GLdouble normals[][6][3],int n)
	{
		static int faces[6][4] =
		  {
		    {0, 1, 2, 3},
		    {3, 2, 6, 7},
		    {7, 6, 5, 4},
		    {4, 5, 1, 0},
		    {5, 6, 2, 1},
		    {7, 4, 0, 3}
		  };
		for(int i=0;i<n;i++)
			{
			   GAPIdrawIrregularBox(normals[i],faces,L[i], GL_TRIANGLE_FAN);
			}
	}


