#include "stdafx.h"
#include "Peine.h"

	Peine::Peine()
	{
	iAltoRanura = 0;
	iAltoPeine = 0;
	iAnchoPeine = 0;
	iLargoPeine = 0;
	iRanuraPeineK = 0;
	iRanuraPeineRg = 0;
	iRan1 = 0;
	iRan2 = 0;
	iRan4 = 0;
	iSeparadorRp = 0;
	iB20 = 0;
	iDistVerticalHerrSup = 0;

	dPosXPeineLc = 0.0;
	dPosYPeineLc = 0.0;
	dDistLateralHerrSup = 0.0;
	dDistLongitudinalHerrSup = 0.0;
	}

	void Peine::calcularPeine (int iLvpst,
							   int iNumLaminas,
							   int iCambiadorCapt,
							   double dA,			//(the :parte-viva :armado-at :soporteria-para-puntas :a)	PTE??
							   double dEspesorPunta,
							   double dEspesorCarton,
							   double dEspesorNucleo,
							   double dBNucleo,
							   double dNucleoF2,
							   double dDimRadialAt,
							   double dDimRadialBt,
							   double dEntreCentrosBoq,
							   TipoNucleo tTipoNucleo,
							   TipoConexion tTipoConexion,
							   TipoAparato tTipoAparato,
							   TipoArticuloHerraje tArt,	//articulo del angulos-2 de herraje superior: UE00
							   bool bZonaSismica
							   )
	{	
		double dLargoAngulos2=0.0, dDistC17=0.0, dDatosAngulo[4];
		double dDistLongitudinalHerrBoq =0.0; //PTE de definir
		int i=0,iDistE1=0;

		for(i=0; i<4; i++)
		{
			dDatosAngulo[i] = 0.0;
		}
		
		//Dimensiones
		iAltoRanura = (iLvpst == 1) ? (100+60) : (60);
		iAltoPeine = (iLvpst == 1) ? (140+50) : (90);
		iAnchoPeine = 50;

		iRan1 = CalcularRanuraPeine (1,iNumLaminas,0,dEspesorPunta,dEspesorCarton);
		iRan2 = CalcularRanuraPeine (2,iNumLaminas,0,dEspesorPunta,dEspesorCarton);
		iRan4 = CalcularRanuraPeine (4,iNumLaminas,0,dEspesorPunta,dEspesorCarton);
		iRanuraPeineK = CalcularRanuraPeine (0,iNumLaminas,1,dEspesorPunta,0);

		iSeparadorRp = 15;

		iRanuraPeineRg = CalcularRanuraPeineRg (iNumLaminas, dEspesorPunta);

		if (iLvpst == 1)
		{
			iB20 = (iCambiadorCapt == 1) ? ROUND(115 + dA/2.0 + 30) : 195;
		}else
		{
			iB20 = (tTipoNucleo == ENROLLADO) ? 80 : 140;
		}

		if (iLvpst == 1)
		{
			iLargoPeine = (iB20 + 2 * iRanuraPeineK + 15 + 15 + iRanuraPeineRg + 50);
		}else
		{
			iLargoPeine = (tTipoConexion == DELTA) ? (135 + 40 + iRan2 + iRan4) :
				(iRanuraPeineK * 2 + SeparadorRp + 15 + 20 + iB20 + iRanuraPeineRg);
		}


		//Posicionamiento
		//:above
		if (tTipoNucleo == ENROLLADO)
		{
			iDistVerticalHerrSup = (bZonaSismica == true) ? 45 : 20;
		}else
		{
			iDistVerticalHerrSup = 100;
		}

		//:lateral
		dPosXPeineLc = dNucleoF2 + ( (tTipoNucleo == ENROLLADO) ? 25 : ( (iLvpst == 1) ? -115 : -60 ) );
		dDistLateralHerrSup = -(dPosXPeineLc + iLargoPeine/2.0 - 20);
		
		//:rear
		obtenerDatosAngulo (tArt, dDatosAngulo);
		dLargoAngulos2 = dDatosAngulo[1];
		
		dPosYPeineLc = ((tTipoConexion == DELTA) && (tTipoAparato == MEXICO)) ? dDistLongitudinalHerrBoq : //PTE??
			(dBNucleo/2.0 + 11 + (dDimRadialBt - dDimRadialAt)/2.0 + dEntreCentrosBoq/2.0 - ( (iLvpst == 1) ? 20 : 65 ));


		dDistC17 = (dEspesorNucleo - 15) / 4.0;
		iDistE1 = int(abs(dBNucleo/2.0 - dEntreCentrosBoq/2.0 - 155));
		if ((tTipoConexion == DELTA) && (tTipoAparato == MEXICO))
		{
			dDistLongitudinalHerrSup = dPosYPeineLc;
		}else if (tTipoNucleo == ENROLLADO)
		{
			//dLargoAngulos2_2 = 0; //PTE??? No se usa este armado si el conductor de baja es solera
			//dDistLongitudinalHerrSup = dPosYPeineLc + ((TconductorBt == SOLERA)? dLargoAngulos2_2 : dLargoAngulos2);
			dDistLongitudinalHerrSup = dPosYPeineLc + dLargoAngulos2;
		}else if (iLvpst == 1)
		{
			dDistLongitudinalHerrSup = iAnchoPeine + 35 - dDistC17;
		}else{
			dDistLongitudinalHerrSup = iDistE1 - iAnchoPeine - 40;
		}
	}