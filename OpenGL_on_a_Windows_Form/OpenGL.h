#pragma once

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
//#include <GL/glut.h>

#include <string>
#include "jet.h"

//*****  GAPI **********
#include <string>


#include <direct.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>

#include <vector>
#include <sstream>
#include <map>
#include "zpr.h"
#include "GMacros.h"
#include "gapila.h"
#include "Matrix.h"
#include "Vector.h"
#include "gapiDrawingTest.h"
#include "GBox.h"
#include "GCylinder.h"
#include "GHollowCylinder.h"
#include "GCone.h"
#include "GTruncatedCone.h"
#include "GHollowTruncatedCone.h"
#include "GSphere.h"
#include "GHollowBox.h"
#include "GRoundedHollowBox.h"
#include "GPartialHollowCylinder.h"
#include "GAsymmetricHollowBox.h"
#include "GAsymmetricRoundedHollowBox.h"
#include "NucleoEnrollado.h"
#include "NucleoMitter.h"
#include "Bobina.h"
#include "GPlainInputs.h"
#include "GXMLInputs.h"
#include "GPolyLine.h"
#include "Canal.h"
#include "Angulo.h"
#include "CanalLateral.h"
#include "Base.h"
#include "Boquilla.h"
#include "Conector.h"
#include "EjemploGBox.h"
#include "Trafo.h"
#include "boquilla.h"

/*************************************/

//#include "cubo_normales.h"
//#include "highlight.h"
using namespace System::Windows::Forms;
using namespace std;
using namespace gapi;
using namespace NSTransformador;


namespace OpenGLForm 
{
	public ref class COpenGL: public System::Windows::Forms::NativeWindow
	{
	public:
		//std::string mousePositionLabelMeters;
		//std::string mousePositionLabelFeet;
		//std::map<gapi::GBox*, std::string> *GAPI_NAMES;

		#define GLERROR                                                    \
		{                                                              \
			GLenum code = glGetError();                                \
			while (code!=GL_NO_ERROR)                                  \
			{                                                          \
				printf("%s\n",(char *) gluErrorString(code));          \
					code = glGetError();                               \
			}                                                          \
		  }
		GBox *root;			
		NSTransformador::Trafo *transformador;
		//NSTransformador::Boquilla *boquilla;
		//void nucleoMitterDemo(GBox *&root);
		//GSphere *sphere;
		GCylinder *cylinder;
		//GHollowCylinder *hcylinder;
		//GPartialHollowCylinder *phcylinder;
		//GCone *cone;
		//GTruncatedCone *tcone;
		//GHollowBox *hbox;
		//GBox *box;
		//GRoundedHollowBox *rhbox;
		//GHollowTruncatedCone *htcone;
		//GAsymmetricRoundedHollowBox *arhbox;
		//GAsymmetricHollowBox *ahbox;
//Boquilla *boquilla;	
			//boq->
		void nucleoEnrolladoDemo(void){
			createRoot(root, GBox);
			root->beginDefinition();
			root->endDefinition();

			NSTransformador::NucleoEnrollado *nucleo;
			createChild(nucleo, NSTransformador::NucleoEnrollado, *root);
			nucleo->beginDefinition();
			nucleo->asignarDimensiones(GPlainInputs("C:\\NucleoTest.xml"));
			nucleo->endDefinition();
		}
		void nucleoMitterDemo(void)
		{
			createRoot(root, GBox);
			root->beginDefinition();
			root->endDefinition();

			NSTransformador::NucleoMitter *nucleo;
			createChild(nucleo, NSTransformador::NucleoMitter, *root);
			nucleo->beginDefinition();
			//nucleo->setDisplayType(SOLID);
			//nucleo->asignarDimensiones(GXMLInputs("d:\\manual oscar montoya\\NucleoTest.xml"));
			nucleo->endDefinition();
		}
		void TrafoDemo(bool duplicaAltura)
		{
			createRoot(root, GBox);
			root->beginDefinition();
			root->endDefinition();
			NSTransformador::Trafo *transformador;
			createChild(transformador,NSTransformador::Trafo,*root);

			transformador->beginDefinition();		
			
			transformador->endDefinition();
		}

//void BoquillaDemo(void){
//	createRoot(root, GBox);
//	root->beginDefinition();
//	root->endDefinition();
//
//	NSTransformador::Boquilla *boquilla;
//	createChild(boquilla, NSTransformador::Boquilla, *root);
//	boquilla->beginDefinition();
//	boquilla->asignarDimensiones(GXMLInputs("C:\\Boquilla.xml"));
//	boquilla->endDefinition();
//}
//void EjemploDemo(void){
//	createRoot(root, GBox);
//	root->beginDefinition();
//	root->endDefinition();
//
//	NSTransformador::EjemploGBox* ejemplo;
//	createChild(ejemplo, NSTransformador::EjemploGBox, *root);
//	ejemplo->beginDefinition();
//	ejemplo->asignarDimensiones(GXMLInputs("E:\\EjemploGBox.xml"));
//	ejemplo->endDefinition();
//
//	GXMLInputs inputs;
//	ejemplo->obtenerDimensiones(inputs);
//	inputs.saveToFile("E:\\EjemploGBox.xml");
//}
//
//void ConectorDemo(void){
//	createRoot(root, GBox);
//	root->beginDefinition();
//	root->endDefinition();
//
//	NSTransformador::Conector *conector;
//	createChild(conector, NSTransformador::Conector, *root);
//	conector->beginDefinition();
//	conector->asignarDimensiones(GXMLInputs("C:\\conector.xml"));
//	conector->endDefinition();
//}
//
//void BaseDemo(void){
//	createRoot(root, GBox);
//	root->beginDefinition();
//	root->endDefinition();
//
//	NSTransformador::Base *base;
//	createChild(base, NSTransformador::Base, *root);
//	base->beginDefinition();
//	base->asignarDimensiones(GXMLInputs("C:\\Base.xml"));
//	base->endDefinition();
//}
//
//void bobinaDemo(void){
//	createRoot(root, GBox);
//	root->beginDefinition();
//	root->endDefinition();
//
//	NSTransformador::Bobina *bobina;
//	createChild(bobina, NSTransformador::Bobina, *root);
//	bobina->beginDefinition();
//	bobina->asignarDimensiones(GXMLInputs("C:\\BobinaTest.xml"));
//	bobina->endDefinition();
//
//	/*GPolyLine *lead;
//	createChild(lead, GPolyLine, *root);
//	lead->beginDefinition();
//		lead->setNw(bobina->getNormal(RIGHT));
//		lead->setLineThickness(inch(0.5));
//		lead->setLineWidth(inch(2));
//		lead->addPoint(bobina->getPoint(TOPLEFTFRONT));
//		lead->addPoint(bobina->getPoint(TOPLEFTREAR));
//		lead->addPoint(bobina->getPoint(TOPRIGHTREAR));
//		lead->addPoint(bobina->getPoint(TOPRIGHTFRONT));
//		lead->addPoint(bobina->getPoint(BOTTOMLEFTFRONT));
//	lead->endDefinition();*/
//
//}
//
//void canalDemo(void){
//	createRoot(root, GBox);
//	root->beginDefinition();
//	root->endDefinition();
//
//	NSTransformador::Canal *canal;
//	createChild(canal, NSTransformador::Canal, *root);
//	canal->beginDefinition();
//		canal->asignarDimensiones(GXMLInputs("/home/khayyam/Desktop/canalTest.xml"));
//	canal->endDefinition();
//	/*GXMLInputs inputs;
//	canal->obtenerDimensiones(inputs);
//	inputs.saveToFile("/home/khayyam/Desktop/canalTest.xml");*/
//}
//
//void anguloDemo(void){
//	createRoot(root, GBox);
//	root->beginDefinition();
//	root->endDefinition();
//
//	NSTransformador::Angulo *angulo;
//	createChild(angulo, NSTransformador::Angulo, *root);
//	angulo->beginDefinition();
//	angulo->asignarDimensiones(GXMLInputs("d:\\canalTest.xml"));
//	angulo->endDefinition();
//	/*GXMLInputs inputs;
//	angulo->obtenerDimensiones(inputs);
//	inputs.saveToFile("d:\\canalTest.xml");*/
//}
//
//void canalLateralDemo(void){
//	createRoot(root, GBox);
//	root->beginDefinition();
//	root->endDefinition();
//
//	NSTransformador::CanalLateral *canal;
//	createChild(canal, NSTransformador::CanalLateral, *root);
//	canal->beginDefinition();
//		//canal->asignarDimensiones(GXMLInputs("/home/khayyam/Desktop/canalLateralTest.xml"));
//	canal->endDefinition();
//	/*GXMLInputs inputs;
//	angulo->obtenerDimensiones(inputs);
//	inputs.saveToFile("/home/khayyam/Desktop/anguloTest.xml");*/
//}
		int iZoom;
		COpenGL(System::Windows::Forms::Form ^ parentForm, GLsizei iWidth, GLsizei iHeight)
		{
			CreateParams^ cp = gcnew CreateParams;

			// Set the position on the form
			cp->X = 0;
			cp->Y = 0;
			cp->Height = iHeight;
			cp->Width = iWidth;

			// Specify the form as the parent.
			cp->Parent = parentForm->Handle;

			// Create as a child of the specified parent and make OpenGL compliant (no clipping)
			cp->Style = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

			// Create the actual window
			this->CreateHandle(cp);

			m_hDC = GetDC((HWND)this->Handle.ToPointer());

			if(m_hDC)
			{
				MySetPixelFormat(m_hDC);
				ReSizeGLScene(iWidth, iHeight);
				InitGL();
			}

			rtri = 0.0f;
			rquad = 0.0f;
		}
/********  A�ADIDO DE GDEVELOP     ************************************************/
		void printtext(int x, int y, const std::string cadena, int Width, int Heigth)
{
	char *str;
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0, Width, 0, Heigth, -1.0f, 1.0f);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glPushAttrib(GL_DEPTH_TEST);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	glDisable(GL_LIGHT0);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
    glColor3d(1.0,1.0,1.0);
    glRasterPos2i(x,y);
    for(int i=0;cadena.c_str()[i];i++)
    {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, cadena.c_str()[i]);
    }
    glPopAttrib();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

/**************************************************************************************/
		void SpecialKeys(int key, int x, int y)
    {
    if(key == GLUT_KEY_UP)
        xRot-= 5.0f;

    if(key == GLUT_KEY_DOWN)
        xRot += 5.0f;

    if(key == GLUT_KEY_LEFT)
        yRot -= 5.0f;

    if(key == GLUT_KEY_RIGHT)
        yRot += 5.0f;

    if(key > 356.0f)
        xRot = 0.0f;

    if(key < -1.0f)
        xRot = 355.0f;

    if(key > 356.0f)
        yRot = 0.0f;

    if(key < -1.0f)
        yRot = 355.0f;
RenderScene();
//    glutPostRedisplay();
    };
		void Display( void )
		{
		   //GLERROR;
		   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		   GAPIdrawAxes(4,.01);
		   
		   //glMatrixMode(GL_PROJECTION);
		   //glLoadIdentity();

		   //printtext(10,30, mousePositionLabelMeters.c_str());
		   //printtext(10,10, mousePositionLabelFeet.c_str());

		   glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GAPIBlue);
		   if(root!=NULL){
			   root->draw();
		   }
		   //glutSwapBuffers();
		   //glMatrixMode(GL_MODELVIEW);
		   glLoadIdentity();
		   //GLERROR;
		}

		void passiveMouseMotion(int x, int y)
		{
			int viewport[4];
			glGetIntegerv(GL_VIEWPORT, viewport);
			GLdouble modelview[16];
			glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
			GLdouble projection[16];
			glGetDoublev(GL_PROJECTION_MATRIX, projection);
			GLfloat winX, winY, winZ=0;
			winX=(GLfloat)x;
			winY=(GLfloat)y;
			winY =(GLfloat)viewport[3] - winY;
			//glReadPixels((int)winX, (int)winY, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);

			GLdouble posX, posY, posZ;
			gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);

			//ostringstream os;
			//os<<"Meters: ("<<posX<<","<<posY<<","<<posZ<<")";
			//mousePositionLabelMeters=os.str();

			//ostringstream os2;
			//os2<<"Feet: ("<<metersToFeet(posX)<<","<<metersToFeet(posY)<<","<<metersToFeet(posZ)<<")";
			//mousePositionLabelFeet=os2.str();
			Display();
		}
void init(/*int argc, char *argv[],int Width, int Height*/){
	// Tell Mesa GLX to use 3Dfx driver in fullscreen mode.
	//putenv("MESA_GLX_FX=fullscreen");
    int Window = 0;
	// Disable 3Dfx Glide splash screen
	//putenv("FX_GLIDE_NO_SPLASH=");

	// Give an initial size and position so user doesn't have to place window
//	glutInitWindowPosition(0, 0);
//	glutInitWindowSize(Width, Height);
//	glutInit( &argc, argv );
//
//	glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
//	Window = glutCreateWindow(argv[0]);
//	glutSetWindowTitle("GDevelop v.1.0");
////	glutKeyboardFunc(keyboardFunc);
//	if (!Window) {
//		printf("Error, couldn't open window\n");
//		exit(1);
//	}
	//glutFullScreen();
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	//glPushMatrix();
	//glScaled(4.00,4.00,4.00);//(0.25,0.25,0.25);//Esta mugre hace la escala
	glScaled(0.25,0.25,0.25);//Esta mugre hace la escala
	glRotated(-90,1,0,0);
	glRotated(-45,0,0,1);
	glRotated(15,1,1,0);
	zprInit();
	//glutPassiveMotionFunc(passiveMouseMotion);
	//============menu=============
	//int valueInit=1;
	//int visibleOptionMenu=createMenu(*root, valueInit, setVisibleFunc);
	//valueInit=1;
	//int drawOptionMenu=createMenu(*root, valueInit, setSolidFunc);
	//glutCreateMenu(setVisibleFunc);
	//glutAddSubMenu("visible/invisible",visibleOptionMenu);
	//glutAddSubMenu("solid/wired",drawOptionMenu);
	//glutAttachMenu(GLUT_RIGHT_BUTTON);
	//==============================
///	glutDisplayFunc( Display );
	
	Display();
	//glutMainLoop();
}
/**********************************************************/
		System::Void Render(bool Girar, int Width, int Height, std::string texto, bool bCambiaAncho)
		{
			glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
			glRotatef(rtri,0.0f,1.0f,0.0f);
			
			if(root == NULL)
			{
				root = new GBox();

			//nucleoMitterDemo();
			//nucleoEnrolladoDemo();
			//TrafoDemo(bCambiaAncho);
				createRoot(root, GBox);
				root->beginDefinition();
				root->endDefinition();
                //createChild(box,GBox,*root);
				//createChild(sphere,GSphere,*root);
				createChild(cylinder,GCylinder,*root);
//createChild(boquilla,Boquilla,*root);
    			//createChild(hcylinder,GHollowCylinder,*root);				
			   //createChild(phcylinder,GPartialHollowCylinder,*root);
			     // createChild(cone,GCone,*root);
				//createChild(tcone,GTruncatedCone,*root);
//				createChild(hbox,GHollowBox,*root);
			//	createChild(htcone,GHollowTruncatedCone,*root);
				//createChild(rhbox,GRoundedHollowBox,*root);
				//createChild(arhbox,GAsymmetricRoundedHollowBox,*root);
				//createChild(ahbox,GAsymmetricHollowBox,*root);				
				//cylinder->setHidden(true);
				//tcone->setTopRadius(1.063);
				//htcone->setTopRadius(1.563);
				//htcone->setTopInnerRadius(1.5);
				//sphere->beginDefinition();
				
				cylinder->beginDefinition();
			    
				cylinder->setName("Cyl");
				//cylinder->setRadius(0);
				//hcylinder->beginDefinition();				hcylinder->setName("hcyl");
				//hcylinder->setRadius(0);
				//hcylinder->setInnerRadius(boquilla->DiametroCampana / 2);
				//sphere->setName("esf");
/*boquilla->beginDefinition();
	boquilla->DimA = millimeter(0.5);
	boquilla->DimB = millimeter(1);			
boquilla->endDefinition();*/
			
			    //phcylinder->beginDefinition();		


			   //cone->beginDefinition();
			   //tcone->beginDefinition();			   

			   //rhbox->beginDefinition();
			   //hbox->beginDefinition();
				//box->beginDefinition();				box->setName("box");
				//arhbox->beginDefinition();
				//ahbox->beginDefinition();
			  // htcone->beginDefinition();
			  
				//sphere->setRadius(1.0);
				//  sphere->endDefinition();
			   cylinder->endDefinition();
			//hcylinder->endDefinition();
			  // phcylinder->endDefinition();
				//cone->endDefinition();
//				tcone->endDefinition();
				//rhbox->endDefinition();
				//arhbox->endDefinition();
				//ahbox->endDefinition();
			//	htcone->endDefinition();
			//createChild(transformador,NSTransformador::Trafo,*root);
				
				//transformador->beginDefinition();		

				//transformador->endDefinition();
			}

			root->getPart(0).setDisplayType(SOLID);
			root->setHidden(true);
			
			/*if (bCambiaAncho)
				transformador->tanque->altoInterior = 2;
			else 
				transformador->tanque->altoInterior = 1;
			transformador->actualizarDimensiones();
			transformador->defParts();*/
			//sphere->setBindParts(2.2);
			cylinder->setBindParts(2.2);			
			//hcylinder->setBindParts(2.2);
			//phcylinder->setBindParts(2.2);
			//cone->setBindParts(2.2);
			//tcone->setBindParts(2.2);
			//rhbox->setBindParts(2.2);
			//hbox->setBindParts(2.2);
			//	box->setBindParts(2.2);
			//htcone->setBindParts(2.2);
			//arhbox->setBindParts(2.2);
			//ahbox->setBindParts(2.2);
			//cylinder->uni("Cyl1","esf");
//boquilla->setBindParts(2.2);
			init();

			printtext(1.5,15,texto, Width,Height);
			if (Girar)
			{
				rtri+=0.2f;											// Increase the rotation variable for the triangle
				rquad-=0.15f;										// Decrease the rotation variable for the quad
			}
		}

		System::Void SwapOpenGLBuffers(System::Void)
		{
			SwapBuffers(m_hDC) ;
		}


	private:
		HDC m_hDC;
		HGLRC m_hglrc;
		GLfloat	rtri;				// Angle for the triangle
		GLfloat	rquad;				// Angle for the quad

	protected:
		~COpenGL(System::Void)
		{
			this->DestroyHandle();
		}

		GLint MySetPixelFormat(HDC hdc)
		{
			static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
				{
					sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
					1,											// Version Number
					PFD_DRAW_TO_WINDOW |						// Format Must Support Window
					PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
					PFD_DOUBLEBUFFER,							// Must Support Double Buffering
					PFD_TYPE_RGBA,								// Request An RGBA Format
					16,										// Select Our Color Depth
					0, 0, 0, 0, 0, 0,							// Color Bits Ignored
					0,											// No Alpha Buffer
					0,											// Shift Bit Ignored
					0,											// No Accumulation Buffer
					0, 0, 0, 0,									// Accumulation Bits Ignored
					16,											// 16Bit Z-Buffer (Depth Buffer)  
					0,											// No Stencil Buffer
					0,											// No Auxiliary Buffer
					PFD_MAIN_PLANE,								// Main Drawing Layer
					0,											// Reserved
					0, 0, 0										// Layer Masks Ignored
				};
			
			GLint  iPixelFormat; 
		 
			// get the device context's best, available pixel format match 
			if((iPixelFormat = ChoosePixelFormat(hdc, &pfd)) == 0)
			{
				MessageBox::Show("ChoosePixelFormat Failed");
				return 0;
			}
			 
			// make that match the device context's current pixel format 
			if(SetPixelFormat(hdc, iPixelFormat, &pfd) == FALSE)
			{
				MessageBox::Show("SetPixelFormat Failed");
				return 0;
			}

			if((m_hglrc = wglCreateContext(m_hDC)) == NULL)
			{
				MessageBox::Show("wglCreateContext Failed");
				return 0;
			}

			if((wglMakeCurrent(m_hDC, m_hglrc)) == NULL)
			{
				MessageBox::Show("wglMakeCurrent Failed");
				return 0;
			}


			return 1;
		}

		bool InitGL(GLvoid)										// All setup for opengl goes here
		{
			glShadeModel(GL_SMOOTH);							// Enable smooth shading
			glClearColor(0.0f, 0.0f, 0.0f, 0.5f);				// Black background
			glClearDepth(1.0f);									// Depth buffer setup
			glEnable(GL_DEPTH_TEST);							// Enables depth testing
			glDepthFunc(GL_LEQUAL);								// The type of depth testing to do
			glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really nice perspective calculations
			return TRUE;										// Initialisation went ok
		}

		GLvoid ReSizeGLScene(GLsizei width, GLsizei height)		// Resize and initialise the gl window
		{
			if (height==0)										// Prevent A Divide By Zero By
			{
				height=1;										// Making Height Equal One
			}

			glViewport(0,0,width,height);						// Reset The Current Viewport

			glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
			glLoadIdentity();									// Reset The Projection Matrix

			// Calculate The Aspect Ratio Of The Window
			gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

			glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
			glLoadIdentity();									// Reset The Modelview Matrix
		}
	};
}