/*
 * GDevelop.cpp
 *
 *  Created on: Dec 2, 2008
 *      Author: khayyam
 */
#include <iostream>
#include <vector>
#include <sstream>
#include <map>
#include "zpr.h"
#include "GMacros.h"
#include "gapila.h"
#include "Matrix.h"
#include "Vector.h"
#include "gapiDrawingTest.h"
#include "GBox.h"
#include "GCylinder.h"
#include "GHollowCylinder.h"
#include "GCone.h"
#include "GTruncatedCone.h"
#include "GHollowTruncatedCone.h"
#include "GSphere.h"
#include "GHollowBox.h"
#include "GRoundedHollowBox.h"
#include "GPartialHollowCylinder.h"
#include "GAsymmetricHollowBox.h"
#include "GAsymmetricRoundedHollowBox.h"
#include "NucleoEnrollado.h"
#include "NucleoMitter.h"
#include "Bobina.h"
#include "GPlainInputs.h"
#include "GXMLInputs.h"
#include "GPolyLine.h"
#include "Canal.h"
#include "Angulo.h"
#include "CanalLateral.h"
#include "Base.h"
#include "Boquilla.h"
#include "Conector.h"
#include "EjemploGBox.h"
using namespace std;
using namespace gapi;

std::map<gapi::GBox*, std::string> GAPI_NAMES;
double angle=0;
int ScreenWidth, ScreenHeight;
GLfloat Xrot = 0.0, Yrot = 0.0;
/*#define WIDTH 1280
#define HEIGHT 800*/
#define WIDTH 640
#define HEIGHT 480
int Window = 0;
int menuState=0;
string mousePositionLabelMeters;
string mousePositionLabelFeet;
//map<GBox*, string> GAPI_NAMES;
/**/


#define GLERROR                                                    \
    {                                                              \
        GLenum code = glGetError();                                \
        while (code!=GL_NO_ERROR)                                  \
        {                                                          \
            printf("%s\n",(char *) gluErrorString(code));          \
                code = glGetError();                               \
        }                                                          \
    }

GBox *root;

void printtext(int x, int y, const char * string)
{
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0, WIDTH, 0, HEIGHT, -1.0f, 1.0f);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glPushAttrib(GL_DEPTH_TEST);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_TEXTURE_2D);
    glColor3d(1.0,1.0,1.0);
    glRasterPos2i(x,y);
    for(int i=0;string[i];i++)
    {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, string[i]);
    }
    glPopAttrib();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

void Display( void )
{
   GLERROR;
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   GAPIdrawAxes(4,.01);


   printtext(10,30, mousePositionLabelMeters.c_str());
   printtext(10,10, mousePositionLabelFeet.c_str());

   glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GAPIBlue);
   if(root!=NULL){
	   root->draw();
   }
   glutSwapBuffers();
   GLERROR;
}

void passiveMouseMotion(int x, int y)
{
	int viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);
	GLdouble modelview[16];
	glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
	GLdouble projection[16];
	glGetDoublev(GL_PROJECTION_MATRIX, projection);
	GLfloat winX, winY, winZ=0;
	winX=(GLfloat)x;
	winY=(GLfloat)y;
	winY =(GLfloat)viewport[3] - winY;
	//glReadPixels((int)winX, (int)winY, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);

	GLdouble posX, posY, posZ;
	gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);

	ostringstream os;
	os<<"Meters: ("<<posX<<","<<posY<<","<<posZ<<")";
	mousePositionLabelMeters=os.str();

	ostringstream os2;
	os2<<"Feet: ("<<metersToFeet(posX)<<","<<metersToFeet(posY)<<","<<metersToFeet(posZ)<<")";
	mousePositionLabelFeet=os2.str();
	Display();
}

map<int, GBox*> menuKeyMap;

void setVisibleFunc(int value)
{
	menuKeyMap[value]->setVisible(!menuKeyMap[value]->getVisible());
}

void setSolidFunc(int value)
{
	if(menuKeyMap[value]->getDisplayType()==WIRED)
		menuKeyMap[value]->setDisplayType(SOLID);
	else
		menuKeyMap[value]->setDisplayType(WIRED);
}




int createMenu(GBox &box, int &valueInit, void (*func)(int)){
	int menu = glutCreateMenu(func);
	menuKeyMap[valueInit]=&box;
	string id=GAPI_NAMES[&box];
	glutAddMenuEntry(id.c_str(), valueInit);
	++valueInit;
	for(set<GBox*>::iterator it=box.getFirstPart(); it!=box.getLastPart();++it)
	{
		if((*it)->partsCount()>0)
		{
			int a=valueInit;
			int subMenu=createMenu(*(*it), valueInit, func);
			int b=valueInit-1;
			ostringstream os;
			os<<"Nodes["<<a<<", "<<b<<"]";
			glutSetMenu(menu);
			glutAddSubMenu(os.str().c_str(), subMenu);
		}
		else
		{
			glutSetMenu(menu);
			string id=GAPI_NAMES[*it];
			glutAddMenuEntry(id.c_str(), valueInit);
			menuKeyMap[valueInit]=(*it);
			++valueInit;
		}

	}
	return menu;
}
void keyboardFunc(unsigned char key, int x, int y){
	switch(key){
		case ' ':
			if(menuState==0)
				glutDetachMenu(GLUT_RIGHT_BUTTON);
			else
				glutAttachMenu(GLUT_RIGHT_BUTTON);
			menuState=1-menuState;
			break;
		case 'X':
			glPopMatrix();
			glPushMatrix();
			glScaled(0.25,0.25,0.25);
			glRotated(-90,0,1,0);
			glRotated(-90,1,0,0);
		break;
		case 'x':
			glPopMatrix();
			glPushMatrix();
			glScaled(0.25,0.25,0.25);
			glRotated(-90,1,0,0);
			glRotated(90,0,0,1);
		break;
		case 'y':
			glPopMatrix();
			glPushMatrix();
			glScaled(0.25,0.25,0.25);
			glRotated(-90,1,0,0);
			glRotated(180,0,0,1);
		break;
		case 'Y':
			glPopMatrix();
			glPushMatrix();
			glScaled(0.25,0.25,0.25);
			glRotated(-90,1,0,0);
		break;
		case 'Z':
			glPopMatrix();
			glPushMatrix();
			glScaled(0.25,0.25,0.25);
		break;

		case 'z':
			glPopMatrix();
			glPushMatrix();
			glScaled(0.25,0.25,0.25);
			glRotated(180,1,0,0);
		break;
		case 13:
			glPopMatrix();
			glPushMatrix();
			glScaled(0.25,0.25,0.25);
			glRotated(-90,1,0,0);
			glRotated(-45,0,0,1);
			glRotated(15,1,1,0);
		break;
	}
	Display();
} 

void init(int argc, char *argv[]){
	// Tell Mesa GLX to use 3Dfx driver in fullscreen mode.
	//putenv("MESA_GLX_FX=fullscreen");

	// Disable 3Dfx Glide splash screen
	putenv("FX_GLIDE_NO_SPLASH=");

	// Give an initial size and position so user doesn't have to place window
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInit( &argc, argv );

	glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
	Window = glutCreateWindow(argv[0]);
	glutSetWindowTitle("GDevelop v.1.0");
	glutKeyboardFunc(keyboardFunc);
	if (!Window) {
		printf("Error, couldn't open window\n");
		exit(1);
	}
	//glutFullScreen();
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glPushMatrix();
	glScaled(0.25,0.25,0.25);
	glRotated(-90,1,0,0);
	glRotated(-45,0,0,1);
	glRotated(15,1,1,0);
	zprInit();
	glutPassiveMotionFunc(passiveMouseMotion);
	//============menu=============
	int valueInit=1;
	int visibleOptionMenu=createMenu(*root, valueInit, setVisibleFunc);
	valueInit=1;
	int drawOptionMenu=createMenu(*root, valueInit, setSolidFunc);
	glutCreateMenu(setVisibleFunc);
	glutAddSubMenu("visible/invisible",visibleOptionMenu);
	glutAddSubMenu("solid/wired",drawOptionMenu);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
	//==============================
	glutDisplayFunc( Display );
	glutMainLoop();
}

/*pair<int, string> getProcessInfo(void)
   {
      char buf[256];
      memset(buf, 0, sizeof(buf));
      int pid=getpid();
      ostringstream os;
      os<<"/proc/"<<pid<<"/exe";
      readlink(os.str().c_str(),buf, sizeof(buf));
      return make_pair(pid,buf);
   }

pair<string, string> extractPathAndFile(string s)
   {
      unsigned p=s.rfind("/");
      if(p==string::npos)
         return make_pair("",s);
      return make_pair(s.substr(0,p+1), s.substr(p+1,s.size()-p-1));
   }

GBox *loadDynamicObject(const char *soName){
	pair<string, string> info=extractPathAndFile(getProcessInfo().second);
    info.first+=soName;
	void *handle=dlopen(info.first.c_str(), RTLD_NOW);
    //void *handle=dlopen(soName, RTLD_NOW);
	if(handle==NULL)
		{
			cout<<dlerror()<<endl;
			return NULL;
		}
	void *mkr=dlsym(handle,"maker");
	GBox *dynObject=((GBox*(*)(GBox&))mkr)(*root);
	return dynObject;
}

*/
//================================================
//=============Construction demos=================
//================================================
void wallsDemo(GBox *&root);
void multiCubeDemo(GBox *&root);
void nucleoMitterDemo(GBox *&root);
void BaseDemo(GBox *&root);
void BoquillaDemo(GBox *&root);
void EjemploDemo(GBox *&root);
void Perno12Demo(GBox *&root);

void nucleoEnrolladoDemo(void){
	createRoot(root, GBox);
	root->beginDefinition();
	root->endDefinition();

	NSTransformador::NucleoEnrollado *nucleo;
	createChild(nucleo, NSTransformador::NucleoEnrollado, *root);
	nucleo->beginDefinition();
	nucleo->asignarDimensiones(GPlainInputs("C:\\NucleoTest.xml"));
	nucleo->endDefinition();
}

void nucleoMitterDemo(void){
	createRoot(root, GBox);
	root->beginDefinition();
	root->endDefinition();

	NSTransformador::NucleoMitter *nucleo;
	createChild(nucleo, NSTransformador::NucleoMitter, *root);
	nucleo->beginDefinition();
	//nucleo->asignarDimensiones(GXMLInputs("d:\\manual oscar montoya\\NucleoTest.xml"));
	nucleo->endDefinition();
}
void BoquillaDemo(void){
	createRoot(root, GBox);
	root->beginDefinition();
	root->endDefinition();

	NSTransformador::Boquilla *boquilla;
	createChild(boquilla, NSTransformador::Boquilla, *root);
	boquilla->beginDefinition();
	boquilla->asignarDimensiones(GXMLInputs("C:\\Boquilla.xml"));
	boquilla->endDefinition();
}
void EjemploDemo(void){
	createRoot(root, GBox);
	root->beginDefinition();
	root->endDefinition();

	NSTransformador::EjemploGBox* ejemplo;
	createChild(ejemplo, NSTransformador::EjemploGBox, *root);
	ejemplo->beginDefinition();
	ejemplo->asignarDimensiones(GXMLInputs("E:\\EjemploGBox.xml"));
	ejemplo->endDefinition();

	GXMLInputs inputs;
	ejemplo->obtenerDimensiones(inputs);
	inputs.saveToFile("E:\\EjemploGBox.xml");
}

void ConectorDemo(void){
	createRoot(root, GBox);
	root->beginDefinition();
	root->endDefinition();

	NSTransformador::Conector *conector;
	createChild(conector, NSTransformador::Conector, *root);
	conector->beginDefinition();
	conector->asignarDimensiones(GXMLInputs("C:\\conector.xml"));
	conector->endDefinition();
}

void BaseDemo(void){
	createRoot(root, GBox);
	root->beginDefinition();
	root->endDefinition();

	NSTransformador::Base *base;
	createChild(base, NSTransformador::Base, *root);
	base->beginDefinition();
	base->asignarDimensiones(GXMLInputs("C:\\Base.xml"));
	base->endDefinition();
}

void bobinaDemo(void){
	createRoot(root, GBox);
	root->beginDefinition();
	root->endDefinition();

	NSTransformador::Bobina *bobina;
	createChild(bobina, NSTransformador::Bobina, *root);
	bobina->beginDefinition();
	bobina->asignarDimensiones(GXMLInputs("C:\\BobinaTest.xml"));
	bobina->endDefinition();

	/*GPolyLine *lead;
	createChild(lead, GPolyLine, *root);
	lead->beginDefinition();
		lead->setNw(bobina->getNormal(RIGHT));
		lead->setLineThickness(inch(0.5));
		lead->setLineWidth(inch(2));
		lead->addPoint(bobina->getPoint(TOPLEFTFRONT));
		lead->addPoint(bobina->getPoint(TOPLEFTREAR));
		lead->addPoint(bobina->getPoint(TOPRIGHTREAR));
		lead->addPoint(bobina->getPoint(TOPRIGHTFRONT));
		lead->addPoint(bobina->getPoint(BOTTOMLEFTFRONT));
	lead->endDefinition();*/

}

void canalDemo(void){
	createRoot(root, GBox);
	root->beginDefinition();
	root->endDefinition();

	NSTransformador::Canal *canal;
	createChild(canal, NSTransformador::Canal, *root);
	canal->beginDefinition();
		canal->asignarDimensiones(GXMLInputs("/home/khayyam/Desktop/canalTest.xml"));
	canal->endDefinition();
	/*GXMLInputs inputs;
	canal->obtenerDimensiones(inputs);
	inputs.saveToFile("/home/khayyam/Desktop/canalTest.xml");*/
}

void anguloDemo(void){
	createRoot(root, GBox);
	root->beginDefinition();
	root->endDefinition();

	NSTransformador::Angulo *angulo;
	createChild(angulo, NSTransformador::Angulo, *root);
	angulo->beginDefinition();
	angulo->asignarDimensiones(GXMLInputs("d:\\canalTest.xml"));
	angulo->endDefinition();
	/*GXMLInputs inputs;
	angulo->obtenerDimensiones(inputs);
	inputs.saveToFile("d:\\canalTest.xml");*/
}

void canalLateralDemo(void){
	createRoot(root, GBox);
	root->beginDefinition();
	root->endDefinition();

	NSTransformador::CanalLateral *canal;
	createChild(canal, NSTransformador::CanalLateral, *root);
	canal->beginDefinition();
		//canal->asignarDimensiones(GXMLInputs("/home/khayyam/Desktop/canalLateralTest.xml"));
	canal->endDefinition();
	/*GXMLInputs inputs;
	angulo->obtenerDimensiones(inputs);
	inputs.saveToFile("/home/khayyam/Desktop/anguloTest.xml");*/
}


int main(int argc, char *argv[])
{
	//root= new GBox();

	
	
	//nucleoMitterDemo();
	nucleoEnrolladoDemo();
	//bobinaDemo();
	//canalDemo();
	//anguloDemo();
	//canalLateralDemo();//corre pero no es correcto lo que muestra
	//BaseDemo();
	//BoquillaDemo();
	//EjemploDemo();///es un Dummy
	
	init(argc, argv);
	delete root;
	return 0;
}

