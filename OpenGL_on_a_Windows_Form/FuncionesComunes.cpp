///////////////////////////////////////////////////////////////////////////////////
//Programa:		FuncionesComunes.cpp
//Autor:		Diana Carolina Osuna Arceo
//Fecha:		20/Mar/07
//Descripcion:	Implementacion de las funciones de uso general del proyecto
//
//				Informacion privilegiada de Prolec-GE.
//				La informacion contenida en este documento es informacion
//				privilegiada de  Prolec-GE y es revelada de manera 
//				confidencial. No debera ser utilizada, reproducida o 
//				revelada a otros sin el consentimiento escrito de Prolec-GE.
//
//				Prolec-GE Privileged information.
//				The information reveled in this document is confidential and
//				Prolec-GE property. Must not be used, reproduced or revealed 
//				to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////

//Revisiones

//Fecha			Descripcion										Persona		Folio
//dd/mmm/aa		Descipcion de revision o modificacion			XXX			XXXYYY


#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

//#if defined(_WINDOWS)
#include <windows.h>
//#endif

#include "Definiciones.h"
#include "FuncionesComunes.h"


using namespace std;



///////////////////////////////////////////////////////////////////////////////////
//Funcion:			ROUND
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Funcion que redondea al entero mas proximo y multiplo del para-
//					metro div es el que corresponde a la funcion redondea-entero-sup 
//					de ICAD
//Fecha:			20/Mar/07
///////////////////////////////////////////////////////////////////////////////////
int ROUND (double valor, int div)
{
	return ( fmod(valor, div) != 0) ? ( (int)valor / div + 1) * div : div;
}

///////////////////////////////////////////////////////////////////////////////////
//Funcion:			ROUND
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Sobrecarga de la funcion que redondea al entero mas proximo 
//Fecha:			02/May/07
///////////////////////////////////////////////////////////////////////////////////
int ROUND (double valor)
{
	return  (int) ((ceil(valor) - valor < 0.5 ) ? ceil(valor) : floor(valor) );
}

///////////////////////////////////////////////////////////////////////////////////
//Funcion:			mm_in
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Funcion que convierte milimetros a pulgadas
//					Es el correspondiente a la funcion mm-in de ICAD
//Fecha:			20/May/07
///////////////////////////////////////////////////////////////////////////////////
double mm_in(double mm)
{
	return (mm/25.4);
}

///////////////////////////////////////////////////////////////////////////////////
//Funcion:			member
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Funcion que busca un elemento en un arreglo y devuelve 1 si lo 
//					encuentra, 0 caso contrario
//Fecha:			28/03/07
///////////////////////////////////////////////////////////////////////////////////
int member (int ielemento, int inumelementos, int* iValores)
{
	int i=0, iencontrado=0;
	while (i<inumelementos && !iencontrado){
		if (ielemento == iValores[i])
			iencontrado = 1;
		else
			iencontrado = 0;
		i++;
	}
	return iencontrado;
}

///////////////////////////////////////////////////////////////////////////////////
//Funcion:			REDONDEASUP1
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Funcion que redondea al superior inmediato
//Fecha:			17/05/07
///////////////////////////////////////////////////////////////////////////////////
double REDONDEASUP1(double val, double redondeo)  //tavo feb-07-06
{
	double res;
	double a,b;
	a=floor(val/redondeo)*redondeo;
	b=val-a;
	if(b>0)
	{
		res=(floor(val/redondeo)+1)/(1/redondeo);
	}
	else
	{
		res=val;
	}
	return res;
}



///////////////////////////////////////////////////////////////////////////////////
//Funcion:			REDONDEASUP_PRE
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Funcion que redondea al superior con la precision especificada
//Fecha:			17/05/07
///////////////////////////////////////////////////////////////////////////////////
double REDONDEASUP_PRE(double val, double redondeo, double precision)
{
	double res;
	if(fmod(val,redondeo)>precision)
	{
		res=(floor(val/redondeo)+1)/(1/redondeo);
	}
	else
	{
		res=val;
	}
	return res;
}

//DOA 20070706 Inicia

////////////////////////////////////////////////////////////////////////////////////
//Funcion:			calcularPesoEspecifico
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Calcula el peso especifico de los materiales
//Fecha:			29/06/07
////////////////////////////////////////////////////////////////////////////////////
double calcularPesoEspecifico (	TipoMaterial tMaterial )
{
	double dPesoEspecifico=0.0;

	switch (tMaterial)
	{
	case AL:
		dPesoEspecifico = 2.703e-6;
		break;
	case CU:
		dPesoEspecifico = 8.89e-6;
		break;
	default:
		dPesoEspecifico = 0;	
	}

	return dPesoEspecifico;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Funcion:			calcularPesoEspecifico
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Calcula el peso especifico de los materiales
//Fecha:			29/06/07
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
double calcularPesoEspecifico (	MatTrafo tMaterial )
{
	double dPesoEspecifico=0.0;

	switch (tMaterial)
	{
	case FIERRO_NEGRO:
		dPesoEspecifico = 7.86e-6;
		break;
	case ACERO_INOXIDABLE:
		dPesoEspecifico = 7.2e-6;
		break;
	default:
		dPesoEspecifico = 0;	
	}

	return dPesoEspecifico;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Funcion:			calcularPesoEspecifico
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Calcula el peso especifico de los materiales
//Fecha:			29/06/07
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
double calcularPesoEspecifico (	MatOtro tMaterial )
{
	double dPesoEspecifico=0.0;

	switch (tMaterial)
	{
	case DIAMANTE:
		dPesoEspecifico = 1.1e-6;
		break;
	case HI_VAL:
		dPesoEspecifico = 1.15e-6;
		break;
	case CREPE:
		dPesoEspecifico = 1.0e-6;
		break;
	case LEBANITE:
		dPesoEspecifico = 1.12e-6;
		break;
	case FLEJE:
		dPesoEspecifico = 7.85e-6;
		break;
	case SILICIO:
		dPesoEspecifico = 7.656e-6;
		break;
	case MADERA_HAYA:
		dPesoEspecifico = 0.78e-6;
		break;
	case CARTON_T4:
		dPesoEspecifico = 1.22e-6;
		break;
	default:
		dPesoEspecifico = 0;	
	}

	return dPesoEspecifico;
}

//DOA 20070706 Termina






///////////////////////////////////////////////////////////////////////////////////
//Funci�n:		sumaA
//Autor:		Jos� Fernando Reyes Salda�a
//Descripci�n:	Suma los elementos de un arreglo de enteros
//Fecha:		19/Jun/06
///////////////////////////////////////////////////////////////////////////////////
int sumaA(int* i_Arreglo, int iTamano){
	int iSuma = 0;
	for(int i=0; i<iTamano; i++){
		iSuma += i_Arreglo[i];
	}
	return(iSuma);
}


///////////////////////////////////////////////////////////////////////////////////
//Funci�n:		sumaA
//Autor:		Jos� Fernando Reyes Salda�a
//Descripci�n:	Suma los elementos de un arreglo de doubles
//Fecha:		19/Jun/06
///////////////////////////////////////////////////////////////////////////////////
double sumaA(double* d_Arreglo, int iTamano){
	double dSuma = 0;
	for(int i=0; i<iTamano; i++){
		dSuma += d_Arreglo[i];
	}
	return(dSuma);
}



///////////////////////////////////////////////////////////////////////////////////
//Funci�n:		maxA
//Autor:		Jos� Fernando Reyes Salda�a
//Descripci�n:	Regresa el elemento mayor de un arreglo
//Fecha:		19/Jun/06
///////////////////////////////////////////////////////////////////////////////////
template <class T> T maxA(T* ptArreglo, int iTamano){
	int i = 0;
	T tMayor = 0;
	for(i=0; i<iTamano; i++){
		if(tMayor < ptArreglo[i]){
			tMayor = ptArreglo[i];
		}
	}
	return(tMayor);
}


///////////////////////////////////////////////////////////////////////////////////
//Funci�n:		maxA
//Autor:		Jos� Fernando Reyes Salda�a
//Descripci�n:	Regresa el elemento mayor de un arreglo
//Fecha:		19/Jun/06
///////////////////////////////////////////////////////////////////////////////////
template <class T> bool miembro(T tElemento, T* tLista, int iTamano){
	for(int i=0; i<iTamano; i++){
		if(tElemento == tLista[i]){
			return(true);
		}
	}

	return(false);
}


