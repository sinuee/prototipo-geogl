///////////////////////////////////////////////////////////////////////////////////
//Programa:		Definiciones.h
//Autor:		Diana Carolina Osuna Arceo
//Fecha:		20/Mar/07
//Descripcion:	Definiciones generales usadas en el proyecto
//
//				Informacion privilegiada de Prolec-GE.
//				La informacion contenida en este documento es informacion
//				privilegiada de  Prolec-GE y es revelada de manera 
//				confidencial. No debera ser utilizada, reproducida o 
//				revelada a otros sin el consentimiento escrito de Prolec-GE.
//
//				Prolec-GE Privileged information.
//				The information reveled in this document is confidential and
//				Prolec-GE property. Must not be used, reproduced or revealed 
//				to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////

//Revisiones

//Fecha			Descripcion										Persona		Folio
//dd/mmm/aa		Descipcion de revision o modificacion			XXX			XXXYYY
//22/Oct/08		Se agrego enum TipoBobina para SST				JJVM
//22/Oct/08		Se agrego enum ArregloBobinaSST					JJVM
//13/Dic/08		Se agregaron valores al enum de radiadores		JJVM

#ifndef DEFINICIONES_H
#define DEFINICIONES_H

#pragma warning(disable:4786)

#include <string>

//Definicion de constantes

//cambios SSTs
//densidades de corriente de conductores
/*:densidad-corriente-bt-max (:prompt "*) Densidad Corr. BT Max" 
			       :domain :number
			       :default (if (eql (the :root :material-bt) :al)
					    2.5
					  3.5)*/

#define DENS_MAX_AL 2.5 //densidad de corriente maxima para el alumnio (para 55 o sobreelevacion 65)
#define DENS_MAX_AL5565 2.5 //densidad de corriente maxima para el aluminio con sobreelevacion 55/65
#define DENS_MAX_CU 3.5 //densidad de corriente maxima para el cobre (para sobreelevacion 55 o sobreelevacion 65)
#define DENS_MAX_CU5565 3.5 //densidad de corriente maxima para el cobre con sobreelevacion 55/65

#define PI 3.1415926535897932384626433832795
#define PESO_ESP_FE 0.00000786
#define MAX_NUM_VAR 100						//Numero maximo de variables para la serializacion
#define MAX_NUM_NOD 60						//Numero maximos de nodos para la serializacion
#define MAX_NUM_REF 5
#define MAX_NUM_TAR 20						//Numero maximo de tarimas

#define A_NUCLEO_MIN 25						//ANucleo Min
#define A_NUCLEO_MAX 140					//ANucleo Max
#define B_NUCLEO_MIN 120					//BNucleo Min
#define B_NUCLEO_MAX 560					//BNucleo Max

//Constantes para embarque
#define FACTOR_RUNNERS  101.6				//FactorRunners
#define ANCHO_PLATAFORMA  2340				//Ancho Plataforma (mm)
#define LARGO_PLATAFORMA  14630				//Largo Plataforma (mm)
#define CAPACIDAD_PLATAFORMA  44000			//Capacidad 44000 lbs * 0.4536 = 199958.4 kgs. 

#define DELETEARRAY(array) delete [] array; array = NULL;
#define DELETEARRAYDOBLE(array,iTamanio) {for(int ii = 0; ii < iTamanio; ii++) { delete [] array[ii]; }} delete [] array; array = NULL;


//AGO0511080656 AGREGADA PARA FUNCION ANCHOINTPRE
// JESH 271008
#define NumRegistrosTablaBoqsLVPST 15	 //Numero de registros para la tabla boquillas-lvpst.table
#define NumRegistrosTablaBoqs 205		 //Numero de registros para la tabla boquillas1.table
//AGO0511080656 FIN ANCHOINTPRE

//AGO27110800800
#define NumRegistrosTablaTensiones 59    //Numero de registros de la tabla de calsetension

//AGO1012081351 SE AGREGO PARA LAS FUNCIONES DE ENFRIAMIENTO
#define NumRegistrosTablaGargantas 75    //Numero de registros de la tabla de gargantas

#define NUM_REGISTROS_TABLA_TENSIONESAT 90 //numero maximo de registros de la tabla tensionesAT
#define NUM_REGISTROS_TABLA_TENSIONESATJ 30 //numero maximo de registros de la tabla tensionesATJ
#define NUM_REGISTROS_TABLA_CM 40// CJFZ 3-Sep-07 numero maximo de registros para la tablaCM 

#define NumRegistrosTablaPinturas 31	 //Numero de registros para la tabla pinturas.txt
#define NumRegistrosTablaBoquillasAT 70 //85  /// Numero de registros para la tabla de Boquillas AT usada en AlturaTanque
#define NumRegistrosTablaBoquillasBT 20  /// Numero de registros para la tabla de Boquillas BT usada en AlturaTanque
#define NumRegistrosTablaDimBoquillasBT 11  /// Numero de registros para la tabla de Dim Boquillas usada en AlturaTanque	//DOA 20070711
#define FactoresCarga 4						//numero de factores de carga usados en el calculo de eficiencias
#define NumRegistrosTablaMandriles 315		//numero elementos en tabla mandriles, usado en geometria nucleo
#define ToleranciaMandriles 1				//tolerancia +-1 usada para elegir de la tabla de mandriles el valor mas
											//cercano al dimetro calculado de la dona chica y dona grande
#define NumRegistrosTablaAislamientos  18	//numero elementos en tabla aislamientos, usada en calculo bobina
#define NumRegistrosTablaFactorLP 58		//numero elementos en tabla factores LP, usada en calculo bobina
#define NumRegistrosTablaEba 19				//numero elementos en tabla EBA, para kvas > 150
#define NumRegistrosTablaEbaMenor 9			//numero elementos en tabla EBA, para kvas <= 150	//DOA 20071203
#define NumRegistrosTablaBiles	  12		//numero elementos en tabla Biles // CJFZ 4-Abril08
#define FactorFa 1.15	//Factor Fa, usado en el calculo de gradientes
#define NumCorrientes 4 //Marca el numero de iteraciones en el calculo de gradientes
#define TemperaturaAmbiente	30 //grados centigrados 
#define AnchoCuna		 10
#define SeparacionCuna	 20			
#define AnchoCuna		 10
#define SeparacionCunaEBA	 15		
#define PesoTirasCrepe   8 * (0.508 * 100 * 210) * calcularPesoEspecifico(CREPE)		//peso tiras crepe, p/aislamiento del eba //DOA 20070914	

/// DATOS DEL MATERIAL ///
#define EsfCedenciaIntA36	2533	//2549					// A36 Esfuerzo de cedencia sist. int. Kg/cm^2		//DOA 20070829
#define EsfCedenciaIntInox	2146	//2100					// Ac.Inox. Esfuerzo de cedencia sist. int. Kg/cm^2 //DOA 20070829
#define	ModuloElastico	  2100000 							// Modulo de elasticidad					Kg/cm^2
#define	FactorSeguridad   1.2 								// Factor de seguridad (debe ser > 1)
#define	DensidadRelat	  0.786 							// Densidad relativa del acero				Kg/m^3
#define DensidadAceite		0.89			//kg/lt

//Dimensiones del Gabinete para norma J y K
#define DimGabNormaJ 540 // Dimension para gabinete con norma J = 540 mm (Profundidad)
#define DimGabNormaK 450 // Dimension para gabinete con norma K = 450 mm (Profundidad)
#define LimInfRelDblVoltaje 1.5 // Limite inferior para la relacion de doble voltaje
#define LimSupRelDblVoltaje 3.53 // 3.5->3.53 ihl 08/25/2008 19688 // Limite superior para la relacion de doble voltaje


//////////////////////////
// Kg/cm^2        A36        AcInox
// EsfCedencia   2,549       2,100
// EsfUltimo     4,222       5,700
//
// PSI            A36        AcInox
// EsfCedencia   36,000      30,500
// EsfUltimo     60,000      81,800
//////////////////////////
#if !defined(max)
#define max(a,b) ( (a)<(b) ? (b) : (a) )  //saca el maximo de dos numeros
#endif

#if !defined(min)
#define min(a,b) ( (a)<(b) ? (a) : (b) )  //saca el minimo de dos numeros
#endif

//Para el calculo de largo y profundidad total del transformador
#define DIST_FRENTE_GAB_ANSI 24				//distancia frente-gabinete para norma ansi
#define DIST_FRENTE_GAB_JK   30.7			//distancia frente-gabinete para norma J y K
#define DIST_GAB_OREJA_ANSI 86				//Distancia Gabinete-oreja para norma ansi
#define DIST_GAB_OREJA_JK 7.6				//Distancia Gabinete-oreja para norma J y K
#define LARGO_OREJA_ANSI 151				//Largo de oreja para norma ansi I y II
#define LARGO_OREJA_JK 151					//Largo de Oreja para norma J y K
#define EXTERIOR_GAB_ANSI 40.64				//Para tomar exterior del gabinete
#define EXTERIOR_GAB_JK 27				    //Para tomar exterior del gabinete
#define MANIJA_ANSI 60						//Manija para norma ansi
#define MANIJA_JK 34.35						//Manija para norma J y K	//DOA 20071011 //32 :width (+ 28 6.35)
#define DIST_TANQUE_CUBIERTA 65				//Distancia de Tanque a cubierta
#define DIST_OBLEA_CABEZAL 150				//Distancia Oblea-cabezal
#define CEJA_TANQUE 50						//Ceja Tanque
#define CEJA_BASE_ZONA_SISMICA 152.4		//Ceja zona sismica 4
#define CEJA_FRENTE	152.4					//PBO 20070704 Ceja frente
#define CEJA_CUBIERTA	65.0				//PBO 20070704 Ceja cubierta
#define CEJA_BASE	50						//PBO 20070704 Ceja base 
#define ALTO_PARTICION	101.6				//PBO 20070704 Alto de particion
#define DIST_OREJA_FRENTE 56.0				//PBO 20070704 distancia de la oreja al frente
#define DIST_BASE_TANQUE 5.0				//PBO 20070704 distancia de la base al tanque
#define DIST_TANQUE_BASE_ZONA_SISMICA	15.0	//PBO 20070704 distancia del tanque a base en zona sismica

#define TAM_LST_MONITOR 300	//numero maximo de trabajos que puede listar el monitor en su pantalla

//Checar nombres en caso de que ya existan valores para ciertos datos
#define CostoAceiteMineral	0.94			//USD/Lts
#define CostoBetaFluid		0.94			//USD/Lts
#define CostoFR3			0.94			//USD/Lts
#define CostoSilicon		0.94			//USD/Lts
#define CostoAceiteCanada	0.94			//USD/Lts

//Valores aproximados
#define CostoA36			0.90			//USD/Kg
#define DensidadA36			7.86			//e-6 Kgs/mm^3
#define CostoInox			5.60			//USD/Kg
#define DensidadInox		8.60			//e-6 Kgs/mm^3

#define A36					0
#define	AcInox				1

#define TOLERANCIA_MITTER	10				//SEBM 31Oct08
#define SILICIO_ORIENTADO	7.656E-6;		//SEBM 31Oct08


enum TipoArticulo
{
	AB19,
	AB20,
	AF37,
	AF38,
	AK03,
	AK04,
	AN35,
	AS76,
	B228,
	B245,
	B289,
	B323,
	B324,
	B325,
	B398,
	BA20,
	CD39,
	CD40,
	CD41,
	CH75,
	CH76,
	CH77,
	CL12,
	CN83,
	CP78,
	CU25,
	D648,
	D650,
	DU92,
	DX13,
	DX16,
	DX20,
	DX24,
	DX26,
	DX29,
	DX31,
	DX33,
	E191,
	E192,
	E193,
	E949,
	EG91,
	EM29,
	ER00,
	F688,
	F689,
	F690,
	F691,
	F692,
	F693,
	F694,
	F695,
	F696,
	F697,
	F698,
	F699,
	F704,
	F706,
	F811,
	F813,
	F981,
	F983,
	F986,
	F989,
	F991,
	FD37,
	GV71,
	GV72,
	GV73,
	JH38,
	JH41,
	K220,
	MG48,
	VZ30,
	Z902,
	Z910
};

enum TipoConector{
	CLEMAAT1,
	CLEMAAT2,
	CLEMABT750U,
	ESPADA,
	PERNO1,
	PERNO2,
	ZAPATA,
	ZAPATAB4
};
enum TipoATBT {
	AT,
	BT,
	AT_BT //AGO0311081056 AGREGADA PARA CUANDO MANEJA AMBAS TENSIONES.
};

//JJVM 1/Dic/08 Dibujos de Herrajes
enum DibujoHerraje
{
STA3220,
STB0678,
STA2046,
STB0673,
STB0688,
STB0717,
STB0264,
STB0762,
STA2958_STA2959_STA2960

};

//JJVM 10/Dic/08 Dibujos de Tapas
enum DibujoTapa
{
STB1041,
STB1042,
STB1043,
STB1044,
STB0171,
STB0897,

NO_APLICA
};
//JJVM 22/Oct/08 Tipo de Bobina
enum TipoBobina {
	BOBINA_NORMAL,
	BOBINA_PAQUETE
};

//JJVM 22/Oct/08 Arreglo de bobina NOTA: Ya existe un arreglo de bobina este se agrego para SST
enum ArregloBobinaSST{	//enumeracion para los diferentes tipos de arreglo de bobina
	BOB_LINEA,
	BOB_TRIANGULO
};

//Estados del disenio en el Config2XML
enum tTipoEstadoDisenio
{
	NUEVO  = 10,			//Nuevo disenio
	ENPROCESO = 11,			//Disenio en proceso de optimizacion
	OPTIMIZADO  = 12,		//Dise�o optimizado enviado al configurador
	ERROR_DIS = 13,			//Error en el disenio
	REVING = 14,			//Disenio en revision de ingenieria
	OPTCONFIG = 15,			//Dise�o Optimizado Listo para enviar al configurador
	CANCELADO = 16			//El dise�o fue cancelado durante su optimizaci�n

};

#define StringEstadoConfig(iEstado) EstadoOptimizacion(iEstado)

#define EstadoOptimizacion(iEstado) \
	(	\
	(iEstado==OPT_DISPONIBLE)?"Disponible": \
	(iEstado==OPT_OPTIMIZADO)?"Optimizado": \
	(iEstado==OPT_PROCESANDO)?"Procesando": \
	(iEstado==OPT_OPTIMIZANDO)?"Optimizando": \
	(iEstado==OPT_COLA)?"En cola": \
	(iEstado==OPT_ERROR)?"Con error": \
	(iEstado==NUEVO)?"Nuevo":	\
	(iEstado==ENPROCESO)?"En proceso":	\
	(iEstado==OPTIMIZADO)?"Optimizado":	\
	(iEstado==ERROR_DIS)?"Error":	\
	(iEstado==REVING)?"Revisi�n Ing.":	\
	(iEstado==OPTCONFIG)?"Carga AUDI":	\
	(iEstado==CANCELADO)?"Cancelado":	\
			"Desconocido"	\
	)

enum EvaluadorErrores {
	EVALUADOR_PESO_TANQUE_SIN_ACCS,
	EVALUADOR_PESO_TANQUE_CON_ACCS,
	EVALUADOR_PESO_PV,
	EVALUADOR_PESO_TOTAL,
	EVALUADOR_PV_DIAMETRO_EXTERIOR_NUCLEO,
	EVALUADOR_PORC_EFICIENCIA_AL_50_CARGA,
	EVALUADOR_ALT_PV_CON_FLEXIBLES,
	EVALUADOR_PORC_EFICIENCIA_AL_50,
	EVALUADOR_DECIBELES,
	EVALUADOR_DENS_MIN_MAX_FLUJO,
	EVALUADOR_DENS_MAX_CORRIENTE_AT,
	EVALUADOR_DENS_MAX_CORRIENTE_BT,
	EVALUADOR_HOTSPOT,
	EVALUADOR_MAXGRAD_AT,
	EVALUADOR_MAXGRAD_BT,
	EVALUADOR_IMPEDANCIA,
	EVALUADOR_VARIMP,
	EVALUADOR_PCSC,
	EVALUADOR_TEMPCONDCC_AT,
	EVALUADOR_TEMPCONDCC_BT,
	EVALUADOR_NIVEL_AISLAMIENTO,
	EVALUADOR_PERDIDAS_VACIO,
	EVALUADOR_PERDIDAS_TOTALES,
	EVALUADOR_IEX,
	EVALUADOR_RELACION_VOLTAJES,
	EVALUADOR_NUM_DUCTOS_PUNTAS_AT,
	EVALUADOR_NUM_DUCTOS_PUNTAS_BT,
	EVALUADOR_NUM_DUCTOS_VENTANA_AT,
	EVALUADOR_NUM_DUCTOS_VENTANA_BT,
	EVALUADOR_NUM_DUCTOS_PUNTAS_VENTANA_AT,
	EVALUADOR_NUM_DUCTOS_PUNTAS_VENTANA_BT,
	EVALUADOR_EBA,
	EVALUADOR_LARGO_BOBINA,
	EVALUADOR_ALT_PV_CONTANDO_FLEXIBLES,
	EVALUADOR_DIST_MIN_PV_BARRENO_PRIMER_BOQ,
	EVALUADOR_TABLA_MANDRILES,
	EVALUADOR_BNUCLEO,
	EVALUADOR_SOBREELEVACION,											//DOA 20070815
	EVALUADOR_WATTSBOB,
	EVALUADOR_LARGOTOTAL,//PBO 20070817
	EVALUADOR_PROFUNDIDADTOTAL, //PBO 20070817
	EVALUADOR_ALTOTOTAL, //PBO 20070817
	EVALUADOR_ANUCLEO, 

	EVALUADOR_TOC,
	EVALUADOR_COSTOMAT,
	EVALUADOR_COSTOPERD,
	EVALUADOR_PENALTOTAL,
	EVALUADOR_TOCMASPENALTOTAL,
	EVALUADOR_LIMINFIMP,
	EVALUADOR_LIMSUPIMP,
	EVALUADOR_ERRIMPLIMINF,
	EVALUADOR_ERRIMPLIMSUP,

	EVALUADOR_TAMANO //este elemento debe ir al final de la enumeracion
};



enum TipoLiquido	//enumeracion de tipo de liquidos usado en funcion entre_cap_at_pp
{
	ACEITE = 1,
	RTEMP = 2,
	BETA_FLUID = 3,
	SILICON = 4,
	FR3 = 5,
	//CJFZ Se agregaron tipos de liquido para TI
	SILICON_DC = 6,			//Silicon D C
	NYNAS_11GBX = 7,		//Nynas 11gbx Us
	LUMINOL_TR_I = 8,		//Luminol Tr I
	LUMINOL_TR_II =9,		//Luminol Tr II
	NYTRO = 10						//Nytro
};

enum TipoConexion //enumeracion de tipo conexion
{
	ESTRELLA,
	DELTA,
	DELTA_MIDTAP
};

enum TipoMaterial //enumeracion de tipo material
{	
	AL,
	CU,
	CUALQUIER_MATERIAL// CJFZ 1-Ago-07

};


enum TipoConductor //enumeracion de tipo conductor
{	LAMINA,
	MAGNETO,
	SOLERA,
	CUALQUIER_CONDUCTOR// CJFZ 1-Ago-07

	//ALAMBRE,		//Material usado en funcion perdidas eddy
	//MAGNETO_REDONDO	//Material usado en funcion perdidas eddy
};

/* //YLD20070709
enum TipoTransformador //enumeracion de tipo transformador
{
	PEDESTAL,
	POZO
	//ESTACION,
	//L_CORTOS,
	//CANGREJO
};
*/

//Inicia CJFZ 30-Jul-07
//Tipo de Eficiencia segun TSL
enum TipoEficTSL 
{
	TSL_NO,
	TSL_1,
	TSL_2,
	TSL_3, 
	TSL_4,
	TSL_5,
	TSL_6
};
//Termina CJFZ 30-Jul-07



enum GradoSilicio
{	M3,
	M5,
	M0H,
	CUALQUIER_LAMINA,
	M4, //CJFZ Cambios SSTS
	M6  //CJFZ Cambios SSTS
};

enum TipoNucleo
{
	ENROLLADO,
	MITTER,
};

/*	YLD20070709
enum TipoCalculo{	//enumeracion para los valores que toman las opciones de Calculo
	ANTERIOR = 0,
	NUEVO = 1
};
*/

/* //YLD20070709
enum TipoNorma { //enumeracion para las  que cumple el sistema
	ANSI, 
	K0000, 
	NMX_J_285
};
*/

//Comienzan cambios YLD20070709
enum ArregloBobina{	//enumeracion para los diferentes tipos de arreglo de bobina
	BOB_RADIAL,
	BOB_AXIAL
};


//DOA 20070718
/*
enum FormaRadiador{ //YLD20070709 Se cambi� nombre de enumeraci�n para no confundir con la ya existente
	SOLDADOS,
	TRIFASICOS,
	BRIDADOS,
	VALVULA
};*/

//Terminan cambios YLD20070709

enum TipoRadTranter{
	INTEGRAL,
	TUBULAR
};

//Inicia CJFZ-26-06-07
//Tipo de perdidas
enum TipoPerdidas {
	// CJFZ 21-sep-07EVALUADAS,      
	// CJFZ 21-sep-07NO_EVALUADAS,
	MAXIMAS,
	MAXIMAS_85,
	GARANTIZADAS,
	GARANTIZADAS_85,
	NO_GARANTIZADAS
};


enum tipoBoquillaATFrente
{
	ANILLO,
	RADIAL
};
//Termina CJFZ-26-06-07

enum TipoAparatoDestino
{
	NACIONAL,
	EXPORTACION
};

//Enumeracion agregada por YLD20070620
enum TipoBase
{
	AUTOVENTILADA,
	ELEVADA,
	//Empieza SEBM 03Dic08 entradas TI
	BASE_NORMAL,
	BASE_I,
	BASE_C_CON_RUEDAS
	//Termina SEBM 03Dic08 entradas TI
};

enum TipoRuedaBase{  //SEBM 03Dic08 entrada TI
	PLANA,
	C_CEJA
};


//Tipo de grabinete
enum TipoGabinete {
	FLANGE,
	NORMAL
};

//Tipo de cerradura
enum TipoCerraduraGabinete {
	PENTAHEAD,
	HEXAHEAD
};

enum TipoRegistro{
	R8X19,
	R10X18,
	R13X17,
	R14X25,
	R15X24,
	R16X24,
	R203X482,	// CJFZ 19-Jul-07
	R346X446,	// CJFZ 19-Jul-07
	R360X400	// CJFZ 19-Jul-07
};


//Tipo de radiador
enum TipoRadiador {
	RADIADOR_PROLEC,
	RADIADOR_TRAGESA,
	RADIADOR_TUBULAR,    //SEBM 26Nov08 entrada TI
	SIN_RADIADOR		//SEBM 26Nov08 entrada TI
};

//Orientaci�n del refuerzo
enum TipoOrientacionRefuerzo {
	REFUERZO_HORIZONTAL,
	REFUERZO_VERTICAL
};

//Cambios YLD20070710
enum TipoRefuerzo
{
	REF_CANAL = 0,
	REF_SOLERA = 1,
	REF_PLACA = 2,
};

/*
//Tipo Nuevo_Anterior
enum TipoNuevo_Anterior {
	NUEVO,
	ANTERIOR
};
enum tipoBoquillaATFrente {
	ANILLO,
	RADIAL
};
*/

enum TipoBoquillaBTFrente { //PBO 20070706
	ESCALONADO,
	LINEA
};

//enumeracion para los valores que toman las opciones de Calculo
enum TipoCalculo {	
	CALCULO_ANTERIOR = 0,
	CALCULO_NUEVO = 1
};

// Tipo de liquido aislante
/* //YLD20070709
enum LiquidoAislante {
	ACEITE = 1, 
	RTEMP = 2, 
	BETA_FLUIDO = 3, 
	SILICON = 4, 
	FR3 = 5
};
*/

// Tipo de aceite
enum TipoDeAceite {
	TIPO1, 
	TIPO2
};
//Inicia CJFZ 8-Ago-07
enum TipoMercado
{
	UTILITIES,		//Utilities I
	MUNICIPALS,		//Municipals R
	COOPERATIVE,	//Cooperative R
	C_I,        //C&I C
	INTERNATIONAL   //International C
};
//Termina CJFZ 8-Ago-07
// Grado de silicio
/* YLD20070709
enum GradoSilicio {
	M3,
	M4,
	M5,
	M6,
	M0H
};
*/

enum TipoPuntas {//PBO 20070705
	PTA_FLEXIBLE,
	PTA_CABLE
};

// Tipo de boquillas de AT
enum TipoBoquillasAT { 
	BOQ_POZO, 
	BOQ_INTEGRALES,
	BOQ_PORCELANA	//CJFZ-26-06-07
};

// Tipo de perno boquillas pozo
enum TipoPernoBoquillaPozo { 
	REMOVIBLE, 
	FIJO
};

// Tipo de inserto boquillas AT
enum InsertoBoquillaAT {
	A726, 
	B070, 
	H699, 
	D170, 
	A305, 
	NO_INSERTOBOQUILLA
};

// Boquilla de AT
enum BoquillaAT { 
	F209, 
	C456, 
	ER42, 
	E540 
};

//Inicia CJFZ-26-06-07  FPG

//Marca de Boquillas de BT
enum TipoBoquillas_BT
{
	CM_BOQ,
	ABB_BOQ,
	HJ_BOQ,
	CELECO_BOQ
};
//Apartarrayos Externo 
enum TipoApartarrayosExterno
{
	SI_APART_EXT,
	NO_APART_EXT,
	PROVISION_APART_EXT
};
// Tipo de seleccionadores
enum Seccionadores {							// c/Silicon Op.Radial PedNac NormaK Else
	ALTERNATE_SOURCE,							//						 x		x		x
	UNA_SECC_DOS_POS,							//	   x		 x		 x		x		x
	DOS_SECC_DOS_POS,							//	   x				 x		x		x
    TRES_SECC_DOS_POS,							//	   x				 x		x		x
	UNA_SECC_4POS_V,							//						 x				x
	UNA_SECC_4POS_T,							//						 x				
	DOS_SECC_DOS_POS_400_UNA_SECC_DOS_POS_300,	//						 		x		
	DOS_SECC_DOS_POS_630_UNA_SECC_DOS_POS_300,	//						 x		x		x
	DOS_SECC_DOS_POS_630,						//						 x		x		x
	TRES_SECC_DOS_POS_630,						//						 x		x		x
	SIN_SECCIONADOR								//	   x         x		 x		x
};
enum TipoSeccionadores {
	EO73,
	EQ90,
	EL18,
	E596,
	IW93,
	EO72,	//EHSM 20070903
	D920,	//EHSM 20070903
	E684,	//EHSM 20070903
	E432,	//EHSM 20070903
	V897,	//EHSM 20070903
	E716	//EHSM 20070903
};
enum MarcaSeccionadores {
	ABB,
	COOPER,
	MOLONEY,
	COOPER_PLAT
};
//Comienza cambio FPG02jul07 DblVoltaje
enum TipoCambiadorDobleVoltaje {
	D873,
	D874,
	F197,	//No aparece en AUDI-3pp
	F198,	//No aparece en AUDI-3pp
	D875	//No hay reglas para incorporarlo
};

enum TipoFusibleCanister {
	/*
	AC30,
	AB69,
	OK15,
	D872,
	H716*/
	AC30,
	ABC6, 
	OK15,
	D872,
	H716
};

enum TipoCambiadorDer {
	D827,
	D829,
	AX71,
	E541,
	E542,
	E543,
	AL25,
	H262
};
//Termina cambio FPG02jul07 DblVoltaje
//Termina CJFZ-26-06-07

/* //DOA 20070718
// Tipo de pintura
enum Pintura {
	PINTURA_EXTERIOR, 
	PINTURA_INTERIOR
};
*/

// Sistema de pintura
enum SistPintura {
	PINTURA_A, 
	PINTURA_B, 
	PINTURA_C, 
	PINTURA_D, 
	PINTURA_E, 
	PINTURA_G, 
	//PINTURA_OTRO //SEBM 27Nov08 Entrada TI
//	PINTURA_H,			//DOA 20070718
//	PINTURA_OTRO_SIS	//DOA 20070718
};

// Color del acabado
enum ColorAcabado { 
	AZUL_TEKSID,
	BLANCO_MUNSELL_N95,
	BLANCO_OSTION,
	CHOCOLATE_CHIP,
	DESERT_TAN,
//	ESPECIAL,			//DOA 20070718
	GRIS_ACERO_16012,
	GRIS_ANSI_24,
	GRIS_ANSI_49,
	GRIS_ANSI_61,
	GRIS_ANSI_70,
	GRIS_ARENA,
	GRIS_AZULADO_ANSI_24,
	MARFIL_CFE_24,
	NEGRO,
	VERDE_CLARO_ANSI_40,
	VERDE_CLARO_CFE_15,
	VERDE_OBSCURO_CFE_12,
	VERDE_OLIVO_MUNSELL_7GY_329_15,
	VERDE_SEG_LYF_516,
	VERDE_SEG_LYF_682,		//DOA 20070718
	VERDE_T_628_PMX
};

// Tipo de registro
/* //YLD20070709
enum TipoRegistro {
	R_8X19,
	R_10X18,
	R_13X17,
	R_14X25,
	R_15X24,
	R_16X24
};
*/

// Tipo de bus-duct
enum BusDuct { 
	NO_BUSDUCT, 
	LATERAL, 
	SUPERIOR
};

// tipo tierra
enum TipoTierra { 
	BARRILES, 
	PLACAS
};

// Tipo antimagnetico tanque
enum TipoAntimagnetico{
	ANTIMAGNETICO_REDUCIDO, 
	ANTIMAGNETICO_NORMAL
};

// Material trafo
enum MatTrafo { //CJFZ25-05-07 Se cambio MaterialTrafo por MatTrafo
	FIERRO_NEGRO,
	ACERO_INOXIDABLE
};

// Componentes inoxidable
enum ComponentesInoxidable { 
	TANQUE, 
	FRENTE_INST,
	BASE, 
	GABINETE, 
	SILL_INF_GB
};

// Tipo de transformador
enum TipoTransformador {
	TPEDESTAL, 
	TPOZO,
	//CJFZ Inicia Cambios SSTS
	ESTACION,
	L_CORTOS,
	CANGREJO
	//CJFZ Termina Cambios SSTS
};

/*// CJFZ 30-Jul-07
// Tipo de tarima
enum TipoTarima {
	TARIMA_BARROTES, 
	TARIMA_EXPORTACION
};
// CJFZ 30-Jul-07*/


// Tipo conexion
/* //YLD20070709
enum TipoConexion {
	ESTRELLA, 
	DELTA, 
	DELTA_MIDTAP
};
*/

//enumeracion usada en el campo aislamiento del archivo conductores-shp
enum TipoAislamiento {
	AISLAMIENTO_FD,
	AISLAMIENTO_NA
};

/*	//YLD20070709
enum TipoNucleo {
	ENROLLADO,
	MITTER
};
*/


// Tension //No modificar el orden de esta enumeracion CJFZ 9-Dic-08
//Tensiones para TI
enum TipoTensionAT { 

	TENSION_2400,
	TENSION_4160,
	TENSION_4800,
	TENSION_6600,
	TENSION_6900,
	TENSION_7200,
	TENSION_7620,
	TENSION_8320,
	TENSION_12000,
	TENSION_12470,
	TENSION_13200,
	TENSION_13800,
	TENSION_14400,
	TENSION_16340,
	TENSION_19050,
	TENSION_20000,
	TENSION_22860,
	TENSION_22900,
	TENSION_23000,
	TENSION_24940,
	TENSION_33000,
	TENSION_34500,

	TENSION_2400Y_1385,
	TENSION_4160Y_2400,
	TENSION_4800Y_2770,
	TENSION_7200Y_4160,
	TENSION_8320Y_4800,
	TENSION_12000Y_6930,
	TENSION_12470Y_7200,
	TENSION_13200Y_7620,
	TENSION_13800Y_7970,
	TENSION_14400Y_8320,
	TENSION_16340Y_9430,
	TENSION_22860Y_13200,

	TENSION_23760Y_13720,
	TENSION_23900Y_13800,
	TENSION_24940Y_14400,
	TENSION_25560Y_14760,
	TENSION_33000Y_19050,
	TENSION_34500Y_19920,

	TENSION_OTRO 
	
};

enum AterrizadoNeutroAT {
	ATERRIZADO_4_5, 
	ATERRIZADO_12
};

/*
// Tipo conexion para BT
enum TipoConexionBT { 
	ESTRELLA, 
	DELTA, 
	DELTA_MIDTAP 
};
*/

// Tipo de soporte para boquillas de BT
enum TiposSoporteBoquillasBT {
	SOPORTE_FRENTE,
	SOPORTE_GANCHO_LATERAL,
	SOPORTE_SUPERIOR
};

//Tipo Conductor
/*	//YLD20070709
enum TipoConductor {
	MAGNETO,
	SOLERA,
	LAMINA
};
*/

//enumeracion de tipo material
/* //YLD20070709
enum TipoMaterial  {	
	CU,
	AL
};
*/

//Conductor especifico
//LEMF: esto no va a funcionar si llegan a meter un nuevo
//conductor a la base de datos, van a tener que modificar el codigo
//para que el optimizador no truene
/*
enum Conductor {
	CONDUCTOR1,
	CONDUCTOR2

};
*/

//Tipo Cliente
enum TipoCliente {
	OTRO, 
	CENTERPOINT_ENERGY, 
	OG_E, 
	TNP
};

//Tipo Idioma
enum TipoIdioma {
	ESPANIOL,
	ENGLISH
};

//Tipo Norma
enum TipoNorma {
	ANSI_I,
	ANSI_II,
	KD000,
	NMX_J_285,
	CSA, //AGO PARA NORMA CAN/CSA
//Para SSTs 
	ANSI_C57_12,
//Para Peq. Pot.
	NMX_J_284, //_ANCE_2006 
//Para estacion
	NMX_J_116
};

//Tipo Shipping
enum TipoShipping {
	LUGAR1,
	LUGAR2
};

//Enumeracion para valores para Temperatura de Sobreelevacion
enum TipoTemperaturaSobreelev
{
	TEMPERATURA55,
	TEMPERATURA65,
	TEMPERATURA55_65,
	TEMPERATURA45_55 //CJFZ SSTs 
};

// Resultado de una funci�n
enum ResultadoFuncion {
	R_OK,
	R_ERROR
};

enum TipoAccesorios{
	MANOVACUOMETRO,
	INDICADORDENIVEL,
	TERMOMETRO,
	VALVULAFILTROPRENSA,
	PLUGLLENADO,
	VALVULASOBREPRESION,
	VALVULADENITROGENO,
	VALVULADRENAJEBT,
	VALVULADRENAJEAT,
	PROVISIONBREAKER,
	PROVISIONCURRENTTRANSFORMER,
	PROVISIONKIRKEY,
	RELEVADORPRESIONSUBITA, //CJFZ 06-Jul-2007
	INDICADORFALLA, //CJFZ 06-Jul-2007
	PROVISIONPT, //CJFZ 06-Jul-2007
	DRIPSHIELD,// CJFZ 23-Jul-07
	RELEVADORMECANICO,//CJFZ 29-Ago-07
	LVPANELBOARDPROVISION, // CJFZ 4-Sep-07

	//Inicia SEBM 01Dic08 entradas TI
	RELEVMECSOBREPRESION,
	TERMOMETRODEVANADOS,
	PLACATERMICA,
	RELEVDESMONTABLEVALVULA
	//Termina SEBM 01Dic08 entradas TI

};

//enum OpcionAccesorio{
//	NO,
//	SI,
//	PROVISION,
//	CONCONTACTOS,
//	SINCONTACTOS,
//	_35SCFM, //vpgr f-17495 30CFM
//	_50SCFM, //vpgr f-17495 50CFM
//	_200SCFM,//vpgr f-17495 200CFM
//	CONOPRESOR,
//	DEPASO,
//	//Inicia CJFZ 29-Ago-07
//	_5PSI, //f-17495 vpgr _8PSI,
//	_10PSI,
//	_10PSICC,
//	_10PSICCSS,
//	//Termina CJFZ 29-Ago-07
//	//Inicia SEBM 01Dic08 entradas TI
//	_2CONTACTOS,
//	_3CONTACTOS,
//	VERTICAL,
//	HORIZONTAL
//	//Termina SEBM 01Dic08 entradas TI
//};


//Tipo de Termometro
enum TipoTermometro{
	NO_T,
	SINCONTACTOS_T,
	CONCONTACTOS_T
};

/*// CJFZ 25-Jul-07
//Tipo Opcion Fusible
enum TipoOpcionFusible{
	A,
	B
};
// CJFZ 25-Jul-07 */

//TipoPosicionPlaca
enum TipoPosicionPlaca{
	PUERTABT,
	FRENTEBT,
	AMBAS
};

enum TipoSujecionPlaca{
	CINTA,
	ATORNILLADA
};

enum TipoSafetyLabels{
	INGLES,
	BILINGUES
};

//tipo de FusibleRepuesto
enum TipoFusibleRepuesto{
	NINGUNO,
	EXP,
	EXP_AIS,
	AIS,
	CLF_RP,
	EXP_CLF_RP,
	CLF_RC
};

enum TipoEtiquetas{
	NO_E, //NO
	K175,
	BP26,
	JE20,
	BP24,
	K177,
	IW56,
	K176,
	BP22
};

enum TipoPosicionEtiquetas{
	NINGUNA,
	P,
	Z
};
//Termina CJFZ21-05-07


// JFRS 20070522
enum TiposPruebas {
	RUTINA,
	ESPECIALES
};

enum TemperaturasEnfto {
	Temp_OA,
	Temp_OAFA,
	Temp_OAFAF
};

//Tipos de Datos
enum TipoDato {
	CADENA,						//tipo char *
	ENTERO,						//tipo int
	FLOTANTE,					//tipo float
	DOBLE,						//tipo double
	PDOBLE,						//tipo double*
	BOLEANO,					//tipo  bool
	ENUM,						//tipo enum
	CSTRING
};

//Estructura de Variables
struct STRUCTXML
{
	char c_name[50];			//Nombre de la variable
	enum TipoDato tdatatype;	//Tipo de dato de la variable
	void *pvValue;				//Apuntador a la variable	
};

//Estructura de Nodos
struct STRUCTNODES
{
	void *pvValue;				//Apuntador al nodo
};

enum tipoEmbarque
{
	PALLET,
	RUNNERS
};


#define MAX_NUM_DES 60 //Numero maximo de destinos
enum destinos
{
	AL_D, 
	AR_D, 
	AZ_D, 
	CA_D, 
	CO_D, 
	CT_D, 
	DE_D, 
	FL_D, 
	GA_D, 
	HI_D, 
	IA_D, 
	ID_D, 
	IL_D, 
	IN_D, 
	KS_D, 
	KY_D, 
	LA_D, 
	MA_D, 
	MD_D, 
	ME_D, 
	MI_D, 
	MN_D, 
	MO_D, 
	MS_D, 
	MT_D, 
	NC_D, 
	ND_D, 
	NE_D, 
	NH_D, 
	NJ_D, 
	NM_D, 
	NV_D, 
	NY_D, 
	OH_D, 
	OK_D, 
	OR_D, 
	PA_D, 
	RI_D, 
	SC_D, 
	SD_D, 
	TN_D, 
	TX_D, 
	UT_D, 
	VA_D, 
	VT_D, 
	WA_D, 
	WI_D, 
	WV_D, 
	WY_D


};
//CJFZ Enumeracion para el tipo de Tapa AudiTI
enum TipoTapa
{
	TAPA_SOLDADA,
	TAPA_ATORNILLADA
};
//CJFZ Enumeracion para el tipo de boquillas en TAPA para apastaros USA
enum TipoBoquillasTapa
{
	BOQTAPA_NO, //no lleva boquillas en Tapa
	BOQTAPA_LL, //boquillas en tapa Lado Largo
	BOQTAPA_LC  //boquillas en tapa Lado Corto
	
};

//CJFZ Enumeracion para el Tipo de sujecion de garganta Audi TI
enum TipoSujecionGarganta
{
	SUJECION_BRIDADA,
	SUJECION_SOLDADA
};

struct StablaTarimas
{
	int iBDim;				//Dimension B
    int iCDim;				//Dimension C
	double dCosto;			//Costo
	char cItem[8];			//Item
	int iCantidadXCamion;	//Cantidad X Camion
};

struct StablaTarifaXDestino
{
	char cDestino[5];           //string Destino
	enum destinos tDestino;		//Destino
	int  iTarifa;				//Tarifa

};

//Inicia CJFZ20/06/07

// Material trafo
/*	//YLD20070709
enum MatTrafo { //CJFZ25-05-07 Se cambio MaterialTrafo por MatTrafo
	FIERRO_NEGRO,
	ACERO_INOXIDABLE
};
*/

/* //YLD20070709
enum TipoSeccionadores
{
	//Tipo Seccionadores
	EO73,
	EQ90,
	EL18,
	E596,
	IW93
};
enum TipoCambiadorDerivaciones
{
	D827,
	D829,
	AX71,
	E541,
	E542,
	E543,
	AL25,
	H262

};

enum TipoFusibleCanister
{
	AC30,
	ABC6, 
	OK15,
	D872,
	H716
};
enum TipoCambiadorDblVoltaje
{
	D873,
	F197,
	D874,
	F198,
	D875
};*/

struct TablaSeccionadores
{
	int iBilAT;
	TipoSeccionadores tTipo;		//tipo de accesorio
	char cTipo[6];           //Tipo de accesorio
	int iSolo;				//Accesorio solo
	int i3Fusibles;			//Accesorio + 3 fusibles
	int iApartarrayos;		//Accesorio + apartarrayos
	int iApar_3Fusibles;		//Accesorio + apartarrayos + 3 fusibles
	int i6Fusibles;			//Accesorio + 6 fusibles
	int iApar_6Fusibles;		//Accesorio + apartarrayos + 6 fusibles

};

struct TablaCambiadorDerivaciones
{
	int iBilAT;
	TipoCambiadorDer tTipo;		//tipo de accesorio
	char cTipo[6];           //Tipo de accesorio
	int iSolo;				//Accesorio solo
	int i3Fusibles;			//Accesorio + 3 fusibles
	int iApartarrayos;		//Accesorio + apartarrayos
	int iApar_3Fusibles;		//Accesorio + apartarrayos + 3 fusibles
	int i6Fusibles;			//Accesorio + 6 fusibles
	int iApar_6Fusibles;		//Accesorio + apartarrayos + 6 fusibles

};
struct TablaCambiadorDblVoltaje
{
	int iBilAT;
	TipoCambiadorDobleVoltaje tTipo;		//tipo de accesorio
	char cTipo[6];           //Tipo de accesorio
	int iSolo;				//Accesorio solo
	int i3Fusibles;			//Accesorio + 3 fusibles
	int iApartarrayos;		//Accesorio + apartarrayos
	int iApar_3Fusibles;		//Accesorio + apartarrayos + 3 fusibles
	int i6Fusibles;			//Accesorio + 6 fusibles
	int iApar_6Fusibles;		//Accesorio + apartarrayos + 6 fusibles

};
struct TablaFusibleCanister
{
	int iBilAT;
	TipoFusibleCanister tTipo;			//tipo de accesorio
	char cTipo[6];           //Tipo de accesorio
	int iSolo;						//Accesorio solo
	int iApartarrayos;				//Accesorio + apartarrayos
};

struct SSeccionadorLargo
{
	int iBilAT;										//Bil de at
	char cTipoSeccionador[7];
	enum TipoSeccionadores tTipoSeccionador;		//Tipo de seccionador
	char cTipoCambDer[7];
	enum TipoCambiadorDer tTipoCambDer;    //Tipo de cambiador de derivaciones //YLD20070709
	int  i1Secc_CLF;								// 1 Secc + CLF
	int  i2Secc_CLF;								// 2Secc + CLF
	int  i3Secc_CLF;								// 3Secc +  CLF
	int i1Secc_Apart;                               // 1Secc + Apartarrayos
	int i2Secc_Apart;                               // 2 Secc + Apartarrayos
	int i3Secc_Apart;								// 3 Secc + Apartarrayos
	int i1Secc_CLF_Apart;							// 1Secc + Apartarrayos + CLF
	int i2Secc_CLF_Apart;							// 2Secc + Apartarrayos + CLF
	int i3Secc_CLF_Apart;							// 3Secc + Apartarrayos + CLF
	int i1Secc_DblV_Apart;							// 1Secc + Apartarrayos + Cambiador DblVoltaje
	int i2Secc_DblV_Apart;							// 2Secc + Apartarrayos + Cambiador DblVoltaje
	int i3Secc_DblV_Apart;							// 3Secc + Apartarrayos + Cambiador DblVoltaje
	int i1Secc_DblV_CLF;							// 1Secc + CLF + Cambiador DblVoltaje
	int i2Secc_DblV_CLF;							// 2Secc + CLF + Cambiador DblVoltaje
	int i3Secc_DblV_CLF;							// 3Secc + CLF + Cambiador DblVoltaje
	int i1Secc_DblV_Apart_CLF;						// 1Secc + Apartarrayos + Cambiador DblVoltaje + CLF
	int i2Secc_DblV_Apart_CLF;						// 2Secc + Apartarrayos + Cambiador DblVoltaje + CLF
	int i3Secc_DblV_Apart_CLF;						// 3Secc + Apartarrayos + Cambiador DblVoltaje + CLF
};

struct SCambiadorDerLargo
{
	int iBilAT;									//Bil de AT
	char cTipo[7];								//tipo
	enum TipoCambiadorDer tTipo;				//Tipo //YLD20070709
	int iCLF;									// + CLF
	int iApart;									// + Apartarrayos
	int iCLF_Apart;								// + CLF + Apartarrayos
	int iDblV_CLF;								// + Cambiador DblVoltaje + CLF
	int iDblV_Apart;							// + Cambiador DblVoltaje + Apartarrayos
	int iDblV_CLF_Apart;						// + Cambiador DblVoltaje + CLF + Apartarrayos
};

struct SCambiadorDblVoltajeLargo
{
	int iBilAT;									//Bil de AT
	char cTipo[7];								//tipo
	enum TipoCambiadorDobleVoltaje tTipo;		//Tipo	//YLD20070709
	int iCLF;									// + CLF
	int iApart;									// + Apartarrayos
	int iCLF_Apart;								// + CLF + Apartarrayos
};

struct SCanisterLargo
{
	int iBilAT;									//Bil de AT
	char cTipo[7];								//tipo
	enum TipoFusibleCanister tTipo;		//Tipo
	int iApart;									// + Apartarrayos
};

//Estructura para almacenar los datos de las tablas de pinturas
struct SPintura 
{
	enum SistPintura tSistPintura;
	enum ColorAcabado tColor;
	double dEspesorPintura; 
	double dRendimiento;
	char cNumArticulo[60];
//	double dCosto;		//DOA 20071019
};

enum MatOtro	//Materiales que no han sido definidos 
{
	DIAMANTE,
	HI_VAL,
	CREPE,
	LEBANITE,
	FLEJE,
	SILICIO,
	MADERA_HAYA,
	CARTON_T4
};

//FPG111Jul07 Inicio
enum TipoClaseRadiador {
	TIPO_SOLDADOS,
	TIPO_DESMONTABLES,
	TIPO_VALVULA_BLOQUEO
};
//FPG11Jul07 Fin


//Inicia CJFZ 11-JUL-07 
//Combinacion de protecciones para fusibles
/*
enum CombinacionProtecciones
{
	LIMITADOR_RANGOCOMPLETO,
	LIMITADOR_EXPULSION,
	AISLAMIENTO_EXPULSION,
	EXPULSION,
	SIN_PROTECCIONES
};

enum OpcionesFusibles
{
	DUAL_SENSING, //Dual Sensing
	CURRENT_SENSING,
	SILVER_LINK,
	DUAL_ELEMENT,
	DUAL_ELEMENT_CLF,
	DUAL_SENSING_CLF,
	CURRENT_SENSING_CLF,
	SILVER_LINK_CLF,
	TERMINAL_BOARD_CLF,
	N_LOAD_FULL

};
//Termina CJFZ 11-JUL-07 
*/

//Inicia CJFZ 25-Jul-07
//Combinacion de protecciones para fusibles
enum CombinacionProtecciones
{
	LIMITADOR_RANGOCOMPLETO,	// Limitador Rango Completo
	LIMITADOR_EXPULSION,		// Limitador y Expulsion
	AISLAMIENTO_EXPULSION,		// Aislamiento y Expulsion
	EXPULSION,					// Expulsion
	SIN_PROTECCIONES			// Sin Protecciones
};

enum OpcionesFusibles
{
	CURRENT_SENSING_ISOLINK,	//	Current Sensing Expulsion Fuse + ISO Link
	DUAL_ELEMENT_ISOLINK,		//	Dual Element Expulsion Fuse + ISO Link
	DUAL_SENSING_ISOLINK,		//	Dual Sensing Expulsion Fuse + ISO Link
	SILVER_LINK_ISOLINK,		//	Silver Link Expulsion Fuse + ISO Link
	CURRENT_SENSING_CLF,		//	Current Sensing Expulsion Fuse + Partial Range CLF
	DUAL_ELEMENT_CLF,			//	Dual Element Expulsion Fuse + Partial Range CLF
	DUAL_SENSING_CLF,			//	Dual Sensing Expulsion Fuse + Partial Range CLF
	SILVER_LINK_CLF,			//	Silver Link Expulsion Fuse + Partial Range CLF
	TERMINAL_BOARD_CLF,			//	Terminal Board Expulsion Fuse + Partial Range CLF
	TERMINAL_BOARD,				//	Terminal Board Expulsion Fuse
	NO_LOAD_BREAK_CANISTER,		//	Full Range CLF + No Load-break Canister
	LOAD_BREAK_CANISTER,		//	Full Range CLF + Load-break Canister
	LOAD_BREAK_CLF				//	Load break Full Range CLF
};
//Termina CJFZ 25-Jul-07
//Termina CJFZ 11-JUL-07 


//Estructura para almacenar los datos de los refuerzos
struct SRefuerzos
{
	int iNumRefuerzos;						
	int iTipoAcero;							//0 = Acero36,	1 = Acero Inox			
	int iPanel;								//0 = AT,		1 = BT,	 2 = Partition,	3 = LL
	int iPosicion;							//0 = Superior, 1 = Inferior,	-1 = NA
	double dPresionDisenio;
    TipoRefuerzo tOptimizarSoleraOCanal;	//REF_CANAL o REF_SOLERA
	//datos del panel
	int iAltoPanel;
	double dLargoPanel;
	double dEspesorPanel;
	bool bCumplePanel;
	//datos del refuerzo
	double dAnchoRefuerzo;
	double dAltoRefuerzo;
	double dEspesorRefuerzo;
	bool bCumpleRefuerzo;
	double dPesoRefuerzo;
	double dMtsSoldadura;
};	//DOA 20070803


//Estructura para almacenar datos de Fusibles 
struct SFusible
{
	std::string sItem1;
	double dCosto1;
	std::string sItem2;
	double dCosto2;
	int iCant1;
	double dImpe;
	int iCant2;
};


//inicia CJFZCambios SSTs
enum TipoAparato
{
	MEXICO,
	USA
};
//fin cambios SSTs

// JESH 271008 
enum TipoBoquillas
{
	TBOQ_NORMAL,
	TBOQ_TIPOCOSTA
};

enum SelConectorAt
{
	NIL,
	CL,
	ESP
};

enum CambiadorAncho
{
	ABB_Cambiador,
	QUA400_200,
	QUA800_200, //SEBM 02Dic08 entradas TI
	ORTO,
	ASP63_5,
	ASP63_6,
	ASP63,
	ASP120_6,
	ASP120_5,
	QUALITY200,
	QUALITY400,
	QUALITY800,
	MOLONEY_Cambiador
}; 


enum TipoFrente
{
	IZQUIERDO,
	DERECHO
};

enum TipoEnfriamiento
{
	OA,
	OA_FA,
	OA_FA_FUTURO //AGO SSTs 
};

/*enum TipoConector34_5 //AGO0511081202 SE USA PARA SELCONECTORAT
{
	ESPADA,
	CLEMA
};*/

enum TipoAlimentacion //SEBM entradas TI 25Nov08
{
	MONOFASICO,
	TRIFASICO
};

enum TipoLocalizacion	//SEBM entradas TI 26Nov08
{
	INTERNO,
	EXTERNO
};

enum TipoGarganta	//SEBM entrada TI 26Nov08
{
	SIN_REGISTRO,
	ESTANDAR,
	DOBLE_REGISTRO,
	REGISTRO_INFERIOR
};

enum TipoConBoqAT //SEBM entradas TI 26Nov08
{
	DEFAULT,
	BARRENOS_2,
	BARRENOS_4
};

enum TipoBoquilla //SEBM entradas TI 26Nov08
{
	BOQ_USA,
	BOQ_MEXICO
};

enum MaterialBoquillas //SEBM entradas TI 26Nov08
{
	PORCELANA,
	EPOXICAS
};

enum Radiadores //SEBM entradas TI 26Nov08
{
	SOLDADOS,
	CON_VALVULA_DE_BLOQUEO,	
	DESMONTABLES,	//JJVM 13/Dic/08 Se agrego el tipo de radiador desmontable
	TIPO_TRAGESA	//JJVM 11/Dic/08 Se agrego este TIPO_TRAGESA para calcularEnfto que puede mandar tragesa
};

enum TipoDeAccesorio //SEBM 01Dic08 entrada TI
{
	ACCESORIO_ESTANDAR,
	ACCESORIO_QUALITROL,
	ACCESORIO_5MTS
};

enum TipoTuberia //SEBM 01Dic08 entrada TI
{
	FLEXIBLE,
	RIGIDA
};

enum TipoValvulaManual //SEBM 02Dic08 entrada TI
{
	SOBREPRESION,
	BLEEDER
};

enum TipoValvulaBloqueoRadial //SEBM 02Dic08 entrada TI
{
	MARIPOSA,
	TRANTE
};

enum TipoCamaraAire //SEBM 02Dic08 entrada TI
{
	AIRE_SECO,
	NITROGENO
};

enum TipoMaterialPlaca //SEBM 02Dic08 entrada TI
{
	ACERO_INOX,
	ALUMINIO,
	LAMICOID_1_16,
	LAMICOID_1_8,
	PLACA_COBRE //CJFZ Material para placa de Tierra
};

enum TipoCalentador //SEBM 02Dic08 entrada TI
{
	SIN_CALENTADOR,
	CALENTADOR_120,
	CALENTADOR_240
};

enum TipoLado //SEBM 03Dic08 entrada TI
{
	LADO_NO,
	GARGANTA,
	LADO_FLANGE,
	FLANGE_55,
	FLANGE_70,
	FLANGE_77,
	FLANGE_90,
	FLANGE_100,
	FLANGE_RECORTADO,
	AKD,
	POWERVAC,
	ATC_55,
	ATC_70,
	ATC_77,
	ATC_90,
	ATC_100,
	ATC_COMPLETO,
	ATC_RECORTADO,
	POWERBREAK,
	MOTOR_CONTROL_CENTER,
	BUSWAY,
	SWITCHBOARD

};

enum TipoMaterialATC //SEBM 04Dic08 entrada TI
{
	MAT_ACERO_INOXIDABLE,
	MAT_ACERO_CARBON
};

enum TipoConectorATBT //SEBM 04Dic08 entrada TI
{
	CTT,
	COMPRESION
};

enum TipoEntrada{
	ARRIBA,
	ABAJO
};

enum TipoCalibre  //SEBM 04Dic08 entrada TI
{
	CALIBRE_6,
	CALIBRE_4,
	CALIBRE_2,
	CALIBRE_1_0,
	CALIBRE_2_0,
	CALIBRE_3_0,
	CALIBRE_4_0,
	CALIBRE_250,
	CALIBRE_300,
	CALIBRE_350,
	CALIBRE_400,
	CALIBRE_500,
	CALIBRE_600,
	CALIBRE_750,
	CALIBRE_1000,
};

enum TipoEspada //SEBM 04Dic08 entrada TI
{
	ESPADA_NORMAL_4_BARRENOS,
	ESPADA_8_BARRENOS,
	ESPADA_12_BARRENOS
};

enum TipoCarga //SEBM 17Dic08 entrada TI
{
	ARC_FURNACE_CURRENT,
	TRIAC_10,
	TRIAC_10_H_30,
	TRIAC_20,
	TRIAC_20_H_30,
	TRIAC_30,
	TRIAC_30_H_30,
	TRIAC_40,
	TRIAC_40_H_30,
	TRIAC_50,
	TRIAC_50_H_30,
	TRIAC_60,
	TRIAC_60_H_30,
	TRIAC_70,
	TRIAC_70_H_30,
	TRIAC_80,
	TRIAC_80_H_30,
	TRIAC_100,
	TRIAC_100_H_30,
	TRIAC_110,
	TRIAC_110_H_30,
	TRIAC_90,
	TRIAC_90_H_30,
	REG_ANG_0,
	REG_ANG_10,
	REG_ANG_20,
	REG_ANG_30,
	REG_ANG_40,
	REG_ANG_50,
	REG_ANG_60,
	REG_ANG_70,
	TRAP_ANG_0,
	TRAP_ANG_10,
	TRAP_ANG_20,
	TRAP_ANG_30,
	TRAP_ANG_40,
	TRAP_ANG_50,
	TRAP_ANG_60,
	TRAP_ANG_70,
	TRAP_ANG_80,
	TRAP_ANG_90,
	FLUORESCENT_LAMP_FL_11W,
	FLUORESCENT_LAMP_FL_15W,
	FLUORESCENT_LAMP_FL_20W,
	FLUORESCENT_LAMP_FL_9W,
	FAX_MACHINE_SWITCHMODE_POWER_SUPPLY,
	HORNO_DE_INDUCCION,
	MARTIN_AND_MELIOPOULOS,
	OTRO_TIPO_CARGA,
	OFFICE_COPY_MACHINE,
	PC_SWITCHMODE_POWER_SUPPLY,
	REC_MEDIA_ONDA_FREC_60,
	REC_NO_CONTROLADO_6_PULSOS,
	SEC_LOAD_CURRENT_75_KVAS,
	SINGLE_PH_CAPACITOR_100,
	SINGLE_PH_CAPACITOR_20,
	STATIC_DRIVES,
	SHITCHMODE_POWER_SUPPLY,
	_12_PULSE_STATIC_POWER_CONVERTER,
	_13_1_STD_519,
	_13_7_STD_519,
	_13_7_STD_519_ALIMENTADOR,
	THRYRISTOR_CONTROLLED_REACTOR,
	REC_12_PULSOS_PRIMARIO,
	REC_12_PULSOS_SECUNDARIO,
	INCANDESCENT_LAMP,
	VARIABLE_SPEED_DRIVE
};

//AGO2711080816 ESTRUCTURA PARA LA TABLA CLASETENSION
struct tblClaseTensiones
{
	int iKvas;
	int iTension;
	double dKv;
	int iBil;
	int iTipoConexion;
};
#endif	//DEFINICIONES_H
