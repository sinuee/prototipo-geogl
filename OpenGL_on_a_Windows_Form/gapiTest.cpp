/*
 * gapiTest.cpp
 *
 *  Created on: Nov 30, 2008
 *      Author: khayyam
 */
/*

#include <iostream>
#include <vector>
#include "gapiDrawingTest.h"
#include "zpr.h"
#include "gapila.h"
using namespace std;
using namespace gapi;

int ScreenWidth, ScreenHeight;
GLfloat Xrot = 0.0, Yrot = 0.0;
#define WIDTH 1280
#define HEIGHT 800
int Window = 0;

#define GLERROR                                                    \
    {                                                              \
        GLenum code = glGetError();                                \
        while (code!=GL_NO_ERROR)                                  \
        {                                                          \
            printf("%s\n",(char *) gluErrorString(code));          \
                code = glGetError();                               \
        }                                                          \
    }



static void Display( void )
{
   GLERROR;
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   GAPIdrawAxes(4,0.01);
   const GLfloat grosor=0.1;
       const GLfloat ancho=0.5;
   	const int n=5;
   	GLfloat P[n][3]={
   						   {-4,0,-3},
   						   { 0,0, 3},
   						   { 4,0,-1},
   						   { 2,0, 2},
   						   {-1,0, 0}
   				   };
   	Lead<GLfloat> lead;
   	for(int i=0;i<n;i++)
   		lead.addPoint(NTuple<GLfloat,3>(P[i]));

   	static GLfloat _nw[3]={0,1,0};
   	NTuple<GLfloat,3> nw(_nw);

   	GLfloat L[n-1][8][3];
   	GLfloat normals[n-1][6][3];
   	lead.buildPoints(nw,ancho,grosor,L,normals);

   GAPIdrawLead(L,normals,n-1);
   glutSwapBuffers();
   GLERROR;
}

void init(int argc, char *argv[])
{
		   //Tell Mesa GLX to use 3Dfx driver in fullscreen mode.
		   putenv("MESA_GLX_FX=fullscreen");

		   //Disable 3Dfx Glide splash screen
		   putenv("FX_GLIDE_NO_SPLASH=");

		   //Give an initial size and position so user doesn't have to place window
		   glutInitWindowPosition(0, 0);
		   glutInitWindowSize(WIDTH, HEIGHT);
		   glutInit( &argc, argv );

       glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
   	   Window = glutCreateWindow(argv[0]);
   	   if (!Window) {
   	      printf("Error, couldn't open window\n");
   	      exit(1);
   	   }
   	   glutFullScreen();
   	   glEnable(GL_LIGHTING);
   	   glEnable(GL_LIGHT0);
   	   glEnable(GL_DEPTH_TEST);
   	   glEnable(GL_CULL_FACE);
   	   glScalef(0.25,0.25,0.25);
   	   zprInit();
   	   glutDisplayFunc( Display );
   	   glutMainLoop();
}


int main(int argc, char *argv[])
{
	//================
	init(argc, argv);





	return 0;
}

*/
