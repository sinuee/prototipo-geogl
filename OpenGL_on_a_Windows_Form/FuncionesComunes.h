///////////////////////////////////////////////////////////////////////////////////
//Programa:		FuncionesComunes.h
//Autor:		Diana Carolina Osuna Arceo
//Fecha:		20/Mar/07
//Descripcion:	Declaracion de las funciones de uso general utilizadas en el proyecto
//
//				Informacion privilegiada de Prolec-GE.
//				La informacion contenida en este documento es informacion
//				privilegiada de  Prolec-GE y es revelada de manera 
//				confidencial. No debera ser utilizada, reproducida o 
//				revelada a otros sin el consentimiento escrito de Prolec-GE.
//
//				Prolec-GE Privileged information.
//				The information reveled in this document is confidential and
//				Prolec-GE property. Must not be used, reproduced or revealed 
//				to others without written consent of Prolec-GE.
///////////////////////////////////////////////////////////////////////////////////

//Revisiones

//Fecha			Descripcion										Persona		Folio
//dd/mmm/aa		Descipcion de revision o modificacion			XXX			XXXYYY


#ifndef FUNCIONESCOMUNES_H
#define FUNCIONESCOMUNES_H


#include <string>

#include "Definiciones.h"

//#undef DllExport
#if !defined(DllExport) && defined(_WIN32)
#define DllExport __declspec(dllexport)
#endif


//using namespace std;

///////////////////////////////////////////////////////////////////////////////////
//Funcion:			ROUND
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Funcion que redondea al entero mas proximo y multiplo del para-
//					metro div es el que corresponde a la funcion redondea-entero-sup 
//					de ICAD
//Fecha:			20/Mar/07
///////////////////////////////////////////////////////////////////////////////////
DllExport int ROUND (double valor, int div);

///////////////////////////////////////////////////////////////////////////////////
//Funcion:			ROUND
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Sobrecarga de la funcion que redondea al entero mas proximo 
//Fecha:			02/May/07
///////////////////////////////////////////////////////////////////////////////////
DllExport int ROUND (double valor);			//Redondea al entero inferior o superior mas cercano

///////////////////////////////////////////////////////////////////////////////////
//Funcion:			mm_in
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Funcion que convierte milimetros a pulgadas
//					Es el correspondiente a la funcion mm-in de ICAD
//Fecha:			20/May/07
///////////////////////////////////////////////////////////////////////////////////
DllExport double mm_in(double mm);

///////////////////////////////////////////////////////////////////////////////////
//Funcion:			member
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Funcion que busca un elemento en un arreglo y devuelve 1 si lo 
//					encuentra, 0 caso contrario
//Fecha:			28/03/07
///////////////////////////////////////////////////////////////////////////////////
DllExport int member (int ielemento, int inumelementos, int* iValores);

///////////////////////////////////////////////////////////////////////////////////
//Funcion:			REDONDEASUP1
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Funcion que redondea al superior inmediato
//Fecha:			17/05/07
///////////////////////////////////////////////////////////////////////////////////
DllExport double REDONDEASUP1(double val, double redondeo);  //tavo feb-07-06

///////////////////////////////////////////////////////////////////////////////////
//Funcion:			REDONDEASUP_PRE
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Funcion que redondea al superior con la precision especificada
//Fecha:			17/05/07
///////////////////////////////////////////////////////////////////////////////////
DllExport double REDONDEASUP_PRE(double val, double redondeo, double precision);

///////////////////////////////////////////////////////////////////////////////////
//Funcion:			calcularPesoEspecifico
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Calcula el peso especifico de los materiales
//Fecha:			29/06/07
///////////////////////////////////////////////////////////////////////////////////
DllExport double calcularPesoEspecifico (	TipoMaterial tMaterial );//DOA 20070706

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Funcion:			calcularPesoEspecifico
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Calcula el peso especifico de los materiales
//Fecha:			29/06/07
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DllExport double calcularPesoEspecifico (	MatTrafo tMaterial );

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Funcion:			calcularPesoEspecifico
//Autor:			Diana Carolina Osuna Arceo
//Descripcion:		Calcula el peso especifico de los materiales
//Fecha:			29/06/07
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DllExport double calcularPesoEspecifico (	MatOtro tMaterial );



void construirDirectorioAplicacion(std::string &CFileName);

std::string itos(int iEntrada);

std::string dtos(double dEntrada, int iDecimales);

int sumaA(int* i_Arreglo, int iTamano);

double sumaA(double* d_Arreglo, int iTamano);

template <class T> T maxA(T* ptArreglo, int iTamano);

// declaración previa para compilación
template int maxA<int>(int*, int);
template double maxA<double>(double*, int);	//DOA 20070711

template <class T> bool miembro(T tElemento, T* tLista, int iTamano);

// declaración previa para compilación
template bool miembro(int, int*, int);



#endif	//FUNCIONESCOMUNES_H
