#include<GL/glut.h> 

static GLfloat xRot = 0.0f;
static GLfloat yRot = 0.0f;


void RenderScene(void)
    {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPushMatrix();
    glRotatef(xRot, 1.0f, 0.0f, 0.0f);
    glRotatef(yRot, 0.0f, 1.0f, 0.0f);

	glColor3f(0.7,0.7,0.7); 
    glBegin(GL_POLYGON);
	glVertex3f(15,0,40);
    glVertex3f(0,0,80);
	glVertex3f(0,-5,40);
	glVertex3f(8,-5,0);
	glVertex3f(8,0,0);
	glVertex3f(15,0,20);
	
    glEnd();

    glColor3f(0.3,0.3,0.3);
	glBegin(GL_POLYGON);
    glVertex3f(-15,0,40);
    glVertex3f(-15,0,20);
	glVertex3f(-8,0,0);
    glVertex3f(-8,-5,0);
	glVertex3f(0,-5,40);	
	glVertex3f(0,0,80);
	glEnd();
     
	glColor3f(.3,.3,.3);
	glBegin(GL_POLYGON);
    glVertex3f(15,5,40);
    glVertex3f(15,5,20);
    glVertex3f(13,12,17);
	glVertex3f(0,15,25);
	glVertex3f(0,13,40);
	glVertex3f(0,0,80);
	glVertex3f(15,0,40);
	glEnd();

	glColor3f(.7,.7,.7);
	glBegin(GL_POLYGON);
    glVertex3f(-15,5,40);
	glVertex3f(-15,0,40);
	glVertex3f(0,0,80);
	glVertex3f(0,13,40);
	glVertex3f(0,15,25);
    glVertex3f(-13,12,17);
    glVertex3f(-15,5,20);
    glEnd();
   
    glColor3f(.5,.5,.5);
	glBegin(GL_POLYGON);
	glVertex3f(13,12,17);
	glVertex3f(15,5,20);
	glVertex3f(15,0,20);
	glVertex3f(8,0,0);
	glVertex3f(8,10,0);
	glEnd();
    
	glColor3f(.5,.5,.5);
	glBegin(GL_POLYGON);
    glVertex3f(-8,10,0);
    glVertex3f(-8,0,0);
    glVertex3f(-15,0,20);
 	glVertex3f(-15,5,20);
    glVertex3f(-13,12,17);
	glEnd();

	//partes negras
	glColor3f(0,0,0);
	glBegin(GL_POLYGON);
	glVertex3f(0,15,25);
	glVertex3f(13,12,17);
	glVertex3f(8,16,14);
	glVertex3f(-8,16,14);
	glVertex3f(-13,12,17);
	glEnd();

	glColor3f(0,0,0);
	glBegin(GL_POLYGON);
    glVertex3f(0,-5,40);
    glVertex3f(-8,-5,0);
    glVertex3f(8,-5,0);
    glEnd();

	glColor3f(0,0,0);
	glBegin(GL_POLYGON);
	glVertex3f(15,0,40);
    glVertex3f(15,0,20);
	glVertex3f(15,5,20);
	glVertex3f(15,5,40);
	glEnd();
    
    glColor3f(0,0,0);
	glBegin(GL_POLYGON);
	glVertex3f(-15,5,40);
    glVertex3f(-15,5,20);
	glVertex3f(-15,0,20);
	glVertex3f(-15,0,40);
	glEnd();
    
	//parte media

	glColor3f(.2,.2,.2);
	glBegin(GL_POLYGON);
	glVertex3f(8,10,0);
	glVertex3f(8,0,0);
	glVertex3f(8,-5,0);
    glVertex3f(13,-5,-4);
	glVertex3f(13,0,-4);
	glVertex3f(13,8,-4);
	glVertex3f(14,13,-6);
    glEnd();
    
	glColor3f(.2,.2,.2);
	glBegin(GL_POLYGON);
	glVertex3f(-8,10,0);
    glVertex3f(-14,13,-6);
	glVertex3f(-13,8,-4);
	glVertex3f(-13,0,-4);
    glVertex3f(-13,-5,-4);
	glVertex3f(-8,-5,0);
	glVertex3f(-8,0,0);
    glEnd();
    
	glColor3f(.8,.8,.8);
    glBegin(GL_POLYGON);
	glVertex3f(13,8,-4);
    glVertex3f(13,0,-4);
	glVertex3f(17,0,-6);
	glVertex3f(17,8,-6);
	glEnd();

	glColor3f(.6,.6,.6);
    glBegin(GL_POLYGON);
	glVertex3f(17,8,-6);
	glVertex3f(17,0,-6);
    glVertex3f(19,0,-15);
	glVertex3f(19,6.5,-15);
	glEnd();

	glColor3f(.4,.4,.4);
	glBegin(GL_POLYGON);
	glVertex3f(19,6.5,-15);
	glVertex3f(19,0,-15);
	glVertex3f(25,0,-19);
	glVertex3f(25,4,-19);
	glEnd();
	glColor3f(.2,.2,.2);
	glBegin(GL_POLYGON);
	glVertex3f(25,4,-19);
	glVertex3f(25,0,-19);
	glVertex3f(19,0,-23);
	glVertex3f(19,2.5,-23);
	glEnd();
	glColor3f(.4,.4,.4);
	glBegin(GL_POLYGON);
	glVertex3f(19,2.5,-23);
	glVertex3f(19,0,-23);
	glVertex3f(13,0,-35);
	glVertex3f(13,1,-35);
	glEnd();

	glColor3f(.8,.8,.8);
    glBegin(GL_POLYGON);
	glVertex3f(-13,0,-4);
    glVertex3f(-13,8,-4);
	glVertex3f(-17,8,-6);
	glVertex3f(-17,0,-6);
	glEnd();

	glColor3f(.6,.6,.6);
    glBegin(GL_POLYGON);
	glVertex3f(-17,0,-6);
	glVertex3f(-17,8,-6);
    glVertex3f(-19,6.5,-15);
	glVertex3f(-19,0,-15);
	glEnd();

	glColor3f(.4,.4,.4);
	glBegin(GL_POLYGON);
	glVertex3f(-19,0,-15);
	glVertex3f(-19,6.5,-15);
	glVertex3f(-25,4,-19);
	glVertex3f(-25,0,-19);
	glEnd();
	glColor3f(.2,.2,.2);
	glBegin(GL_POLYGON);
	glVertex3f(-25,0,-19);
	glVertex3f(-25,4,-19);
	glVertex3f(-19,2.5,-23);
	glVertex3f(-19,0,-23);
	glEnd();
	glColor3f(.4,.4,.4);
	glBegin(GL_POLYGON);
	glVertex3f(-19,0,-23);
	glVertex3f(-19,2.5,-23);
	glVertex3f(-13,1,-35);
	glVertex3f(-13,0,-35);
	glEnd();
	glColor3f(.2,.2,.2);
	glBegin(GL_POLYGON);
    glVertex3f(13,1,-35);
	glVertex3f(13,0,-35);
	glVertex3f(-13,0,-35);
	glVertex3f(-13,1,-35);
	glEnd();

	glColor3f(.3,.3,.3);
	glBegin(GL_POLYGON);

	glVertex3f(13,-5,-4);
	glVertex3f(13,0,-35);
	glVertex3f(19,0,-23);
	glVertex3f(25,0,-19);
	glVertex3f(19,0,-15);
	glVertex3f(17,0,-6);
	glVertex3f(13,0,-4);
	
    glEnd();
	glColor3f(.3,.3,.3);
	glBegin(GL_POLYGON);
	glVertex3f(-13,-5,-4);
    glVertex3f(-13,0,-4);
	glVertex3f(-17,0,-6);
	glVertex3f(-19,0,-15);
	glVertex3f(-25,0,-19);	
	glVertex3f(-19,0,-23);
	glVertex3f(-13,0,-35);
	glEnd();

	glColor3f(0,0,0);
	glBegin(GL_POLYGON);
    glVertex3f(-8,-5,0);
    glVertex3f(-10,0,-35);
	glVertex3f(10,0,-35);
	glVertex3f(8,-5,0);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3f(-8,10,0);
	glVertex3f(-13,12,17);
	glVertex3f(0,18,-6);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3f(8,10,0);
	glVertex3f(0,18,-6);
	glVertex3f(13,12,17);
	glEnd();
	glColor3f(.8,.8,.8);
	glBegin(GL_POLYGON);
	glVertex3f(-8,10,0);
	glVertex3f(-13,12,17);
	glVertex3f(-8,16,14);
	glVertex3f(0,18,-6);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3f(8,10,0);
	glVertex3f(0,18,-6);
	glVertex3f(8,16,14);
	glVertex3f(13,12,17);
	glEnd();
	glColor3f(1,1,1);
	glBegin(GL_POLYGON);
	glVertex3f(0,18,-6);
	glVertex3f(-8,16,14);
	glVertex3f(8,16,14);
	glEnd();
    
    glColor3f(.6,.6,.6);
	glBegin(GL_POLYGON);
	glVertex3f(-8,-5,0);
	glVertex3f(-13,-5,-4);
	glVertex3f(-13,0,-35);
	glVertex3f(-10,0,-35);
	glEnd();

    glColor3f(.6,.6,.6);
	glBegin(GL_POLYGON);
	glVertex3f(10,0,-35);
	glVertex3f(13,0,-35);
	glVertex3f(13,-5,-4);
	glVertex3f(8,-5,0);
	glEnd();

	glColor3f(.5,.5,.5);
	glBegin(GL_POLYGON);
	glVertex3f(14,13,-6);
	glVertex3f(0,4,-25);
	glVertex3f(0,18,-6);
	glVertex3f(8,10,0);
	glEnd();
     
	glColor3f(.6,.6,.6);
	glBegin(GL_POLYGON);
	glVertex3f(0,18,-6);
	glVertex3f(0,4,-25);
	glVertex3f(-14,13,-6);		
	glVertex3f(-8,10,0);
	glEnd();

	glColor3f(.3,.3,.3);
	glBegin(GL_POLYGON);
    glVertex3f(0,4,-25);
	glVertex3f(14,13,-6);
	glVertex3f(13,8,-4);
	glVertex3f(17,8,-6);
	glVertex3f(19,6.5,-15);
	glVertex3f(25,4,-19);
	glVertex3f(19,2.5,-23);
	glVertex3f(13,1,-35);
	glVertex3f(0,1,-35);
	glEnd();

    glColor3f(0.5,0.5,0.5);
	glBegin(GL_POLYGON);
	glVertex3f(-0,4,-25);
	glVertex3f(-0,1,-35);
	glVertex3f(-13,1,-35);
	glVertex3f(-19,2.5,-23);
	glVertex3f(-25,4,-19);
	glVertex3f(-19,6.5,-15);
	glVertex3f(-17,8,-6);
    glVertex3f(-13,8,-4);	
  	glVertex3f(-14,13,-6);  
	glEnd();

	//turbinas traseras
    //turbina1
    glColor3f(.5,.5,.5);
	glBegin(GL_POLYGON);
    glVertex3f(10,8,-37);
	glVertex3f(10,-8,-37);
	glVertex3f(8.5,-6.5,-37);
	glVertex3f(8.5,6.5,-37);
    glVertex3f(4.5,6.5,-37);
    glVertex3f(3,8,-37);
	glEnd();
	
	glColor3f(.5,.5,.5);
	glBegin(GL_POLYGON);
	glVertex3f(3,-8,-37);
	glVertex3f(3,8,-37);
	glVertex3f(4.5,6.5,-37);
    glVertex3f(4.5,-6.5,-37);
    glVertex3f(8.5,-6.5,-37);
	glVertex3f(10,-8,-37);
	glEnd();

	glColor3f(.9,.9,.9);
	glBegin(GL_POLYGON);
	glVertex3f(10,8,-37);
	glVertex3f(3,8,-37);
	glVertex3f(5,0,-25);
	glVertex3f(8,0,-25);
	glEnd();

	glColor3f(.9,.9,.9);
	glBegin(GL_POLYGON);
	glVertex3f(10,-8,-37);
    glVertex3f(8,0,-25);
	glVertex3f(5,0,-25);
	glVertex3f(3,-8,-37);
	glEnd();

	glColor3f(.1,.1,.1);
	glBegin(GL_POLYGON);
	glVertex3f(3,-8,-37);
	glVertex3f(5,0,-25);
	glVertex3f(3,8,-37);
	glEnd();  
	
	glColor3f(.1,.1,.1);
	glBegin(GL_POLYGON);
	glVertex3f(10,8,-37);
	glVertex3f(8,0,-25);
	glVertex3f(10,-8,-37);
	glEnd();  
    
	glColor3f(0,0,0);
	glBegin(GL_POLYGON);
    glVertex3f(8.5,6.5,-37);
	glVertex3f(8.5,-6.5,-37);
	glVertex3f(4.5,-6.5,-37);
	glVertex3f(4.5,6.5,-37);
	glEnd();

	//turbina2
	glColor3f(.5,.5,.5);
    glBegin(GL_POLYGON);
    glVertex3f(-10,8,-37);
    glVertex3f(-3,8,-37);	
    glVertex3f(-4.5,6.5,-37);	
	glVertex3f(-8.5,6.5,-37);
	glVertex3f(-8.5,-6.5,-37);	
	glVertex3f(-10,-8,-37);
	glEnd();

     glColor3f(.5,.5,.5);
	glBegin(GL_POLYGON);
	glVertex3f(-3,-8,-37);
	glVertex3f(-10,-8,-37);
    glVertex3f(-8.5,-6.5,-37);
    glVertex3f(-4.5,-6.5,-37);	
	glVertex3f(-4.5,6.5,-37);
	glVertex3f(-3,8,-37);
	glEnd();

	glColor3f(.9,.9,.9);
	glBegin(GL_POLYGON);
	glVertex3f(-10,-8,-37);
	glVertex3f(-3,-8,-37);
	glVertex3f(-5,0,-25);
	glVertex3f(-8,0,-25);
	glEnd();

	glColor3f(.9,.9,.9);
	glBegin(GL_POLYGON);
	glVertex3f(-10,8,-37);
    glVertex3f(-8,0,-25);
	glVertex3f(-5,0,-25);
	glVertex3f(-3,8,-37);
	glEnd();

	glColor3f(.1,.1,.1);
	glBegin(GL_POLYGON);
	glVertex3f(-3,8,-37);
	glVertex3f(-5,0,-25);
	glVertex3f(-3,-8,-37);
	glEnd();  
	
	glColor3f(.1,.1,.1);
	glBegin(GL_POLYGON);
    glVertex3f(-10,-8,-37);
	glVertex3f(-8,0,-25);
	glVertex3f(-10,8,-37);
	glEnd();

	glColor3f(0,0,0);
	glBegin(GL_POLYGON);
    glVertex3f(-8.5,6.5,-37);
    glVertex3f(-4.5,6.5,-37);
	glVertex3f(-4.5,-6.5,-37);
	glVertex3f(-8.5,-6.5,-37);
	glEnd();
    

	//turbinas delanteras 
    
	//turbina1
	glColor3f(0,0,0);
	glBegin(GL_POLYGON);
	glVertex3f(15.5,-1.5,-2);
	glVertex3f(12.5,-1.5,-2);
	glVertex3f(12.5,-8.5,-2);
	glVertex3f(15.5,-8.5,-2);
	glEnd();
	glColor3f(.7,.7,.7);
    glBegin(GL_POLYGON);
	glVertex3f(12.5,-1.5,-2);
	glVertex3f(15.5,-1.5,-2);
	glVertex3f(17,0,-2);
	glVertex3f(11,0,-2);
	glVertex3f(11,-10,-2);
	glVertex3f(12.5,-8.5,-2);
	glEnd();
    glBegin(GL_POLYGON);
	glVertex3f(15.5,-8.5,-2);
    glVertex3f(12.5,-8.5,-2);
	glVertex3f(11,-10,-2);
	glVertex3f(17,-10,-2);
	glVertex3f(17,0,-2);
	glVertex3f(15.5,-1.5,-2);
	glEnd();
	glColor3f(.5,.5,.5);
	glBegin(GL_POLYGON);
	glVertex3f(17,0,-2);
	glVertex3f(14,0,-20);
	glVertex3f(11,0,-2);
	glEnd(); 
    glBegin(GL_POLYGON);
	glVertex3f(17,-10,-2);
	glVertex3f(11,-10,-2);
	glVertex3f(14,0,-20);
	glEnd(); 
    glColor3f(.8,.8,.8);
	glBegin(GL_POLYGON);
	glVertex3f(17,0,-2);
	glVertex3f(17,-10,-2);
	glVertex3f(14,0,-20);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3f(11,-10,-2);
	glVertex3f(11,0,-2);
	glVertex3f(14,0,-20);
	glEnd();
	//turbina2
	glColor3f(0,0,0);
	glBegin(GL_POLYGON);
	glVertex3f(-15.5,-1.5,-2);
    glVertex3f(-15.5,-8.5,-2);
	glVertex3f(-12.5,-8.5,-2);
	glVertex3f(-12.5,-1.5,-2);
	glEnd();

	glColor3f(.7,.7,.7);
	glBegin(GL_POLYGON);
	glVertex3f(-15.5,-8.5,-2);
	glVertex3f(-15.5,-1.5,-2);
	glVertex3f(-17,0,-2);
	glVertex3f(-17,-10,-2);
	glVertex3f(-11,-10,-2);
	glVertex3f(-12.5,-8.5,-2);
	glEnd();

    glBegin(GL_POLYGON);
		glVertex3f(-11,0,-2); 
	glVertex3f(-17,0,-2);
	glVertex3f(-15.5,-1.5,-2);
	glVertex3f(-12.5,-1.5,-2);
	glVertex3f(-12.5,-8.5,-2);
	glVertex3f(-11,-10,-2);
	glEnd();
    glColor3f(.5,.5,.5);
	glBegin(GL_POLYGON);
	glVertex3f(-17,0,-2);
	glVertex3f(-11,0,-2);
	glVertex3f(-14,0,-20);
    glEnd(); 
    glBegin(GL_POLYGON);
	glVertex3f(-17,-10,-2);
	glVertex3f(-14,0,-20);
	glVertex3f(-11,-10,-2);
	glEnd();
	glColor3f(.8,.8,.8);
	glBegin(GL_POLYGON);
	glVertex3f(-17,0,-2);
	glVertex3f(-14,0,-20);
	glVertex3f(-17,-10,-2);
	glEnd();
    glColor3f(.8,.8,.8);
	glBegin(GL_POLYGON);
	glVertex3f(-11,-10,-2);
	glVertex3f(-14,0,-20);
	glVertex3f(-11,0,-2);
	glEnd();

	//alas principales
    //1
    glColor3f(.45,.45,0.45);
	glBegin(GL_POLYGON);
	glVertex3f(-25,0,-19);
	glVertex3f(-75,0,-16);
	glVertex3f(-80,0,-19);
	glVertex3f(-13,0,-30);
	glVertex3f(-19,0,-23);
	glEnd();
    
	glColor3f(.75,.75,.75);
	glBegin(GL_POLYGON);
	glVertex3f(-25,4,-19);
	glVertex3f(10,0,-30);
    glVertex3f(-13,0,-30);   
	glVertex3f(-80,0,-19);
	glVertex3f(-75,0,-16);
	glEnd();
    
	glColor3f(.6,.6,.6);
	glBegin(GL_POLYGON);
	glVertex3f(-25,4,-19);
	glVertex3f(-75,0,-16);
	glVertex3f(-25,0,-19);
	glEnd(); 


    //2
	glColor3f(.45,.45,.45);
	glBegin(GL_POLYGON);
	glVertex3f(25,0,-19);
    glVertex3f(19,0,-23);
	glVertex3f(13,0,-30);
	glVertex3f(80,0,-19);
	glVertex3f(75,0,-16);
	glEnd();

	glColor3f(.75,.75,.75);
	
	glBegin(GL_POLYGON);
	glVertex3f(10,0,-30); 
	glVertex3f(25,4,-19);
	glVertex3f(75,0,-16);
	glVertex3f(80,0,-19);
    glVertex3f(13,0,-30);
	
	glEnd();
 
    glColor3f(.6,.6,.6);
	glBegin(GL_POLYGON);
	glVertex3f(25,4,-19);
	glVertex3f(25,0,-19);
	glVertex3f(75,0,-16);
	glEnd(); 

	//alas secundarias

	//arriba
	//1
    glColor3f(.3,.3,.3);
	glBegin(GL_POLYGON);
	glVertex3f(10,8,-37);
	glVertex3f(15,20,-43);
	glVertex3f(7,0,-28);
	glEnd();
    glColor3f(.5,.5,.5);
    glBegin(GL_POLYGON);
	glVertex3f(5,8,-37);
	glVertex3f(7,0,-28);
	glVertex3f(15,20,-43);
	glEnd(); 
	glColor3f(1,1,1);
	glBegin(GL_POLYGON);
	glVertex3f(5,8,-37);
	glVertex3f(15,20,-43);
	glVertex3f(10,8,-37);

	glEnd();
	//2
	glColor3f(.3,.3,.3);
	glBegin(GL_POLYGON);
	glVertex3f(10,-8,-37);
	glVertex3f(5,0,-28);
	glVertex3f(15,-20,-43); 
	glEnd();
	
	glColor3f(.5,.5,.5);
    glBegin(GL_POLYGON);
	glVertex3f(5,-8,-37);
	glVertex3f(15,-20,-43);
	glVertex3f(7,0,-28);
	glEnd(); 
	glColor3f(1,1,1);
	glBegin(GL_POLYGON);
	glVertex3f(5,-8,-37);
	glVertex3f(10,-8,-37);
	glVertex3f(15,-20,-43);
    glEnd();

	//abajo
	//uno
    glColor3f(.3,.3,.3);
	glBegin(GL_POLYGON);
	glVertex3f( -10,8,-37);
	glVertex3f(-7,0,-28);
	glVertex3f(-15,20,-43);
	glEnd(); 
	glColor3f(.5,.5,.5);
    glBegin(GL_POLYGON);
	glVertex3f(-5,8,-37);
	glVertex3f(-15,20,-43);
	glVertex3f(-7,0,-28);
	glEnd(); 
	glColor3f(1,1,1);
	glBegin(GL_POLYGON);
	glVertex3f(-5,8,-37);
	glVertex3f(-10,8,-37);
	glVertex3f(-15,20,-43);
    glEnd();
    //2
    glColor3f(.3,.3,.3);
	glBegin(GL_POLYGON);
	glVertex3f(-10,-8,-37);
	glVertex3f(-15,-20,-43); 
	glVertex3f(-5,0,-28);
	glEnd();
	glColor3f(.5,.5,.5);
    glBegin(GL_POLYGON);
	glVertex3f(-5,-8,-37);
	glVertex3f(-7,0,-28);
	glVertex3f(-15,-20,-43);
	glEnd(); 
	glColor3f(1,1,1);
	glBegin(GL_POLYGON);
	glVertex3f(-5,-8,-37);
	glVertex3f(-15,-20,-43);
	glVertex3f(-10,-8,-37);
	glEnd();
	glPopMatrix();

    //glutSwapBuffers();
    };

void SetupRC()
    {
    glEnable(GL_DEPTH_TEST);	
    glEnable(GL_CULL_FACE);		
    glFrontFace(GL_CCW);		

    glClearColor(0.0f, 0.0f, 05.f,1.0f);
    };

//void SpecialKeys(int key, int x, int y)
//    {
//    if(key == GLUT_KEY_UP)
//        xRot-= 5.0f;
//
//    if(key == GLUT_KEY_DOWN)
//        xRot += 5.0f;
//
//    if(key == GLUT_KEY_LEFT)
//        yRot -= 5.0f;
//
//    if(key == GLUT_KEY_RIGHT)
//        yRot += 5.0f;
//
//    if(key > 356.0f)
//        xRot = 0.0f;
//
//    if(key < -1.0f)
//        xRot = 355.0f;
//
//    if(key > 356.0f)
//        yRot = 0.0f;
//
//    if(key < -1.0f)
//        yRot = 355.0f;
//
//    glutPostRedisplay();
//    };


void ChangeSize(int w, int h)
    {
    GLfloat nRange = 80.0f;
    
    if(h == 0)
        h = 1;

    glViewport(0, 0, w, h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    if (w <= h) 
        glOrtho (-nRange, nRange, -nRange*h/w, nRange*h/w, -nRange, nRange);
    else 
        glOrtho (-nRange*w/h, nRange*w/h, -nRange, nRange, -nRange, nRange);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    };
/*
int main(int argc, char* argv[])
    {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800,600);
    glutCreateWindow("Mi jet");
    glutReshapeFunc(ChangeSize);
    glutSpecialFunc(SpecialKeys);
    glutDisplayFunc(RenderScene);
    SetupRC();
    glutMainLoop();

    return 0;
    }

*/